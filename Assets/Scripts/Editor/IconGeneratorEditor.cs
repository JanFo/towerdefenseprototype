#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static UnityEditor.Rendering.CameraUI;


[CustomEditor(typeof(IconGenerator), false)]
[CanEditMultipleObjects]
public class IconGeneratorEditor : Editor
{
   
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        IconGenerator iconGenerator = target as IconGenerator;

        if (GUILayout.Button("Generate Current"))
        {
            iconGenerator.SaveCurrentTargetToTexture();
        }

       
    }
}
#endif