#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static UnityEditor.Rendering.CameraUI;


[CustomEditor(typeof(GenerateIcon), false)]
[CanEditMultipleObjects]
public class GenerateIconEditor : Editor
{
    private Color backgroundColor = new Color(0.0f, 0.0f, 1f, 0.075f);

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GenerateIcon generateIcon = target as GenerateIcon;

        if (GUILayout.Button("Capture"))
        {
            generateIcon.CaptureCameraSettings(FindObjectOfType<IconGenerator>().GetComponent<Camera>());
            EditorUtility.SetDirty(generateIcon);
        }

    }
}
#endif