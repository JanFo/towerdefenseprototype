using Assets.Scripts.Model;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public enum GameActionType
{
    Move,
    Interact,
    Build,
    Destroy,
    Start,
    NextTower,
    Cancel,
    ZipAction,
    Special1,
    Special2,
}

public class PlayerInputController 
{
    private int _index;

    private World _world;

    private GameObject _cameraHandle;

    private PlayerInput _input;
    private Player _player;
    private float _sqr2;

    private Character _character;

    private Tower _selectedTower;

    private float moveSpeed = 3f;

    private Vector3 _moveInput;

    private int _towerIndex;

    public PlayerInputController(Player player, PlayerInput input, World world)
    {
        _index = player.index;
        _world = world;
        _cameraHandle = GameObject.Find("CameraHandle");
        _input = input;
        _player = player;

        _sqr2 = Mathf.Sqrt(2);

        InitializeInput();
    }

    private void InitializeInput()
    {
        AddListener(GameActionType.Start, InputActionPhase.Performed, OnStart);

        AddListener(GameActionType.Move, InputActionPhase.Performed, OnMove);
        AddListener(GameActionType.Move, InputActionPhase.Canceled, OnMove);

        AddListener(GameActionType.Interact, InputActionPhase.Started, OnStartInteract);
        AddListener(GameActionType.Interact, InputActionPhase.Canceled, OnCancelInteract);

        AddListener(GameActionType.Cancel, InputActionPhase.Started, OnStartCancel);
        AddListener(GameActionType.Cancel, InputActionPhase.Canceled, OnCancelCancelAction);

        AddListener(GameActionType.Build, InputActionPhase.Started, OnStartBuild);
        AddListener(GameActionType.Build, InputActionPhase.Canceled, OnCancelBuild);

        AddListener(GameActionType.Destroy, InputActionPhase.Started, OnStartDestroy);
        AddListener(GameActionType.Destroy, InputActionPhase.Canceled, OnCancelDestroy);

        AddListener(GameActionType.Special1, InputActionPhase.Started, OnStartSpecial1);
    }

    private void OnStartSpecial1(CallbackContext obj)
    {
        if (_character.isDead)
        {
            return;
        }

        if (_character is Soldier)
        {
            _character.currentActionType = CharacterActionType.CallAirStrike;
        }
    }

    private void OnStartDestroy(CallbackContext obj)
    {
        GridNode node = _world.grid.GetNode(_character.actionTarget.x, _character.actionTarget.y);
        Entity targetEntity = _world.GetEntity(node.entityId);

        if (targetEntity is Tower)
        {
            _character.currentActionType = CharacterActionType.Deconstruct;
            _character.SetDirty();
        }
    }

    private void OnCancelDestroy(CallbackContext obj)
    {

    }

    private void OnStartCancel(CallbackContext obj)
    {
        if (_character.isDead)
        {
            return;
        }
        
      

        Spy spy = _character as Spy;

        if(spy != null && spy.connectingTowerId != 0)
        {
            spy.connectingTowerId = 0;
            _character.SetDirty();
        }
    }

    private void OnCancelCancelAction(CallbackContext obj)
    {
        _character.currentActionType = CharacterActionType.None;
        _character.SetDirty();
    }


    private void OnStart(CallbackContext obj)
    {
        if(_world.castle == null)
        {
            Debug.Log("No castle, no game!");
            return; 
        }

        //if (!_world.barracks.respawnData[_index].isRequested && (_character == null || _character.isDead))
        //{
        //    _world.barracks.respawnData[_index].isRequested = true;
        //    _world.castle.SetRealDirty();
        //}
        //else if(_character != null)
        //{
        //    FloatingText.Create(_character.worldPosition, "Hello World", Color.red);
        //}
    }

    private void OnStartInteract(CallbackContext obj)
    {
        if (_character.isDead)
        {
            return;
        }
        
        GridNode node = _world.grid.GetNode(_character.actionTarget.x, _character.actionTarget.y);
        Entity targetEntity = _world.GetEntity(node.entityId);

        if (_character.currentActionType == CharacterActionType.UseZip)
        {
            _character.currentActionType = CharacterActionType.None;
            _character.SetDirty();
            return;
        }

        if (targetEntity is ZipTower)
        {
            ZipTower zipTower = targetEntity as ZipTower;
            int targetTowerId = zipTower.destinations.FirstOrDefault(i => true);

            _character.currentActionType = CharacterActionType.UseZip;
            _character.actionData.x = targetTowerId;
            return;
        }

        if (targetEntity is Tower && (targetEntity as Tower).canBeRotated)
        {
            _character.currentActionType = CharacterActionType.RotateTower;
            _character.actionData.x = targetEntity.id;
        }

        if(targetEntity is Barracks)
        {
            // Castle castle = targetEntity as Castle;
            _character.playerIndex = -1;
            _character = null;
        }

        if (_character != null)
        {
            _character.SetRealDirty();
        }
    }

    //private void UseZipTower(ZipTower sourceTower)
    //{
    //    var targetId = sourceTower.destinations.FirstOrDefault(i => true);      // FIXME make this selectable!

    //    if (targetId > 0)
    //    {
    //        var targetTower = _world.GetEntity(targetId) as ZipTower;

    //        if (targetTower != null)
    //        {
    //            _character.currentActionType = CharacterActionType.UseZip;
    //            _character.actionData.y = sourceTower.id;
    //            _character.actionData.z = targetTower.id;
    //        }
    //    }
    //}

    private void OnCancelInteract(CallbackContext obj)
    {
        if(_character == null)
        {
            return;
        }

        // TODO: move to zip action
        if (_character.currentActionType == CharacterActionType.UseZip)
        {
            return;
        }

        _character.currentActionType = CharacterActionType.None;
        _character.SetRealDirty();
    }

    private void OnStartBuild(CallbackContext obj)
    {
        if (_character.isDead)
        {
            return;
        }

        GridNode node = _world.grid.GetNode(_character.actionTarget.x, _character.actionTarget.y);
        Entity targetEntity = _world.GetEntity(node.entityId);

        if (node.isBuildable)
        {
            _character.actionData.x = _player.constructionLineIndex;
            _character.currentActionType = CharacterActionType.Build;
        }

        ZipTower zipTower = targetEntity as ZipTower;

        if(_character is Spy && zipTower != null && zipTower.isBuilt)
        { 
            Spy spy = _character as Spy;
            spy.currentActionType = CharacterActionType.ConnectZip;
        }

        _character.SetRealDirty();
    }

    private void OnCancelBuild(CallbackContext obj)
    {
        _character.currentActionType = CharacterActionType.None;
        _character.SetRealDirty();
    }

    private void OnMove(CallbackContext obj)
    {
        Vector2 input = obj.ReadValue<Vector2>();
        _moveInput = new Vector3(input.x, 0.0f, input.y);
    }

    public void SetCharacter(Character character)
    {
        _character = character;
    }

    public void Update(float timekey)
    {
        if (_character == null || _character.isDead)
        {
            FindCharacter();
            return;
        }

        UpdateCharacterMovement();
        _cameraHandle.transform.position = _character.worldPosition;
    }

    private void FindCharacter()
    {
        _character = _world.characters.Find(c => c.playerIndex == _index && !c.isDead);

        if (_character != null)
        {
            _character.isOwn = true;
            PathfinderDebug.character = _character;
        }
    }

    private void UpdateCharacterMovement()
    {
        if (_character.currentActionType != CharacterActionType.None && _character.currentActionType != CharacterActionType.ConnectZip)
        {
            return;
        }

        _moveInput.Normalize();

        UpdateActionTarget(_moveInput);

        if (_moveInput.sqrMagnitude < 0.01f)
        {
            return;
        }

        if (_character.isDead)
        {
            _moveInput = Vector3.zero;
        }

        _character.rotation = Vector3.SignedAngle(Vector3.forward, _moveInput, Vector3.up);
        UpdateWorldPosition(_moveInput);
        _character.SetDirty();
    }

    private void UpdateWorldPosition(Vector3 move)
    {
        Vector3 targetPosition = _character.worldPosition + moveSpeed * move * Time.deltaTime;

        Vector2Int gridPosition = GridUtils.GetGridPosition(_character.worldPosition);

        for(int i = gridPosition.x - 1; i <= gridPosition.x + 1; i++)
        {
            for (int j = gridPosition.y - 1; j <= gridPosition.y + 1; j++)
            {
                GridNode node = _world.grid.GetNode(i, j);

                if(node == null || !node.isWalkable)
                {
                    targetPosition = CorrectPosition(targetPosition, 0.12f, node);
                }
            }
        }

        _character.worldPosition = targetPosition;
    }

    private Vector3 CorrectPosition(Vector3 targetPosition, float v, GridNode node)
    {
        // TODO: fix this
        float sqrDist = (node.worldPosition - targetPosition).sqrMagnitude;

        if (sqrDist > GameConfiguration.tileSize * GameConfiguration.tileSize)
        {
            return targetPosition;
        }

        float distX = Mathf.Abs(targetPosition.x - node.worldPosition.x) - (GameConfiguration.tileSize / 2.0f + v);
        float distZ = Mathf.Abs(targetPosition.z - node.worldPosition.z) - (GameConfiguration.tileSize / 2.0f + v);

        if(distX > 0 || distZ > 0)
        {
            return targetPosition;
        }

        if(distX > distZ)
        {
            if(targetPosition.x > node.worldPosition.x)
            {
                targetPosition.x -= distX;
            }
            else
            {
                targetPosition.x += distX;
            }
        }
        else
        {
            if (targetPosition.z > node.worldPosition.z)
            {
                targetPosition.z -= distZ;
            }
            else
            {
                targetPosition.z += distZ;
            }
        }

        return targetPosition;
    }

    private void UpdateActionTarget(Vector3 move)
    {
        if(_character.isDead)
        {
            _character.actionTarget = new Vector2Int(0, 0);
            return;
        }


        GridNode node = _world.grid.GetNode(_character.worldPosition);
        Vector2Int position = node.position;
        float r = _character.rotation;

        if (r >= -45 && r < 45)
        {
            position.y++;
        }
        else if(r >= 45 && r < 135)
        {
            position.x++;
        }
        else if(r >= 135 && r >= -135)
        {
            position.y--;
        }
        else
        {
            position.x--;
        }

        _character.actionTarget = position;
    }


    private void AddListener(GameActionType action, InputActionPhase phase, Action<CallbackContext> method)
    {
        InputAction inputAction = _input.actions.FindAction(action.ToString());

        switch (phase)
        {
            case InputActionPhase.Performed:
                inputAction.performed += method;
                break;
            case InputActionPhase.Started:
                inputAction.started += method;
                break;
            case InputActionPhase.Canceled:
                inputAction.canceled += method;
                break;
        }
    }
}
