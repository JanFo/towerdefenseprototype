﻿public interface IDamageContext
{
    public void ApplyDamage(Monster monster, float multipier = 1.0f);
}