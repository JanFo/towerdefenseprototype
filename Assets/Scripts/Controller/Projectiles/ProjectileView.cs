
//using UnityEngine;
///// <summary>
///// Handles the monster count, should also handle the tower ids on the grid.
///// </summary>
//public abstract class ProjectileView : MonoBehaviour
//{
//    protected int damage;

//    protected Monster target;

//    protected IProjectileContext context;

//    public static ProjectileView Create(TowerView towerView, Monster target, IProjectileContext context)
//    {
//        ProjectileView instance = CreateInstance(towerView);
//        instance.damage = GameConfiguration.GetDamage(towerView.damage);
//        instance.target = target;
//        instance.context = context;
//        instance.Initialize(towerView);

//        return instance;
//    }

//    public abstract bool CanBeRemoved { get; }

//    protected abstract void Initialize(Tower tower);

//    public abstract void Update();

//    private static ProjectileView CreateInstance(Tower tower)
//    {
//        return new SimpleBullet();
//    }
//}