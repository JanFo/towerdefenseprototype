//using System;
//using UnityEngine;
///// <summary>
///// Handles the monster count, should also handle the tower ids on the grid.
///// </summary>
//public class SimpleBullet : ProjectileView
//{
//    private Vector3 _worldPosition;

//    private float _projectileSpeed;

//    private GameObject _view;

//    private bool _isDone;

//    protected override void Initialize(Tower tower)
//    {
//        _projectileSpeed = 10.0f;
//        _worldPosition = PathUtils.GetWorldPosition(tower.x, tower.y);

//        _view = UnityEngine.Object.Instantiate(GameConfiguration.instance.simpleBulletPrefab);
//        _view.transform.position = _worldPosition;
//        _isDone = false;
//    }

//    public override void Update()
//    {
//        _worldPosition = Vector3.MoveTowards(_worldPosition, target.worldPosition, Time.deltaTime * _projectileSpeed);
//        _view.transform.position = _worldPosition;

//        if (target.isDead || Vector3.Distance(_worldPosition, target.worldPosition) < 0.1f)
//        {
//            context.ApplyDamage(target, damage);
//            _isDone = true;
//            UnityEngine.Object.Destroy(_view);
//        }
//    }

//    public override bool CanBeRemoved => _isDone;

//}