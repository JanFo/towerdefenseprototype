﻿using System;
using UnityEngine;

namespace Assets.Scripts.Controller
{
    class CreatureRespawnController
    {

        private Creature _respawCreature;
        private float _lastDeathTime = -1f;
        private float _respawnDelay;
        private Vector3 _respawnPosition;

        public CreatureRespawnController(Creature creature, float spawnDelay, Vector3 position)
        {
            _respawCreature = creature;
            _respawnDelay = spawnDelay;
            _respawnPosition = position;
            _lastDeathTime = Time.time;
        }

        public bool Respawn()
        {
            if(_lastDeathTime + _respawnDelay < Time.time)
            {
                _respawCreature.isDead = false;
                _respawCreature.hp = _respawCreature.initialHp;
                _respawCreature.worldPosition = _respawnPosition;
                _lastDeathTime = -1f;
                Debug.Log("RESPAWN CHARACTER " + _respawCreature.id);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
