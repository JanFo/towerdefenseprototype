﻿using Assets.Scripts.Model;
using System.Collections;

public class RotateTowerAction : CharacterAction
{
    public override IEnumerator OnCreateProcess()
    {
        var targetTower = world.GetEntity((int)character.actionData.x) as Tower;

        if(targetTower != null)
        {
            targetTower.Rotate();
            targetTower.SetRealDirty();
        }

        yield return null;
    }
}
