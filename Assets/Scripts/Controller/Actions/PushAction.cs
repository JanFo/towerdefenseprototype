using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class PushAction : CharacterAction
{
    public MoveDirection direction;

    public override IEnumerator OnCreateProcess()
    {
        GameObject characterObject = entityViewController.GetEntityView(character.id);
        CharacterView characterBehaviour = characterObject.GetComponent<CharacterView>();
        while (time < 0.75f)
        {
            characterBehaviour.PlayPushAnimation();

            if (isCancelled)
            {
                characterBehaviour.PlayIdleAnimation();
                yield break;
            }

            yield return null;
        }


        characterBehaviour.PlayIdleAnimation();

        GridNode node = world.grid.GetNode(target.x, target.y);

        if (node.entityId == 0)
        {
            character.currentActionType = CharacterActionType.None;
            yield break;
        }

        Tower tower = world.towers.Find(t => t.id == node.entityId);

        if(tower == null)
        {
            yield break;
        }

        int neighborX = tower.x;
        int neighborY = tower.y;

        switch (direction)
        {
            case MoveDirection.Right:
                neighborX++;
                break;
            case MoveDirection.Left:
                neighborX--;
                break;
            case MoveDirection.Up:
                neighborY++;
                break;
            case MoveDirection.Down:
                neighborY--;
                break;
        }

        GridNode targetNode = world.grid.GetNode(neighborX, neighborY);

        if(targetNode.entityId > 0)
        {
            character.currentActionType = CharacterActionType.None;
            yield break;
        }

        if(world.isMasterWorld)
        {
            tower.x = neighborX;
            tower.y = neighborY;
            tower.SetRealDirty();
        }
    }
}