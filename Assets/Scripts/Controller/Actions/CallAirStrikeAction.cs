﻿using Assets.Scripts.Model;
using System.Collections;
using UnityEngine;

public class CallAirStrikeAction : CharacterAction
{
    public override IEnumerator OnCreateProcess()
    {
        AirStrikeBeacon beacon = world.towers.Find(t => t is AirStrikeBeacon) as AirStrikeBeacon;

        if (beacon == null)
        {
            yield break;
        }

        GameObject airStrikeObject = Object.Instantiate(GameConfiguration.instance.airStrikePrefab);

        AirStrikeController airStrikeController = airStrikeObject.GetComponent<AirStrikeController>();
        airStrikeController.Initialize(world);


        int direction = (int)beacon.direction;

        airStrikeObject.transform.position = beacon.worldPosition;
        airStrikeObject.transform.eulerAngles = new Vector3(0, direction * 90.0f, 0);

        airStrikeController.Strike();

        yield return null;
    }
}
