﻿using Assets.Scripts.Model;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Controller.Actions
{
    class ConnectZipAction : CharacterAction
    {

        public override IEnumerator OnCreateProcess()
        {
            Spy spy = character as Spy;
            GameObject characterObject = entityViewController.GetEntityView(character.id);
            CharacterView characterBehaviour = characterObject.GetComponent<CharacterView>();

            GridNode node = world.grid.GetNode(target.x, target.y);

            characterBehaviour.PlayBuildAnimation();
            yield return new WaitForSeconds(0.2f);
            characterBehaviour.PlayIdleAnimation();

            if (node.entityId == 0)
            {
                yield break;
            }

            if (spy.connectingTowerId != 0)
            {

                ZipTower sourceTower = world.GetEntity(spy.connectingTowerId) as ZipTower;
                ZipTower targetTower = world.GetEntity(node.entityId) as  ZipTower;

                if (sourceTower == null || targetTower == null)
                {
                    FloatingText.Create(character.worldPosition, "Not a Zipline tower!", Color.red);
                    yield break;
                }

                //float distance = Vector3.Distance(sourceTower.worldPosition, targetTower.worldPosition);
                //int constructionCost = (int)(distance * (sourceTower as ZipTower).zipLineCostPerTile);

                //if (world.castle.gold < constructionCost || distance >= (targetTower as Tower).range * world.grid.cellSize)
                //{
                //    FloatingText.Create(character.worldPosition, "No enough money, bro!", Color.red);
                //    yield break;
                //}

                if (world.isMasterWorld)
                {
                    //world.castle.gold -= constructionCost;
                    //world.castle.SetDirty();

                    Debug.Log(string.Format("Connecting tower {0} to {1}!", sourceTower.id, targetTower.id));

                    spy.connectingTowerId = 0;
                    spy.SetDirty();

                    sourceTower.DrawZipLine(targetTower);
                }
            }
            else
            {
                spy.connectingTowerId = node.entityId;
            }
        }
    }
}
