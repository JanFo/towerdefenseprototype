using System.Collections;
using UnityEngine;

public class DeconstructAction : CharacterAction
{
    private UIProgressBar progressBar;

    private float _progressBarOffset = 0.1f;

    public override IEnumerator OnCreateProcess()
    {
        GameObject characterObject = entityViewController.GetEntityView(character.id);
        CharacterView characterBehaviour = characterObject.GetComponent<CharacterView>();

        GridNode node = world.grid.GetNode(target.x, target.y);

        if (node.entityId == 0)
        {
            characterBehaviour.PlayIdleAnimation();
            yield break;
        }

        Tower tower = world.GetEntity(node.entityId) as Tower;

        if (character.isOwn)
        {
            CreateProgressBar(tower);
        }

        while (time < GameConfiguration.instance.deconstructDuration)
        {
            characterBehaviour.PlayPushAnimation();

            if (progressBar != null)
            {
                Vector3 screenPoint = Camera.main.WorldToScreenPoint(tower.worldPosition + Vector3.up * _progressBarOffset);
                progressBar.GetComponent<RectTransform>().anchoredPosition = new Vector2(screenPoint.x, screenPoint.y) / canvas.scaleFactor;
                progressBar.Progress = time / GameConfiguration.instance.deconstructDuration;
            }

            if (isCancelled)
            {
                if(progressBar != null)
                {
                    Object.Destroy(progressBar.gameObject);
                }

                characterBehaviour.PlayIdleAnimation();
                yield break;
            }

            yield return null;
        }

        if (progressBar != null)
        {
            Object.Destroy(progressBar.gameObject);
        }

        characterBehaviour.PlayIdleAnimation();

        if (tower == null)
        {
            yield break;
        }

        if(world.isMasterWorld)
        {
            // TODO: Refund...

            world.RemoveEntity(tower);
        }
    }

    private void CreateProgressBar(Tower tower)
    {
        progressBar = Object.Instantiate(GameConfiguration.instance.deconstructProgressBarPrefab);
        progressBar.Progress = tower.constructionProgress;

        RectTransform rectTransform = progressBar.GetComponent<RectTransform>();
        rectTransform.anchorMin = Vector2.zero;
        rectTransform.anchorMax = Vector2.zero;
        rectTransform.pivot = new Vector2(0.5f, 0.5f);
        progressBar.transform.SetParent(canvas.transform);
    }

   
}
