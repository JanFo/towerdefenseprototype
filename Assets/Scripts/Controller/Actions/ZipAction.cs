﻿using Assets.Scripts.Model;
using System.Linq;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Controller.Actions
{
    class ZipAction : CharacterAction
    {
        private float _climbUpTime;
        private float _climbDownTime;

        public ZipAction(float climbUpTime = 0.3f, float climbDownTime = 0.3f)
        {
            _climbUpTime = climbUpTime;
            _climbDownTime = climbDownTime;
        }

        public override IEnumerator OnCreateProcess()
        {
            GridNode targetNode = world.grid.GetNode(target.x, target.y);

            ZipTower sourceTower = world.GetEntity(targetNode.entityId) as ZipTower;
            ZipTower targetTower = world.GetEntity((int)character.actionData.x) as ZipTower;

            if (sourceTower == null || targetTower == null)
            {
                FloatingText.Create(character.worldPosition, "Not a Zipline tower!", Color.red);
                yield break;
            }

            GameObject characterObject = entityViewController.GetEntityView(character.id);
            CharacterView characterView = characterObject.GetComponent<CharacterView>();

            ZipTowerView sourceTowerView = entityViewController.GetEntityView(sourceTower.id).GetComponent<ZipTowerView>();
            ZipTowerView targetTowerView = entityViewController.GetEntityView(targetTower.id).GetComponent<ZipTowerView>();

            Vector3 sourceTowerCenter = sourceTower.worldPosition + new Vector3(0f, sourceTowerView.topOffset, 0f);
            Vector3 targetTowerCenter = targetTower.worldPosition + new Vector3(0f, targetTowerView.topOffset, 0f);
            Vector3 direction = targetTowerCenter - sourceTowerCenter;
            direction.y = 0;
            direction.Normalize();

            float targetRotation = Vector3.SignedAngle(Vector3.forward, direction, Vector3.up);

            characterView.PlayUseZipLineAnimation();

            float timer = 0;

            Vector3 startPosition = sourceTowerCenter + direction * 0.2f;
            Vector3 endPosition = targetTowerCenter - direction * 0.2f;

            character.isZipping = true;
            character.SetDirty();

            while (timer < _climbUpTime)
            {
                float climbUpProgress = timer / _climbUpTime;

                character.worldPosition = Vector3.Lerp(character.worldPosition, startPosition, climbUpProgress);
                character.rotation = targetRotation; 

                timer += Time.deltaTime;
                yield return null;
            }

            float distance = Vector3.Distance(startPosition, endPosition);
            float normalizedSpeed = GameConfiguration.instance.zipLineSpeed / distance;
            float progress = 0.0f;

            Vector3 dropOffPosition;

            while (progress < 1.0f)
            {
                progress += Time.deltaTime * normalizedSpeed;

                if (isCancelled && world.grid.GetNode(character.worldPosition).isWalkable)
                {
                    break;
                }

                character.worldPosition = Vector3.Lerp(startPosition, endPosition, progress);
                character.rotation = targetRotation;
                character.SetDirty();

                yield return null;
            }

            timer = 0;
            dropOffPosition = character.worldPosition;
            dropOffPosition.y = 0;

            characterView.PlayIdleAnimation();

            while (timer < _climbDownTime)
            {
                float climbDownProgress = timer / _climbDownTime;

                character.worldPosition = Vector3.Lerp(character.worldPosition, dropOffPosition, climbDownProgress);
                character.rotation = targetRotation; 

                timer += Time.deltaTime;
                yield return null;
            }

            character.isZipping = false;
            character.SetDirty();

        }
    }
}
