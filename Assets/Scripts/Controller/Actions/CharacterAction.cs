using Assets.Scripts.Controller.Actions;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using static ActionEngine;

public abstract class CharacterAction 
{
    private ActionState _actionState;

    private bool _hasStarted;

    protected bool isCancelled;

    protected Character character;

    protected float time;

    protected World world;

    protected Vector2Int target;

    protected EntityViewController entityViewController;

    protected Canvas canvas;

    public bool isDone { get; internal set; }

    public bool wasStopped { get; internal set; }

    public void Begin()
    {
        OnBegin();
        _actionState = ActionEngine.CreateAction(OnCreateProcess());
        _actionState.Finished += new ActionState.FinishedHandler(OnFinish);
        _actionState.Finished += new ActionState.FinishedHandler(SetDone);
        _actionState.Start();
    }

    private void SetDone(bool wasStopped)
    {
        isDone = true;
        this.wasStopped = wasStopped;
    }

    protected virtual void OnBegin()
    {

    }

    protected virtual void OnFinish(bool wasStopped)
    {

    }

    public abstract IEnumerator OnCreateProcess();

    public void Stop()
    {
        _actionState.Stop();
    }

    public void Cancel()
    {
        isCancelled = true;
    }

    public void Update()
    {
        if (!_hasStarted)
        {
            Begin();
            _hasStarted = true;
        }

        time += Time.deltaTime;

        if (_actionState == null)
        {
            isDone = true;
        }
    }

    public static CharacterAction CreateAction(Character character, World world, EntityViewController entityViewManager, Canvas canvas)
    {
        CharacterAction action = null;

        if (character.currentActionType == CharacterActionType.Build)
        {
            action = new BuildAction();
        }

        if (character.currentActionType == CharacterActionType.Deconstruct)
        {
            action = new DeconstructAction();
        }

        if(character.currentActionType == CharacterActionType.RotateTower)
        {
            action = new RotateTowerAction();
        }

        if(character.currentActionType == CharacterActionType.CallAirStrike)
        {
            action = new CallAirStrikeAction();
        }

        if (character.currentActionType == CharacterActionType.ConnectZip)
        {
            action = new ConnectZipAction();
        }

        if (character.currentActionType == CharacterActionType.UseZip)
        {
            action = new ZipAction();
        }

        if (character.currentActionType == CharacterActionType.Push)
        {
            PushAction pushAction = new PushAction();

            GridNode node = world.grid.GetNode(character.worldPosition);

            if(character.actionTarget.x < node.position.x)
            {
                pushAction.direction = MoveDirection.Left;
            }

            if (character.actionTarget.x > node.position.x)
            {
                pushAction.direction = MoveDirection.Right;
            }

            if (character.actionTarget.y > node.position.y)
            {
                pushAction.direction = MoveDirection.Up;
            }

            if (character.actionTarget.y < node.position.y)
            {
                pushAction.direction = MoveDirection.Down;
            }

            action = pushAction;
        }

        if (action != null)
        {
            action.entityViewController = entityViewManager;
            action.character = character;
            action.world = world;
            action.target = character.actionTarget;
            action.canvas = canvas;
        }

        return action;
    }
}
