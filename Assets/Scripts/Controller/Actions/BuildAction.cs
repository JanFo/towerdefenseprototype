using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildAction : CharacterAction
{
    private Tower tower;

    public override IEnumerator OnCreateProcess()
    {
        int lineIndex = (int)character.actionData.x;

        CharacterClassConfiguration classConfiguration = GameConfiguration.GetCharacterClassConfiguration(character.type);
        TowerConfiguration towerConfig = classConfiguration.GetTowerConfiguration(lineIndex);
        int constructionCost = towerConfig.cost;

        if (world.castle.gold < constructionCost)
        {
            yield break;
        }

        GameObject characterObject = entityViewController.GetEntityView(character.id);
        CharacterView characterBehaviour = characterObject.GetComponent<CharacterView>(); 

        characterBehaviour.PlayBuildAnimation();

        // Build if not exists
        if (world.isMasterWorld)
        {
            GridNode node = world.grid.GetNode(target.x, target.y);

            if (node.isBuildable && node.entityId == 0)
            {
                // Last safety check for sufficient funds
                if (world.castle.gold < constructionCost)
                {
                    characterBehaviour.PlayIdleAnimation();
                    yield break;
                }

                world.castle.gold -= constructionCost;
                world.castle.SetDirty();

                // Instances are restricted
                if(towerConfig.maxInstances > 0)
                {
                    List<Tower> instances = world.towers.GetAll(t => t.towerId == towerConfig.id);

                    if(instances.Count >= towerConfig.maxInstances)
                    {
                        world.RemoveEntity(instances.Find(t => t.id == instances.Min(t => t.id)));
                    }

                }

                tower = towerConfig.CreateTower();
                tower.x = target.x;
                tower.y = target.y;

                world.AddEntity(tower);
            }
            else
            {
                tower = world.entities[node.entityId] as Tower;

                if (tower.isBuilt)
                {
                    int currentLevel = tower.level;

                    towerConfig = GameConfiguration.instance.towerConfigurations[tower.towerId].upgrade;

                    if (towerConfig == null)
                    {
                        characterBehaviour.PlayIdleAnimation();
                        yield break;
                    }

                    if (world.castle.gold < towerConfig.cost)
                    {
                        characterBehaviour.PlayIdleAnimation();
                        yield break;
                    }

                    // Upgrade Tower
                    world.RemoveEntity(tower);

                    world.castle.gold -= towerConfig.cost;
                    world.castle.SetDirty();

                    tower = towerConfig.CreateTower();
                    tower.level = (byte)(currentLevel + 1);
                    tower.x = target.x;
                    tower.y = target.y;
                    tower.constructionProgress = 0;
                    world.AddEntity(tower);
                }
            }
        }

       
        while (true)
        {
            if(isCancelled)
            {
                characterBehaviour.PlayIdleAnimation();
                break;
            }

            if (world.isMasterWorld)
            {
                tower.constructionProgress += Time.deltaTime / towerConfig.constructionTime;
                tower.SetDirty();

                if (tower.constructionProgress >= 1.0f)
                {
                    characterBehaviour.PlayIdleAnimation();
                    tower.constructionProgress = 1.0f;
                    tower.SetRealDirty();
                    break;
                }
            }

            yield return null;
        }

        characterBehaviour.PlayIdleAnimation();
    }
}
