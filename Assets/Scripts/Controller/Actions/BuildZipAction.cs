﻿//using Assets.Scripts.Model;
//using System;
//using System.Collections;
//using System.Linq;
//using UnityEngine;

//public class BuildZipAction : BuildAction
//{

//    protected override void OnFinish(bool wasStopped)
//    {
//        if (wasStopped)
//        {
//            return;
//        }

//        var spy = character as Spy;
//        var node = world.grid.GetNode(target.x, target.y);
//        var targetEntity = (Building)world.GetEntity(node.entityId);
//        if(targetEntity != null && targetEntity is ZipTower && targetEntity.isBuilt)
//        {
//            var tower = (ZipTower)targetEntity;
//            if (spy.connectingTowerIds.y != 0)
//            {
//                var previousTower = world.GetEntity(spy.connectingTowerIds.y);
//                var distance = Vector3.Distance(previousTower.worldPosition, tower.worldPosition);

//                if (previousTower != null && previousTower is ZipTower && distance > tower.range * world.grid.cellSize)     // TODO this fails silent if the distance is too long
//                {
//                    (previousTower as ZipTower).DrawZipLine(tower);
//                }
//            }

//            spy.connectingTowerIds.y = tower.id;                            // in case of zip lines this is the source tower
//            character.currentActionType = CharacterActionType.ConnectZip;   // by default we assume the scout is about to build the next zip connection after that
//            world.AddEntity(tower);
//        }
//    }
//}
