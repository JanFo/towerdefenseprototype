using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Manages entity view creation and gives access to entity views
/// </summary>
public class EntityViewController
{
    private World _world;
    private TowerViewController _towerViewController;
    private Dictionary<int, GameObject> _entityViews;


    public EntityViewController(World world)
    {
        _towerViewController = new TowerViewController(world);
        _entityViews = new Dictionary<int, GameObject>();
        _world = world;
    }


    public void Update()
    {
        foreach (Entity entity in _world.entities.Values)
        {
            if (!_entityViews.ContainsKey(entity.id))
            {
                _entityViews[entity.id] = CreateEntityView(entity);
            }
        }

        _towerViewController.Update();
    }

    private GameObject CreateEntityView(Entity entity)
    {
        //if(entity is Tower)
        //{
        //    return _towerViewController.CreateView(entity as Tower);
        //}

        if (entity is Character)
        {
            return CreateCharacterView(entity as Character);
        }

        if (entity is Monster)
        {
            return CreateMonsterView(entity as Monster);
        }

        if (entity is Castle)
        {
            return CreateCastleView(entity as Castle);
        }

        return null;
    }

    private GameObject CreateCastleView(Castle castle)
    {
        GameObject castleObject = new GameObject("castle");
        MeshFilter meshFilter = castleObject.AddComponent<MeshFilter>();
        meshFilter.mesh = GameConfiguration.instance.socketMesh;
        MeshRenderer meshRenderer = castleObject.AddComponent<MeshRenderer>();
        meshRenderer.material = GameConfiguration.instance.castleMaterial;
        castleObject.transform.localScale = new Vector3(GameConfiguration.tileSize / 2.0f, GameConfiguration.tileSize / 2.0f, GameConfiguration.tileSize / 2.0f);
        castleObject.transform.localEulerAngles = new Vector3(-90, 0, 0);
        castleObject.transform.position = new Vector3(castle.x + 0.5f, 0, castle.y + 0.5f);

        CastleView castleView = castleObject.AddComponent<CastleView>();
        castleView.Initialize(castle);

        return castleObject;
    }

    private GameObject CreateCharacterView(Character character)
    {
        var config = ArrayUtils.Find(GameConfiguration.instance.classConfigurations, c => c.entityType == character.type);
        GameObject characterObject = UnityEngine.Object.Instantiate(config.prefab);
        
        CharacterView characterBehaviour = characterObject.GetComponent<CharacterView>();
        characterBehaviour.Initialize(character, _world);

        return characterObject;
    }

    private GameObject CreateMonsterView(Monster monster)
    {
        MonsterView view = UnityEngine.Object.Instantiate(_world.level.monsters[monster.viewIndex]);
        view.Initialize(monster);
        return view.gameObject;
    }

 
    public GameObject GetEntityView(int id)
    {
        GameObject result = _entityViews[id];

        if (result == null)
        {
            return _towerViewController.GetEntityView(id);
        }

        return result;
    }
}
