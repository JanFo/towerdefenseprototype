﻿using Assets.Scripts.Model;
using System;
using System.Collections.Generic;
using UnityEngine;
using static Entity;

/// <summary>
/// The mighty synchronizer of worlds. Keeps track of the last entity revision that
/// has been sent over the network per entity in the world and notifies all connected
/// peers about entity changes. 
/// 
/// The master client should interpret entity updates as entity update requests that require
/// confirmation and thus a rebroadcast to all piers. 
/// 
/// All non-master clients should interpret entity updates as the new truth.
/// 
/// Additionally, all entities should have an owner, while most entities belong to the master client
/// only owners should be able to make changes to entities.
/// </summary>
public class WorldSynchronizer
{
    private NetworkManager _network;
    private float _interval;
    private World _world;

    private Dictionary<int, int> _lastsyncedRevisons = new Dictionary<int, int>();

    private float _timer;

    private Dictionary<int, bool> _isQueuedMap;

    private Queue<Entity> _entitiesToSync;

    private int _maxEntitySnycsPerFrame;

    public WorldSynchronizer(World world, NetworkManager network)
    {
        _maxEntitySnycsPerFrame = 16;
        _entitiesToSync = new Queue<Entity>();
        _lastsyncedRevisons = new Dictionary<int, int>(new EntityIdEqualityComparer());
        _isQueuedMap = new Dictionary<int, bool>(new EntityIdEqualityComparer());
        _world = world;
        _network = network;
        _interval = 0.1f;
        _timer = _interval;

        _network.RegisterProcedures(this);
    }

    public void Update()
    {
        foreach (Entity entity in _world.entities.Values)
        {
            bool isQueued = ArrayUtils.Get(_isQueuedMap, entity.id, false);

            if(isQueued)
            {
                continue;
            }

            int lastSyncedRevision = ArrayUtils.Get(_lastsyncedRevisons, entity.id, -1);

            if (entity.revision <= lastSyncedRevision)
            {
                continue;
            }

            _isQueuedMap[entity.id] = true;
            _entitiesToSync.Enqueue(entity);
        }


        for (int i = 0; i < _maxEntitySnycsPerFrame; i++)
        {
            if(_entitiesToSync.Count == 0)
            {
                break;
            }

            Entity entity = _entitiesToSync.Dequeue();
            // Debug.Log($"[{_network.Name}] Sending Entity {entity.type}_{entity.id}.");
            IMessageWriter writer = _network.AppendMessage("OnEntityUpdated", entity.requestReliableSync);
            entity.GetEntityData().Serialize(writer);
            entity.Serialize(writer);
            writer.Send();

            entity.requestReliableSync = false;
            _isQueuedMap[entity.id] = false;
            _lastsyncedRevisons[entity.id] = entity.revision;
        }
    }

    [HostProcedure, ClientProcedure]
    private void OnEntityUpdated(IMessageReader reader)
    {
        EntityData generic = new EntityData();
        generic.Deserialize(reader);

        var entity = ArrayUtils.Get(_world.entities, generic.id, null);

        if (entity == null)
        {
            entity = CreateFromMessage(generic, reader);
            _world.AddEntity(entity);
            // Debug.Log($"[{_network.Name}] Created Entity {entity.type}_{entity.id}.");
        }
        else
        {
            if (entity.revision < generic.revision)
            {
                entity.Deserialize(reader);
                entity.revision = generic.revision;
                // Debug.Log($"[{_network.Name}] Updated Entity {entity.type}_{entity.id}.");
            }
        }

        if (!_network.IsHost)
        {
            _lastsyncedRevisons[entity.id] = entity.revision;
        }
    }

    public void Resend()
    {
        // Works but is highly inefficient
        _lastsyncedRevisons.Clear();
    }

    public static Entity CreateFromMessage(EntityData generic, IMessageReader reader)
    {
        Entity entity = null;      

        switch (generic.type)
        {   
            case EntityType.Castle:
                entity = new Castle(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.Spy:
                entity = new Spy(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.Monster:
                entity = new Monster(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.Tower:
                entity = new Tower(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.Anchorman:
                entity = new Anchorman(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.AirStrikeBeacon:
                entity = new AirStrikeBeacon(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.Barracks:
                entity = new Barracks(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.Demolisher:
                entity = new Demolisher(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.Soldier:
                entity = new Soldier(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.Spawn:
                entity = new Spawn(generic);
                entity.Deserialize(reader);
                break;
            case EntityType.ZipTower:
                entity = new ZipTower(generic);
                entity.Deserialize(reader);
                break;

        }
        return entity;
    }

}
