using System.Collections.Generic;
using UnityEngine;

public class CharacterActionController
{
    private World _world;

    private EntityViewController _viewManager;

    private Dictionary<int, CharacterActionType> _previousActions;

    private Canvas _canvas;

    public CharacterActionController    (World world, EntityViewController viewManager, Canvas canvas)
    {
        _world = world;
        _viewManager = viewManager;
        _previousActions = new Dictionary<int, CharacterActionType>();
        _canvas = canvas;
    }

    public void Update()
    {
        foreach (Character character in _world.characters)
        {
            if (!_previousActions.ContainsKey(character.id))
            {
                _previousActions[character.id] = character.currentActionType;
            }

            if (_previousActions[character.id] != character.currentActionType)
            {
                if (character.currentActionType != CharacterActionType.None)
                {
                    character.currentAction = CharacterAction.CreateAction(character, _world, _viewManager, _canvas);
                }
                else if (character.currentAction != null)
                {
                    character.currentAction.Cancel();
                }

                _previousActions[character.id] = character.currentActionType;
            }

            if (character.currentAction != null)
            {
                if (! character.currentAction.isDone)
                {
                    character.currentAction.Update();
                }
                else
                {
                    character.currentActionType = CharacterActionType.None;
                    character.currentAction = null;
                }
            }
        }
    }
}