using System;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UserInterface;
/// <summary>
/// Nicht sicher, ob das so doll ist, ist eigentlich ein Sub-Job des Game Controllers,
/// also vielleicht wieder in den GameController packen
/// </summary>
public class UserInterfaceController
{
    private World _world;

    private PlayerInput _input;

    private Player _player;

    private int _currentPlayerGold;

    private int _currentPlayerHealth;

    private RectTransform _gameOverPanel;

    private UIButton _tryAgainButton;

    private RectTransform _root;

    private int _currentWave;

    private TMP_Text _playerGoldText;

    private TMP_Text _playerHealthText;

    private RectTransform _messagePanel;

    private Text _waveText;

    private string _waveString = "A wave is approaching from {0}!";

    private long _disableAfter = 0L;

    private static int _displayWaveMsgMs = 5000;

    private UIInputModule _uiModule;

    private HealthBarController _healthbarController;
    private ConstructionProgressBarController _constructionProgressController;
    private ConstructionPanelController _constructionPanelController;
    private CharacterSelectionPanelController _characterSelectionPanelController;

    public UserInterfaceController(Player player, World world, PlayerInput input, Canvas canvas)
    {
        _world = world;
        _input = input;
        _player = player;
        _root = canvas.GetComponent<RectTransform>();
       

        _uiModule = new UIInputModule(player.index, input);
        _healthbarController = new HealthBarController(_world, canvas);
        _constructionProgressController = new ConstructionProgressBarController(_world, canvas);
        _constructionPanelController = new ConstructionPanelController(player, world, _root);
        _characterSelectionPanelController = new CharacterSelectionPanelController(player, world, _root);

        Initialize();

    }

    private void Initialize()
    {
        _playerGoldText = UIQuery.Get<TMP_Text>(_root, "castle-gold");
        _playerHealthText = UIQuery.Get<TMP_Text>(_root, "castle-health");
        _waveText = UIQuery.Get<Text>(_root, "wave-text");
        _currentPlayerGold = -1;
        _currentPlayerHealth = -1;

        _messagePanel = UIQuery.Get<RectTransform>(_root, "message-panel");
        _messagePanel.gameObject.SetActive(false);
        _currentWave = -1;

        _gameOverPanel = UIQuery.Get<RectTransform>(_root, "game-over-panel");
        _gameOverPanel.gameObject.SetActive(false);

        _tryAgainButton = UIQuery.Get<UIButton>(_root, "try-again-button");
        _tryAgainButton.OnClick.AddListener(OnTryAgainButtonClicked);

        _uiModule.TargetInputSystem = _root.GetComponent<UIInputSystem>();
        _uiModule.Enabled = true;
    }

    private void OnTryAgainButtonClicked(UISelectable arg0, UIInputEventArgs arg1)
    {
        Debug.Log("BOING");
    }

    public void Update()
    {
        _healthbarController.Update();
        _constructionPanelController.Update();
        _constructionProgressController.Update();
        _characterSelectionPanelController.Update();

        int goldAmount = _world.castle?.gold ?? 0;
        int healthAmount = _world.castle?.hp ?? 0;

        if (_currentPlayerGold != goldAmount)
        {
            _playerGoldText.text = goldAmount.ToString();
            _currentPlayerGold = goldAmount;
        }

        if (_currentPlayerHealth != healthAmount)
        {
            _playerHealthText.text = healthAmount.ToString();
            _currentPlayerHealth = healthAmount;
        }

        if (_world.castle != null && _world.castle.waveIndex != _currentWave)
        {
            int spawnIndex = _world.castle.waveIndex % _world.spawns.Count;
            Spawn spawn = _world.spawns[spawnIndex];

            _currentWave = _world.castle.waveIndex;
            _waveText.text = string.Format(_waveString, spawn.name);
            _messagePanel.gameObject.SetActive(true);
            _disableAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() + _displayWaveMsgMs;
        }

        if (DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() > _disableAfter)
        {
            _messagePanel.gameObject.SetActive(false);
        }

        if(_world.castle != null && _world.castle.gameState == GameState.Lost)
        {
            _gameOverPanel.gameObject.SetActive(true);
        }
    }
}
