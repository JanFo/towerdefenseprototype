//using System.Linq;
//using UnityEngine;



//public class CharacterMovementController
//{
//    private World _world;

//    private NetworkManager _network;

//    public CharacterMovementController(World world, NetworkManager network)
//    {
//        _world = world;
//        _network = network;

//        _network.AddMessageHandler<CharacterPositionUpdatedMessage>(OnRemoteCharacterPositionUpdated);
//    }

//    // Update own character position here
//    public void MoveCharacter(Character character, Vector3 position)
//    {
//        if(!character.isLocal)
//        {
//            return;
//        }

//        character.position = position;

//        _network.EnqueueMessage(new CharacterPositionUpdatedMessage()
//        {
//            characterId = character.id,
//            position = position
//        });
//    }

//    // Receive the position updates from other clients
//    private void OnRemoteCharacterPositionUpdated(CharacterPositionUpdatedMessage message)
//    {
//        // Find character by id and set position...
//        Character character = _world.characters.FirstOrDefault(c => c.id == message.characterId);

//        if(character != null)
//        {
//            character.position = message.position;
//        }
//    }

//    // Set the view position for local and remote characters
//    public void Update()
//    {
//        foreach(Character character in _world.characters)
//        {
//            if(!character.isLocal)
//            {
//                character.transform.position = Vector3.MoveTowards(character.transform.position, character.position, 2.0f * Time.deltaTime);
//            }
//            else
//            {
//                character.transform.position = character.position;
//            }
//        }
//    }
//}