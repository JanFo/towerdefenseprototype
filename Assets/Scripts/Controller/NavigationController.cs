using Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Nicht sicher, ob das so doll ist, ist eigentlich ein Sub-Job des Game Controllers,
/// also vielleicht wieder in den GameController packen
/// </summary>
public class NavigationController
{
    private World _world;


    // private AstarPathfinder _astarPathFinder;

    private ExternalPathfinder _externalPathFinder;



    public NavigationController(World world, Level level)
    {
        _world = world;
        _world.grid.InvalidatePaths();

        // _astarPathFinder = new AstarPathfinder(world.grid);
        _externalPathFinder = new ExternalPathfinder(world.grid);
        
        PathfinderDebug debugView = UnityEngine.Object.FindObjectOfType<PathfinderDebug>();

        if (debugView != null) 
        {
            debugView.DrawDebug(world.grid);
        }

        _navMeshPath = new NavMeshPath();
        _queryFilter = new NavMeshQueryFilter()
        {
            agentTypeID = 0,
            areaMask = NavMesh.AllAreas
        };
    }

    public void Update()
    {
        foreach (Monster monster in _world.monsters)
        {
            if (monster.path == null && !monster.isRioting)
            {
                _externalPathFinder.FindPath(monster, _world.castle.worldPosition);

                if (monster.path == null)
                {
                    monster.isRioting = true;
                }
            }
        }

        if (_world.grid.hasInvalidPaths)
        {
            RecalculateMonsterPaths();
            _world.grid.ValidatePaths();
        }

    }


    private static float MinForkDistance = -0.5F;

    private NavMeshPath _navMeshPath;

    private NavMeshQueryFilter _queryFilter;

    private Vector3 CalculateNextPathFork(Monster monster)
    {
        if (monster.path == null || monster.path.Length == 0)
            return monster.worldPosition;

        var dist = monster.distanceOnPath;
        var pathIdx = 0;
        while(dist > MinForkDistance && monster.distanceArray.Length > pathIdx)
        {
            dist = dist - monster.distanceArray[pathIdx];
            if (pathIdx < monster.distanceArray.Length - 1)
                pathIdx++;
        }
        return monster.path[pathIdx];
    }

    private void RecalculateMonsterPaths()
    {
        foreach(Spawn spawn in _world.spawns)
        {
            _externalPathFinder.FindPath(spawn, _world.castle.worldPosition);
        }

        foreach(Monster monster in _world.monsters)
        {
            _externalPathFinder.FindPath(monster, _world.castle.worldPosition);
        }
    }

    //private void CalculatePath(Spawn spawn)
    //{
    //    _externalPathFinder.FindPath(spawn, _world.castle.worldPosition);
    //}

    //private void CalculatePath(Monster monster)
    //{
    //    //Vector3 forkPosition = CalculateNextPathFork(monster);

    //    // swap out pathfinders here:


    //     _externalPathFinder.FindPath(monster, _world.castle.worldPosition);
    
    //    // TakeWhile und der LINQ Kram sind so viel overhead
    //    //Vector3 forkPosition = CalculateNextPathFork(monster);
    //    //IEnumerable<Vector3> pathStump = monster.path == null ? new Vector3[] { } : monster.path.TakeWhile(n => n != forkPosition);
    //    //_astarPathFinder.InvokePathFinding(forkPosition, _world.castle.worldPosition, (p) => OnAstarPathComplete(pathStump, p, monster));
    //    //_externalPathFinder.FindPath(forkPosition, _world.castlePosition, (p) => OnExternalPathComplete(p, monster));

    //    // TakeWhile und der LINQ Kram sind so viel overhead
    //    //IEnumerable<Vector3> pathStump = monster.path == null ? new Vector3[] { } : monster.path.TakeWhile(n => n != forkPosition);
    //    //_astarPathFinder.InvokePathFinding(forkPosition, _world.castle.worldPosition, (p) => OnAstarPathComplete(pathStump, p, monster));


    //}

    //private void OnAstarPathComplete(IEnumerable<Vector3> stump, Vector3[] path, Monster monster)
    //{
    //    if (path != null)
    //    {
    //        monster.path = stump.Concat(path).ToArray();
    //        monster.distanceArray = PathUtils.CreateDistanceArray(monster.path);
    //    }
    //}

    //private void OnExternalPathComplete(Path p, Monster monster)
    //{
    //    monster.path = p.vectorPath.ToArray(); 
    //    monster.distanceArray = PathUtils.CreateDistanceArray(monster.path);
    //    monster.distanceOnPath = 0;
    //}

}
