using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnController
{
    public int id { get; }

    public Vector3 position { get; }

    private float _lastSpawnTime;

    public float prevelance { get; set; }

    public Wave currentWave { get; private set; }

    public LevelTag currentMonsterLevel { get; set; }

    private Level _level;

    private World _world;

    private Coroutine _theRoutine;

    private List<Wave> _waves;

    public SpawnController(int id, Vector3 position, World world, float prevelance)
    {
        _world = world;
        this.id = id;
        this.position = position;
        _lastSpawnTime = Time.time;
        this.prevelance = prevelance;

    }

    //public SpawnController(World world, Level level)
    //{
    //    _level = level;
    //    _world = world;
    //    _lastSpawnTime = Time.time;

    //    _waves = new List<Wave>();

    //    for (int i = 0; i < world.level.monsters.Length; i++)
    //    {
    //        _waves.Add(new Wave(world.level, i));
    //    }

    //    // _waves = WaveGenerator.GenerateWaves(level);
    //}

    public SpawnController(World world)
    {
        _world = world;
        _lastSpawnTime = Time.time;

        _waves = new List<Wave>();

        for (int i = 0; i < world.level.waveCount; i++)
        {
            _waves.Add(new Wave(world.level, i));
        }
    }


    public void Update()
    {
        if(_world.castle.waveIndex >= _waves.Count - 1)
        {
            return;
        }

        if (Time.time - _lastSpawnTime > _waves[_world.castle.waveIndex + 1].initialDelay)
        {
            _world.castle.waveIndex++;

            // spawn a wave at the preferred spawn!
            InitiateWave();
            _lastSpawnTime = Time.time;
        }

        foreach(Spawn spawn in _world.spawns)
        {
            if(spawn.path != null && spawn.path.Length == 0)
            {
                var destroy = _world.towers.ToList().Find(t => t.hp > 0 && t.id == _world.towers.ToList().Max(t => t.id));
                if (destroy != null)
                {
                    destroy.hp = 0;
                    destroy.SetRealDirty();
                    spawn.AssignPath(new Vector3[] { spawn.worldPosition });
                    break;
                }
            }
        }
    }

    private void InitiateWave()
    { 
        Wave wave = _waves[_world.castle.waveIndex];

        Debug.Log(string.Format("Initiated new wave with power {0}.",
               new object[] { wave.healthScale }));

        if (_theRoutine != null)
        {
            WaveEngine.StopRoutine(_theRoutine);
        }

        _theRoutine = WaveEngine.StartRoutine(SendWave(wave));

        //if (currentWave == null || currentWave.IsDone())
        //{
        //    Action<Monster> AddMonster = delegate (Monster monster)
        //    {
        //        monster.worldPosition = this.position;
        //        _world.AddEntity(monster);
        //    };

        //    this.currentMonsterLevel = (LevelTag)Enum.ToObject(typeof(LevelTag), config.monsterLevel);
        //    UnityEngine.Assertions.Assert.IsTrue(config.waveId >= 0);


        //    Debug.Log(string.Format("Initiated new wave {0} with {1} monsters and a duration of {2} seconds.",
        //        new object[] { config.waveId, config.monsterCount, config.waveLengthSec }));
        //    currentWave = new Wave(config, this.prevelance);

        //    WaveEngine.StopRoutine(_theRoutine);
        //    _theRoutine = WaveEngine.StartRoutine(currentWave.GenerateWave(AddMonster));
        //    return true;
        //}
        //else
        //{
        //    return false;
        //}
    }

    public IEnumerator SendWave(Wave wave)
    {
        int[] spawnAmounts = new int[_world.spawns.Count];

        List<Monster> monsters = WaveGenerator.GenerateMonsters(wave);

        for(int i = 0; i < monsters.Count; i++)
        {
            int spawnIndex = SelectSpawn(wave.prevalence);
            spawnAmounts[spawnIndex]++;
        }

        while (monsters.Count > 0)
        {
            // Wait a bit

            // TODO Maybe roll the spawnInterval
            yield return new WaitForSeconds(wave.spawnInterval);

            // Select the spawn based on the wave prevalence

            for(int i = 0; i < spawnAmounts.Length; i++)
            {
                if (spawnAmounts[i] == 0)
                {
                    continue;
                }

                SpawnMonster(monsters[0], _world.spawns[i]);

                spawnAmounts[i]--;
                monsters.RemoveAt(0);
            }

            yield return null;
        }

        yield break;
    }

    private void SpawnMonster(Monster monster, Spawn spawn)
    {
        monster.worldPosition = GridUtils.GetWorldPosition(spawn.position.x, spawn.position.y);
        monster.AssignPath(spawn.path);

        _world.AddEntity(monster);
    }

    private int SelectSpawn(float[] prevalence)
    {
        return 0;

        //if(prevalence.Length != _level.spawnLocations.Count)
        //{
        //    return 0;
        //}

        //float rand = UnityEngine.Random.value;
        //float prevalenceSum = 0.0f;

        //for (int i = 0; i < prevalence.Length; i++)
        //{
        //    prevalenceSum += prevalence[i];

        //    if (rand < prevalenceSum)
        //    {
        //        return i;
        //    }
        //}

        //return 0;
    }
}
