using Assets.Scripts.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


/// <summary>
/// NUR AUF DEM MASTER CLIENT
/// 
/// A) Monster Spawnen
/// B) Towers abfeuern
/// C) Towerziele suchen
/// D) Pathfinding
/// E) Monsterposition updaten
/// F) Waves Updaten (wie synchronisieren wir waves?)
/// </summary>
public class GameController 
{
    private World _world;

    private SpawnController _spawnController;

    private CharacterSpawnController _chracterCreator;

    private MonsterCollisionController _monsterCollisions;

    // private Dictionary<int, List<WaveConfig>> _waveTracker = new Dictionary<int, List<WaveConfig>>();

    //private float _lastWaveTime;


    public GameController(World world, Level level)
    {
        _world = world;

        _spawnController = new SpawnController(world);
        _chracterCreator = new CharacterSpawnController(world);
        _monsterCollisions = new MonsterCollisionController(world);
    }

    public void Update()
    {
        if(_world.castle.gameState != GameState.Running)
        {
            return;
        }

        UpdateCastle();
        // TODO: Remove
        // UpdateTowerSpawns();

        // create or respawn characters
        _chracterCreator.Update();

        // A) Monster Spawnen
        _spawnController.Update();
        //UpdateMonsterSpawns();

        // D + E) Monster Updaten
        UpdateMonsters();

        // B + C) Towerziele finden und losballern
        UpdateTowers();

        // check for collisions between monsters and players
        _monsterCollisions.Update();

    }

    private void UpdateCastle()
    {
        if(_world.castle.hp <= 0)
        {
            _world.castle.gameState = GameState.Lost;
            _world.castle.SetRealDirty();
        }
    }

    private void UpdateTowers()
    {
        foreach (Tower tower in _world.towers)
        {
            if(tower.constructionProgress < 1.0f)
            {
                continue;
            }

            FindTowerTarget(tower);

            if(tower.target == 0)
            {
                continue;
            }

            if (Time.time - tower.lastFireTime > tower.speed)
            {
                FireTower(tower);
            }
        }
    }

    private void FindTowerTarget(Tower tower)
    {
        foreach(Monster monster in _world.monsters)
        {
            if(monster.isDead)
            {
                continue;
            }

            Vector3 towerPosition = PathUtils.GetWorldPosition(tower.x, tower.y);

            if (Vector3.Distance(towerPosition, monster.worldPosition) < GameConfiguration.GetRange(tower.range))
            {
                tower.target = monster.id;
                return;
            }
        }

        tower.target = 0;
    }

    private void FireTower(Tower tower)
    {
        Monster monster = _world.monsters.Find(m => m.id == tower.target);
        
        if(monster == null)
        {
            return;
        }

        tower.fireCounter++;
        tower.lastFireTime = Time.time;
        tower.SetDirty();
    }

    private void UpdateMonsters()
    {
        for (int i = _world.monsters.Count - 1; i >= 0; i--)
        {
            Monster monster = _world.monsters[i];

            if(monster.hp <= 0 && !monster.isDead)
            {
                _world.castle.gold += monster.bounty;
                
                monster.isDead = true;
                monster.SetRealDirty();
            }

            if(monster.isDead)
            {
                continue;
            }

            monster.distanceOnPath += Time.deltaTime * monster.speed;
            Vector3 position;

            if (PathUtils.SamplePath(monster.path, monster.distanceArray, monster.distanceOnPath, out position))
            {
                Vector3 delta = (position - monster.worldPosition).normalized;


                monster.rotation = -Vector3.SignedAngle(delta, Vector3.forward, Vector3.up);
                monster.worldPosition = position;

                if(Vector3.Distance(monster.worldPosition, _world.castle.worldPosition) < 0.1f)
                {
                    // lose life and remove the monster
                    monster.isDead = true;
                    monster.SetRealDirty();

                    _world.castle.hp--;
                    _world.castle.SetRealDirty();
                }


                monster.SetDirty();
            }
        }
    }

    //private void UpdateMonsterSpawns()
    //{
    //    var numberOfSpawns = _world.spawnPositions.Count;
    //    int spawnIndex = _world.currentWave / numberOfSpawns;


    //    Vector3 position = _world.spawnPositions[spawnIndex];

    //    if(Time.time - _lastWaveTime > 30.0f)
    //    {
    //        // spawn a wave at the preferred spawn!
    //        InitiateWave(position, GameConfiguration.GetWaveConfig(_world.currentWave));

    //        _lastWaveTime = Time.time;
    //        _world.currentWave++;
    //    }


    //    //foreach (Spawn spawn in _world.spawns)
    //    //{
    //    //    // Something with wave times?

    //    //    if(spawn.currentWave == null || spawn.currentWave.IsDone())
    //    //    {
    //    //         var waveConfig = GameConfiguration.GetWaveConfig(_world.currentWave);

    //    //        if(waveConfig != null)
    //    //        {
    //    //            spawn.InitiateWave(waveConfig);
    //    //        }

    //    //        _waveTracker.TryGetValue(_world.currentWave, out List<WaveConfig> currentWave);
                
    //    //        if (currentWave == null)
    //    //        {
    //    //            currentWave = new List<WaveConfig>();
    //    //            _waveTracker.Add(_world.currentWave, currentWave);
    //    //        }

    //    //        currentWave.Add(waveConfig);

    //    //        if (currentWave.Count == _world.spawns.Count)
    //    //        {
    //    //            _world.currentWave++;
    //    //        }
    //    //    }
    //    //}
    //}
}
