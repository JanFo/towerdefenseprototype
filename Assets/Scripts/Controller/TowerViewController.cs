using System;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Handles the monster count, should also handle the tower ids on the grid.
/// </summary>
public class TowerViewController
{
    private World _world;

    private Dictionary<int, int> _fireCounters;

    private List<ProjectileView> _projectiles;

    private Dictionary<int, TowerView> _towerViews;

    public TowerViewController(World world)
    {
        _towerViews = new Dictionary<int, TowerView>();
        _projectiles = new List<ProjectileView>();
        _fireCounters = new Dictionary<int, int>(new EntityIdEqualityComparer());
        _world = world;

        _world.OnTowerAdded += OnTowerAdded;
        _world.OnTowerRemoved += OnTowerRemoved;
    }

    private void OnTowerRemoved(Tower tower)
    {
        _towerViews[tower.id].Demolish();

        UnityEngine.Object.Destroy(_towerViews[tower.id].gameObject);
        _towerViews.Remove(tower.id);
    }

    private void OnTowerAdded(Tower tower)
    {
        TowerConfiguration config = GameConfiguration.instance.towerConfigurations[tower.towerId];

        TowerView towerView = UnityEngine.Object.Instantiate(config.prefab);
        towerView.Initialize(tower, _world);

        _towerViews.Add(tower.id, towerView);
    }

    public void ApplyDamage(Monster monster, int amount)
    {
        if(!_world.isMasterWorld)
        {
            return;
        }

        monster.hp -= (short)amount;
        monster.SetDirty();
    }


    public void Update()
    {

        foreach (Tower tower in _world.towers)
        {
            TowerView towerView = _towerViews[tower.id];
            UpdateTowerView(towerView, tower);
        }
    }

    private void UpdateTowerView(TowerView towerView, Tower tower)
    {
        if (tower.target == 0 || !_world.entities.ContainsKey(tower.target))
        {
            towerView.targetPosition = null;
        }
        else
        {
            towerView.targetPosition = (_world.entities[tower.target] as Creature).worldPosition;
        }
    }

    internal GameObject GetEntityView(int id)
    {
        if (_towerViews.ContainsKey(id))
        {
            return _towerViews[id].gameObject;
        }

        return null;
    }
}