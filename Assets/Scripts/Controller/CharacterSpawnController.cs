﻿using System;
using UnityEngine;

namespace Assets.Scripts.Controller
{
    public class CharacterSpawnController
    {

        private World _world;

        public CharacterSpawnController(World world)
        {
            _world = world;
        }

        public void Update()
        {
            for (int i = 0; i < _world.barracks.respawnData.Length; i++)
            {
                RespawnData respawnData = _world.barracks.respawnData[i];

                if(respawnData.cooldown > 0)
                {
                    respawnData.cooldown -= Time.deltaTime;
                    continue;
                }

                if (respawnData.isRequested)
                {
                    Character character = ArrayUtils.Find(_world.characters, c => c.playerIndex == i);

                    if (character != null)
                    {
                        character.playerIndex = -1;
                        continue;
                    }

                    respawnData.isRequested = false;

                    character = _world.characters.Find(c => c.type == respawnData.characterType && c.playerIndex == -1);

                    //switch(respawnData.characterType)
                    //{
                    //    case EntityType.Soldier:
                    //        character = new Soldier();
                    //        break;
                    //    case EntityType.Spy:
                    //        character = new Spy();
                    //        break;
                    //    case EntityType.Demolisher:
                    //        character = new Demolisher();
                    //        break;
                    //    case EntityType.Anchorman:
                    //        character = new Anchorman();
                    //        break;
                    //}

                    if(character == null)
                    {
                        continue;
                    }

                    character.playerIndex = i;
                    character.worldPosition = respawnData.position;
                    character.initialHp = GameConfiguration.defaultCharacterHealth;
                    character.hp = GameConfiguration.defaultCharacterHealth;
                    character.isDeployed = true;
                    character.SetRealDirty();
                    // character.level = 0;

                    //_world.AddEntity(character);
                }

                //if (character != null)
                //{
                //    if (_world.characters[i].isDead)
                //    {
                //        _world.castle.respawnData[i].isRequested = ! respawner[i].Respawn();
                //        if (! _world.castle.respawnData[i].isRequested)
                //        {
                //            respawner[i] = null;
                //        }
                //    }
                //}
                //else
                //{
                //    // Prompt player choice
                //    Debug.Log("ADDED CHARACTER " + i);
                //    _world.AddEntity(new Spy()         // TODO set level
                //    {
                //        playerIndex = i,
                //        level = 0,
                //        identity = (CharacterIdentity) i,   // TODO
                //        worldPosition = respawnData.position,
                //        initialHp = GameConfiguration.defaultCharacterHealth,
                //        hp = GameConfiguration.defaultCharacterHealth
                //    }) ;

                //    _world.castle.respawnData[i].isRequested = false;
                //}
            }
        }
    }
}
