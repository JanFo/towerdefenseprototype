﻿using System;
using System.Collections;
using UnityEngine;

public class VaxinatorController : MonoBehaviour, ITowerComponentController
{
    // Gameobjects need to control rotation and aiming
    [SerializeField]
    private Transform _baseTransform = null;

    [SerializeField]
    private Transform[] _holes = null;

    [SerializeField]
    private float _projectileSpeed = 3;

    [SerializeField]
    private float _overshootFactor = 1.25f;

    [SerializeField]
    public ParticleSystem _muzzlePrefab;

    [SerializeField]
    private VaxinatorDartController _vaxinatorDartControllerPrefab; 
    
    private Monster _targetMonster;

    private Tower _tower;

    private World _world;

    private int _holeIndex;

    private void Update()
    {
        if ((_targetMonster?.id ?? 0) != _tower.target)
        {
            if (_tower.target == 0)
            {
                _targetMonster = null;
            }
            else
            {
                _targetMonster = _world.entities[_tower.target] as Monster;
            }
        }

        int direction = (int)_tower.direction;
        _baseTransform.eulerAngles = new Vector3(0, direction * 90.0f, 0);
    }

    public IEnumerator FireProjectile()
    {
        if (_targetMonster == null)
        {
            yield break;
        }

        // Pick a hole

        _holeIndex = ArrayUtils.RepeatIndex(_holeIndex + 1, _holes.Length);
        Transform hole = _holes[_holeIndex];

        if (_muzzlePrefab != null)
        {
            ParticleSystem muzzleInstance = Instantiate(_muzzlePrefab);
            muzzleInstance.transform.position = hole.position;
            muzzleInstance.transform.eulerAngles = hole.eulerAngles;
        }

        if(_vaxinatorDartControllerPrefab != null)
        {
            VaxinatorDartController vaxinatorDartController = Instantiate(_vaxinatorDartControllerPrefab);
            vaxinatorDartController.transform.position = hole.position;
            vaxinatorDartController.transform.eulerAngles = hole.eulerAngles;
            vaxinatorDartController.Initialize(_tower, _world, hole.forward * _projectileSpeed, _tower.range * _overshootFactor);
        }
    }

    public void OnInitialize(Tower tower, World world)
    {
        _tower = tower;
        _world = world;
    }


    public void OnFire()
    {
        StartCoroutine(FireProjectile());
    }

    public void OnDemolish()
    {

    }

    public void OnComplete()
    {
    }
}
