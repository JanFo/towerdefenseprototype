﻿using System;
using System.Collections;
using UnityEngine;

public class MortarShellController : MonoBehaviour
{
    [SerializeField]
    private GameObject _explosionPrefab = null;

    private Tower _tower;

    private World _world;

    private Vector3 _velocity;

    private float _explosionRadius;

    private AnimationCurve _damageCurve;

    private float _gravity;

    private float _simulationSpeed;

    public void Initialize(Tower tower, World world, Vector3 velocity, float explosionRadius, AnimationCurve damageCurve, 
        float gravity, float simulationSpeed)
    {
        _tower = tower;
        _world = world;
        _velocity = velocity;
        _explosionRadius = explosionRadius;
        _damageCurve = damageCurve;
        _gravity = gravity;
        _simulationSpeed = simulationSpeed;
    }

    private void Update()
    {
        _velocity += Vector3.down * Time.deltaTime * _simulationSpeed * _gravity;
        transform.position += _velocity * Time.deltaTime * _simulationSpeed;
        transform.forward = _velocity.normalized;

        if (transform.position.y <= 0)
        {
            if (_world.isMasterWorld)
            {
                Vector3 worldPosition = transform.position;

                foreach (Monster monster in _world.monsters)
                {
                    float distanceToMonster = Vector3.Distance(monster.worldPosition, worldPosition);

                    if (distanceToMonster < _explosionRadius)
                    {
                        float scale = _damageCurve.Evaluate(1.0f - distanceToMonster / _explosionRadius);

                        monster.hp -= (short)(_tower.damage * scale);
                        monster.SetDirty();
                    }
                }
            }

            GameObject explosionInstance = Instantiate(_explosionPrefab);
            explosionInstance.transform.position = transform.position;

            Destroy(explosionInstance.gameObject, 3.0f);

            enabled = false;
            Destroy(gameObject);
        }
    }

}
