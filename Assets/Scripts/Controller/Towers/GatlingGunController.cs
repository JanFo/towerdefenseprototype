﻿using System.Collections;
using UnityEngine;

public class GatlingGunController : MonoBehaviour, ITowerComponentController
{
    // Gameobjects need to control rotation and aiming
    [SerializeField]
    private Transform _baseTransform = null;

    [SerializeField]
    private Transform _gunBodyTransform = null;

    [SerializeField]
    private Transform _barrelTransform = null;

    [SerializeField]
    private Transform _barrelOpening = null;

    [SerializeField]
    private GameObject _projectilePrefab = null;

    // Gun barrel rotation
    public float barrelRotationSpeed;

    // Particle system for the muzzel flash
    public ParticleSystem muzzelFlash;

    private Monster _targetMonster;

    private float currentRotationSpeed;

    private float targetBarrelRotationSpeed;

    private float _projectileSpeed = 50;
    
    private Tower _tower;
    
    private World _world;

    public void OnComplete()
    {
    }

    private void Update()
    {
        if((_targetMonster?.id ?? 0) != _tower.target)
        {
            if(_tower.target == 0)
            {
                _targetMonster = null;
            }
            else
            {
                _targetMonster = _world.entities[_tower.target] as Monster;
            }
        }


        targetBarrelRotationSpeed = _targetMonster == null ? 0 : barrelRotationSpeed;
        currentRotationSpeed = Mathf.Lerp(targetBarrelRotationSpeed, 0, 10 * Time.deltaTime);
        // Gun barrel rotation
        _barrelTransform.Rotate(0, 0, currentRotationSpeed * Time.deltaTime);

        if(currentRotationSpeed > 50)
        {
            if (!muzzelFlash.isPlaying)
            {
                muzzelFlash.Play();
            }
        }

        if(currentRotationSpeed < 50)
        {
            if (muzzelFlash.isPlaying)
            {
                muzzelFlash.Stop();
            }
        }

        Aim();
    }

    void Aim()
    {
        // start particle system 
        if (_targetMonster == null)
        {
            return;
        }

        // aim at enemy
        Vector3 baseTargetPostition = new Vector3(_targetMonster.worldPosition.x, transform.position.y, _targetMonster.worldPosition.z);
        Vector3 gunBodyTargetPostition = _targetMonster.worldPosition;

        _baseTransform.transform.LookAt(baseTargetPostition);
        _gunBodyTransform.transform.LookAt(gunBodyTargetPostition);
    }

    public IEnumerator FireProjectile()
    {
        if(_targetMonster == null)
        {
            yield break;
        }

        GameObject projectileInstance = Instantiate(_projectilePrefab);
        _barrelOpening.Append(projectileInstance.transform);
        ParticleSystem particleSystem = projectileInstance.GetComponent<ParticleSystem>();
        ParticleSystem.MainModule main = particleSystem.main;

        Destroy(projectileInstance, 2.0f);

        float distanceToTarget = Vector3.Distance(_barrelOpening.position, _targetMonster.worldPosition);
        main.startLifetime = (distanceToTarget / _projectileSpeed);

        yield return new WaitForSeconds(distanceToTarget / _projectileSpeed);

        if (!_world.isMasterWorld)
        {
            yield break;
        }

        if (_targetMonster != null)
        {
            _targetMonster.hp -= (short)_tower.damage;
            _targetMonster.SetDirty();
        }
    }

    public void OnInitialize(Tower tower, World world)
    {
        _tower = tower;
        _world = world;
    }

    public void OnFire()
    {
        StartCoroutine(FireProjectile());
    }

    public void OnDemolish()
    {

    }
}
