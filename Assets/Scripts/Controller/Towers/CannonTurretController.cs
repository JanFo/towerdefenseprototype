﻿using System.Collections;
using UnityEngine;

public class CannonTurretController : MonoBehaviour, ITowerComponentController
{
    // Gameobjects need to control rotation and aiming
    [SerializeField]
    private Transform _baseTransform = null;

    [SerializeField]
    private Transform _gunBodyTransform = null;

    [SerializeField]
    private Transform _barrelOpening = null;

    [SerializeField]
    private GameObject _projectilePrefab = null;

    // Particle system for the muzzel flash
    public ParticleSystem muzzelFlash;

    private Monster _targetMonster;

    [SerializeField]
    private float _projectileSpeed = 50;

    private Tower _tower;

    private World _world;

    private void Update()
    {
        if ((_targetMonster?.id ?? 0) != _tower.target)
        {
            if (_tower.target == 0)
            {
                _targetMonster = null;
            }
            else
            {
                _targetMonster = _world.entities[_tower.target] as Monster;
            }
        }

        Aim();
    }

    void Aim()
    {
        // start particle system 
        if (_targetMonster == null)
        {
            return;
        }

        // aim at enemy
        Vector3 baseTargetPostition = new Vector3(_targetMonster.worldPosition.x, transform.position.y, _targetMonster.worldPosition.z);
        Vector3 gunBodyTargetPostition = _targetMonster.worldPosition;

        _baseTransform.transform.LookAt(baseTargetPostition);

        if (_gunBodyTransform)
        {
            _gunBodyTransform.transform.LookAt(gunBodyTargetPostition);
        }
    }

    public IEnumerator FireProjectile()
    {
        if (_targetMonster == null)
        {
            yield break;
        }

        Monster fireTarget = _targetMonster;

        if (muzzelFlash && !muzzelFlash.isPlaying)
        {
            muzzelFlash.Play();
        }

        GameObject projectileInstance = Instantiate(_projectilePrefab);
        Transform projectileTransform = projectileInstance.transform;

        projectileTransform.position = _barrelOpening.position;
        projectileTransform.rotation = _barrelOpening.rotation;

        Vector3 diff = fireTarget.worldPosition - projectileTransform.position;

        while(diff.sqrMagnitude > 0.2f)
        {
            projectileInstance.transform.position = Vector3.MoveTowards(projectileTransform.position, fireTarget.worldPosition,
                Time.deltaTime * _projectileSpeed);

            diff = fireTarget.worldPosition - projectileTransform.position;

            yield return null;
        }

        Destroy(projectileInstance);


        if (!_world.isMasterWorld)
        {
            yield break;
        }

        if (fireTarget != null)
        {
            fireTarget.hp -= (short)_tower.damage;
            fireTarget.SetDirty();
        }

    }

    public void OnInitialize(Tower tower, World world)
    {
        _tower = tower;
        _world = world;
    }

    public void OnFire()
    {
        StartCoroutine(FireProjectile());
    }

    public void OnDemolish()
    {

    }

    public void OnComplete()
    {
    }
}
