﻿using System;
using System.Collections;
using UnityEngine;

public class MortarController : MonoBehaviour, ITowerComponentController
{
    // Gameobjects need to control rotation and aiming
    [SerializeField]
    private Transform _baseTransform = null;

    [SerializeField]
    private Transform _gunBodyTransform = null;

    [SerializeField]
    private GameObject _projectilePrefab = null;

    [SerializeField]
    private float _projectileVelocity = 3;

    [SerializeField]
    private float _explosionRadius;

    [SerializeField]
    private float _cannonHeight;

    [SerializeField]
    private float _lookUpStepSizeDeg = 1.0f;

    [SerializeField]
    private float _gravity;

    [SerializeField]
    private float _simulationSpeed;

    [SerializeField]
    private float lookAheadDistance;

    [SerializeField]
    private AnimationCurve _damageCurve;

    // Particle system for the muzzel flash
    public ParticleSystem muzzelFlash;

    private Monster _targetMonster;


    private Tower _tower;

    private World _world;


    private float[] _angleLookup;

    private void Update()
    {
        if ((_targetMonster?.id ?? 0) != _tower.target)
        {
            if (_tower.target == 0)
            {
                _targetMonster = null;
            }
            else
            {
                _targetMonster = _world.entities[_tower.target] as Monster;
            }
        }

        Aim();
    }

    void Aim()
    {
        // start particle system 
        if (_targetMonster == null)
        {
            return;
        }

        // aim at enemy
        Vector3 baseTargetPostition = new Vector3(_targetMonster.worldPosition.x, transform.position.y, _targetMonster.worldPosition.z);
        // PathUtils.SamplePath(_targetMonster.path, _targetMonster.distanceOnPath + lookAheadDistance, out baseTargetPostition)

        Vector3 direction = _targetMonster.worldPosition - transform.position;
        direction.y = 0;

        float distance = direction.magnitude;
        float angle = LookUpAngle(distance);

        Vector3 projectileDirection = Vector3.RotateTowards(direction.normalized, Vector3.up, Mathf.Deg2Rad * angle, 0);
        Vector3 gunBodyTargetPostition = transform.position + Vector3.up + projectileDirection;


        _baseTransform.transform.LookAt(baseTargetPostition);

        if (_gunBodyTransform)
        {
            _gunBodyTransform.transform.LookAt(gunBodyTargetPostition);
        }
    }

    public IEnumerator FireProjectile()
    {
        if (_targetMonster == null)
        {
            yield break;
        }

        if (muzzelFlash && !muzzelFlash.isPlaying)
        {
            muzzelFlash.Play();
        }

        Vector3 targetPosition;

        if (!PathUtils.SamplePath(_targetMonster.path, _targetMonster.distanceOnPath + lookAheadDistance, out targetPosition))
        {
            yield break;
        }

        GameObject projectileInstance = Instantiate(_projectilePrefab);
        projectileInstance.transform.position = transform.position + Vector3.up * _cannonHeight;

        Vector3 direction = targetPosition - transform.position;
        direction.y = 0;

        float distance = direction.magnitude;
        float angle = LookUpAngle(distance);

        Debug.DrawLine(targetPosition, targetPosition + Vector3.up, Color.red, 3.0f);

        Vector3 projectileDirection = Vector3.RotateTowards(direction.normalized, Vector3.up, Mathf.Deg2Rad * angle, 0);

        projectileInstance.transform.forward = projectileDirection;

        MortarShellController shellController = projectileInstance.GetComponent<MortarShellController>();
        shellController.Initialize(_tower, _world, projectileDirection * _projectileVelocity, _explosionRadius, _damageCurve, _gravity, _simulationSpeed);
    }

    public void OnInitialize(Tower tower, World world)
    {
        _tower = tower;
        _world = world;

        BuildAngleLookupTable();
    }

    private void BuildAngleLookupTable()
    {
        float angleDeg = 45.0f;
        _angleLookup = new float[44];
        
        for(int i = 0; i < _angleLookup.Length; i++)
        {
            _angleLookup[i] = MathUtils.GetThrowDistance(_cannonHeight, _projectileVelocity, Mathf.Deg2Rad * angleDeg, _gravity);
            angleDeg += _lookUpStepSizeDeg;
        }
    }

    private float LookUpAngle(float distance)
    {
        float angleDeg = 45.0f;

        for (int i = 0; i < _angleLookup.Length; i++)
        {
            if(distance > _angleLookup[i])
            {
                return angleDeg;
            }

            angleDeg += _lookUpStepSizeDeg;
        }

        return 0.0f;
    }

    public void OnFire()
    {
        StartCoroutine(FireProjectile());
    }

    public void OnDemolish()
    {

    }

    public void OnComplete()
    {
    }
}
