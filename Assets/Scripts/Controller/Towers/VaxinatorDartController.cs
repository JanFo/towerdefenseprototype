﻿using System;
using System.Collections;
using UnityEngine;

public class VaxinatorDartController : MonoBehaviour
{
    [SerializeField]
    private float _sqrRadius;

    private Tower _tower;
    private World _world;
    private Vector3 _velocity;
    private Transform _transform;
    private float _maxRangeSqr;
    private Vector3 _startPosition;

    public void Initialize(Tower tower, World world, Vector3 velocity, float maxRange)
    {
        _tower = tower;
        _world = world;
        _velocity = velocity;
        _transform = transform;
        _maxRangeSqr = maxRange * maxRange;
        _startPosition = _transform.position;
    }

    private void Update()
    {
        _transform.position += Time.deltaTime * _velocity;

        if((_startPosition - _transform.position).sqrMagnitude > _maxRangeSqr)
        {
            SpawnDestructionEffect();
            Destroy(gameObject);
            enabled = false;
            return;
        }

        foreach (Monster monster in _world.monsters)
        {
            Vector3 diff = monster.worldPosition - _transform.position;

            if (diff.x * diff.x + diff.z * diff.z < _sqrRadius)
            {
                if (_world.isMasterWorld)
                {
                    monster.hp -= (short)_tower.damage;
                    monster.SetDirty();
                }

                SpawnDestructionEffect();
                Destroy(gameObject);
                enabled = false;
                return;
            }

        }
    }

    private void SpawnDestructionEffect()
    {

    }
}
