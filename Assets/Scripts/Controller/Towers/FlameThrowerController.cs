﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameThrowerController : MonoBehaviour, ITowerComponentController
{
    // Gameobjects need to control rotation and aiming
    [SerializeField]
    private Transform _baseTransform = null;

    [SerializeField]
    private Transform _barrelOpening = null;

    [SerializeField]
    private ParticleSystem _flames = null;

    [SerializeField]
    private float _angle;

    private Monster _targetMonster;

    private float _projectileSpeed = 50;
    
    private Tower _tower;
    
    private World _world;

    
    private void Update()
    {
        if((_targetMonster?.id ?? 0) != _tower.target)
        {
            if(_tower.target == 0)
            {
                _targetMonster = null;
            }
            else
            {
                _targetMonster = _world.entities[_tower.target] as Monster;
            }
        }

        if(_targetMonster == null && _flames.isPlaying)
        {
            _flames.Stop();
        }

        if(_targetMonster != null && !_flames.isPlaying)
        {
            _flames.Play();
        }

        Aim();
    }

    public void OnComplete()
    {

    }
    void Aim()
    {
        // start particle system 
        if (_targetMonster == null)
        {
            return;
        }

        // aim at enemy
        Vector3 baseTargetPostition = new Vector3(_targetMonster.worldPosition.x, transform.position.y, _targetMonster.worldPosition.z);
        _baseTransform.transform.LookAt(baseTargetPostition);
    }


    public IEnumerator FireProjectile()
    {
        if(_targetMonster == null)
        {
            yield break;
        }

        float distanceToTarget = Vector3.Distance(_barrelOpening.position, _targetMonster.worldPosition);

        yield return new WaitForSeconds(distanceToTarget / _projectileSpeed);

        if (!_world.isMasterWorld || _targetMonster == null)
        {
            yield break;
        }

        foreach(Monster monster in SelectMonsters(_targetMonster.worldPosition))
        {
            monster.hp -= (short)_tower.damage;
            monster.SetDirty();

        }

    }

    private IEnumerable<Monster> SelectMonsters(Vector3 worldPosition)
    {
        Vector3 targetDir = worldPosition - transform.position;

        foreach (Monster monster in _world.monsters)
        {
            Vector3 dir = monster.worldPosition - transform.position;

            if (dir.sqrMagnitude > _tower.range * _tower.range)
            {
                continue;
            }

            if (Vector3.Angle(targetDir, dir) > _angle)
            {
                continue;
            }

            yield return monster;
        }
    }

    public void OnInitialize(Tower tower, World world)
    {
        _tower = tower;
        _world = world;


    }

    public void OnFire()
    {
        StartCoroutine(FireProjectile());
    }

    public void OnDemolish()
    {

    }
}
