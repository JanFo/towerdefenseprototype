﻿namespace Assets.Scripts.Controller
{
    public class MonsterCollisionController
    {
        // in percent of GameConfiguration.tileSize
        public static float KillDistance = 0f;

        private World _world;

        public MonsterCollisionController(World wc)
        {
            _world = wc;
        }

        private bool Collides(Entity a, Entity b)
        {
            float centerDist = (a.worldPosition - b.worldPosition).magnitude; // TODO Change this to sqrMagnitude
            float skinDist = centerDist - a.radius - b.radius;
            return skinDist < GameConfiguration.tileSize * KillDistance;
        }

        public void Update()
        {
            foreach (Monster monster in _world.monsters)
            {
                foreach (Character character in _world.characters)
                {
                    if(character.playerIndex == -1 || character.isDead || monster.isDead || character.isZipping)
                    {
                        continue;
                    }

                    if (Collides(monster, character))
                    {
                        character.hp = 0;
                        character.isDead = true;
                        character.SetRealDirty();

                        _world.barracks.respawnData[character.playerIndex].cooldown = 5;
                        _world.barracks.SetRealDirty();
                    }
                }
            }
        }

    }
}
