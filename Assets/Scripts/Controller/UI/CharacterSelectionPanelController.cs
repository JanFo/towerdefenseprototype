﻿using System;
using TMPro;
using UnityEngine;
using UserInterface;

internal class CharacterSelectionPanelController
{
    private Player _player;

    private World _world;

    private RectTransform _root;

    private RectTransform _panel;
    private TMP_Text _cooldownText;
    private UIButton _selectSoldierButton;
    private UIButton _selectSpyButton;
    private UIButton _selectDemolisherButton;
    private UIButton _selectAnchormanButton;
    private Character _character;

    private RespawnData _respawnData;

    public CharacterSelectionPanelController(Player player, World world, RectTransform root)
    {
        _player = player;
        _world = world;
        _root = root; 
        
        Initialize();
    }

    private void Initialize()
    {
        _panel = UIQuery.Get<RectTransform>(_root, "character-selection-panel");
        _cooldownText = UIQuery.Get<TMP_Text>(_root, "cooldown-text");
        _selectSoldierButton = UIQuery.Get<UIButton>(_root, "soldier-button");
        _selectSpyButton = UIQuery.Get<UIButton>(_root, "spy-button");
        _selectDemolisherButton = UIQuery.Get<UIButton>(_root, "demolisher-button");
        _selectAnchormanButton = UIQuery.Get<UIButton>(_root, "anchorman-button");

        _selectSoldierButton.OnClick.AddListener((s, args) => OnSelectCharacterClass(s, args, EntityType.Soldier));
        _selectSpyButton.OnClick.AddListener((s, args) => OnSelectCharacterClass(s, args, EntityType.Spy));
        _selectDemolisherButton.OnClick.AddListener((s, args) => OnSelectCharacterClass(s, args, EntityType.Demolisher));
        _selectAnchormanButton.OnClick.AddListener((s, args) => OnSelectCharacterClass(s, args, EntityType.Anchorman));
    }

    

    private void OnSelectCharacterClass(UISelectable arg0, UIInputEventArgs arg1, EntityType type)
    {
        if (_respawnData != null)
        {
            _respawnData.characterType = type;
            _respawnData.isRequested = true;

            _world.barracks.SetRealDirty();
        }
    }

    public void Update()
    {
        _character = ArrayUtils.Find(_world.characters, c => c.playerIndex == _player.index);
        _panel.gameObject.SetActive(_character == null);

        if (_world.barracks != null)
        {
            _respawnData = _world.barracks.respawnData[_player.index];
        }

        if(_respawnData != null)
        {
            if(_respawnData.cooldown > 0)
            {
                _cooldownText.enabled = true;

                if(_respawnData.isRequested)
                {
                    _cooldownText.text = "Respawning as " + _respawnData.characterType + " in " + Mathf.CeilToInt(_respawnData.cooldown) + " seconds.";
                }
                else
                {
                    _cooldownText.text = "Dead for " + Mathf.CeilToInt(_respawnData.cooldown) + " seconds.";
                }
            }
            else
            {
                _cooldownText.enabled = false;
            }
        }

    }
}