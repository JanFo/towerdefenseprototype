using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UserInterface;

public struct UITowerIconData
{
    public string identifier;
    public Image iconImage;
    public Image selection;
}

public class ConstructionPanelController
{
    private World _world;

    private RectTransform _root;

    private Player _player;

    private Character _character;

    private RectTransform _panel;

    private UIButton _previousButton;

    private UIButton _nextButton;

    private TMP_Text _text;

    private RectTransform _parent;

    private int? _currentCharacterId;

    public List<UITowerIconData> _towerIconData;

    public ConstructionPanelController(Player player, World world, RectTransform root)
    {
        _world = world;
        _root = root;
        _player = player;
        _towerIconData = new List<UITowerIconData>();

        Initialize();
    }

    private void Initialize()
    {
        _panel = UIQuery.Get<RectTransform>(_root, "construction-panel");
        _previousButton = UIQuery.Get<UIButton>(_panel, "construction-panel-previous");
        _nextButton = UIQuery.Get<UIButton>(_panel, "construction-panel-next");
        _text = UIQuery.Get<TMP_Text>(_panel, "construction-panel-text");
        _parent = UIQuery.Get<RectTransform>(_panel, "construction-panel-parent");

        _previousButton.OnClick.AddListener(OnPreviousButtonClicked);
        _nextButton.OnClick.AddListener(OnNextButtonClicked);

        UpdateText();
    }

    private void UpdateText()
    {
      
        if (_character != null)
        {
            CharacterClassConfiguration classConfig = GameConfiguration.GetCharacterClassConfiguration(_character.type);
            int towerTypeCount = classConfig.towers.Length;

            _player.constructionLineIndex = ArrayUtils.RepeatIndex(_player.constructionLineIndex, towerTypeCount);
            TowerConfiguration towerConfig = classConfig.GetTowerConfiguration(_player.constructionLineIndex);
            _text.text = towerConfig.GetLocalizedName();
        }
        else
        {
            _text.text = "";
        }
    }

    private void OnNextButtonClicked(UISelectable arg0, UIInputEventArgs arg1)
    {
        CharacterClassConfiguration config = GameConfiguration.GetCharacterClassConfiguration(_character.type);
        int towerTypeCount = config.towers.Length;

        _player.constructionLineIndex = ArrayUtils.RepeatIndex(_player.constructionLineIndex + 1, towerTypeCount);

       

        UpdateSelection();
        UpdateText();
    }

    private void UpdateSelection()
    {
        foreach (UITowerIconData iconData in _towerIconData)
        {
            if (iconData.selection != null)
            {
                iconData.selection.enabled = false;
            }
        }

        UITowerIconData currentIconData = ArrayUtils.Get(_towerIconData, _player.constructionLineIndex, new UITowerIconData());

        if (currentIconData.selection != null)
        {
            currentIconData.selection.enabled = true;
        }
    }

    private void OnPreviousButtonClicked(UISelectable arg0, UIInputEventArgs arg1)
    {
        CharacterClassConfiguration config = GameConfiguration.GetCharacterClassConfiguration(_character.type);
        int towerTypeCount = config.towers.Length;

        _player.constructionLineIndex = ArrayUtils.RepeatIndex(_player.constructionLineIndex - 1, towerTypeCount);

        UpdateSelection();
        UpdateText();
    }

        public void Update()
    {
        _character = ArrayUtils.Find(_world.characters, c => c.playerIndex == _player.index && !c.isDead);

        if(_currentCharacterId != _character?.id)
        {
            UpdateConstructionPanel();
            UpdateSelection();
            UpdateText();
            _currentCharacterId = _character?.id;
        }
    }

    private void UpdateConstructionPanel()
    {
        _towerIconData.Clear();
        _parent.ClearChildren();

        if (_character == null)
        {
            return;
        }

        CharacterClassConfiguration config = GameConfiguration.GetCharacterClassConfiguration(_character.type);

        foreach(TowerConfiguration constructionLine in config.towers)
        {
            RectTransform iconInstance = Object.Instantiate(GameConfiguration.instance.towerIconPrefab);

            UITowerIconData iconData = new UITowerIconData()
            {
                iconImage = UIQuery.Get<Image>(iconInstance, "icon-image"),
                selection = UIQuery.Get<Image>(iconInstance, "icon-selection"),
                identifier = constructionLine.name
            };

            iconData.iconImage.sprite = constructionLine.icon;

            _towerIconData.Add(iconData);
            _parent.Append(iconInstance);
        }
    }   
}
