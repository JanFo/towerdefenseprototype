using System.Collections.Generic;
using UnityEngine;

public class HealthBarController
{
    private World _world;

    private Canvas _canvas;

    private Dictionary<int, HealthBar> _healthBars;

    private float _healthBarOffset;

    public HealthBarController(World world, Canvas canvas)
    {
        _healthBars = new Dictionary<int, HealthBar>();
        _world = world;
        _canvas = canvas;
        _healthBarOffset = 0.5f;
    }

    public void Update()
    {
        for (int i = _world.monsters.Count - 1; i >= 0; i--)
        {
            Monster monster = _world.monsters[i];

            if (monster.isDead)
            {
                DestroyHealthBar(monster);
                continue;
            }

            if (!_healthBars.ContainsKey(monster.id))
            {
                CreateHealthBar(monster);
            }

            UpdateHealthBar(monster, _healthBars[monster.id]);
        }

    }

    private void DestroyHealthBar(Monster monster)
    {
        if (_healthBars.ContainsKey(monster.id))
        {
            Object.Destroy(_healthBars[monster.id].gameObject);
            _healthBars.Remove(monster.id);
        }
    }

    private void UpdateHealthBar(Monster monster, HealthBar healthBar)
    {
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(monster.worldPosition + Vector3.up * _healthBarOffset);
        healthBar.rectTransform.anchoredPosition = new Vector2(screenPoint.x, screenPoint.y) / _canvas.scaleFactor;
        healthBar.SetHealth(monster.hp);
    }

    private void CreateHealthBar(Monster monster)
    {
        HealthBar healthBar = Object.Instantiate(GameConfiguration.healthBarPrefab);
        healthBar.SetMaxHealth(monster.hp);
        healthBar.rectTransform = healthBar.GetComponent<RectTransform>();
        healthBar.rectTransform.anchorMin = Vector2.zero;
        healthBar.rectTransform.anchorMax = Vector2.zero;
        healthBar.rectTransform.pivot = new Vector2(0.5f, 0.5f);

        _healthBars.Add(monster.id, healthBar);

        healthBar.transform.SetParent(_canvas.transform);
    }
}
