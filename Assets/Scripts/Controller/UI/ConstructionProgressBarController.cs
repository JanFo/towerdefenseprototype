using System.Collections.Generic;
using UnityEngine;

public class ConstructionProgressBarController
{
    private World _world;

    private Canvas _canvas;

    private Dictionary<int, UIProgressBar> _progressBars;

    private float _progressBarOffset;

    public ConstructionProgressBarController(World world, Canvas canvas)
    {
        _progressBars = new Dictionary<int, UIProgressBar>();
        _world = world;
        _canvas = canvas;
        _progressBarOffset = 0.1f;

        world.OnTowerRemoved += World_OnTowerRemoved;
    }

    private void World_OnTowerRemoved(Tower obj)
    {
        DestroyProgressBar(obj);
    }

    public void Update()
    {
        for (int i = _world.towers.Count - 1; i >= 0; i--)
        {
            Tower tower = _world.towers[i];

            if (tower.isBuilt)
            {
                DestroyProgressBar(tower);
                continue;
            }

            if(!tower.isBuilt && !tower.isFoundation && !_progressBars.ContainsKey(tower.id))
            {
                CreateProgressBar(tower);
            }

            if (_progressBars.ContainsKey(tower.id))
            {
                UpdateProgressBar(tower, _progressBars[tower.id]);
            }
        }


    }

    private void DestroyProgressBar(Tower tower)
    {
        if (_progressBars.ContainsKey(tower.id))
        {
            Object.Destroy(_progressBars[tower.id].gameObject);
            _progressBars.Remove(tower.id);
        }
    }

    private void UpdateProgressBar(Tower tower, UIProgressBar healthBar)
    {
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(tower.worldPosition + Vector3.up * _progressBarOffset);
        healthBar.GetComponent<RectTransform>().anchoredPosition = new Vector2(screenPoint.x, screenPoint.y) / _canvas.scaleFactor;
        healthBar.Progress = tower.constructionProgress;
    }

    private void CreateProgressBar(Tower tower)
    {
        UIProgressBar healthBar = Object.Instantiate(GameConfiguration.instance.buildProgressBarPrefab); 
        healthBar.Progress = tower.constructionProgress;

        RectTransform rectTransform = healthBar.GetComponent<RectTransform>();

        rectTransform = healthBar.GetComponent<RectTransform>();
        rectTransform.anchorMin = Vector2.zero;
        rectTransform.anchorMax = Vector2.zero;
        rectTransform.pivot = new Vector2(0.5f, 0.5f);

        _progressBars.Add(tower.id, healthBar);
        healthBar.transform.SetParent(_canvas.transform);
    }
}
