//using System;
//using System.Collections.Generic;
///// <summary>
///// Handles the monster count, should also handle the tower ids on the grid.
///// </summary>
//public class ProjectileController : IProjectileContext
//{
//    private World _world;

//    private Dictionary<int, int> _fireCounters;

//    private List<ProjectileView> _projectiles;

//    public ProjectileController(World world)
//    {
//        _projectiles = new List<ProjectileView>();
//        _fireCounters = new Dictionary<int, int>(new EntityIdEqualityComparer());
//        _world = world;
//    }

//    public void ApplyDamage(Monster monster, int amount)
//    {
//        if(!_world.isMasterWorld)
//        {
//            return;
//        }

//        monster.hp -= (short)amount;
//        monster.SetDirty();
//    }

//    public Entity GetEntity(int target)
//    {
//        return _world.entities[target];
//    }

//    public void Update()
//    {
//        for(int i = _projectiles.Count - 1; i >= 0; i--)
//        {
//            if(_projectiles[i].CanBeRemoved)
//            {
//                _projectiles.RemoveAt(i);
//                continue;
//            }

//            _projectiles[i].Update();
//        }

//        foreach (Tower tower in _world.towers)
//        {
//            UpdateTower(tower);
//        }
//    }

//    private void UpdateTower(Tower tower)
//    {
//        if(!_fireCounters.ContainsKey(tower.id))
//        {
//            _fireCounters[tower.id] = tower.fireCounter;
//        }

//        if(tower.fireCounter > _fireCounters[tower.id])
//        {
//            Monster monster = ArrayUtils.Get(_world.entities, tower.target, null) as Monster;

//            if (monster != null)
//            {
//                _projectiles.Add(ProjectileView.Create(tower, monster, this));
//                _fireCounters[tower.id] = tower.fireCounter;
//            }
//        }
//    }

//}