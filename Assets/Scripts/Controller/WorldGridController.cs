using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the monster count, should also handle the tower ids on the grid.
/// </summary>
public class WorldGridController
{
    private World _world;

    private WorldGrid _worldGrid;

    private Dictionary<int, Vector2Int> _monsterGridPositions;

    private Dictionary<int, Vector2Int> _towerGridPositions;

    public WorldGridController(World world)
    {
        _world = world;
        _worldGrid = world.grid;
        _towerGridPositions = new Dictionary<int, Vector2Int>(new EntityIdEqualityComparer());
        _monsterGridPositions = new Dictionary<int, Vector2Int>(new EntityIdEqualityComparer());
       
        if (world.barracks != null)
        {
            OnBarracksAdded(world.barracks);
        }

        if (world.castle != null)
        {
            OnCastleAdded(world.castle);
        }

        _world.OnTowerAdded += OnTowerAdded;
        _world.OnTowerRemoved += OnTowerRemoved;
    }

    private void OnBarracksAdded(Barracks barracks)
    {
        _towerGridPositions[barracks.id] = new Vector2Int(barracks.x, barracks.y);
        _worldGrid.SetBuilding(barracks);
    }

    private void OnTowerRemoved(Tower tower)
    {
        _towerGridPositions.Remove(tower.id);
        _worldGrid.RemoveBuilding(tower.x, tower.y);
    }

    private void OnTowerAdded(Tower tower)
    {
        _towerGridPositions[tower.id] = new Vector2Int(tower.x, tower.y);
        _worldGrid.SetBuilding(tower);
    }

    private void OnCastleAdded(Castle castle)
    {
        // _worldGrid.SetBuilding(castle);
    }

    public void Update()
    {
        foreach(Monster monster in _world.monsters)
        {
            UpdateMonster(monster);
        }
    }

    private void UpdateMonster(Monster monster)
    {
        int x = (int)(monster.worldPosition.x / GameConfiguration.tileSize);
        int y = (int)(monster.worldPosition.z / GameConfiguration.tileSize);

        // Monster is registered?
        if (_monsterGridPositions.ContainsKey(monster.id))
        {
            // Find the position!
            Vector2Int gridPosition = _monsterGridPositions[monster.id];

            // Monster might be dead - remove it from the grid and unregister it from this controller
            if (monster.isDead)
            {
                _worldGrid.GetNode(gridPosition.x, gridPosition.y).monsterCount--;
                _monsterGridPositions.Remove(monster.id);
                return;
            }


            // Not dead and grid position has changed?
            if (gridPosition.x != x || gridPosition.y != y)
            {
                _worldGrid.GetNode(gridPosition.x, gridPosition.y).monsterCount--;
                _worldGrid.GetNode(x, y).monsterCount++;
                _monsterGridPositions[monster.id] = new Vector2Int(x, y);
            }

            return;
        }

        // Dead and not registered? Fuck outta here!
        if (monster.isDead)
        {
            return;
        }

        // Not registered but alive? Welcome to the grid!
        _worldGrid.GetNode(x, y).monsterCount++;
        _monsterGridPositions[monster.id] = new Vector2Int(x, y);
    }
}