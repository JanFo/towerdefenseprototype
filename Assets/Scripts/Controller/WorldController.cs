using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the monster count, should also handle the tower ids on the grid.
/// </summary>
public class WorldController
{
    private World _world;

    private WorldGrid _worldGrid;

    private Dictionary<int, Vector2Int> _monsterGridPositions;

    private Dictionary<int, Vector3Int> _towerGridPositions;

    private Color _goldColor;

    public WorldController(World world)
    {
        _world = world;
        _worldGrid = world.grid;
        _towerGridPositions = new Dictionary<int, Vector3Int>(new EntityIdEqualityComparer());
        _monsterGridPositions = new Dictionary<int, Vector2Int>(new EntityIdEqualityComparer());
        _goldColor = new Color(0.8705883f, 0.764706f, 0.2705882f);

        if (world.barracks != null)
        {
            OnBarracksAdded(world.barracks);
        }

        if (world.castle != null)
        {
            OnCastleAdded(world.castle);
        }

        _world.OnTowerAdded += OnTowerAdded;
        _world.OnTowerRemoved += OnTowerRemoved;
    }

    private void OnBarracksAdded(Barracks barracks)
    {
        _towerGridPositions[barracks.id] = new Vector3Int(barracks.x, barracks.y, barracks.revision);
        _worldGrid.SetBuilding(barracks);
    }

    private void OnTowerRemoved(Tower tower)
    {
        _towerGridPositions.Remove(tower.id);
        _worldGrid.RemoveBuilding(tower.x, tower.y);
    }

    private void OnTowerAdded(Tower tower)
    {
        _towerGridPositions[tower.id] = new Vector3Int(tower.x, tower.y, tower.revision);
        _worldGrid.SetBuilding(tower);
    }

    private void OnCastleAdded(Castle castle)
    {
        // _worldGrid.SetBuilding(castle);

        //_worldGrid.UpdateGrid(castle.x + 2, castle.y + 2, (uint)NodeFlag.BlockedNode);
        //_worldGrid.UpdateGrid(castle.x + 1, castle.y + 2, (uint)NodeFlag.BlockedNode);
        //_worldGrid.UpdateGrid(castle.x + 2, castle.y + 1, (uint)NodeFlag.BlockedNode);

        //_worldGrid.UpdateGrid(castle.x + 2, castle.y - 2, (uint)NodeFlag.BlockedNode);
        //_worldGrid.UpdateGrid(castle.x + 1, castle.y - 2, (uint)NodeFlag.BlockedNode);
        //_worldGrid.UpdateGrid(castle.x + 2, castle.y - 1, (uint)NodeFlag.BlockedNode);

        //_worldGrid.UpdateGrid(castle.x - 2, castle.y + 2, (uint)NodeFlag.BlockedNode);
        //_worldGrid.UpdateGrid(castle.x - 1, castle.y + 2, (uint)NodeFlag.BlockedNode);
        //_worldGrid.UpdateGrid(castle.x - 2, castle.y + 1, (uint)NodeFlag.BlockedNode);

        //_worldGrid.UpdateGrid(castle.x - 2, castle.y - 2, (uint)NodeFlag.BlockedNode);
        //_worldGrid.UpdateGrid(castle.x - 1, castle.y - 2, (uint)NodeFlag.BlockedNode);
        //_worldGrid.UpdateGrid(castle.x - 2, castle.y - 1, (uint)NodeFlag.BlockedNode);
    }

    public void Update()
    {
        foreach (Monster monster in _world.monsters)
        {
            UpdateMonster(monster);
        }

        foreach(Tower tower in _world.towers)
        {
            UpdateTower(tower);
        }

        for(int i = _world.monsters.Count - 1; i >= 0; i--)
        {
            if(_world.monsters[i].isDead)
            {
                if (_world.monsters[i].hp <= 0)
                {
                    FloatingText.Create(_world.monsters[i].worldPosition, "+" + _world.monsters[i].bounty, _goldColor);
                }

                _world.RemoveEntity(_world.monsters[i]);
            }
        }
    }

    private void UpdateTower(Tower tower)
    {
        Vector3Int positionAndRevision = _towerGridPositions[tower.id];

        int revision = positionAndRevision.z;

        if(tower.IsDirty(ref revision))
        {
            //if(tower.x != positionAndRevision.x || tower.y != positionAndRevision.y)
            //{
            //    _worldGrid.RemoveBuilding(positionAndRevision.x, positionAndRevision.y);
            //    _worldGrid.SetBuilding(tower.x, tower.y, tower.id);

            //    positionAndRevision.x = tower.x;
            //    positionAndRevision.y = tower.y;
            //}

            positionAndRevision.z = revision;
            _towerGridPositions[tower.id] = positionAndRevision;
        }

        if(tower.hp <= 0)
        {
            _worldGrid.RemoveBuilding(positionAndRevision.x, positionAndRevision.y);
            _towerGridPositions.Remove(tower.id);
            _world.RemoveEntity(tower);
        }
    }

    private void UpdateMonster(Monster monster)
    {
        int x = (int)(monster.worldPosition.x / GameConfiguration.tileSize);
        int y = (int)(monster.worldPosition.z / GameConfiguration.tileSize);

        monster.speed = monster.initialSpeed * _worldGrid.GetNode(x, y).speedModifier;

        // Monster is registered?
        if (_monsterGridPositions.ContainsKey(monster.id))
        {
            // Find the position!
            Vector2Int gridPosition = _monsterGridPositions[monster.id];

            // Monster might be dead - remove it from the grid and unregister it from this controller
            if (monster.isDead)
            {
                _worldGrid.GetNode(gridPosition.x, gridPosition.y).monsterCount--;

                _monsterGridPositions.Remove(monster.id);
                return;
            }


            // Not dead and grid position has changed?
            if (gridPosition.x != x || gridPosition.y != y)
            {
                
                _worldGrid.GetNode(gridPosition.x, gridPosition.y).monsterCount--;
                _worldGrid.GetNode(x, y).monsterCount++;
                _monsterGridPositions[monster.id] = new Vector2Int(x, y);
            }

            return;
        }

        // Dead and not registered? Fuck outta here!
        if(monster.isDead)
        {
            return;
        }

        // Not registered but alive? Welcome to the grid!
        _worldGrid.GetNode(x, y).monsterCount++;

        _monsterGridPositions[monster.id] = new Vector2Int(x, y);
    }
}

//using System;
//using System.Collections.Generic;
//using UnityEngine;

///// <summary>
///// Handles the monster count, should also handle the tower ids on the grid.
///// </summary>
//public class WorldController
//{
//    private World _world;

//    private WorldGrid _worldGrid;

//    private Dictionary<int, int> _towerRevisions;

//    private Color _goldColor;

//    public WorldController(World world)
//    {
//        _world = world;
//        _worldGrid = world.grid;
//        _towerRevisions = new Dictionary<int, int>(new EntityIdEqualityComparer());
//        _goldColor = new Color(0.8705883f, 0.764706f, 0.2705882f);
//    }

//    public void Update()
//    {
//        foreach (Monster monster in _world.monsters)
//        {
//            UpdateMonster(monster);
//        }

//        foreach (Tower tower in _world.towers)
//        {
//            UpdateTower(tower);
//        }

//        for (int i = _world.monsters.Count - 1; i >= 0; i--)
//        {
//            if (_world.monsters[i].isDead)
//            {
//                if (_world.monsters[i].hp <= 0)
//                {
//                    FloatingText.Create(_world.monsters[i].worldPosition, "+" + _world.monsters[i].bounty, _goldColor);
//                }

//                _world.RemoveEntity(_world.monsters[i]);
//            }
//        }
//    }

//    private void UpdateTower(Tower tower)
//    {
//        int revision = _towerRevisions[tower.id];

//        if (tower.IsDirty(ref revision))
//        {
//            //if(tower.x != positionAndRevision.x || tower.y != positionAndRevision.y)
//            //{
//            //    _worldGrid.RemoveBuilding(positionAndRevision.x, positionAndRevision.y);
//            //    _worldGrid.SetBuilding(tower.x, tower.y, tower.id);

//            //    positionAndRevision.x = tower.x;
//            //    positionAndRevision.y = tower.y;
//            //}

//            _towerRevisions[tower.id] = revision;
//        }

//        if (tower.hp <= 0)
//        {
//            _towerRevisions.Remove(tower.id);
//            _world.RemoveEntity(tower);
//        }
//    }

//    private void UpdateMonster(Monster monster)
//    {
//        int x = (int)(monster.worldPosition.x / GameConfiguration.tileSize);
//        int y = (int)(monster.worldPosition.z / GameConfiguration.tileSize);
//        monster.speed = monster.initialSpeed * _worldGrid.GetNode(x, y).speedModifier;
//    }
//}