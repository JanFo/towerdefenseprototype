using UnityEngine;

public class Spy : Character
{
    public Spy() { }

    public bool isInvisible;

    public int connectingTowerId;

    public Spy(EntityData generic) : base(generic) { }

    public override EntityType type { get { return EntityType.Spy; } protected set { /* do nothing */ } }

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);
        isInvisible = reader.ReadBool();
        connectingTowerId = reader.ReadInt();
       
    }

    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);
        writer.WriteBool(isInvisible);
        writer.WriteInt(connectingTowerId);
    }
}
