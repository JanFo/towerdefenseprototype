using UnityEngine;

public class Demolisher : Character
{
    public Demolisher() { }

    public Demolisher(EntityData data) : base(data) { }

    public override EntityType type { get { return EntityType.Demolisher; } protected set { /* do nothing */ } }

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);
       
    }

    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);
    }
}
