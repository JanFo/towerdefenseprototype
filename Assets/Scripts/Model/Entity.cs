using System;
using System.Collections.Generic;
using UnityEngine;

public class EntityIdEqualityComparer : IEqualityComparer<int>
{
    public bool Equals(int x, int y)
    {
        return x == y;
    }

    public int GetHashCode(int obj)
    {
        return obj;
    }
}


public class EntityEqualityComparer : IEqualityComparer<Entity>
{
    public bool Equals(Entity x, Entity y)
    {
        return x.id == y.id;
    }

    public int GetHashCode(Entity obj)
    {
        return obj.id;
    }
}

public abstract class Entity
{
    private static int _entityIdCounter = 1000;

    public bool requestReliableSync { get; set; }

    public int id { get; protected set; }

    public abstract EntityType type { get; protected set; }

    public int revision { get; set; }

    public byte level { get; set; }

   

    public virtual Vector3 worldPosition { get; set; }

    // girth is fix for now and not propagated in networking
    public static float ENTITY_DEFAULT_RADIUS = 0.3f;

    public float radius = ENTITY_DEFAULT_RADIUS;

    public Entity()
    {
        id = _entityIdCounter++;
    }

    public Entity(EntityData data)
    {
        id = data.id;
        revision = data.revision;
        level = data.level;
    }

    public void SetRealDirty()
    {
        revision++;
        requestReliableSync = true;
    }

    public void SetDirty()
    {
        revision++;
    }
    public abstract void Serialize(IMessageWriter writer);

    public abstract void Deserialize(IMessageReader reader);


    public bool IsDirty(ref int revision)
    {
        if (revision != this.revision)
        {
            revision = this.revision;
            return true;
        }

        return false;
    }

    public EntityData GetEntityData()
    {
        var bas = new EntityData();
        bas.id = id;
        bas.revision = revision;
        bas.level = level;
        bas.type = type;
        return bas;
    }

    public struct EntityData
    {
        public int id;
        public int revision;
        public EntityType type;
        public byte level;

        public void Deserialize(IMessageReader reader)
        {
            id = reader.ReadInt();
            revision = reader.ReadInt();
            type = (EntityType)reader.ReadByte();
            level = reader.ReadByte();
        }

        public void Serialize(IMessageWriter writer)
        {
            writer.WriteInt(id);
            writer.WriteInt(revision);
            writer.WriteByte((byte)type);
            writer.WriteByte(level);
        }
    }
}
