using UnityEngine;

public class Anchorman : Character
{
    public Anchorman() { }

    public Anchorman(EntityData data) : base(data) { }

    public override EntityType type { get { return EntityType.Anchorman; } protected set { /* do nothing */ } }

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);
       
    }

    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);
    }
}
