﻿public enum MonsterTag
{
    Small = 0x01,
    Big = 0x02,
    Fast = 0x04,
    Slow = 0x08,
    Boss = 0x10,
    Immune = 0x20,
    Wildlife = 0x40,
    Horde = 0x80,
    Rich = 0x100
}