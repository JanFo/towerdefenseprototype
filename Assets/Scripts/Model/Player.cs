using System;
using System.Collections.Generic;


/// <summary>
/// Local model to communicate inputs and selections between controllers
/// </summary>
public class Player
{
    public int index;

    public int constructionLineIndex;

    public EntityType targetCharacterType;
}
