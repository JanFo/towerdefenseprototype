﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Model
{
    public enum WorldDirection
    {
        North,
        East,
        South,
        West
    }

    public sealed class AirStrikeBeacon : Tower
    {
        public AirStrikeBeacon() { }

        internal AirStrikeBeacon(EntityData generic) : base(generic) { }

        public override EntityType type { get { return EntityType.AirStrikeBeacon; } protected set { /* do nothing */ } }
    }
}
