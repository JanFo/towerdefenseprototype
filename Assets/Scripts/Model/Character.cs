using UnityEngine;
public abstract class Character : Creature
{
    public Character() 
    {
        playerIndex = -1;
        cooldowns = new float[2];
    }

    public Character(EntityData generic) : base(generic)
    {
        playerIndex = -1;
        cooldowns = new float[2];
    }

    public int playerIndex;

    public CharacterActionType currentActionType;

    public Vector2Int actionTarget;

    public Vector3 actionData;

    public bool isOwn;

    public bool isDeployed;

    public float[] cooldowns;

    public bool isZipping;

    public CharacterAction currentAction { get; set; }

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);
        playerIndex = reader.ReadInt();
        currentActionType = (CharacterActionType)reader.ReadByte();
        actionTarget.x = reader.ReadInt();
        actionTarget.y = reader.ReadInt();
        actionData.x = reader.ReadFloat();
        actionData.y = reader.ReadFloat();
        actionData.z = reader.ReadFloat();
        isDeployed = reader.ReadBool();
        isZipping = reader.ReadBool();
        cooldowns = reader.ReadFloatArray();
    }

    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);
        writer.WriteInt(playerIndex);
        writer.WriteByte((byte)currentActionType);
        writer.WriteInt(actionTarget.x);
        writer.WriteInt(actionTarget.y);
        writer.WriteFloat(actionData.x);
        writer.WriteFloat(actionData.y);
        writer.WriteFloat(actionData.z);
        writer.WriteBool(isDeployed);
        writer.WriteBool(isZipping);
        writer.WriteFloatArray(cooldowns);
    }
}
