
public class Barracks : Building
{
    // public string name;

    public RespawnData[] respawnData;

    public Barracks()
    {
        isBlocking = false;
        respawnData = new RespawnData[GameConfiguration.maxPlayers];
        for (int i = 0; i < respawnData.Length; i++)
        {
            respawnData[i] = new RespawnData();
        }
    }

    public Barracks(EntityData generic) : base(generic)
    {
        isBlocking = false;
        respawnData = new RespawnData[GameConfiguration.maxPlayers];
        for (int i = 0; i < respawnData.Length; i++)
        {
            respawnData[i] = new RespawnData();
        }
    }

   
    public void SetSpawnLocation(SpawnLocation playerSpawn)
    {
        x = playerSpawn.position.x;
        y = playerSpawn.position.y;
        // name = playerSpawn.name;

        InitializeRespawnData();
    }

    public void InitializeRespawnData()
    {
        for (int i = 0; i < respawnData.Length; i++)
        {
            respawnData[i].position = worldPosition;
            respawnData[i].cooldown = 0;
            respawnData[i].isRequested = false;
        }
    }

    public override EntityType type { get => EntityType.Barracks; protected set => throw new System.NotImplementedException(); }

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);

        // name = reader.ReadString();

        for (int i = 0; i < 4; i++)
        {
            respawnData[i].cooldown = reader.ReadFloat();
            respawnData[i].isRequested = reader.ReadBool();
            respawnData[i].position = reader.ReadVector3();
            respawnData[i].characterType = (EntityType)reader.ReadByte();
        }
    }

    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);

        // writer.WriteString(name);

        for (int i = 0; i < 4; i++)
        {
            writer.WriteFloat(respawnData[i].cooldown);
            writer.WriteBool(respawnData[i].isRequested);
            writer.WriteVector3(respawnData[i].position);
            writer.WriteByte((byte)respawnData[i].characterType);
        }
    }
}
