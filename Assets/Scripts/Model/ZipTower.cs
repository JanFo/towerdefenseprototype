﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Model
{
    public sealed class ZipTower : Tower
    {
        public ZipTower() { }

        internal ZipTower(EntityData generic) : base(generic) { }

        public override EntityType type { get { return EntityType.ZipTower; } protected set { /* do nothing */ } }

        public List<int> destinations = new List<int>();

        public int zipLineCostPerTile = 1;

        public float zipLineHeight;

        public Vector2Int landingZone;

        public void DrawZipLine(ZipTower other)
        {
            if (id != other.id)
            {
                destinations.Add(other.id);
                other.destinations.Add(id);
                SetDirty();
                other.SetDirty();
            }
        }

        public override void Serialize(IMessageWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(destinations.Count());
            foreach(int i in destinations)
            {
                writer.WriteInt(i);
            }
        }

        public override void Deserialize(IMessageReader reader)
        {
            base.Deserialize(reader);
            var dest = new List<int>(reader.ReadInt());
            for(int i = 0; i < dest.Capacity; i++)
            {
                dest.Add(reader.ReadInt());
            }
            destinations = dest;
        }
    }
}
