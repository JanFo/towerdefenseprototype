using UnityEngine;

public class Spawn : Building, IPathSeeker
{
    public string name;

    public Vector3[] path;

    public float[] distanceArray;

    public Spawn(EntityData generic) : base(generic)
    {
    }

    public Spawn()
    {

    }

    public override EntityType type { get => EntityType.Spawn; protected set => throw new System.NotImplementedException(); }

    Vector3 IPathSeeker.worldPosition => base.worldPosition;

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);
    }

    public void AssignPath(Vector3[] path)
    {
        this.path = path;
        distanceArray = PathUtils.CreateDistanceArray(path);
    }

    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);
    }
}
