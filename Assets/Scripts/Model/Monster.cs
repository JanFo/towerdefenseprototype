using System;
using UnityEngine;


[Serializable]
public class Monster : Creature, IPathSeeker
{
    public bool isRioting;

    public short bounty;

    public int viewIndex;

    [HideInInspector]
    public float distanceOnPath;

    public Monster() { }

    public Monster(EntityData generic) : base(generic) { }

    public override EntityType type { get { return EntityType.Monster; } protected set { /* do nothing */ } }

    Vector3 IPathSeeker.worldPosition => worldPosition;

    [HideInInspector]
    public Vector3[] path;


    [HideInInspector]
    public float[] distanceArray;

    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);
        writer.WriteShort(bounty);
        writer.WriteInt(viewIndex);
        writer.WriteFloat(distanceOnPath);
    }

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);
        bounty = reader.ReadShort();
        viewIndex = reader.ReadInt();
        distanceOnPath = reader.ReadFloat();
    }

    public void AssignPath(Vector3[] path)
    {
        if (path != null && path.Length != 0)
        {
            this.path = path;
            distanceArray = PathUtils.CreateDistanceArray(path);
            distanceOnPath = 0;
        }
    }
}
