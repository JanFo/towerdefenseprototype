using System;
using UnityEngine;

[Serializable]
public abstract class Building : Entity
{
    public Building() { }

    public Building(EntityData generic) : base(generic) { }

    public int x { get => position.x; set => position.x = value; }

    public int y { get => position.y; set => position.y = value; }

    public Vector2Int position;

    public float constructionProgress;

    public bool isBuilt => constructionProgress >= 1.0f;

    public bool isFoundation => constructionProgress < 0.02f;

    public bool isBlocking;

    public int hp;

    public override Vector3 worldPosition
    {
        get
        {
            return GridUtils.GetWorldPosition(position);
        }
        set
        {
            base.worldPosition = value;
            position = GridUtils.GetGridPosition(value);
        }
    }



    public override void Serialize(IMessageWriter writer)
    {
        writer.WriteInt(x);
        writer.WriteInt(y);
        writer.WriteInt(hp);
        writer.WriteBool(isBlocking);
        writer.WriteFloat(constructionProgress);
    }

    public override void Deserialize(IMessageReader reader)
    {
        x = reader.ReadInt();
        y = reader.ReadInt();
        hp = reader.ReadInt();
        isBlocking = reader.ReadBool();
        constructionProgress = reader.ReadFloat();
    }
}
