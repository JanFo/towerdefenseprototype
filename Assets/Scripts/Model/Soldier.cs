public class Soldier : Character
{
    public Soldier() { }

    public Soldier(EntityData data) : base(data) { }

    public override EntityType type { get { return EntityType.Soldier; } protected set { /* do nothing */ } }

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);
    }

    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);
    }
}
