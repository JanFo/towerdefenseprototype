using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;
using System;

public class RespawnData
{
    public float cooldown;
    public bool isRequested;
    public Vector3 position;
    public EntityType characterType;
}

public enum GameState
{
    Running,
    Lost, 
    Won
}

public class Castle : Building
{

    public int gold;

    public short waveIndex;

    public GameState gameState;


    public override EntityType type { get { return EntityType.Castle; } protected set { /* do nothing */ } }

    public Castle()
    {
        gameState = GameState.Running;
        isBlocking = false;
    }
    
    public Castle(Level level)
    {
        hp = level.startingHealth;
        gold = level.startingGold;
        x = level.castlePosition.x;
        y = level.castlePosition.y;
        gameState = GameState.Running;
        isBlocking = false;
    }

    public Castle(EntityData generic) : base(generic)
    {

    }

    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);
        writer.WriteInt(gold);
        writer.WriteShort(waveIndex);
        writer.WriteByte((byte)gameState);
    }

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);
        gold = reader.ReadInt();
        waveIndex = reader.ReadShort();
        gameState = (GameState)reader.ReadByte();
    }
}
