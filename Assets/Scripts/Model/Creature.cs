﻿using UnityEngine;

public abstract class Creature : Entity
{
    public static byte DefaultSize => 128;

    public short initialHp;

    public short hp;

    public float initialSpeed;

    public float speed;

    public bool isDead;

    public float rotation;

    public byte size = DefaultSize;

    public Creature() { }

    public Creature(EntityData generic) : base(generic) { }

    public override void Serialize(IMessageWriter writer)
    {
        writer.WriteShort(hp);
        writer.WriteFloat(speed);
        writer.WriteBool(isDead);
        writer.WriteVector3(worldPosition);
        writer.WriteFloat(rotation);
        writer.WriteByte(size);
    }

    public override void Deserialize(IMessageReader reader)
    {
        hp = reader.ReadShort();
        speed = reader.ReadFloat();
        isDead = reader.ReadBool();
        worldPosition = reader.ReadVector3();
        rotation = reader.ReadFloat();
        size = reader.ReadByte();
    }
}
