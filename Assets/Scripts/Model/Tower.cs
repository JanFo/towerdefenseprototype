using Assets.Scripts.Model;
using System;

[Serializable]
public class Tower : Building
{
    public Tower() { }

    internal Tower(EntityData generic) : base(generic) { }

    public override EntityType type { get { return EntityType.Tower; } protected set { /* do nothing */ } }

    public int range;

    public float speed;

    public int damage;

    public int target;

    public int fireCounter;

    public float lastFireTime;

    public int towerId;

    public WorldDirection direction;

    public bool canBeRotated;

    public void Rotate()
    {
        byte directionByte = (byte)direction;
        directionByte = (byte)((directionByte + 1) % 4);
        direction = (WorldDirection)directionByte;
    }


    public override void Serialize(IMessageWriter writer)
    {
        base.Serialize(writer);
        writer.WriteInt(towerId);
        writer.WriteInt(damage);
        writer.WriteInt(range);
        writer.WriteFloat(speed);
        writer.WriteInt(target);
        writer.WriteInt(fireCounter);
        writer.WriteFloat(lastFireTime);
        writer.WriteByte((byte)direction);
        writer.WriteBool(canBeRotated);
    }

    public override void Deserialize(IMessageReader reader)
    {
        base.Deserialize(reader);
        towerId = reader.ReadInt();
        damage = reader.ReadInt();
        range = reader.ReadInt();
        speed = reader.ReadFloat();
        target = reader.ReadInt();
        fireCounter = reader.ReadInt();
        lastFireTime = reader.ReadFloat();
        direction = (WorldDirection)reader.ReadByte();
        canBeRotated = reader.ReadBool();
    }


   
}
