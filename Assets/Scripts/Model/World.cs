using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

// Game data that needs to be kept in sync across clients
// Each player holds an instance of this class
public class World
{
    public event Action<Castle> OnCastleAdded;

    public event Action<Barracks> OnBarracksAdded;

    public event Action<Tower> OnTowerAdded;

    public event Action<Tower> OnTowerRemoved;

    public event Action<Character> OnCharacterRemoved;

    public int size;

    public int center { get; }

    public Castle castle;

    public Barracks barracks;

    public ReadOnlyCollection<Tower> towers;

    public ReadOnlyCollection<Monster> monsters;

    public ReadOnlyCollection<Character> characters;


    public ReadOnlyCollection<Spawn> spawns;

    public WorldGrid grid { get; }

    public Dictionary<int, Entity> entities = new Dictionary<int, Entity>();

    private List<Tower> _towers;

    private List<Monster> _monsters;

    private List<Spawn> _spawns;

    private List<Character> _characters;

    internal Level level => _level;

    private Level _level;


    public bool isMasterWorld { get; }

    public World(Level level, bool isMasterWorld)
    {
        _level = level;
        // this.size = level.size;
        this.isMasterWorld = isMasterWorld;

        entities = new Dictionary<int, Entity>();

        // TODO: Make these lists read only
        _towers = new List<Tower>();
        _monsters = new List<Monster>();
        _characters = new List<Character>();
        _spawns = new List<Spawn>();

        towers = new ReadOnlyCollection<Tower>(_towers);
        monsters = new ReadOnlyCollection<Monster>(_monsters);
        characters = new ReadOnlyCollection<Character>(_characters);
        spawns = new ReadOnlyCollection<Spawn>(_spawns);

        if (level != null)
        {
            size = level.size;
            center = size / 2;
            grid = new WorldGrid(level);
        }

    }

    public void RemoveEntity(Entity entity)
    {
        entities.Remove(entity.id);
        
        if(entity is Monster)
        {
            _monsters.Remove(entity as Monster);
        }

        if(entity is Tower)
        {
            _towers.Remove(entity as Tower);
            OnTowerRemoved?.Invoke(entity as Tower);
        }

        if(entity is Character)
        {
            _characters.Remove(entity as Character);
            OnCharacterRemoved?.Invoke(entity as Character);
        }
    }

    public void AddEntity(Entity entity)
    {
        entities[entity.id] = entity;

        if(entity is Tower)
        {
            Tower tower = entity as Tower;

            _towers.Add(tower);
            OnTowerAdded?.Invoke(tower);
        }

        if(entity is Character)
        {
            _characters.Add(entity as Character);
        }

        if(entity is Monster)
        {
            _monsters.Add(entity as Monster);
        }

        if(entity is Barracks)
        {
            barracks = entity as Barracks;
            OnBarracksAdded?.Invoke(barracks);
        }

        if(entity is Castle)
        {
            Castle castle = entity as Castle;

            SetCastle(castle);
            OnCastleAdded?.Invoke(castle);
        }

        if(entity is Spawn)
        {
            Spawn spawn = entity as Spawn;

            _spawns.Add(spawn);
        }
    }

    public Entity GetEntity(int entityId)
    {
        return ArrayUtils.Get(entities, entityId, null);
    }

    private void SetCastle(Castle castle)
    {
        this.castle = castle;
        castle.waveIndex = -1;

        //for (int i = center - 1; i < center + 2; i++)
        //{
        //    for (int j = center - 1; j < center + 2; j++)
        //    {
        //        grid.UpdateGrid(i, j, NodeFlag.CastleNode);
        //    }
        //}
    }  
}
