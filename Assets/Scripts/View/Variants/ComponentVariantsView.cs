﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Entities.Views
{
    /// <summary>
    /// Listens to audio playback requests from an entity. Since this is a fairly common request
    /// this event can be invoked on any entity
    /// </summary>
    [ExecuteInEditMode]
    public class ComponentVariantsView : MonoBehaviour
    {

        private void OnEnable()
        {
            Shuffle();
        }

        public void Shuffle()
        {
            ComponentVariantsViewElement[] elements = GetComponentsInChildren<ComponentVariantsViewElement>(true);
            List<string> keys = new List<string>();

            foreach(var element in elements)
            {
                ShuffleElement(element, keys);
            }
        }

        private void ShuffleElement(ComponentVariantsViewElement element, List<string> keys)
        {
            if(element.meshRenderer == null)
            {
                return;
            }

            string[] conditions = element.conditions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            foreach(var condition in conditions)
            {
                if(!keys.Contains(condition))
                {
                    element.meshRenderer.enabled = false;
                    return;
                }
            }

            float random = UnityEngine.Random.value;
            float threshold = 0.0f;

            foreach(var variant in element.variants)
            {
                threshold += variant.probability;

                if(random < threshold)
                {
                    if (variant.material != null)
                    {
                        element.meshRenderer.sharedMaterial = variant.material;
                    }

                    element.meshRenderer.enabled = true;
                    string[] variantKeys = variant.keys.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    keys.AddRange(variantKeys);
                    return;
                }
            }

            element.meshRenderer.enabled = false;
        }
    }
}
