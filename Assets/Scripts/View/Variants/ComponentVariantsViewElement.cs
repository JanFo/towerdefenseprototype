﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Entities.Views
{
    public class ComponentVariantsViewElement : MonoBehaviour
    {
        public string conditions;
        public SkinnedMeshRenderer meshRenderer;
        public RendererVariant[] variants;
    }

    [Serializable]
    public struct RendererVariant
    {
        public float probability;
        public string keys;
        public Material material;
    }

}
