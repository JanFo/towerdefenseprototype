﻿using Assets.Scripts.Model;
using System;
using System.Collections.Generic;
using UnityEngine;

class BillboardTowerController : MonoBehaviour, ITowerComponentController, IGridNodeModifier
{
    private Tower _tower;

    private World _world;

    private Vector2Int[] _positions;

    [SerializeField]
    private float _speedModifier;

    [SerializeField]
    private int _range;

    public float speedModifier => _speedModifier;

    public Vector2Int[] positions => _positions;

    public void OnFire()
    {
        // Does not fire
    }

    public void OnComplete()
    {
        if (_world.isMasterWorld)
        {
            _world.grid.Register(this);
        }
    }

    public void OnInitialize(Tower tower, World world)
    {
        _tower = tower;
        _world = world;

        _positions = new Vector2Int[8];

        _positions[0] = new Vector2Int(tower.x, tower.y) + new Vector2Int(-1, -1);
        _positions[1] = new Vector2Int(tower.x, tower.y) + new Vector2Int(0, -1);
        _positions[2] = new Vector2Int(tower.x, tower.y) + new Vector2Int(1, -1);
        _positions[3] = new Vector2Int(tower.x, tower.y) + new Vector2Int(-1, 0);
        _positions[4] = new Vector2Int(tower.x, tower.y) + new Vector2Int(-1, 1);
        _positions[5] = new Vector2Int(tower.x, tower.y) + new Vector2Int(1, 1);
        _positions[6] = new Vector2Int(tower.x, tower.y) + new Vector2Int(1, 0);
        _positions[7] = new Vector2Int(tower.x, tower.y) + new Vector2Int(0, 1);
    }

    public void OnDemolish()
    {
        if (_world.isMasterWorld)
        {
            _world.grid.Unregister(this);
        }
    }

    public void OnApply(GridNode node)
    {
        node.speedModifier = Mathf.Min(node.speedModifier, _speedModifier);
    }
}
