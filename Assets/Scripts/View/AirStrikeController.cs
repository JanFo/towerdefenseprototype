﻿using System.Collections;
using UnityEngine;

public class AirStrikeController : MonoBehaviour
{
    private World _world;

    public GameObject explosionPrefab;

    [SerializeField]
    private int _explosionCount;

    [SerializeField]
    private float _strikeDelay;

    [SerializeField]
    private float _timeBetweenStrikes;

    [SerializeField]
    private float _strikeOffset;

    private float _explosionRadius = 1f;

    private int _explosionDamage = 100;

    [SerializeField]
    private AnimationCurve _damageCurve;

    public IEnumerator DoAirStrike()
    {
        yield return new WaitForSeconds(_strikeDelay);

        float area = _strikeOffset * (_explosionCount - 1);
        float strikeStart = -0.5f * area;

        for (int i = 0; i < _explosionCount; i++)
        {
            GameObject explosionInstance = Instantiate(explosionPrefab);
            explosionInstance.transform.SetParent(transform);
            explosionInstance.transform.localPosition = new Vector3(0, 0, strikeStart + i * _strikeOffset);

            if (_world.isMasterWorld)
            {
                Vector3 worldPosition = explosionInstance.transform.position;

                foreach (Monster monster in _world.monsters)
                {
                    float distanceToMonster = Vector3.Distance(monster.worldPosition, worldPosition);

                    if (distanceToMonster < _explosionRadius)
                    {
                        float scale = _damageCurve.Evaluate(1.0f - distanceToMonster / _explosionRadius);

                        monster.hp -= (short)(monster.initialHp * scale);
                        monster.SetDirty();
                    }
                }
            }

            Destroy(explosionInstance, 3.0f);
            yield return new WaitForSeconds(_timeBetweenStrikes);
        }

        Destroy(gameObject, 3.0f);
    }

    public void Strike()
    {
        StartCoroutine(DoAirStrike());
    }

    public void Initialize(World world)
    {
        _world = world;
    }

}
