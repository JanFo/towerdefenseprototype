using Animancer;
using UnityEngine;

/// <summary>
/// Updates all visual stuff for a tower
/// </summary>
public class CharacterView : MonoBehaviour
{
    [SerializeField]
    private Character _character;

    private Animator _characterAnimator;

    [SerializeField]
    private Transform _cursorTransform;

    [SerializeField]
    private AnimationClip _castAnimation;

    [SerializeField]
    private AnimationClip _useZipLineAnimation;

    [SerializeField]
    private AnimationClip _deathAnimation;

    [SerializeField]
    private AnimancerMovementStateConfiguration _config;

    private Transform _transform;
   
    private AnimancerComponent _animancerComponent;

    private AnimancerMovementState _movementState;

    private float _smoothedForward;

    private float _smoothedAngle;

    private Vector3 _lastSavedPosition;

    [SerializeField]
    private float _lerpSpeed = 10.0f;

    [SerializeField]
    private float _cursorHeight = 0.05f;

    private MeshRenderer _cursorRenderer;

    private ICharacterComponentController[] _viewExtensions;

    public void Initialize(Character character, World world)
    {
        if(character == null)
        {
            return;
        }

        _viewExtensions = GetComponents<ICharacterComponentController>();

        foreach (ICharacterComponentController extension in _viewExtensions)
        {
            extension.Initialize(character, world);
        }

        _transform = transform;
        _character = character;
        _lastSavedPosition = _character.worldPosition;

        GameObject cursor = new GameObject("cursor");
        MeshFilter meshFilter = cursor.AddComponent<MeshFilter>();
        meshFilter.mesh = GameConfiguration.instance.cursorMesh;
        _cursorRenderer = cursor.AddComponent<MeshRenderer>();
        _cursorRenderer.material = GameConfiguration.instance.cursorMaterial;
        cursor.transform.localScale = new Vector3(GameConfiguration.tileSize, GameConfiguration.tileSize, GameConfiguration.tileSize);

        _characterAnimator = GetComponentInChildren<Animator>();

        _cursorTransform = cursor.transform;
        _cursorTransform.eulerAngles = new Vector3(90.0f, 0, 0);

        _animancerComponent = GetComponent<AnimancerComponent>();
        _movementState = AnimancerMovementState.Create(_config);

        _animancerComponent.Play(_movementState);
    }

    private void OnDestroy()
    {
        if (_cursorTransform != null)
        {
            Destroy(_cursorTransform.gameObject);
        }
    }

    private void Start()
    {
        gameObject.SetActive(_character != null);
    }

    private void Update()
    {
        _cursorRenderer.enabled = _character.playerIndex != -1;
        _characterAnimator.gameObject.SetActive(_character.playerIndex != -1);

        if (_character.playerIndex == -1)
        {
            return;
        }

        if (_character.hp <= 0)
        {
            _animancerComponent.Play(_deathAnimation);
            return;
        }

        _smoothedAngle = Mathf.MoveTowardsAngle(_smoothedAngle, _character.rotation, Time.deltaTime * 540);
        _transform.eulerAngles = new Vector3(0.0f, _smoothedAngle, 0.0f);

        if (_character.isOwn || (_transform.position - _character.worldPosition).sqrMagnitude > 4)
        {
            _transform.position = _character.worldPosition;
        }
        else
        {
            _transform.position = Vector3.MoveTowards(_transform.position, _character.worldPosition, Time.deltaTime * _lerpSpeed);
        }

        _cursorTransform.position = TowerView.GetTowerPosition(_character.actionTarget.x, _character.actionTarget.y) + Vector3.up * _cursorHeight;
    }

    private void FixedUpdate()
    {
        Vector3 delta = _transform.InverseTransformDirection(_transform.position - _lastSavedPosition);

        _smoothedForward = Mathf.Clamp01(Mathf.MoveTowards(_smoothedForward, delta.z / Time.fixedDeltaTime, Time.fixedDeltaTime * 4.0f));
        _movementState.ParameterX = _smoothedForward;
        _lastSavedPosition = _transform.position;
    }
    internal void PlayPushAnimation()
    {
        AnimancerState state = _animancerComponent.Play(_castAnimation, 0.3f);
        state.Speed = 2.0f;
    }

    internal void PlayBuildAnimation()
    {
        AnimancerState state = _animancerComponent.Play(_castAnimation, 0.3f);
        state.Speed = 1.0f;
    }

    public void PlayUseZipLineAnimation()
    {
        AnimancerState state = _animancerComponent.Play(_useZipLineAnimation, 0.3f);
        state.Speed = 1.0f;
    }
    internal void PlayZipBuildAnimation()
    {
        AnimancerState state = _animancerComponent.Play(_castAnimation, 0.3f);
        state.Speed = 1f;
    }

    internal void PlayIdleAnimation()
    {
        _animancerComponent.Play(_movementState, 0.3f);
    }
}
