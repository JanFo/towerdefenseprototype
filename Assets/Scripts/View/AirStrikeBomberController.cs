﻿using System.Collections;
using UnityEngine;

public class AirStrikeBomberController : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve _heightCurve;

    [SerializeField]
    private float _flightTime;

    [SerializeField]
    private float _forwardDelta = 0.01f;

    private Vector3 _startPosition;

    private Vector3 _targetPosition;

    private float _timer;

    public void Start()
    {
        _startPosition = transform.localPosition;
        _targetPosition = _startPosition;
        _targetPosition.z = -_targetPosition.z;
        _timer = 0.0f;
    }

    private void Update()
    {
        _timer += Time.deltaTime;
        float progress = _timer / _flightTime;

        if(progress + _forwardDelta >= 1.0f)
        {
            gameObject.SetActive(false);
            return;
        }


        Vector3 position = Vector3.Lerp(_startPosition, _targetPosition, progress);
        position.y = _heightCurve.Evaluate(progress);


        Vector3 forwardPosition = Vector3.Lerp(_startPosition, _targetPosition, progress + _forwardDelta);
        forwardPosition.y = _heightCurve.Evaluate(progress + _forwardDelta);

        transform.localPosition = position;
        transform.forward = transform.parent.TransformDirection((forwardPosition - position).normalized);
    }


}
