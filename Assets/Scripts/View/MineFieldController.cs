﻿using Assets.Scripts.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class MineFieldController : MonoBehaviour, ITowerComponentController
{
    private Tower _tower;

    private World _world;

    [SerializeField]
    private ParticleSystem[] _explosionSystems;

    [SerializeField]
    private float _explosionDelay = 0.3f;

    [SerializeField]
    private float _timeBetweenExplosions = 0.1f;

    [SerializeField]
    private float _explosionRadius = 1;

    [SerializeField]
    private AnimationCurve _damageCurve;

    public void OnFire()
    {
        StartCoroutine(Explode());
    }

    private IEnumerator Explode()
    {
        List<ParticleSystem> systems = new List<ParticleSystem>(_explosionSystems);

        yield return new WaitForSeconds(_explosionDelay);

        while(systems.Count > 0)
        {
            ParticleSystem system = ArrayUtils.GetRandom(systems);
            systems.Remove(system);
            system.Play();

            if (_world.isMasterWorld)
            {
                Vector3 worldPosition = transform.position;

                foreach (Monster monster in _world.monsters)
                {
                    float distanceToMonster = Vector3.Distance(monster.worldPosition, worldPosition);

                    if (distanceToMonster < _explosionRadius)
                    {
                        float scale = _damageCurve.Evaluate(1.0f - distanceToMonster / _explosionRadius);

                        monster.hp -= (short)(_tower.damage * scale / _explosionSystems.Length);
                        monster.SetDirty();
                    }
                }
            }



            yield return new WaitForSeconds(_timeBetweenExplosions);

        }

    }

    public void OnComplete()
    {
    }

    public void OnInitialize(Tower tower, World world)
    {
        _tower = tower;
        _world = world;


    }

   
    public void OnDemolish()
    {

    }
}
