using Animancer;
using Assets.Scripts.Model;
using UnityEngine;

/// <summary>
/// Updates all visual stuff for a tower
/// </summary>
public class SpyView : MonoBehaviour, ICharacterComponentController
{
    private World _world;

    private Spy _spy;

    private int _currentConnectionId;

    [SerializeField]
    private LineRenderer _zipRope;

    [SerializeField]
    private float _verticalOffset;

    [SerializeField]
    private float _verticalTowerOffset = 1.25f;
    private ZipTower _zipTower;

    public void Initialize(Character character, World world)
    {
        _world = world;
        _spy = character as Spy;
        _currentConnectionId = -1;
    }

    private void Update()
    {
        if (_spy.connectingTowerId != _currentConnectionId)
        {
            _zipRope.enabled = false;

            if (_spy.connectingTowerId != 0)
            {
                _zipTower = _world.GetEntity(_spy.connectingTowerId) as ZipTower;

                if (_zipRope != null)
                {
                    _zipRope.enabled = true;
                    _zipRope.useWorldSpace = true;
                    _zipRope.positionCount = 2;
                }
            }

            _currentConnectionId = _spy.connectingTowerId;
        }

        if (_zipTower != null)
        {
            _zipRope.SetPositions(new Vector3[]
            {
                _zipTower.worldPosition + Vector3.up * _verticalTowerOffset,
                _spy.worldPosition + Vector3.up * _verticalOffset
            });
        }
    }
}
