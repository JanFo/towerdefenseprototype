using Animancer;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Updates all visual stuff for a tower
/// </summary>
public class MonsterView : MonoBehaviour
{
    [SerializeField]
    private AnimancerComponent _animancerComponent;

    [SerializeField]
    private AnimationClip _idleAnimation;

    [SerializeField]
    private AnimationClip _walkAnimation;

    [SerializeField]
    private AnimationClip _runAnimation;

    [SerializeField]
    private AnimationClip _deathAnimation;

    [SerializeField]
    private AnimationClip _tauntAnimation;

    [SerializeField]
    private AnimationClip _attackAnimation;

    [SerializeField]
    private Monster _monster;

    private Transform _transform;

    private AnimancerMovementState _movementState;

    private float lerpRate = 32;

    private Vector3 _lastSavedPosition;

    private float _smoothedForward;

    private float _smoothedAngle;
    
    public Vector3 position => _transform.position;

    public void Initialize(Monster monster)
    {
        if(monster == null)
        {
            return;
        }

        _transform = transform;
        _monster = monster;

        _movementState = AnimancerMovementState.Create(_idleAnimation, _walkAnimation, _runAnimation);
        _animancerComponent.Play(_movementState);

        _transform.eulerAngles = new Vector3(0, monster.rotation, 0);
        _transform.position = monster.worldPosition;
    }

    private void Start()
    {
        gameObject.SetActive(_monster != null);
    }

    private void Update()
    {
        _smoothedAngle = Mathf.MoveTowardsAngle(_smoothedAngle, _monster.rotation, Time.deltaTime * 540);

        _transform.position = Vector3.Lerp(_transform.position, _monster.worldPosition, Time.deltaTime * lerpRate);
        _transform.eulerAngles = new Vector3(0.0f, _smoothedAngle, 0.0f);

        if (_monster.isDead)
        {
            if (_monster.hp <= 0)
            {
                _animancerComponent.Play(_deathAnimation);
            }

            Destroy(gameObject, 2.0f);
        }

    }

    private void FixedUpdate()
    {
        Vector3 delta = _transform.InverseTransformDirection(_transform.position - _lastSavedPosition);

        _smoothedForward = Mathf.Clamp01(Mathf.MoveTowards(_smoothedForward, delta.z / Time.deltaTime, Time.deltaTime * 4.0f));
        _movementState.ParameterX = _smoothedForward;

        _lastSavedPosition = _transform.position;
    }

    private void OnDrawGizmos()
    {
        if(_monster.path == null)
        {
            return;
        }

        for (int i = 0; i < _monster.path.Length - 1; i++)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(_monster.path[i], _monster.path[i + 1]);
        }
    }
}
