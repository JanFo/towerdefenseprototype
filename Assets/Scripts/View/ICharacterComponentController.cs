﻿internal interface ICharacterComponentController
{
    void Initialize(Character character, World world);
}