﻿using UnityEngine;


public interface IGridNodeModifier
{
    Vector2Int[] positions { get; }

    void OnApply(GridNode node);
}
