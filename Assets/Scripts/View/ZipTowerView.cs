﻿using Assets.Scripts.Model;
using System;
using System.Collections.Generic;
using UnityEngine;

class ZipTowerView : MonoBehaviour, ITowerComponentController
{
    private ZipTower _zipTower;

    private World _world;

    public float topOffset = 8;

    private Dictionary<int, LineRenderer> _connections;

    [SerializeField]
    private LineRenderer _ropePrefab;

    public void OnComplete()
    {

    }
    public void OnFire()
    {
        // Does not fire
    }

    public void OnInitialize(Tower tower, World world)
    {
        _connections = new Dictionary<int, LineRenderer>();
        _zipTower = tower as ZipTower;
        _world = world;
    }

    private void Update()
    {
        foreach(int connection in _zipTower.destinations)
        {
            if(connection < _zipTower.id)
            {
                if(!_connections.ContainsKey(connection))
                {
                    ZipTower targetTower = _world.GetEntity(connection) as ZipTower;

                    LineRenderer renderer = Instantiate(_ropePrefab);
                    renderer.useWorldSpace = true;
                    renderer.positionCount = 2;
                    renderer.SetPositions(new Vector3[]
                    {
                        _zipTower.worldPosition + Vector3.up * topOffset,
                        targetTower.worldPosition + Vector3.up * topOffset
                    });

                    _connections.Add(connection, renderer);
                }

            }
        }
    }

    public void OnDemolish()
    {

    }
}
