using UnityEngine;

/// <summary>
/// Updates all visual stuff for a tower
/// </summary>
public class Rotate : MonoBehaviour
{
    private Transform _transform;

    [SerializeField]
    private float _speed;

    [SerializeField]
    private Vector3 _axis;

    private void Start()
    {
        _transform = transform;
    }

    private void Update()
    {
        _transform.Rotate(_axis, Time.deltaTime * _speed);
    }

}
