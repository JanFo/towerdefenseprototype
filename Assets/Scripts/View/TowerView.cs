using System;
using UnityEngine;

public interface ITowerComponentController
{
    void OnInitialize(Tower tower, World world);
    void OnComplete();
    void OnFire();
    void OnDemolish();
}


/// <summary>
/// Updates all visual stuff for a tower
/// </summary>
public class TowerView : MonoBehaviour
{
    private Tower _tower;

    public GameObject[] hideOnConstructionParts ;

    private ITowerComponentController[] _viewExtensions;

    private Transform _transform;

    private Vector3 _targetPosition;

    public Vector3 position => _transform.position;

    public Vector3? targetPosition;

    private int _fireCounter;

    private bool _isComplete;

    private void Awake()
    {
        foreach (GameObject go in hideOnConstructionParts)
        {
            go.SetActive(false);
        }
    }

    public void Initialize(Tower tower, World world)
    {
        _isComplete = false;
        _tower = tower;
        _viewExtensions = GetComponents<ITowerComponentController>();

        foreach (GameObject go in hideOnConstructionParts)
        {
            go.SetActive(_tower.constructionProgress == 1.0f);
        }

        foreach (ITowerComponentController extension in _viewExtensions)
        {
            extension.OnInitialize(_tower, world);
        }

        _transform = transform;
        _targetPosition = GetTowerPosition(tower.x, tower.y);
        _transform.position = _targetPosition;
    }

    public void Demolish()
    {
        foreach (ITowerComponentController extension in _viewExtensions)
        {
            extension.OnDemolish();
        }
    }

    public static Vector3 GetTowerPosition(int x, int y)
    {
        return new Vector3(0.5f + x, 0.0f, 0.5f + y) * GameConfiguration.tileSize;
    }

    private void Update()
    {
        foreach(GameObject go in hideOnConstructionParts)
        {
            go.SetActive(_tower.constructionProgress == 1.0f);
        }

        if(_tower.constructionProgress < 1.0f)
        {
            return;
        }

        if(!_isComplete)
        {
            _isComplete = true;

            foreach (ITowerComponentController extension in _viewExtensions)
            {
                extension.OnComplete();
            }
        }

        if (_tower.fireCounter > _fireCounter)
        {
            foreach (ITowerComponentController extension in _viewExtensions)
            {
                extension.OnFire();
            }

            _fireCounter = _tower.fireCounter;
        }
    }
}
