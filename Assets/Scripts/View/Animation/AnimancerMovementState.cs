using UnityEngine;
using Animancer;
using System;

public class AnimancerMovementState : CartesianMixerState
{
    private float targetForwardAmount;

    private float forwardAmount;

    private float changeTreshold = 0.01f;

    public float Forward
    {
        set
        {
            if (Mathf.Abs(ParameterX - value) < changeTreshold)
            {
                return;
            }

            ParameterX = value;
            targetForwardAmount = Mathf.Sign(value + ForwardThreshold);
        }
    }

    protected override void Update(out bool needsMoreUpdates)
    {
        base.Update(out needsMoreUpdates);

        if (Mathf.Abs(forwardAmount - targetForwardAmount) < changeTreshold)
        {
            return;
        }

        forwardAmount = Mathf.MoveTowards(forwardAmount, targetForwardAmount, UnityEngine.Time.deltaTime * StrafeDirectionBlendSpeed);

        if (StrafeMixers == null)
        {
            return;
        }

        for (int i = 0; i < StrafeMixers.Length; i++)
        {
            StrafeMixers[i].Parameter = forwardAmount;
        }
    }


    public float Strafe
    {
        set
        {
            if (Mathf.Abs(ParameterY - value) < changeTreshold)
            {
                return;
            }

            ParameterY = value;
        }
    }

    public float Turn
    {
        set
        {
            float turn = Mathf.Abs(ParameterX) > 0.01f ? 0.0f : value;

            if (Mathf.Abs(turn - value) < changeTreshold)
            {
                return;
            }

            IdleMixer.Parameter = turn;
        }
    }

    public new float Speed
    {
        set
        {
            if (Mathf.Abs(base.Speed - value) < changeTreshold)
            {
                return;
            }

            IdleMixer.Speed = value;
            base.Speed = value;
        }
    }

    public float StrafeDirectionBlendSpeed { get; set; }
    public float ForwardThreshold { get; set; }
    public LinearMixerState IdleMixer { get; set; }
    public LinearMixerState[] StrafeMixers { get; set; }
    public string Identifier { get; set; }

    public static AnimancerMovementState Create(AnimancerMovementStateConfiguration config)
    {
        float walkThreshold = 0.4f;

        if (config.idle == null)
        {
            return null;
        }

        AnimancerMovementState mixer = new AnimancerMovementState();

        LinearMixerState idleMixer = new LinearMixerState();
        idleMixer.Initialise(new AnimationClip[] {
            config.turnLeft != null ? config.turnLeft : config.idle,
            config.idle,
            config.turnRight != null ? config.turnRight : config.idle }, new float[] { -1, 0.0f, 1 });

        idleMixer.GetChild(0).Speed = 2;
        idleMixer.GetChild(2).Speed = 2;

        mixer.Initialise(8);

        mixer.SetThreshold(0, Vector2.zero);
        mixer.SetChild(0, idleMixer);

        mixer.SynchroniseChildren = new bool[] { false, true, true, true, true };
        if (config.walkFront) mixer.CreateChild(1, config.walkFront, new Vector2(walkThreshold, 0.0f));
        if (config.walkBack) mixer.CreateChild(2, config.walkBack, new Vector2(-walkThreshold, 0.0f));

        if (config.strafeWalkRight && config.strafeWalkLeft)
        {
            LinearMixerState strafeLeftMixer = new LinearMixerState();
            strafeLeftMixer.Initialise(new AnimationClip[] {
            config.strafeWalkBackwardsLeft != null ? config.strafeWalkBackwardsLeft : config.strafeWalkRight,
            config.strafeWalkLeft }, new float[] { -1, 1 });
            strafeLeftMixer.SynchroniseChildren = new bool[] { false, false };

            LinearMixerState strafeRightMixer = new LinearMixerState();
            strafeRightMixer.Initialise(new AnimationClip[] {
            config.strafeWalkBackwardsRight != null ? config.strafeWalkBackwardsRight : config.strafeWalkLeft,
            config.strafeWalkRight }, new float[] { -1, 1 });

            strafeRightMixer.SynchroniseChildren = new bool[] { false, false };

            mixer.SetThreshold(3, new Vector2(0, -walkThreshold));
            mixer.SetChild(3, strafeLeftMixer);

            mixer.SetThreshold(4, new Vector2(0, walkThreshold));
            mixer.SetChild(4, strafeRightMixer);

            mixer.StrafeDirectionBlendSpeed = 8.0f;
            mixer.ForwardThreshold = 0.1f;
            mixer.StrafeMixers = new LinearMixerState[] { strafeLeftMixer, strafeRightMixer };
        }

        if (config.run != null)
            mixer.CreateChild(5, config.run, new Vector2(1, 0.0f));

        if (config.strafeRunLeft != null)
            mixer.CreateChild(6, config.strafeRunLeft, new Vector2(walkThreshold, -1));

        if (config.strafeRunRight != null)
            mixer.CreateChild(7, config.strafeRunRight, new Vector2(walkThreshold, 1));

        mixer.IdleMixer = idleMixer;
        mixer.Identifier = config.name;
        mixer.Speed = config.speed;
        return mixer;
    }

    public static AnimancerMovementState Create(AnimationClip idle, AnimationClip walk, AnimationClip run)
    {
        float walkThreshold = 0.4f;

        if (idle == null)
        {
            return null;
        }

        AnimancerMovementState mixer = new AnimancerMovementState();
        mixer.Initialise(3);
        mixer.SetThreshold(0, Vector2.zero);
        mixer.CreateChild(0, idle, new Vector2(0.0f, 0.0f));
        if (walk) mixer.CreateChild(1, walk, new Vector2(walkThreshold, 0.0f));
        if (run) mixer.CreateChild(2, run, new Vector2(1, 0.0f));

        return mixer;
    }
}
