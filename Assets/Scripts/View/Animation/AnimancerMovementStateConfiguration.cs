using Animancer;
using UnityEngine;

[CreateAssetMenu(fileName = "AnimancerConfig", menuName = "Animations/Movement State Configuration")]
public class AnimancerMovementStateConfiguration : ScriptableObject
{
    public float speed = 1.0f;

    public AnimationClip turnLeft;

    public AnimationClip turnRight;

    public AnimationClip idle;

    public AnimationClip walkFront;

    public AnimationClip run;

    public AnimationClip walkBack;

    public AnimationClip strafeWalkLeft;

    public AnimationClip strafeRunLeft;

    public AnimationClip strafeWalkBackwardsLeft;

    public AnimationClip strafeWalkRight;

    public AnimationClip strafeRunRight;

    public AnimationClip strafeWalkBackwardsRight;

  
}
