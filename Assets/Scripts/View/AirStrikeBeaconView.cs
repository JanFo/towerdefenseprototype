﻿using Assets.Scripts.Model;
using System;
using System.Collections.Generic;
using UnityEngine;

class AirStrikeBeaconView : MonoBehaviour, ITowerComponentController
{
    private AirStrikeBeacon _beacon;

    private World _world;

    public void OnFire()
    {
        // Does not fire
    }

    public void OnComplete()
    {
    }

    public void OnInitialize(Tower tower, World world)
    {
        _beacon = tower as AirStrikeBeacon;
        _world = world;
    }

    private void Update()
    {
        int direction = (int)_beacon.direction;
        transform.eulerAngles = new Vector3(0, direction * 90.0f, 0);
    }

    public void OnDemolish()
    {

    }
}
