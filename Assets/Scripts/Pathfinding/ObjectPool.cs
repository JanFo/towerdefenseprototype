﻿using System.Collections.Generic;

public abstract class ObjectPool<T> where T : new()
{
    private Stack<T> _objects;
    
    public ObjectPool()
    {
        _objects = new Stack<T>();
    }

    public void Release(T obj)
    {
        OnReset(obj);
        _objects.Push(obj);
    }

    protected abstract void OnReset(T obj);

    public T Claim()
    {
        if(_objects.Count == 0)
        {
            return new T();
        }
        
        return _objects.Pop();
    }

}