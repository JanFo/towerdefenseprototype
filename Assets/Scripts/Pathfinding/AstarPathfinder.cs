//using System;
//using System.Collections.Generic;
//using UnityEngine;
//using System.Threading;

///// <summary>
///// Used by the host to determine creature paths (game controller)
///// </summary>
//public class AstarPathfinder
//{
//    private WorldGrid _grid;
//    private PathfinderDebug debugView;
//    private PathOptimizer _optimizer;

//    public AstarPathfinder(WorldGrid grid)
//    {
//        _grid = grid;
//        debugView = GameObject.FindObjectOfType<PathfinderDebug>();
//        if (debugView != null) debugView.DrawDebug(_grid);
//        _optimizer = GameObject.FindObjectOfType<PathOptimizer>();
//    }


//    private List<PathTraceNode> AddToOrdered(List<PathTraceNode> list, PathTraceNode n)
//    {
//        var idx = 0;
//        while (idx < list.Count && list[idx].distanceVsCost < n.distanceVsCost)
//        {
//            idx++;
//        }
//        list.Insert(idx, n);
//        return list;
//    }

//    /// <summary>
//    /// The Astar celll flooding...
//    /// </summary>
//    private Dictionary<int, PathTraceNode> generatePath(GridNode start, GridNode end)
//    {
//        var lookup = new Dictionary<int, PathTraceNode>();
//        var s = PathTraceNode.FromGridNode(start);
//        lookup.Add(s.nodeIndex, s);
//        var e = PathTraceNode.FromGridNode(end);

//        Ray staightLine = new Ray(start.worldPosition, end.worldPosition - start.worldPosition);
//        List<PathTraceNode> openList = new List<PathTraceNode>();
//        HashSet<PathTraceNode> openAsSet = new HashSet<PathTraceNode>();
//        HashSet<PathTraceNode> closedSet = new HashSet<PathTraceNode>();
//        List<GridNode> adjacencies;
//        PathTraceNode current = s;

//        // add start node to Open List
//        openList.Add(s);
//        openAsSet.Add(s);

//        while (openList.Count > 0 && !lookup.ContainsKey(end.nodeIndex))
//        {
//            current = openList[0];
//            openList.RemoveAt(0);
//            openAsSet.Remove(current);
//            closedSet.Add(current);
//            adjacencies = _grid.GetAdjacentNodes(current.associate);

//            foreach (GridNode node in adjacencies)
//            {
//                var ptn = PathTraceNode.FromGridNode(node);

//                if (!openAsSet.Contains(ptn) && !closedSet.Contains(ptn) && node.isWalkable)
//                {
//                    ptn.parentIdx = current.nodeIndex;
//                    var pathCost = CostFunction(node.worldPosition, staightLine);
//                    if (node.parent != null && lookup.ContainsKey(node.parent.nodeIndex))
//                    {
//                        ptn.cost = node.weight + pathCost + lookup[node.parent.nodeIndex].cost;
//                    }
//                    else
//                    {
//                        ptn.cost = node.weight + pathCost;
//                    }

//                    lookup[node.nodeIndex] = ptn;
//                    openList = AddToOrdered(openList, ptn);
//                    openAsSet.Add(ptn);
//                }
//            }
//        }
//        if (debugView != null)
//        {
//            debugView.UpdateTraceNodes(lookup);
//        }
//        return lookup;
//    }

//    /// <summary>
//    /// Calculates the cost function for the traversal of a given cell;
//    /// </summary>
//    /// <param name="position">The current position.</param>
//    /// <param name="line">The direct line between source and target</param>
//    /// <returns>The calculated cost.</returns>
//    private float CostFunction(Vector3 position, Ray line)
//    {
//        float diff1 = line.origin.x - position.x;
//        float diff2 = line.origin.z - position.z;
//        var distanceToTarget = diff1 * diff1 + diff2 * diff2;
//        var divergencePenalty = Vector3.Cross(line.direction, position - line.origin).magnitude;
//        var distanceFactor = _optimizer != null ? _optimizer.attractionToTarget : PathOptimizer.DefaultAttractionToTarget;
//        var divergenceFactor = _optimizer != null ? _optimizer.attractionToStraightLine : PathOptimizer.DefaultAttractionToStraightLine;
//        return divergencePenalty * divergenceFactor + distanceToTarget * distanceFactor;
//    }

//    /// <summary>
//    /// Invokes the pathfinding on a different thread.
//    /// </summary>
//    /// <param name="seeker">The seeker object</param>
//    /// <param name="target">The target position</param>
//    public void InvokePathFinding(IPathSeeker seeker, Vector3 target)
//    {
//        // Call PathFiniding in a pooled thread
//        ThreadPool.QueueUserWorkItem(new WaitCallback(InvokePathFinding), (seeker, target));
//    }

//    /// <summary>
//    /// The actual execute function for a different thread (this signature is required)
//    /// </summary>
//    private void InvokePathFinding(object pos)
//    {
//        var (seeker, target) = ((IPathSeeker, Vector3)) pos;
//        FindPath(seeker, target);
//    }

//    private int _maxTries = 100000;

//    /// <summary>
//    /// Executes the pathfinding algorithm on the current thread.
//    /// </summary>
//    /// <param name="startPosition">The start position</param>
//    /// <param name="endPosition">The target position</param>
//    /// <returns>The calculated path</returns>
//    public void FindPath(IPathSeeker seeker, Vector3 endPosition)
//    {
//        // we want to search paths from castle zo monster
//        GridNode end = _grid.GetNode(seeker.worldPosition);
//        GridNode start = _grid.GetNode(endPosition);

//        if (!end.isWalkable || !start.isWalkable)
//        {
//            return;
//        }

//        List<Vector3> path = new List<Vector3>();
//        Dictionary<int, PathTraceNode> result = generatePath(start, end);

//        // construct path, if end was not closed return null
//        if (!result.ContainsKey(end.nodeIndex))
//        {
//            return;
//        }

//        int temp = end.nodeIndex;
//        int safety = _maxTries;

//        do
//        {
//            if (result.ContainsKey(temp))
//            {
//                var cell = result[temp];
//                path.Insert(0, cell.worldPosition);
//                temp = cell.parentIdx;
//            }
//            else
//            {
//                return;      // happens if no parent cell was set;
//            }
//            safety--;
//            if (safety <= 0)
//            {
//                Debug.LogError("Jan fucked up @ a-star, Markus is'nt blameless either.");
//                break;
//            }
//        }
//        while (temp != start.nodeIndex);

//        path.Insert(0, start.worldPosition);
//        path.Reverse();
//        if(_optimizer != null)
//        {
//            path = _optimizer.SmoothSimple(path);
//        }
//        seeker.AssignPath(path.ToArray());
//    }
//}
