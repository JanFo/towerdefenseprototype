﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// This should be a helper structure to keep track of available or blocked nodes (USED ON ALL CLIENTS)
/// Should not know about pathfindiing as it is MASTER CLIENT ONLY!
/// </summary>
public class WorldGrid : IWorldGrid
{
    public delegate void WalkableChangedEvent(int x, int y, bool walkable);

    private GridNode[,] _nodes;
    public int rows { get; }

    public int cols { get; }

    public bool hasInvalidPaths { get; private set; }

    public float cellSize => GameConfiguration.tileSize;

    public void Register(IGridNodeModifier modifier)
    {
        foreach (Vector2Int position in modifier.positions)
        {
            GetNode(position.x, position.y).Register(modifier);
        }
    }

    public void Unregister(IGridNodeModifier modifier)
    {
        foreach (Vector2Int position in modifier.positions)
        {
            GetNode(position.x, position.y).Unregister(modifier);
        }
    }

    public event WalkableChangedEvent OnWalkableChanged;

    public WorldGrid(Level level)
    {
        rows = level.size;
        cols = level.size;

        InitializeGrid(level);
    }

    private void InitializeGrid(Level level)
    {
        _nodes = new GridNode[rows, cols];

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                _nodes[i, j] = new GridNode();
                _nodes[i, j].nodeIndex = i * rows + j;
                _nodes[i, j].position = new Vector2Int(i, j);
                _nodes[i, j].worldPosition = new Vector3((0.5f + i) * GameConfiguration.tileSize, 0.0f, (0.5f + j) * GameConfiguration.tileSize);
                _nodes[i, j].weight = 1;
                _nodes[i, j].isWalkable = level.map[i, j] != MapTile.Mountain;
                _nodes[i, j].isBuildable = level.map[i, j] == MapTile.Normal;
            }
        }
    }



    public void InvalidatePaths()
    {
        hasInvalidPaths = true;
    }

    public void ValidatePaths()
    {
        hasInvalidPaths = false;
    }

    public List<GridNode> GetAdjacentNodes(GridNode node)
    {
        List<GridNode> temp = new List<GridNode>();

        int row = node.position.y;
        int col = node.position.x;

        if (row + 1 < rows)
        {
            temp.Add(GetNode(col, row + 1));
        }
        if (row - 1 >= 0)
        {
            temp.Add(GetNode(col, row - 1));
        }
        if (col - 1 >= 0)
        {
            temp.Add(GetNode(col - 1, row));
        }
        if (col + 1 < cols)
        {
            temp.Add(GetNode(col + 1, row));
        }

        return temp;
    }

    public GridNode GetNode(Vector3 position)
    {
        Vector2Int pos = GridUtils.GetGridPosition(position);
        return GetNode(pos.x, pos.y);
    }

    public GridNode GetNode(int x, int y)
    {
        return _nodes[x, y];
    }

    //public void UpdateGrid(int x, int y, NodeFlag flag)
    //{
    //    UpdateGrid(x, y, (uint)flag);
    //}

    public void UpdateGrid(int x, int y, uint flag)
    {
        var node = GetNode(x, y);
        var walkable = BitMaskUtils.IsWalkableFlag(flag);
        // if(walkable) node.entityId = 0;

        if (node.isWalkable && walkable)
        {
            node.flag = node.flag | flag;       // we just pile on existing flags as long if it walkable => cos multiple non blocking thing can be in one cell
        }
        else
        {
            node.flag = flag;
        }

        OnWalkableChanged?.Invoke(x, y, walkable);
        InvalidatePaths();
    }

    //public void RemoveTowerId(int x, int y)
    //{
    //    SetBuilding(x, y, 0);
    //}

    public void SetWalkable(int x, int y, bool isWalkable)
    {
        GridNode node = GetNode(x, y);
        node.isWalkable = isWalkable;
    }

    public void RemoveBuilding(int x, int y)
    {
        GridNode node = GetNode(x, y);
        node.entityId = 0;
        node.isWalkable = true;

        OnWalkableChanged?.Invoke(x, y, node.isWalkable);
        InvalidatePaths();
    }

    public void SetBuilding(Building building)
    {
        GridNode node = GetNode(building.x, building.y);

        node.entityId = building.id;
        node.isWalkable = !building.isBlocking;

        OnWalkableChanged?.Invoke(building.x, building.y, node.isWalkable);
        InvalidatePaths();
    }

    /// <summary>
    /// The id of a tower is based on the Flag id of Flag.TowerNode.
    /// We enter its id instead of the base Flag id.
    /// </summary>
    //public void UpdateGrid(int x, int y, Tower tower)
    //{
    //    var node = GetNode(x, y);
    //    node.entityId = tower.id;
    //    node.flag = (uint)NodeFlag.TowerNode;


    //    OnWalkableChanged?.Invoke(x, y, false);
    //    MakeGridDirty();
    //}

    //public void SetTag(int x, int y, uint tag)
    //{
    //    GetNode(x, y).tag = tag;
    //    InvalidatePaths();
    //}

    //private void resetPenalty(int x, int y)
    //{
    //    GetNode(x, y).penalty = 0u;
    //    hasInvalidPaths = true;
    //}

    //public void ModifyPenaltyInArea(RectInt area, int penalty)
    //{
    //    for (int i = 1; i <= area.width; i++)
    //    {
    //        for (int j = 1; j <= area.height; j++)
    //        {
    //            ModifyPenalty(area.x + i, area.y + j, penalty);
    //        }
    //    }
    //}

    
    public void SetPenalty(int x, int y, float penalty)
    {
        var node = GetNode(x, y);
        node.penalty = Mathf.Clamp(penalty, 0, 1);

        InvalidatePaths();
    }
}
