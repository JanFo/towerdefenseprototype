using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathUtils
{
	public static Vector3 GetWorldPosition(int x, int y)
	{
		var cellSize = GameConfiguration.tileSize;
		return new Vector3(cellSize / 2 + x, 0.0f, cellSize / 2 + y) * cellSize;
	}

	public static Vector2Int GetGridPosition(Vector3 worldPosition)
	{
		worldPosition /= GameConfiguration.tileSize;
		return new Vector2Int((int)worldPosition.x, (int)worldPosition.z);
	}

	/// <summary>Divides each segment in the list into subSegments segments and fills the result list with the new points</summary>
	public static void Subdivide(List<Vector3> points, List<Vector3> result, int subSegments)
	{
		for (int i = 0; i < points.Count - 1; i++)
			for (int j = 0; j < subSegments; j++)
				result.Add(Vector3.Lerp(points[i], points[i + 1], j / (float)subSegments));

		result.Add(points[points.Count - 1]);
	}

	public static float[] CreateDistanceArray(Vector3[] corners)
	{
		if (corners == null)
		{
			return null;
		}

		if (corners.Length == 0)
		{
			return new float[0];
		}

		float[] result = new float[corners.Length - 1];

		for (int i = 0; i < result.Length; i++)
		{
			result[i] = Vector3.Distance(corners[i + 1], corners[i]);
		}

		return result;
	}

	public static float GetPathLength(Vector3[] corners)
	{
		return GetPathLength(corners, corners.Length);
	}

	public static float GetPathLength(Vector3[] corners, int maxIndex)
	{
		float lng = 0.0f;

		if (corners.Length > 1)
		{
			for (int i = 1; i < maxIndex; ++i)
			{
				lng += Vector3.Distance(corners[i - 1], corners[i]);
			}
		}

		return lng;
	}

	public static float GetPathLength(NavMeshPath path)
	{
		float lng = 0.0f;

		if ((path.status != NavMeshPathStatus.PathInvalid) && (path.corners.Length > 1))
		{
			for (int i = 1; i < path.corners.Length; ++i)
			{
				lng += Vector3.Distance(path.corners[i - 1], path.corners[i]);
			}
		}

		return lng;
	}

	public static bool SamplePath(Vector3[] corners, float distance, out Vector3 position)
	{
		float[] distances = CreateDistanceArray(corners);

		return SamplePath(corners, distances, distance, out position);
	}

	internal static bool SamplePath(NavMeshPath path, float distance, out Vector3 position)
	{
		float[] distances = CreateDistanceArray(path.corners);

		return SamplePath(path.corners, distances, distance, out position);
	}

	// sample a given path with a given distance array at a given distance
	public static bool SamplePath(Vector3[] corners, float[] distanceArray, float distance, out Vector3 position)
	{
		if (corners == null || distanceArray == null)
		{
			position = Vector3.zero;
			return false;
		}

		if (corners.Length == 0)
		{
			position = Vector3.zero;
			return false;
		}

		if (distance < 0.0f)
		{
			position = corners[0];
			return false;
		}

		float dist = distance;

		int i = 0;

		while (i < distanceArray.Length)
		{
			if (dist <= distanceArray[i])
			{
				break;
			}

			dist -= distanceArray[i++];
		}

		if (i < distanceArray.Length)
		{
			position = Vector3.Lerp(corners[i], corners[i + 1], dist / distanceArray[i]);
			return true;
		}
		else
		{
			position = corners[i];
			return false;
		}

	}

	public static Vector2 InputToForwardTurn(Transform characterTransform, Vector3 input, float forwardBoost = 1.5f)
    {
        if (input.magnitude == 0.0f)
        {
            return Vector2.zero;
        }

        input.y = 0.0f;

        float forwardAmount = Mathf.Clamp01(Vector3.Dot(characterTransform.forward, input));

        float forward = Mathf.Clamp01(3.0f * Mathf.Pow(forwardAmount, 2) - 2.0f * Mathf.Pow(forwardAmount, 3));
        forward = Mathf.Clamp01(Mathf.Pow(forward, 4) * forwardBoost);

        float angle = Vector3.Angle(characterTransform.forward, input);

        Vector3 cross = Vector3.Cross(characterTransform.forward, input);
        angle = cross.y < 0 ? -angle : angle;

        float turn = Mathf.Clamp(angle / 90.0f, -1.0f, 1.0f);

        return new Vector2(forward, turn);
    }

    public static Vector3[] RefinePath(IEnumerable<Vector3> c, float p)
    {
        List<Vector3> corners = new List<Vector3>(c);

        int i = 0;
        while (i < corners.Count - 1)
        {
            if (Vector3.Distance(corners[i], corners[i + 1]) > p)
            {
                corners.Insert(i + 1, (corners[i] + corners[i + 1]) / 2.0f);
            }
            else
            {
                i++;
            }
        }

        return corners.ToArray();
    }

    public static Vector3[] MakeSmoothCurve(Vector3[] arrayToCurve, float smoothness)
    {

        List<Vector3> points;
        List<Vector3> curvedPoints;
        if (smoothness < 1.0f) smoothness = 1.0f;

        int pointsLength = arrayToCurve.Length;

        int curvedLength = pointsLength * Mathf.RoundToInt(smoothness) - 1;
        curvedPoints = new List<Vector3>(curvedLength);

        float t = 0.0f;

        for (int pointInTimeOnCurve = 0; pointInTimeOnCurve < curvedLength + 1; pointInTimeOnCurve++)
        {
            t = Mathf.InverseLerp(0, curvedLength, pointInTimeOnCurve);

            points = new List<Vector3>(arrayToCurve);

            for (int j = pointsLength - 1; j > 0; j--)
            {
                for (int i = 0; i < j; i++)
                {
                    points[i] = (1 - t) * points[i] + t * points[i + 1];
                }
            }

            curvedPoints.Add(points[0]);
        }

        return (curvedPoints.ToArray());
    }

    public static Vector3[] RelaxPath(Vector3[] corners, int iterations)
    {
        for (int k = 0; k < iterations; k++)
        {
            for (int i = 1; i < corners.Length - 1; i++)
            {
                corners[i].x = (corners[i - 1].x + corners[i + 1].x) / 2.0f;
                corners[i].z = (corners[i - 1].z + corners[i + 1].z) / 2.0f;
            }
        }

        return corners;
    }
}