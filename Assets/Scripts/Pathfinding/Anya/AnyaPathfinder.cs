using System;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public struct AnyaNode
{
    public Vector2Int r;
    public Vector2Int a;
    public Vector2Int b;

    public bool IsFlatNode()
    {
        return r.y == a.y;
    }

    public Vector2Int GetFarthestFrom(Vector2Int r)
    {
        throw new NotImplementedException();
    }

    public bool IsStartNode()
    {
        return r.x == int.MaxValue;
    }
}

/// <summary>
/// Used by the host to determine creature paths (game controller)
/// </summary>
public class AnyaPathfinder
{
    private WorldGrid _grid;

    private Vector2Int _startNode;

    public AnyaPathfinder(WorldGrid grid)
    {
        _grid = grid;
    }

    public void Search(IPathSeeker seeker, Vector3 target)
    {
        _startNode = GridUtils.GetGridPosition(seeker.worldPosition);

    }

    public List<AnyaNode> ComputeSuccessors(AnyaNode node)
    {
        if(node.IsStartNode())
        {
            return GenerateStartSuccessors();
        }

        List<AnyaNode> successors;

        if (node.IsFlatNode())
        {
            Vector2Int p = node.GetFarthestFrom(node.r);
            successors = GenerateFlatSuccessors(p, node.r);

            if (IsTurningPointOnTautPath(p, node.r))
            {
                successors.AddRange(GenerateConeSuccessors(p, p, node.r));
            }
        }
        else
        {
            successors = GenerateConeSuccessors(node.a, node.b, node.r);

            if (IsTurningPointOnTautPath(node.a, node.r))
            {
                successors.AddRange(GenerateFlatSuccessors(node.a, node.r));
                successors.AddRange(GenerateConeSuccessors(node.a, node.a, node.r));
            }

            if (IsTurningPointOnTautPath(node.b, node.r))
            {
                successors.AddRange(GenerateFlatSuccessors(node.b, node.r));
                successors.AddRange(GenerateConeSuccessors(node.b, node.b, node.r));
            }
        }

        return successors;
    }

    private List<AnyaNode> GenerateConeSuccessors(Vector2Int p1, Vector2Int p2, Vector2Int r)
    {
        throw new NotImplementedException();
    }

    private List<AnyaNode> GenerateFlatSuccessors(Vector2Int p, Vector2Int r)
    {
        throw new NotImplementedException();
    }

    private bool IsTurningPointOnTautPath(Vector2Int p, Vector2Int r)
    {
        throw new NotImplementedException();
    }

    private List<AnyaNode> GenerateStartSuccessors()
    {
        throw new NotImplementedException();
    }

  
}
