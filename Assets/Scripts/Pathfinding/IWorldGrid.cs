﻿using UnityEngine;

/// <summary>
/// The World Grid Interface
/// </summary>
public interface IWorldGrid
{
    GridNode GetNode(Vector3 position);

    GridNode GetNode(int x, int y);

    int rows { get; }

    float cellSize { get; }

    int cols { get; }

    bool hasInvalidPaths { get; }

}
