using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class GridNode
{
    public int nodeIndex;
    public Vector2Int position;
    public int distanceToTarget;
    public int weight;
    public GridNode parent;
    public int cost;
    public Vector3 worldPosition;
    public uint flag = 0u;
    public float penalty = 0;
    public uint tag = 0u;
    public int entityId = 0;
    public int monsterCount = 0;

    public bool isWalkable;
    public bool isBuildable;

    public float speedModifier;

    public List<IGridNodeModifier> modifiers;

    public GridNode()
    {
        modifiers = new List<IGridNodeModifier>();
        speedModifier = 1.0f;
        weight = 1;
    }

    public float distanceVsCost
    {
        get
        {
            if (distanceToTarget != -1 && cost != -1)
                return distanceToTarget + cost;
            else
                return -1;
        }
    }

    private void OnModified()
    {
        Reset();
        foreach (IGridNodeModifier mod in modifiers)
        {
            mod.OnApply(this);
        }
    }

    private void Reset()
    {
        speedModifier = 1.0f;
    }

    public void Register(IGridNodeModifier modifier)
    {
        modifiers.Add(modifier);
        OnModified();
    }

    public void Unregister(IGridNodeModifier modifier)
    {
        modifiers.Remove(modifier);
        OnModified();
    }

    /// <summary>
    /// A node is 'free' if it is not flagged at all, this does not mean that the node is not walkable.
    /// </summary>
    public bool IsFreeNode()
    {
        return flag == 0;
    }

    public List<NodeFlag> GetNodeFlags()
    {
        var flags = Enumerable.Range(0, (int)BitMaskUtils.MaxNodeFlagInUse)
            .Select(i => (NodeFlag)Enum.ToObject(typeof(NodeFlag), (flag & (1 << i))))
            .Where(f => f != NodeFlag.FreeNode);

        if (flags.Count() == 0)
        {
            return new List<NodeFlag>() { NodeFlag.FreeNode };
        }
        else
        {
            return flags.ToList();
        }
    }
}