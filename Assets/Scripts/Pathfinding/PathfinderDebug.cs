using System;
using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Updates all visual stuff for a tower
/// </summary>
public class PathfinderDebug : MonoBehaviour
{

    private Vector3[] _debugPath;

    private Dictionary<int, PathTraceNode> _traceNodes;
    private IWorldGrid _debugGrid;
    private NavMeshQueryFilter _queryFilter;

    private NavMeshPath _navMeshPath;

    public static Character character = null;

    public static int debugDrawRange = 6;

    [SerializeField]
    private Transform _targetTransform;

    [SerializeField]
    private Transform _sourceTransform;

    [SerializeField]
    private Material _lineRendererMaterial;

    private static World _world;

    private static LineRenderer _debugLineRenderer;

    public void DrawDebug(WorldGrid grid)
    {
        _debugGrid = grid;
    }

    public void ShowDebug(World world)
    {
        _world = world;

        GameObject lineRendererObject = new GameObject("Line_Debug");
        _debugLineRenderer = lineRendererObject.AddComponent<LineRenderer>();
        _debugLineRenderer.material = _lineRendererMaterial;
        _debugLineRenderer.startColor = new Color(1, 1, 1, 0.2f);
        _debugLineRenderer.endColor = new Color(1, 1, 1, 0.2f);
        _debugLineRenderer.startWidth = 0.1f;
        _debugLineRenderer.endWidth = 0.1f;
    }

    internal void UpdateTraceNodes(Dictionary<int, PathTraceNode> traceNodes)
    {
        this._traceNodes = traceNodes;
    }

    private void Start()
    {
        //_navMeshPath = new NavMeshPath();
        _queryFilter = new NavMeshQueryFilter()
        {
            agentTypeID = 0,
            areaMask = NavMesh.AllAreas
        };
    }

    private void Update()
    {
        if (_world != null)
        {
            foreach (Spawn spawn in _world.spawns)
            {
                if (spawn.path != null)
                {
                    _debugLineRenderer.SetPositions(spawn.path.Select(p => p + Vector3.up * 0.1f).ToArray());
                    _debugLineRenderer.positionCount = spawn.path.Length;
                }
            }
        }
        //NavMesh.CalculatePath(_sourceTransform.position, _targetTransform.position, _queryFilter, _navMeshPath);
    }

    private void OnDrawGizmos()
    {
        if (_debugGrid != null)
        {
            Vector3 size = new Vector3(_debugGrid.cellSize / 2.0f, 0.1f, _debugGrid.cellSize / 2.0f);

            for (int i = 0; i < _debugGrid.rows; i++)
            {
                for (int j = 0; j < _debugGrid.cols; j++)
                {
                    //GridNode node = _debugGrid.GetNode(i, j);
                    //Gizmos.color = node.IsWalkable() ? Color.white : Color.red;
                    //Gizmos.DrawWireCube(node.worldPosition, size);
                }
            }
        }

        if (_navMeshPath != null)
        {
            for (int i = 0; i < _navMeshPath.corners.Length - 1; i++)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(_navMeshPath.corners[i], _navMeshPath.corners[i + 1]);
            }
        }


        if (_debugPath != null)
        {
            for (int i = 0; i < _debugPath.Length - 1; i++)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(_debugPath[i], _debugPath[i + 1]);
            }
        }
    }

    //private void OnGUI()
    //{
    //    if (_debugGrid != null && character != null)
    //    {
    //        var position = PathUtils.GetGridPosition(character.worldPosition);

    //        for (int i = position.x - debugDrawRange; i < position.x + debugDrawRange + 1; i++)
    //        {
    //            for (int j = position.y - debugDrawRange; j < position.y + debugDrawRange + 1; j++)
    //            {
    //                GridNode node = _debugGrid.GetNode(i, j);
    //                //PathTraceNode pathCost = ArrayUtils.Get(_traceNodes, node.nodeIndex, PathTraceNode.Default);
    //                //string text = pathCost.cost.ToString("0.#");

    //                //var screenPosition = Camera.main.WorldToScreenPoint(node.worldPosition);
    //                //var textSize = GUI.skin.label.CalcSize(new GUIContent(text));
    //                //GUI.Label(new Rect(screenPosition.x, Screen.height - screenPosition.y, textSize.x, textSize.y), text);
    //            }
    //        }

      
    //    }

    //}
}
