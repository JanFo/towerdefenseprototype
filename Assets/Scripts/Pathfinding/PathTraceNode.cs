﻿
using UnityEngine;
using System;

internal struct PathTraceNode : IEquatable<PathTraceNode>
{
    public static PathTraceNode Default = default(PathTraceNode);

    public int parentIdx { get; set; }
    public float cost { get; set; }

    public Vector3 worldPosition { get { return associate.worldPosition; } }
    public Vector2Int position { get { return associate.position; } }
    public int weight { get { return associate.weight; } }
    public int nodeIndex { get { return associate.nodeIndex; } }

    public GridNode associate { get; private set; }

    /// <summary>
    /// If true, the node can be walked on.
    /// Their might be som active flag, bit all are considered non-impeding.
    /// </summary>
    public bool IsWalkable()
    {
        return BitMaskUtils.IsWalkableFlag(associate.flag);
    }

    public float distanceVsCost
    {
        get
        {
            if (cost != -1)
                return cost;
            else
                return -1;
        }
    }

    public static PathTraceNode FromGridNode(GridNode node)
    {
        var ptn = new PathTraceNode();
        ptn.cost = float.MaxValue;
        ptn.associate = node;
        return ptn;
    }

    public override string ToString()
    {
        return position.ToString() + " - " + distanceVsCost;
    }

    public bool Equals(PathTraceNode other)
    {
        return other.nodeIndex == this.nodeIndex;
    }

    public override int GetHashCode()
    {
        return nodeIndex.GetHashCode();
    }

    public override bool Equals(object obj)
    {
        if(obj.GetType() == this.GetType())
        {
            return Equals((PathTraceNode)obj);
        }
        else
        {
            return false;
        }
    }
}
