using UnityEngine;

public interface IPathSeeker
{
    Vector3 worldPosition { get; }
    void AssignPath(Vector3[] path);
}

/// <summary>
/// Used by the host to determine creature paths (game controller)
/// </summary>
public class PathfinderTask
{
    public IPathSeeker receiver { get; set; }

    public Vector3 targetPosition { get; set; }
}
