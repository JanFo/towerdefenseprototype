using Pathfinding;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used by the host to determine creature paths (game controller)
/// </summary>
public class ExternalPathfinder
{
    private GridGraph _graph;

    private Seeker _seeker;

    private WorldGrid _grid;

    private PathfinderTaskPool _taskPool;

    private Queue<PathfinderTask> _tasks;

    private PathfinderTask _currentTask;

    public ExternalPathfinder(WorldGrid grid)
    {
        _taskPool = new PathfinderTaskPool();
        _tasks = new Queue<PathfinderTask>();
        _seeker = UnityEngine.Object.FindObjectOfType<Seeker>();

        _graph = AstarPath.active.data.AddGraph(typeof(GridGraph)) as GridGraph;
        _graph.center = new Vector3(GameConfiguration.tileSize * grid.rows / 2, 0.0f, GameConfiguration.tileSize * grid.cols / 2);
        _graph.SetDimensions(grid.rows, grid.cols, GameConfiguration.tileSize);
        _graph.neighbours = NumNeighbours.Four;

        AstarPath.active.Scan();
        AstarPath.active.logPathResults = PathLog.None;

        _grid = grid;
        _grid.OnWalkableChanged += OnWalkableChanged;

        for(int i = 0; i < grid.rows; i++)
        {
            for(int j = 0; j < grid.cols; j++)
            {
                _graph.GetNode(i, j).Walkable = grid.GetNode(i, j).isWalkable;
            }
        }

        //HanUrasKoenigOptimizer optimizer = _seeker.GetComponent<HanUrasKoenigOptimizer>();
        //optimizer.grid = grid;
    }

    private void OnWalkableChanged(int x, int y, bool walkable)
    {
        _graph.GetNode(x, y).Walkable = walkable;
    }

    public void FindPath(IPathSeeker monster, Vector3 endPosition)
    {
        PathfinderTask task = _taskPool.Claim();
        task.receiver = monster;
        task.targetPosition = endPosition;

        _tasks.Enqueue(task);

        if(_currentTask == null)
        {
            StartNextTask();
        }
    }

    private void StartNextTask()
    {
        _currentTask = _tasks.Dequeue();
        _seeker.StartPath(_currentTask.receiver.worldPosition, _currentTask.targetPosition, OnPathComplete);
    }

    private void OnPathComplete(Path p)
    {

        _currentTask.receiver.AssignPath(p.vectorPath.ToArray());

        _taskPool.Release(_currentTask);
        _currentTask = null;

        if (_tasks.Count > 0)
        {
            StartNextTask();
        }
    }
}
