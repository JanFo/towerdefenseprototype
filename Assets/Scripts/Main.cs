using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public int port = 1235;

    [SerializeField]
    private GameConfiguration _gameConfiguration;

    [SerializeField]
    private LevelConfiguration _levelConfiguration;

    private World _world;

    private PlayerInput _input;

    private NetworkManager _network;

    private Player _player;

    private PlayerInputController _playerInputController;

    private Level _level;

    private GameController _gameController;

    private EntityViewController _entityViewManager;

    private CharacterActionController _characterActionController;

    private WorldSynchronizer _worldSynchronizer;

    private WorldController _worldController;
    private NavigationController _navigationController;
    private UserInterfaceController _userInterfaceController;

    private string _ipAddressString;

    private bool _isPlaying;

    // Start is called before the first frame update
    void Start()
    {
        _ipAddressString = "127.0.0.1";
        _gameConfiguration.Initialize();
        _input = GetComponent<PlayerInput>();
        _network = new NetworkManager();
        _player = new Player();

        _network.RegisterProcedures(this);
    }

    private void HostGame()
    {
        _network.Listen(port);

        _level = new Level(_levelConfiguration);
        _level.Load();

        _world = new World(_level, true);

        InitializeWorld();

        FindObjectOfType<PathfinderDebug>().ShowDebug(_world);

        InitializeCommonControllers();

        _gameController = new GameController(_world, _level);

        _isPlaying = true;
    }

    private void InitializeWorld()
    {
        _world.AddEntity(new Castle(_level));

        Barracks barracks = new Barracks();
        barracks.SetSpawnLocation(_level.playerSpawn);

        _world.AddEntity(barracks);

        _world.AddEntity(new Spy()
        {
            worldPosition = barracks.worldPosition
        });

        _world.AddEntity(new Demolisher()
        {
            worldPosition = barracks.worldPosition
        });

        _world.AddEntity(new Soldier()
        {
            worldPosition = barracks.worldPosition
        });

        _world.AddEntity(new Anchorman()
        {
            worldPosition = barracks.worldPosition
        });

        foreach (SpawnLocation spawnLocation in _level.spawnLocations)
        {
            _world.AddEntity(new Spawn()
            {
                x = spawnLocation.position.x,
                y = spawnLocation.position.y,
                name = spawnLocation.name
            });
        }
    }

    private void JoinGame()
    {
        _network.Connect(IPAddress.Parse(_ipAddressString), port);
        _network.OnConnectedToHost.AddListener(OnConnectedToHost);
    }

    private void OnConnectedToHost()
    {
        _level = new Level(_levelConfiguration);
        _level.Load();

        _world = new World(_level, false);

        FindObjectOfType<PathfinderDebug>().ShowDebug(_world);
        InitializeCommonControllers();
        _isPlaying = true;

        Debug.Log("Connected to Host! Sending Ready-Message!");
        IMessageWriter writer = _network.AppendMessage("OnClientReadyToPlay", true);
        writer.Send();
    }

    [HostProcedure]
    private void OnClientReadyToPlay(IMessageReader reader)
    {
        Debug.Log("Ready message received!");
        _worldSynchronizer.Resend();
    }

    /// <summary>
    /// Controllers that are being run on ALL clients (including host)
    /// </summary>
    private void InitializeCommonControllers()
    {
        Canvas canvas = FindObjectOfType<Canvas>();

        _player.index = _network.ClientId;

        // Manages all scene representations of entities. Can be used to retrieve GameObjects for entities
        _entityViewManager = new EntityViewController(_world);

        // Handles player input
        _playerInputController = new PlayerInputController(_player, _input, _world);

        // Runs the character actions
        _characterActionController = new CharacterActionController(_world, _entityViewManager, canvas);

        // Synchronizes the world between clients
        _worldSynchronizer = new WorldSynchronizer(_world, _network);

        // Keeps the world grid in sync with the world entities
        _worldController = new WorldController(_world);

        _navigationController = new NavigationController(_world, _level);

        //_worldGridController = new WorldGridController(_world);
        // Makes the bang bang visuals
        // _projectileController = new ProjectileController(_world);

        // User interface controller
        _userInterfaceController = new UserInterfaceController(_player, _world, _input, canvas);
    }

    // Update is called once per frame
    void Update()
    {
        _network.Update();

        if(!_isPlaying)
        {
            return;
        }

        if (_network.IsHost)
        {
            _gameController.Update();
        }

        _navigationController.Update();
        _playerInputController.Update(Time.deltaTime);
        _characterActionController.Update();
        _entityViewManager.Update();
        _worldSynchronizer.Update();
        _userInterfaceController.Update();
        _worldController.Update();
    }


    private void OnGUI()
    {
        if (_network.IsConnected)
        {
            if (GUI.Button(new Rect(10, 35, 200, 30), "Express joy"))
            {
                Debug.Log("HORRAYY!");
            }
        }

        if (_network.IsActive || _network.IsConnecting)
        {
            if (GUI.Button(new Rect(10, 35, 200, 30), "Cancel"))
            {
                _network.Close();
            }

            if (GUI.Button(new Rect(10, 130, 200, 30), "Debug Build"))
            {
                StartCoroutine(BuildMania());
            }

            return;
        }


        _ipAddressString = GUI.TextField(new Rect(10, 10, 200, 20), _ipAddressString, 25);

        if (GUI.Button(new Rect(10, 35, 200, 30), "Host"))
        {
            HostGame();
        }

        if (GUI.Button(new Rect(10, 80, 200, 30), "Join"))
        {
            JoinGame();
        }

    
    }

    private IEnumerator BuildMania()
    {
        int size = 16;

        while (true)
        {
            for (int i = 0; i < 500; i++)
            {
                Tower tower = ArrayUtils.GetRandom(GameConfiguration.instance.towerConfigurations).CreateTower();
                tower.x = Mathf.RoundToInt(UnityEngine.Random.value * (size - 1));
                tower.y = Mathf.RoundToInt(UnityEngine.Random.value * (size - 1));
                tower.constructionProgress = 1.0f;

                if (!_world.grid.GetNode(tower.x, tower.y).isBuildable)
                {
                    continue;
                }

                if (_world.grid.GetNode(tower.x, tower.y).entityId != 0)
                {
                    continue;
                }

                // Debug.Log("Building Tower " + GameConfiguration.instance.towerConfigurations[tower.towerId].name + ".");

                _world.AddEntity(tower);

               //  Debug.Log("Back from callback handlers.");

                yield return new WaitForSeconds(0.01f);
            }

            List<Tower> towers = _world.towers.ToList();


            Debug.Log("Clearing Towers.");
            foreach (Tower tower in towers)
            {
                _world.RemoveEntity(tower);
            }

            yield return new WaitForSeconds(0.01f);
        }
    }
}
