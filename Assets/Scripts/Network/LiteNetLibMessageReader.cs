﻿using System;
using LiteNetLib;
using UnityEngine;

public class LiteNetLibMessageReader : IMessageReader
{
    private NetPacketReader reader;
    private byte operation;

    public NetPeer Sender { get; private set; }

    public IMessageReader ReadMessage(NetPacketReader reader, NetPeer sender) 
    {
        Sender = sender;
        this.reader = reader;

        return this;
    }

    public bool ReadBool()
    {
        return reader.GetBool();
    }

    public bool[] ReadBoolArray()
    {
        return reader.GetBoolArray();
    }

    public bool[,] ReadBoolMatrix()
    {
        throw new NotImplementedException();
    }

    public byte ReadByte()
    {
        return reader.GetByte();
    }

    public byte[] ReadBytes(int length)
    {
        return null; // return reader.GetBytes(length);
    }

    public DateTime ReadDate()
    {
        throw new NotImplementedException();
    }

    public float ReadFloat()
    {
        return reader.GetFloat();
    }

    public Guid ReadGuid()
    {
        throw new NotImplementedException();
    }

    public int ReadInt()
    {
        return reader.GetInt();
    }

    public int[] ReadIntArray()
    {
        return reader.GetIntArray();
    }

    public short ReadShort()
    {
        return reader.GetShort();
    }

    public short[] ReadShortArray()
    {
        return reader.GetShortArray();
    }

    public sbyte ReadSignedByte()
    {
        throw new NotImplementedException();
    }

    public string ReadString()
    {
        return reader.GetString();
    }

    public string[] ReadStringArray()
    {
        return reader.GetStringArray();
    }

    public uint ReadUInt()
    {
        return reader.GetUInt();
    }

    public uint[] ReadUIntArray()
    {
        return reader.GetUIntArray();
    }

    public float[] ReadFloatArray()
    {
        return reader.GetFloatArray();
    }

    public double[] ReadDoubleArray()
    {
        return reader.GetDoubleArray();
    }

    public long[] ReadLongArray()
    {
        return reader.GetLongArray();
    }

    public Vector3 ReadVector3()
    {
        return new Vector3(ReadFloat(), ReadFloat(), ReadFloat());
    }

    public Vector3Int ReadIntVector3()
    {
        return new Vector3Int(ReadInt(), ReadInt(), ReadInt());
    }

    public double ReadDouble()
    {
        return reader.GetDouble();
    }

    public long ReadLong()
    {
        return reader.GetLong();
    }
}
