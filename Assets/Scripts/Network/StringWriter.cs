﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class StringWriter : IMessageWriter
{
    private List<byte> data;

    public int ByteCount => data.Count;

    public String Text
    {
        get { return Convert.ToBase64String(data.ToArray()); }
    }

    public StringWriter()
    {
        data = new List<byte>();
    }

    public void WriteFloat(float f)
    {
        appendBytes(BitConverter.GetBytes(f));
    }

    public void WriteString(string s)
    {
        if (s == null)
        {
            s = "";
        }

        appendBytes(BitConverter.GetBytes(s.Length));
        appendBytes(ByteConverter.GetBytes(s));
    }

    public void WriteInt(int OwnerId)
    {
        appendBytes(BitConverter.GetBytes(OwnerId));
    }

    public void WriteUInt(uint id)
    {
        appendBytes(BitConverter.GetBytes(id));
    }

    public void WriteBool(bool b)
    {
        appendBytes(BitConverter.GetBytes(b));
    }

    public void WriteBytes(byte[] p)
    {
        appendBytes(p);
    }

    public void WriteByte(byte p)
    {
        appendBytes(p);
    }

    public void WriteDouble(double d)
    {
        appendBytes(BitConverter.GetBytes(d));
    }

    public void WriteLong(long l)
    {
        appendBytes(BitConverter.GetBytes(l));
    }

    public void WriteSignedByte(sbyte p)
    {
        appendBytes((byte)(p + 128));
    }

    public void WriteGuid(Guid guid)
    {
        appendBytes(guid.ToByteArray());
    }



    public void WriteStringArray(string[] array)
    {
        if (array == null)
        {
            array = new string[0];
        }

        WriteInt(array.Length);

        foreach (string s in array)
        {
            WriteString(s);
        }
    }

    public void WriteIntArray(int[] array)
    {
        if (array == null)
        {
            array = new int[0];
        }

        WriteInt(array.Length);

        foreach (int s in array)
        {
            WriteInt(s);
        }
    }

    private void appendBytes(params byte[] p)
    {
        data.AddRange(p);
    }

    public void WriteDate(DateTime dateTime)
    {
        appendBytes(BitConverter.GetBytes(dateTime.ToBinary()));
    }

    public void WriteShort(short s)
    {
        appendBytes(BitConverter.GetBytes(s));
    }

    public void WriteBoolArray(bool[] array)
    {
        if (array == null)
        {
            array = new bool[0];
        }

        WriteInt(array.Length);

        foreach (bool s in array)
        {
            WriteBool(s);
        }
    }

    public void WriteBoolMatrix(bool[,] unlocks)
    {
        throw new NotImplementedException();
    }

    public void WriteShortArray(short[] array)
    {
        if (array == null)
        {
            array = new short[0];
        }

        WriteInt(array.Length);

        foreach (short s in array)
        {
            WriteShort(s);
        }
    }

    public void Send()
    {
        throw new NotImplementedException();
    }

    public void WriteUIntArray(uint[] array)
    {
        if (array == null)
        {
            array = new uint[0];
        }

        WriteInt(array.Length);

        foreach (uint s in array)
        {
            WriteUInt(s);
        }
    }

    public void WriteFloatArray(float[] array)
    {
        if (array == null)
        {
            array = new float[0];
        }

        WriteInt(array.Length);

        foreach (float s in array)
        {
            WriteFloat(s);
        }
    }

    public void WriteDoubleArray(double[] array)
    {
        if (array == null)
        {
            array = new double[0];
        }

        WriteInt(array.Length);

        foreach (double s in array)
        {
            WriteDouble(s);
        }
    }

    public void WriteLongArray(long[] array)
    {
        if (array == null)
        {
            array = new long[0];
        }

        WriteInt(array.Length);

        foreach (long s in array)
        {
            WriteLong(s);
        }
    }

    public void WriteVector3(Vector3 point)
    {
        WriteFloat(point.x);
        WriteFloat(point.y);
        WriteFloat(point.z);
    }

    public void WriteIntVector3(Vector3Int point)
    {
        WriteInt(point.x);
        WriteInt(point.y);
        WriteInt(point.z);
    }
}
