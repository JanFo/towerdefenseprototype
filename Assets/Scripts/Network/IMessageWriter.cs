﻿using System;
using UnityEngine;

public interface IMessageWriter
{
    void WriteFloat(float f);

    void WriteString(string s);

    void WriteInt(int OwnerId);

    void WriteShort(short s);

    void WriteDouble(double d);

    void WriteLong(long l);

    void WriteShortArray(short[] array);

    void WriteBool(bool b);

    void WriteBoolMatrix(bool[,] unlocks);

    void WriteBytes(byte[] p);

    void WriteByte(byte p);

    void WriteSignedByte(sbyte p);

    void WriteGuid(Guid guid);

    void WriteStringArray(string[] array);

    void WriteIntArray(int[] array);

    void WriteFloatArray(float[] array);

    void WriteDoubleArray(double[] array);

    void WriteLongArray(long[] array);

    void WriteDate(DateTime dateTime);

    void WriteBoolArray(bool[] skills);

    void Send();

    void WriteUInt(uint id);

    void WriteUIntArray(uint[] playerCharacterFrameGuids);

    void WriteVector3(Vector3 point);

    void WriteIntVector3(Vector3Int point);
}
