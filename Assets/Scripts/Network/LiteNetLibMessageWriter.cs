﻿using System;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;


public class LiteNetLibMessageWriter : IMessageWriter
{
    public NetDataWriter NetDataWriter { get; private set; }

    public Action SendCallback { get; internal set; }

    public LiteNetLibMessageWriter()
    {
        NetDataWriter = new NetDataWriter(true);
    }

    public IMessageWriter WriteMessage(byte operation) 
    {
        NetDataWriter.Reset();
        NetDataWriter.Put(operation);

        return this;
    }

    public void WriteBool(bool b)
    {
        NetDataWriter.Put(b);
    }

    public void WriteBoolArray(bool[] skills)
    {
        NetDataWriter.PutArray(skills);
    }

    public void WriteBoolMatrix(bool[,] unlocks)
    {
        throw new NotImplementedException();
    }

    public void WriteByte(byte p)
    {
        NetDataWriter.Put(p);
    }

    public void WriteBytes(byte[] p)
    {
        NetDataWriter.PutBytesWithLength(p);
    }

    public void WriteDate(DateTime dateTime)
    {
        throw new NotImplementedException();
    }

    public void WriteFloat(float f)
    {
        NetDataWriter.Put(f);
    }

    public void WriteGuid(Guid guid)
    {
        throw new NotImplementedException();
    }

    public void WriteInt(int OwnerId)
    {
        NetDataWriter.Put(OwnerId);
    }

    public void WriteIntArray(int[] array)
    {
        NetDataWriter.PutArray(array);
    }

    public void WriteFloatArray(float[] array)
    {
        NetDataWriter.PutArray(array);
    }

    public void WriteDoubleArray(double[] array)
    {
        NetDataWriter.PutArray(array);
    }

    public void WriteLongArray(long[] array)
    {
        NetDataWriter.PutArray(array);
    }

    public void WriteShort(short s)
    {
        NetDataWriter.Put(s);
    }

    public void WriteShortArray(short[] array)
    {
        NetDataWriter.PutArray(array);
    }

    public void WriteSignedByte(sbyte p)
    {
        NetDataWriter.Put(p);
    }

    public void WriteString(string s)
    {
        NetDataWriter.Put(s);
    }

    public void WriteStringArray(string[] array)
    {
        NetDataWriter.PutArray(array);
    }

    public void Send()
    {
        SendCallback.Invoke();
    }

    public void WriteUInt(uint id)
    {
        NetDataWriter.Put(id);
    }

    public void WriteUIntArray(uint[] array)
    {
        NetDataWriter.PutArray(array);
    }

    public void WriteVector3(Vector3 point)
    {
        NetDataWriter.Put(point.x);
        NetDataWriter.Put(point.y);
        NetDataWriter.Put(point.z);
    }

    public void WriteIntVector3(Vector3Int point)
    {
        NetDataWriter.Put(point.x);
        NetDataWriter.Put(point.y);
        NetDataWriter.Put(point.z);
    }

    public void WriteDouble(double s)
    {
        NetDataWriter.Put(s);
    }

    public void WriteLong(long l)
    {
        NetDataWriter.Put(l);
    }
}
