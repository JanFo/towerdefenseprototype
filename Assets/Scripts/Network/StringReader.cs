﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LiteNetLib;
using UnityEngine;

public class StringReader : IMessageReader
{
    private byte[] data;
    private int index;
    private string text;

    public NetPeer Sender => throw new NotImplementedException();

    public StringReader(string text)
    {
        this.data = Convert.FromBase64String(text);
        this.index = 0;
        this.text = text;
    }

    public byte GetOperation()
    {
        return 0;
    }

    public int GetSender()
    {
        return 0;
    }

    public int GetLength()
    {
        return data.Length;
    }

    public IMessageReader Clone()
    {
        return new StringReader(text);
    }

    public int ReadInt()
    {
        int i = BitConverter.ToInt32(data, index);
        index += 4;

        return i;
    }

    public int Index => index;

    public uint ReadUInt()
    {
        uint i = BitConverter.ToUInt32(data, index);
        index += 4;

        return i;
    }

    public short ReadShort()
    {
        short i = BitConverter.ToInt16(data, index);
        index += 2;
        return i;
    }

    public byte ReadByte()
    {
        byte b = data[index];
        index += 1;
        return b;
    }

    public byte[] ReadBytes(int length)
    {
        byte[] b = new byte[length];

        for (int i = 0; i < length; i++)
        {
            b[i] = data[index];
            index += 1;
        }

        return b;
    }


    public bool ReadBool()
    {
        bool b = BitConverter.ToBoolean(data, index);
        index += 1;
        return b;
    }

    public sbyte ReadSignedByte()
    {
        byte c = (byte)data[index];
        index += 1;
        return (sbyte)(c - 128);
    }

    public Guid ReadGuid()
    {
        byte[] guidData = new byte[16];

        for (int i = 0; i < 16; i++)
        {
            guidData[i] = data[index + i];
        }

        index += 16;
        return new Guid(guidData);
    }

    public float ReadFloat()
    {
        float f = BitConverter.ToSingle(data, index);
        index += 4;
        return f;
    }

    public double ReadDouble()
    {
        double d = BitConverter.ToDouble(data, index);
        index += 8;
        return d;
    }

    public long ReadLong()
    {
        long l = BitConverter.ToInt64(data, index);
        index += 8;
        return l;
    }

    public bool[] ReadBoolArray()
    {
        bool[] result = new bool[ReadInt()];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = ReadBool();
        }

        return result;
    }

    public string ReadString()
    {
        int length = BitConverter.ToInt32(data, index);
        index += 4;

        string result = ByteConverter.ToString(data, index, length);
        index += length;
        return result;
    }

    public string[] ReadStringArray()
    {
        string[] result = new string[ReadInt()];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = ReadString();
        }

        return result;
    }

    public int[] ReadIntArray()
    {
        int[] result = new int[ReadInt()];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = ReadInt();
        }

        return result;
    }



    public DateTime ReadDate()
    {
        long dbl = BitConverter.ToInt64(data, index);
        index += 8;
        return DateTime.FromBinary(dbl);
    }

    public bool[,] ReadBoolMatrix()
    {
        return null;
    }

    public short[] ReadShortArray()
    {
        short[] result = new short[ReadInt()];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = ReadShort();
        }

        return result;
    }

    public uint[] ReadUIntArray()
    {
        uint[] result = new uint[ReadInt()];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = ReadUInt();
        }

        return result;
    }

    public float[] ReadFloatArray()
    {
        float[] result = new float[ReadInt()];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = ReadFloat();
        }

        return result;
    }

    public double[] ReadDoubleArray()
    {
        double[] result = new double[ReadInt()];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = ReadDouble();
        }

        return result;
    }

    public long[] ReadLongArray()
    {
        long[] result = new long[ReadInt()];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = ReadLong();
        }

        return result;
    }

    public Vector3 ReadVector3()
    {
        return new Vector3(
            ReadFloat(),
            ReadFloat(),
            ReadFloat());
    }

    public Vector3Int ReadIntVector3()
    {
        return new Vector3Int(
            ReadInt(),
            ReadInt(),
            ReadInt());
    }
}

