﻿using LiteNetLib;
using System;
using UnityEngine;

public interface IMessageReader
{
    NetPeer Sender { get; }

    int ReadInt();

    bool ReadBool();

    short ReadShort();

    short[] ReadShortArray();

    float ReadFloat();

    double ReadDouble();

    long ReadLong();

    string ReadString();

    byte ReadByte();

    byte[] ReadBytes(int length);

    sbyte ReadSignedByte();

    Guid ReadGuid();

    string[] ReadStringArray();

    int[] ReadIntArray();

    float[] ReadFloatArray();

    double[] ReadDoubleArray();

    long[] ReadLongArray();

    DateTime ReadDate();

    bool[] ReadBoolArray();

    bool[,] ReadBoolMatrix();

    uint ReadUInt();

    uint[] ReadUIntArray();

    Vector3 ReadVector3();

    Vector3Int ReadIntVector3();
}
