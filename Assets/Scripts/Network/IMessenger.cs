﻿public interface IMessenger
{
    bool IsHost { get; }

    IMessageWriter AppendMessage(string handlerFunctionName, bool isReliable = false);
}
