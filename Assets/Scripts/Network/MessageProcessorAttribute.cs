﻿using System;


[AttributeUsage(AttributeTargets.Method)]
public class ClientProcedureAttribute : Attribute
{
    public bool ProcessOnlyKnownClients { get; private set; }

    public ClientProcedureAttribute(bool processOnlyKnownClients = true)
    {
        ProcessOnlyKnownClients = processOnlyKnownClients;
    }
}

[AttributeUsage(AttributeTargets.Method)]
public class HostProcedureAttribute : Attribute
{
    public bool ProcessOnlyKnownClients { get; private set; }

    public HostProcedureAttribute(bool processOnlyKnownClients = true)
    {
        ProcessOnlyKnownClients = processOnlyKnownClients;
    }
}