﻿using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine.Events;
using System.Linq;
using UnityEngine;
using System.Reflection;
using LiteNetLib;
using System.Net.Sockets;
using LiteNetLib.Utils;

public enum SendMode
{
    Response,
    All,
}


public class NetworkManager : INetEventListener, IMessenger
{
    private List<IPEndPoint> lostRemoteSenderEndpoints;

    public bool IsActive { get; private set; }

    public bool IsHost { get; private set; }

    public string Name
    {
        get
        {
            if (ClientId == -1)
            {
                return "Unassigned";
            }

            if (ClientId == 0)
            {
                return "Master";
            }

            return "Client " + ClientId;
        }
    }

    public short ClientId { get; private set; }

    public void Disconnect()
    {
        manager.Stop();
        manager.DisconnectAll();
    }

    private short hostClientId;

    private short clientIdCounter;

    public bool IsConnecting { get; private set; }

    private Dictionary<byte, Action<IMessageReader>> hostProcessorMap;

    private Dictionary<byte, Action<IMessageReader>> clientProcessorMap;

    private Dictionary<string, byte> operationMap;

    private Dictionary<byte, string> inverseOperationMap;

    private Dictionary<short, NetPeer> remotePeers;

    private byte operationCounter;

    private NetManager manager;

    public UnityEvent OnConnectionFailed { get; private set; }

    public bool IsConnected => remotePeers.Count > 0;

    private LiteNetLibMessageReader messageReader;

    private LiteNetLibMessageWriter messageWriter;

    private NetPacketProcessor netPacketProcessor;

    private NetPeer currentPeer;

    public UnityEvent OnConnectedToHost { get; private set; }

    public NetworkManager()
    {
        manager = new NetManager(this);

        OnConnectedToHost = new UnityEvent();

        messageWriter = new LiteNetLibMessageWriter();
        messageReader = new LiteNetLibMessageReader();

        hostClientId = 0;
        clientIdCounter = (short)(hostClientId + 1);

        operationCounter = 1;

        OnConnectionFailed = new UnityEvent();
        ClientId = -1;
        // OnLostConnection = new IntEvent();

        remotePeers = new Dictionary<short, NetPeer>();
        hostProcessorMap = new Dictionary<byte, Action<IMessageReader>>();
        clientProcessorMap = new Dictionary<byte, Action<IMessageReader>>();
        operationMap = new Dictionary<string, byte>();
        inverseOperationMap = new Dictionary<byte, string>();

        IsActive = false;
        IsConnecting = false;

        RegisterProcedures(this);
    }

    public void Listen(int port)
    {
        IsHost = true;
        ClientId = hostClientId;
        manager.Stop();
        manager.Start(port);
        IsActive = true;
        Debug.Log($"[{Name}] Listening on port {port}.");
    }

    /// <summary>
    ///   INetEventListener OnPeerConnected
    /// </summary>
    /// <param name="peer"></param>
    public void OnPeerConnected(NetPeer peer)
    {
      
        if(IsHost)
        {
            short remotePeerId = clientIdCounter++;
            Debug.Log($"[{Name}] A client connected. Assigning client ID {remotePeerId}.");

            IMessageWriter message = AppendMessage("OnClientIdAssigned", true, peer);
            message.WriteShort(remotePeerId);
            message.Send();

            peer.Tag = remotePeerId;
        }
    }

    /// <summary>
    /// INetEventListener connection accept condition
    /// </summary>
    /// <param name="request"></param>
    public void OnConnectionRequest(ConnectionRequest request)
    {
        Debug.Log($"[{Name}] Received a connection request.");
        request.AcceptIfKey("TowerDefensePrototype");
    }


    /// <summary>
    /// Connects to a remote client
    /// </summary>
    /// <param name="address"></param>
    /// <param name="port"></param>
    public void Connect(IPAddress address, int port)
    {
        Debug.Log($"[{Name}] Connecting to {address}:{port}.");
        IsConnecting = true;

        manager.Start();
        manager.Connect(new IPEndPoint(address, port), "TowerDefensePrototype");
    }

    /// <summary>
    /// Receives the answer to Connect
    /// </summary>
    /// <param name="reader"></param>
    [ClientProcedure]
    private void OnClientIdAssigned(IMessageReader reader)
    {
        ClientId = reader.ReadShort();

        IsConnecting = false;
        IsActive = true;
        Debug.Log($"[{Name}] Received client ID {ClientId}. I am now a client!");

        OnConnectedToHost.Invoke();
    }

    /// <summary>
    /// INetEventListener OnPeerDisconnected
    /// </summary>
    /// <param name="peer"></param>
    /// <param name="disconnectInfo"></param>
    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        // OnLostConnection.Invoke(peer.Id);
    }

    /// <summary>
    /// INetEventListener OnNetworkReceive
    /// </summary>
    /// <param name="peer"></param>
    /// <param name="reader"></param>
    /// <param name="deliveryMethod"></param>
    public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        byte operation = reader.GetByte();
        messageReader.ReadMessage(reader, peer);

        var actionMap = IsHost ? hostProcessorMap : clientProcessorMap;
        ArrayUtils.Get(actionMap, operation, null)?.Invoke(messageReader);
    }


    /// <summary>
    /// INetEventListener OnNetworkError
    /// </summary>
    /// <param name="endPoint"></param>
    /// <param name="socketError"></param>
    public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
    {

    }

    /// <summary>
    /// INetEventListener unused
    /// </summary>
    /// <param name="remoteEndPoint"></param>
    /// <param name="reader"></param>
    /// <param name="messageType"></param>
    public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, 
        UnconnectedMessageType messageType)
    {
        
    }

    /// <summary>
    /// INetEventListener unused
    /// </summary>
    /// <param name="peer"></param>
    /// <param name="latency"></param>
    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        
    }
 
    /// <summary>
    /// Closes the connection to all remote clients
    /// </summary>
    public void Close()
    {
        manager.DisconnectAll();
        IsActive = false;
        IsConnecting = false;
    }

    public IMessageWriter AppendMessage(Action<IMessageReader> handlerFunction, bool isReliable = false,
       NetPeer peer = null)
    {
        return AppendMessage(handlerFunction.Method.Name, isReliable, peer);
    }

    public IMessageWriter AppendMessage(string handlerFunctionName, bool isReliable = false,
        NetPeer peer = null)
    {
        byte operation = operationMap[handlerFunctionName];
        DeliveryMethod method = isReliable ? DeliveryMethod.ReliableOrdered : DeliveryMethod.Unreliable;

        messageWriter.WriteMessage(operation);
        messageWriter.SendCallback = () => Send(messageWriter.NetDataWriter, peer, method);

        return messageWriter;
    }

    private void Send(NetDataWriter writer, NetPeer peer = null, DeliveryMethod method = DeliveryMethod.Unreliable)
    {
        if (peer == null)
        {
            manager.SendToAll(writer, method);
        }
        else 
        {
            peer.Send(writer, method);
        }
    }

    /// <summary>
    /// Has to be called every frame in order to update the net manager
    /// </summary>
    public void Update()
    {
        if(manager.IsRunning)
        {
            manager.PollEvents();
        }
    }

    public void RegisterProcedures(object processor)
    {
        Type type = processor.GetType();
        MethodInfo[] info = type.GetMethods(BindingFlags.NonPublic | BindingFlags.FlattenHierarchy | BindingFlags.Instance);

        foreach (MethodInfo methodInfo in info)
        {
            ClientProcedureAttribute clientAttribute = methodInfo.GetCustomAttribute<ClientProcedureAttribute>(true);
            HostProcedureAttribute hostAttribute = methodInfo.GetCustomAttribute<HostProcedureAttribute>(true);

            if (clientAttribute == null && hostAttribute == null)
            {
                continue;
            }

            Action<IMessageReader> action = (Action<IMessageReader>)
                Delegate.CreateDelegate(typeof(Action<IMessageReader>), processor, methodInfo);

            byte operation;

            if (operationMap.ContainsKey(methodInfo.Name))
            {
                Debug.LogWarning("Trying to register duplicate message processor " + methodInfo.Name + ". " +
                    "The old action will be overwritten.");
                operation = operationMap[methodInfo.Name];
            }
            else
            {
                operation = operationCounter++;
            }

           
            operationMap[methodInfo.Name] = operation;
            inverseOperationMap[operation] = methodInfo.Name;

            if (clientAttribute != null)
            {
                clientProcessorMap[operation] = action;
            }

            if (hostAttribute != null)
            {
                hostProcessorMap[operation] = action;
            }
        }

        foreach(var p in clientProcessorMap)
        {
            Debug.Log($"[{Name}] Registered Client Procedure {p.Key}: {inverseOperationMap[p.Key]}.");
        }

        foreach (var p in hostProcessorMap)
        {
            Debug.Log($"[{Name}] Registered Host Procedure {p.Key}: {inverseOperationMap[p.Key]}.");
        }
    }

    public void UnregisterMessageProcessor(object processor)
    { 

    }

    public IMessageWriter AppendMessage(string handlerFunctionName, bool isReliable = false)
    {
        return AppendMessage(handlerFunctionName, isReliable, null);
    }
}
