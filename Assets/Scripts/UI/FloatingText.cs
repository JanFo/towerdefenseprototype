using TMPro;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    private static GameObjectPool<FloatingText> _pool;

    private static Canvas _canvas;

    private TMP_Text _textMesh;

    private RectTransform _transform;

    private Color _color;

    private float _alpha;

    private Vector3 _position;

    private float _offset;

    public static void Initialize()
    {
        _pool = new GameObjectPool<FloatingText>(OnCreateFloatingText);
        _canvas = FindObjectOfType<Canvas>();
    }

    private static FloatingText OnCreateFloatingText()
    {
        GameObject obj = new GameObject("Floating_Text");
        FloatingText text = obj.AddComponent<FloatingText>();
        text._textMesh = obj.AddComponent<TextMeshProUGUI>();
        text._transform = obj.GetComponent<RectTransform>();
        text._transform.anchorMin = Vector2.zero;
        text._transform.anchorMax = Vector2.zero;
        text._textMesh.alignment = TextAlignmentOptions.Center;
        return text;
    }

    private void Start()
    {

    }

    private void Update()
    {
        _alpha -= Time.deltaTime;
        _color.a = Mathf.Clamp01(_alpha);

        _textMesh.color = _color;
        _offset += Time.deltaTime * 50.0f;

        Vector3 screenPoint = Camera.main.WorldToScreenPoint(_position + Vector3.up);
        _transform.anchoredPosition = new Vector2(screenPoint.x, screenPoint.y + _offset) / _canvas.scaleFactor;

        if (_alpha <= 0.0f)
        {
            _pool.Release(this);
        }
    }

    public static void Create(Vector3 position, string text, Color color, float duration = 0.5f)
    {
        if(_pool == null)
        {
            Initialize();
        }

        FloatingText floatingText = _pool.GetInstance();
        floatingText._textMesh.text = text;
        floatingText._textMesh.color = color;
        floatingText._color = color;
        floatingText._alpha = 1.0f + duration;
        floatingText._position = position;
        floatingText.transform.SetParent(_canvas.transform);
        floatingText._offset = 0.0f;

        Vector3 screenPoint = Camera.main.WorldToScreenPoint(position + Vector3.up);
        floatingText._transform.anchoredPosition = new Vector2(screenPoint.x, screenPoint.y) / _canvas.scaleFactor;
    }
}
