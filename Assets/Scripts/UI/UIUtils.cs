﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using System;
using UnityEditor;

public class UIUtils
{
    public static T GetComponentInParent<T>(Transform transform) where T : Behaviour
    {
        T[] parents = transform.GetComponentsInParent<T>(true);

        if (parents.Length > 0)
        {
            return parents[0];
        }

        return null;
    }


    public static Rect GetWorldRect(RectTransform rt, Canvas canvas)
    {
        if (canvas == null)
        {
            return new Rect();
        }

        Vector3[] corners = new Vector3[4];
        rt.GetWorldCorners(corners);

        return new Rect(corners[0].x, corners[0].y, corners[3].x - corners[0].x, corners[1].y - corners[0].y);
    }

    internal static bool IsDestroyed(UISelectable s)
    {
        throw new NotImplementedException();
    }

    internal static Rect GetWorldRectUnscaled(RectTransform rt, Canvas canvas)
    {
        if (canvas == null)
        {
            return new Rect();
        }

        Vector3[] corners = new Vector3[4];
        rt.GetWorldCorners(corners);

        for(int i = 0; i < corners.Length; i++)
        {
            corners[i] = corners[i] / canvas.scaleFactor;
        }

        //// Rescale the size appropriately based on the current Canvas scale
        //Vector2 scaledSize = new Vector2(canvas.scaleFactor * rt.rect.size.x + 1,
        //    canvas.scaleFactor * rt.rect.size.y + 1);

        //Rect rect = rt.rect;
        //rect.size = rect.size * canvas.scaleFactor;
        //rect.position = rect.position * canvas.scaleFactor;


        return new Rect(corners[0].x, corners[0].y, corners[3].x - corners[0].x, corners[1].y - corners[0].y);
    }

    public static int RepeatIndex(int v, int length)
    {
        throw new NotImplementedException();
    }
}