﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UIPlaceholderImage : MonoBehaviour
{
    [SerializeField]
    private Sprite placeHolderImage = null;

    private Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    public Sprite Sprite
    {
        get
        {
            return image.sprite ?? placeHolderImage;
        }
        set
        {
            image.sprite = value ?? placeHolderImage;
        }
    }
}
