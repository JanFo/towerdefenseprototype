﻿using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Style", menuName = "UI/Class")]
public class UIClass : ScriptableObject
{

    [SerializeField]
    private Vector4 padding;

    [Header("Background")]

    [SerializeField]
    private Sprite backgroundSprite = null;

    [SerializeField]
    private Image.Type backgroundType = Image.Type.Sliced;

    [SerializeField]
    private bool backgroundFillCenter = true;

    [SerializeField]
    private int backgroundPixelsPerUnitMultiplier = 8;

    [SerializeField]
    private Color backgroundColorNormal = Color.white;

    [SerializeField]
    private Color backgroundColorSelected = Color.white;

    [SerializeField]
    private Color backgroundColorDisabled = Color.white;

    [SerializeField]
    private Color backgroundColorPressed = Color.white;

    [Header("Text")]

    [SerializeField]
    private TMP_FontAsset textFont = null;

    [SerializeField]
    private int textFontSize = 32;

    [SerializeField]
    private Color textColorNormal = Color.white;

    [SerializeField]
    private Color textColorSelected = Color.white;

    [SerializeField]
    private Color textColorDisabled = Color.white;

    [SerializeField]
    private Color textColorPressed = Color.white;


    public void Apply(GameObject gameObject)
    {
        Image image = gameObject.GetComponent<Image>();

        if(image != null)
        {
            image.sprite = backgroundSprite;
            image.type = backgroundType;
            image.fillCenter = backgroundFillCenter;
            image.pixelsPerUnitMultiplier = backgroundPixelsPerUnitMultiplier;
            image.color = backgroundColorNormal;

            UITintableImage tintableImage = image.GetComponent<UITintableImage>();

            if(tintableImage != null)
            {
                tintableImage.Color = backgroundColorNormal;
                tintableImage.Pressed = backgroundColorPressed;
                tintableImage.Selected = backgroundColorSelected;
                tintableImage.Disabled = backgroundColorDisabled;
                tintableImage.SelectedDisabled = Color.Lerp(backgroundColorDisabled, backgroundColorSelected, 0.5f);
            }
        }


        TMP_Text text = gameObject.GetComponent<TMP_Text>();

        if(text != null)
        {
            text.font = textFont;
            text.color = textColorNormal;
            text.fontSize = textFontSize;

            UITintableImage tintableImage = text.GetComponent<UITintableImage>();

            if (tintableImage != null)
            {
                tintableImage.Color = textColorNormal;
                tintableImage.Pressed = textColorPressed;
                tintableImage.Selected = textColorSelected;
                tintableImage.Disabled = textColorDisabled;
                tintableImage.SelectedDisabled = Color.Lerp(textColorDisabled, textColorSelected, 0.5f);
            }
        }

        LayoutGroup layoutGroup = gameObject.GetComponent<LayoutGroup>();

        if (layoutGroup != null)
        {
            layoutGroup.padding = new RectOffset((int)padding.x, (int)padding.y, (int)padding.z, (int)padding.w);
        }
    }

}
