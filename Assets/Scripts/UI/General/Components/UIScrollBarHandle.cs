﻿using UnityEngine;

/// <summary>
/// A scroll bar for a scroll view
/// </summary>
public class UIScrollBarHandle : UISelectable
{
    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();

        Rect = GetComponent<RectTransform>();
    }

    public RectTransform Rect { get; private set; }

    public float Height
    {
        get
        {
            return Rect.sizeDelta.y;
        }
    }

    public float Position
    {
        get
        {
            return Rect.anchoredPosition.y;
        }
        set
        {
            Rect.anchoredPosition = new Vector2(Rect.anchoredPosition.x, value);
        }
    }
}
