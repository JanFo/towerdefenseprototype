﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UISpinner : MonoBehaviour {

    public float focusScale;

    public RectTransform contentTransform;

    public IPanel[] panels;

    [HideInInspector]
    public int index;

    private Canvas _canvas;
    private RectTransform _rectTransform;
    private Rect _frameRectagle;

    private void OnValidate()
    {
        if (contentTransform == null)
        {
            return;
        }

        UpdateRectangles();
    }

    public void SetPanels(IPanel[] panels)
    {

    }

    private void UpdateRectangles()
    {
        if (_canvas == null)
        {
            _canvas = GetComponentInParent<Canvas>();
        }

        if (_canvas == null)
        {
            return;
        }

        if (_rectTransform == null)
        {
            _rectTransform = GetComponent<RectTransform>();
        }

        _frameRectagle = UIUtils.GetWorldRectUnscaled(_rectTransform, _canvas);


    }

}
