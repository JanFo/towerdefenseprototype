﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using System.Linq;
//using System;
//using TMPro;
//using UnityEngine.Events;

//[ExecuteInEditMode]
//public class UITabControl : MonoBehaviour
//{
//    [SerializeField]
//    private UITab[] tabs = null;

//    private int activeTabIndex = 0;

//    public IEnumerable<UITab> Tabs
//    {
//        get
//        {
//            return tabs;
//        }
//    }

//    public UITab ActiveTab
//    {
//        get
//        {
//            return tabs[activeTabIndex];
//        }
//    }

//    public int ActiveTabIndex { get => activeTabIndex; set => activeTabIndex = value; }

//    public int TabCount => tabs.Length;

//    public void ShowTab(int index)
//    {
//        for (int i = 0; i < tabs.Length; i++)
//        {
//            tabs[i].Hide();
//        }

//        activeTabIndex = index;

//        tabs[activeTabIndex].Show();
//    }

//    //public void NextTab()
//    //{
//    //    ShowTab(SecondScourgeUtils.RepeatIndex(activeTabIndex + 1, tabs.Length));
//    //}

//    //public void PreviousTab()
//    //{
//    //    ShowTab(SecondScourgeUtils.RepeatIndex(activeTabIndex - 1, tabs.Length));
//    //}

//    internal void HideAll()
//    {
//        for (int i = 0; i < tabs.Length; i++)
//        {
//            if (tabs[i].IsVisible)
//            {
//                tabs[i].Hide();
//            }
//        }
//    }

//    internal void Disable(int v)
//    {
//        tabs[v].IsTabDisabled = true;
//    }
//}
