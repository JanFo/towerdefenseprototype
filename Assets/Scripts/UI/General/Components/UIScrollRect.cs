﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A scroll bar for a scroll view
/// </summary>
public class UIScrollRect : MonoBehaviour
{
    [SerializeField]
    private RectTransform contentTransform = null;

    [SerializeField, Range(0, 1)]
    private float scrollPosition = 0.0f;

    private Canvas canvas;

    private RectTransform rectTransform;

    private Rect frameRectagle;

    private Rect contentRectangle;

    private float contentFrameDelta;

    //private void HandlePointerDrag(UIInputEventArgs arg0)
    //{
    //    float delta = arg0.CursorPosition.y - mouseStartPosition;
    //    float barHeightDelta = rect.sizeDelta.y - handle.Rect.sizeDelta.y;

    //    scrollPosition = Mathf.Clamp01(startScrollPosition - delta / barHeightDelta);

    //}


    //private void HandlePointerDown(UIInputEventArgs arg0)
    //{
    //    startScrollPosition = scrollPosition;
    //    mouseStartPosition = arg0.CursorPosition.y;
    //}

    //public override void OnPointerDown(UIInputEventArgs eventArgs)
    //{
    //    base.OnPointerDown(eventArgs);

    //    if(eventArgs.ActionType == SCInterfaceActionType.Click)
    //    {
    //        float target = -(rect.InverseTransformPoint(Input.mousePosition).y - Height / 2.0f);
    //        scrollPosition = target / Height; 

    //        MoveHandle();
    //    }
    //}

    private void OnValidate()
    {
        if(contentTransform == null)
        {
            return;
        }

        UpdateRectangles();
        contentTransform.anchoredPosition = Vector2.up * contentFrameDelta * scrollPosition;
    }

    private void UpdateRectangles()
    {
        if (canvas == null)
        {
            canvas = GetComponentInParent<Canvas>();
        }

        if (canvas == null)
        {
            return;
        }

        if (rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
        }

        frameRectagle = UIUtils.GetWorldRectUnscaled(rectTransform, canvas);
        contentRectangle = UIUtils.GetWorldRectUnscaled(contentTransform, canvas);
        contentFrameDelta = contentRectangle.height - frameRectagle.height;
    }


    internal void ScrollToContent(RectTransform elementRectTransform)
    {
        if(contentTransform == null)
        {
            return;
        }

        UpdateRectangles();

        Rect contentElementRect = UIUtils.GetWorldRectUnscaled(elementRectTransform, canvas);
        float targetMax = contentRectangle.y + contentRectangle.height - contentElementRect.y;
        float targetMin = targetMax - contentElementRect.height;

        if (contentFrameDelta > 0.0f)
        {
            // float targetHeight = targetMin; // + scrollPadding;

            if (targetMax >= contentRectangle.height)
            {
                scrollPosition = 1.0f;
            } 
            else if(targetMin <= 0)
            {
                scrollPosition = 0.0f;
            }
            else
            {
                float visibleMin = Mathf.Lerp(0, contentFrameDelta, scrollPosition);
                float visibleMax = Mathf.Lerp(frameRectagle.height, contentRectangle.height, scrollPosition);

                if(targetMin < visibleMin)
                {
                    scrollPosition = targetMin / contentFrameDelta;
                }

                if(targetMax > visibleMax)
                {
                    scrollPosition = (targetMax - frameRectagle.height) / contentFrameDelta;
                }
                    
            }
        }
        else
        {
            scrollPosition = 0.0f;
        }

        contentTransform.anchoredPosition = Vector2.up * contentFrameDelta * scrollPosition;

    }

    //private float GetScrollPosition(int i, float contentHeightDelta, float elementHeight)
    //{
    //    float scrollAmount = i * elementHeight;
    //    return Mathf.Clamp01(scrollAmount / contentHeightDelta);
    //}


    //public float Height
    //{
    //    get
    //    {
    //        return rect.sizeDelta.y;
    //    }
    //}

}
