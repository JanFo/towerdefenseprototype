﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public interface IProgressProvider 
{
    float CurrentAmount { get; }
    float MaximumAmount { get; }
}

public class UIProgressBar : MonoBehaviour
{
    [SerializeField]
    private Image progressImage = null;

    public float Progress
    {
        set
        {
            if (progressImage != null)
                progressImage.fillAmount = value;

            
        }
    }

    
}
