﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;


/// <summary>
/// A simple UI button
/// </summary>
public sealed class UIButton : UISelectable
{
    /// <summary>
    /// A list of tintable images. These will be tinted according to the 
    /// state of the button
    /// </summary>
    [SerializeField]
    private List<UITintableImage> images = null;

    /// <summary>
    /// The duration that registeres as a long press on this button in seconds
    /// </summary>
    [SerializeField, Range(0, 10)]
    private float longPressDuration = 1.0f;

    /// <summary>
    /// The click event of the button
    /// </summary>
    [SerializeField, HideInInspector]
    private UISelectableEvent onClick = null;

    /// <summary>
    /// The long click event on the button
    /// </summary>
    [SerializeField, HideInInspector]
    private UISelectableEvent onLongClick = null;

    /// <summary>
    /// The cached selection state of the button (needed for update onDisable)
    /// </summary>
    private UISelectionState selectionState;

    /// <summary>
    /// A flag denoting if the button is long pressed to cancel a duplicate click event
    /// </summary>
    private bool longPressed;

    private Coroutine longPressCoroutine;

    protected override void OnActiveStateChanged()
    {
        DoStateTransition(selectionState, true);
    }

    /// <summary>
    /// Do a state transition
    /// </summary>
    /// <param name="state"></param>
    /// <param name="instant"></param>
    protected override void DoStateTransition(UISelectionState state, bool instant)
    {
        selectionState = state;

        switch (state)
        {
            case UISelectionState.Pressed:
                if (!Disabled)
                {
                    images.ForEach(i => i.Image.color = i.Pressed);
                }
                break;
            case UISelectionState.Normal:
                images.ForEach(i => i.Image.color = Disabled ? i.Disabled : i.Color);
                break;
            case UISelectionState.Highlighted:
                images.ForEach(i => i.Image.color = Disabled ? i.SelectedDisabled : i.Selected);
                break;
        }


        base.DoStateTransition(state, instant);
    }

    /// <summary>
    /// Pointer down called by selectable
    /// </summary>
    /// <param name="eventArgs"></param>
    public override void OnPointerDown(UIInputEventArgs eventArgs)
    {
        base.OnPointerDown(eventArgs);

        if (!Disabled)
        {
            longPressCoroutine = StartCoroutine(LongClick(eventArgs));
            longPressed = false;
        }
    }

    /// <summary>
    /// Pointer up called by selectable
    /// </summary>
    /// <param name="eventArgs"></param>
    public override void OnPointerUp(UIInputEventArgs eventArgs)
    {
        base.OnPointerUp(eventArgs);

        if (!Disabled && !longPressed) // && (!eventArgs.HasCursor || eventArgs.CursorWithinRectangle(Rectangle)))
        {
            eventArgs.Payload = Payload;
            onClick.Invoke(this, eventArgs);
        }

        if (longPressCoroutine != null)
        {
            StopCoroutine(longPressCoroutine);
        }

        longPressed = false;
    }

    /// <summary>
    /// Coroutine for long press
    /// </summary>
    /// <param name="eventArgs"></param>
    /// <returns></returns>
    private IEnumerator LongClick(UIInputEventArgs eventArgs)
    {
        yield return new WaitForSeconds(longPressDuration);
        eventArgs.Payload = Payload;
        onLongClick.Invoke(this, eventArgs);
        longPressed = true;
    }

    /// <summary>
    /// Deselect override to cancel long clicks
    /// </summary>
    /// <param name="eventData"></param>
    public override void OnSelectableDeselect(UIInputEventArgs eventData)
    {
        if (longPressCoroutine != null)
        {
            StopCoroutine(longPressCoroutine);
        }

        longPressed = false;

        base.OnSelectableDeselect(eventData);
    }

    public void AddLongClickListener(UnityAction<UISelectable, UIInputEventArgs> listener)
    {
        HasLongPressInteraction = true;
        onLongClick.AddListener(listener);
    }

    /// <summary>
    /// On Click event getter
    /// </summary>
    public UnityEvent<UISelectable, UIInputEventArgs> OnClick => onClick;

    public float LongPressDuration => longPressDuration;

    public bool HasLongPressInteraction { get; set; }

    public object Payload { get; set; }
}
