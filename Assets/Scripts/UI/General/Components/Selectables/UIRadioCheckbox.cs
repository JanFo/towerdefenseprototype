﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// A scroll bar for a scroll view
/// </summary>
public class UIRadioCheckbox : UIBehaviour
{
    [SerializeField]
    private Image checkMark = null;

    [SerializeField]
    private Image placeHolder = null;

    [SerializeField]
    private UnityEvent onCheck = null;

    public int Index { get; set; }

    public bool IsChecked
    {
        set
        {
            if(checkMark) checkMark.enabled = value;
            if(placeHolder) placeHolder.enabled = !value;
            onCheck.Invoke();
        }
    }

    public Image Placeholder => placeHolder;

    public Image Checkmark => checkMark;
}
