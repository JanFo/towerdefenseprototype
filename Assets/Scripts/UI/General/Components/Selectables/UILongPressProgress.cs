﻿using UnityEngine;
using UnityEngine.UI;

public class UILongPressProgress : MonoBehaviour
{
    private float duration;

    private Image image;
    private UIButton button;
    private bool isPressed;
    private float timePressed;

    private void Start()
    {
        image = GetComponent<Image>();

        button = GetComponentInParent<UIButton>();
        duration = button.LongPressDuration;

       
        button.onPointerDown.AddListener(OnPointerDown);
        button.onPointerUp.AddListener(OnPointerUp);
    }

    private void OnPointerUp(UISelectable sender, UIInputEventArgs arg0)
    {
        isPressed = false;
        timePressed = 0.0f;
    }

    private void OnPointerDown(UISelectable sender, UIInputEventArgs arg0)
    {
        isPressed = true;
    }

    private void Update()
    {
        image.fillAmount = Mathf.Clamp01(timePressed / duration);

        if (!button.HasLongPressInteraction)
        {
            return;
        }

        if (isPressed)
        {
            timePressed += Time.deltaTime;
        }

    }
}
