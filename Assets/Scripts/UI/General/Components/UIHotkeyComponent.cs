﻿using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// A UI element that can be pressed via hotkey
/// </summary>
public class UIHotkeyComponent : MonoBehaviour
{
    [SerializeField]
    private UIHotkeyBinding hotkeyBinding = null;

    [SerializeField]
    private UIHotkeyImage hotkeyImage = null;

    [SerializeField]
    private bool hasBinding = true;

    private UserInterfaceActionType hotkey = UserInterfaceActionType.None;

    public UserInterfaceActionType Hotkey
    {
        set
        {
            hotkey = value;

            if (hotkeyBinding && hasBinding)
                hotkeyBinding.ActionType = value;

            if (hotkeyImage)
                hotkeyImage.ActionType = value;
        }
    }

    public bool HasBinding
    {
        set
        {
            hasBinding = value;
            hotkeyBinding.ActionType = hasBinding ? hotkey : UserInterfaceActionType.None;
        }
    }
}
