﻿using UnityEngine;

/// <summary>
/// A scroll bar for a scroll view
/// </summary>
public class UIRadioGroup : MonoBehaviour
{
    public IntEvent OnSelectionChanged => onSelectionChanged;

    [SerializeField]
    private IntEvent onSelectionChanged = null;

    private UIRadioCheckbox[] checkBoxes;

    private int selectedIndex = -1;

    private void Awake()
    {
        if(checkBoxes == null)
        {
            Initialize();
        }
    }

    private void OnTransformChildrenChanged()
    {
        
    }

    private void Initialize()
    {
        checkBoxes = GetComponentsInChildren<UIRadioCheckbox>(true);

        for (int i = 0; i < checkBoxes.Length; i++)
        {
            int index = i;
            UIButton button = checkBoxes[i].GetComponent<UIButton>();
            button.OnClick.AddListener((sender, args) => Select(index));
        }
    }

    internal void Clear()
    {
        transform.ClearChildren();
        checkBoxes = null;
    }

    public void Disable(int index)
    {
        if (checkBoxes == null)
        {
            Initialize();
        }

        checkBoxes[index].gameObject.SetActive(false);
    }

    public void Select(int index)
    {
        if (checkBoxes == null)
        {
            Initialize();
        }

        Select(checkBoxes[index]);
        selectedIndex = index;
        OnSelectionChanged.Invoke(index);
    }

    private void Select(UIRadioCheckbox boxToSelect)
    {
        foreach(UIRadioCheckbox checkbox in checkBoxes)
        {
            checkbox.IsChecked = false;
        }

        boxToSelect.IsChecked = true;
    }

    public void SelectNext()
    {
        if (checkBoxes == null)
        {
            Initialize();
        }

        selectedIndex = UIUtils.RepeatIndex(selectedIndex + 1, checkBoxes.Length);

        if(checkBoxes[selectedIndex].gameObject.activeSelf)
        {
            Select(selectedIndex);
        }
        else
        {
            SelectNext();
        }
    }

    public void SelectPrevious()
    {
        if (checkBoxes == null)
        {
            Initialize();
        }

        selectedIndex = UIUtils.RepeatIndex(selectedIndex - 1, checkBoxes.Length);

        if (checkBoxes[selectedIndex].gameObject.activeSelf)
        {
            Select(selectedIndex);
        }
        else
        {
            SelectPrevious();
        }
    }
}
