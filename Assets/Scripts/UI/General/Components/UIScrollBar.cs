﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A scroll bar for a scroll view
/// </summary>
public class UIScrollBar : UISelectable
{
    /// <summary>
    /// The scroll handle transform
    /// </summary>
    [SerializeField]
    private UIScrollBarHandle handle = null;

    /// <summary>
    /// The content transform
    /// </summary>
    [SerializeField]
    private RectTransform content = null;

    /// <summary>
    /// The scroll position of the handle 
    /// </summary>
    [SerializeField, Range(0, 1)]
    private float scrollPosition = 0.0f;

    [SerializeField]
    private int elementPadding = 1;

    /// <summary>
    /// The rect transform of this gameobject
    /// </summary>
    private RectTransform rect;
    private float handleStartPosition;

    private float startScrollPosition;

    private float mouseStartPosition;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        rect = GetComponent<RectTransform>();

        handle.onPointerDown.AddListener(HandlePointerDown);
        handle.onPointerDrag.AddListener(HandlePointerDrag);

        StartCoroutine(ResizeHandle());
    }

    private void HandlePointerDrag(UISelectable sender, UIInputEventArgs arg0)
    {
        float delta = arg0.CursorPosition.y - mouseStartPosition;
        float barHeightDelta = rect.sizeDelta.y - handle.Rect.sizeDelta.y;

        scrollPosition = Mathf.Clamp01(startScrollPosition - delta / barHeightDelta);

        MoveHandle();
    }


    private void HandlePointerDown(UISelectable sender, UIInputEventArgs arg0)
    {
        startScrollPosition = scrollPosition;
        mouseStartPosition = arg0.CursorPosition.y;
    }

    public override void OnPointerDown(UIInputEventArgs eventArgs)
    {
        base.OnPointerDown(eventArgs);

        if(eventArgs.ActionType == UserInterfaceActionType.Click)
        {
            float target = -(rect.InverseTransformPoint(Input.mousePosition).y - Height / 2.0f);
            scrollPosition = target / Height; 

            MoveHandle();
        }
    }

    public void ScrollToIndex(int i)
    {
        float contentHeightDelta = content.sizeDelta.y - rect.sizeDelta.y;

        if (contentHeightDelta > 0.0f)
        {
            float elementHeight = content.sizeDelta.y / (content.childCount);
            int numVisibleElements = (int)(rect.sizeDelta.y / elementHeight);

            float min = Mathf.Clamp01(elementHeight * (i - numVisibleElements + elementPadding) / contentHeightDelta);
            float max = Mathf.Clamp01(elementHeight * (i - elementPadding) / contentHeightDelta);

            scrollPosition = Mathf.Clamp(scrollPosition, min, max);
        }
        else
        {
            scrollPosition = 0.0f;
        }

        MoveHandle();
    }

    private float GetScrollPosition(int i, float contentHeightDelta, float elementHeight)
    {
        float scrollAmount = i * elementHeight;
        return Mathf.Clamp01(scrollAmount / contentHeightDelta);
    }

    private void MoveHandle()
    {
        RectTransform rect = GetComponent<RectTransform>();
        float barHeightDelta = rect.sizeDelta.y - handle.Rect.sizeDelta.y;
        float contentHeightDelta = content.sizeDelta.y - rect.sizeDelta.y;

        handle.Rect.anchoredPosition = new Vector2(handle.Rect.anchoredPosition.x, -1 * barHeightDelta * scrollPosition);
        content.anchoredPosition = new Vector2(content.anchoredPosition.x, contentHeightDelta * scrollPosition);
    }

    IEnumerator ResizeHandle()
    {
        yield return null;

        RectTransform rect = GetComponent<RectTransform>();

        float heightRatio = rect.sizeDelta.y / content.sizeDelta.y;

        handle.Rect.sizeDelta = new Vector2(handle.Rect.sizeDelta.x, Mathf.Min(1.0f, heightRatio) * rect.sizeDelta.y);

        MoveHandle();
    }

    public float Height
    {
        get
        {
            return rect.sizeDelta.y;
        }
    }
}
