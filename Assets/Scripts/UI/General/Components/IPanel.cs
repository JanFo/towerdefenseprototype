﻿using UnityEngine;

public interface IPanel
{
    RectTransform CreateInstance();
}