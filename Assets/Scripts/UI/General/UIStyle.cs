﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

[ExecuteInEditMode]
public class UIStyle : UIBehaviour
{
    [SerializeField]
    private UIClass uiClass = null;

#if UNITY_EDITOR
    protected override void OnEnable()
    {
        ApplyStyle();
    }

    protected override void OnValidate()
    {
        ApplyStyle();
    }

    public void ApplyStyle()
    {
        if (uiClass == null)
        {
            return;
        }

        uiClass.Apply(gameObject);
    }
#endif
}
