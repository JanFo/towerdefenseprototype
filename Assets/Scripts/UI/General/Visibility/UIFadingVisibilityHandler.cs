﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[RequireComponent(typeof(CanvasGroup))]
public class UIFadingVisibilityHandler : UIVisibilityHandler
{
    [SerializeField]
    private float fadeSpeed = 4.0f;

    private float targetAlpha = 1.0f;

    private bool isVisible = true;

    private CanvasGroup canvasGroup;

    [SerializeField]
    private bool inactiveOnHidden = true;

    public override bool IsVisible
    {
        get
        {
            return isVisible;
        }
    }

    private void OnEnable()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    protected override void OnHide()
    {
        targetAlpha = 0.0f;
        isVisible = false;
        enabled = true;
    }

    protected override void OnShow()
    {
        targetAlpha = 1.0f;
        isVisible = true;
        enabled = true;

        if(inactiveOnHidden)
            gameObject.SetActive(true);
    }

    private void Update()
    {
        if(targetAlpha != canvasGroup.alpha)
        {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, targetAlpha, Time.deltaTime * fadeSpeed);

            if(targetAlpha == canvasGroup.alpha)
            {
                enabled = false;

                if(canvasGroup.alpha == 0.0f && inactiveOnHidden)
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
}
