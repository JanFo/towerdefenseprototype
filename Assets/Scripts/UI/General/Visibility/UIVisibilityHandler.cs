﻿using UnityEngine;
using UnityEngine.Events;

public class UIVisibilityHandler : MonoBehaviour
{
    public delegate void VisibilityEvent();

    public event VisibilityEvent onShow = null;

    public event VisibilityEvent onHide = null;

    public virtual bool IsVisible
    {
        get
        {
            return gameObject.activeInHierarchy;
        }
    }

    public virtual void Hide()
    {
        OnHide();

        if (onHide != null)
        {
            onHide.Invoke();
        }
    }

    public virtual void Show()
    {
        OnShow();

        if (onShow != null)
        {
            onShow.Invoke();
        }
    }

    protected virtual void OnShow()
    {
        gameObject.SetActive(true);
    }

    protected virtual void OnHide()
    {
        gameObject.SetActive(false);
    }
}
