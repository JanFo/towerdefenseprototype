﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;

[RequireComponent(typeof(Graphic))]
public class UITintableImage : UIBehaviour
{
    [SerializeField]
    private Color _selected = Color.white;

    [SerializeField]
    private Color _pressed = Color.white;

    [SerializeField]
    private Color _disabled = Color.white;

    [SerializeField]
    private Color _selectedDisabled = Color.white;

    [SerializeField, HideInInspector]
    private Graphic _image;

    [SerializeField, HideInInspector]
    private Color _normal = Color.white;

    protected override void Awake()
    {
        _image = GetComponent<Graphic>();
        _normal = Image.color;
    }

    public Color Color
    {
        set
        {
            _normal = value;
        }
        get
        {
            return _normal;
        }
    }

    public Graphic Image
    {
        get
        {
            return _image;
        }
    }

    public Color Selected
    {
        get
        {
            return _selected;
        }
        set
        {
            _selected = value;
        }
    }

    public Color Pressed
    {
        get
        {
            return _pressed;
        }
        set
        {
            _pressed = value;
        }
    }

    public Color Disabled
    {
        get
        {
            return _disabled;
        }
        set
        {
            _disabled = value;
        }
    }

    public Color SelectedDisabled
    {
        get
        {
            return _selectedDisabled;
        }
        set
        {
            _selectedDisabled = value;
        }
    }


    public void DoStateTransition(UISelectionState state, bool disabled = false)
    {
        switch (state)
        {
            case UISelectionState.Pressed:
                _image.color = _pressed;
                break;
            case UISelectionState.Normal:
                _image.color = disabled ? _disabled : _normal;
                break;
            case UISelectionState.Highlighted:
                _image.color = disabled ? _selectedDisabled : _selected;
                break;
        }
    }
}
