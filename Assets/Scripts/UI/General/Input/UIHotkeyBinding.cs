﻿using UnityEngine;
using UnityEngine.EventSystems;


/// <summary>
/// Monobehaviour implementing the IHotkeyBinding interface. Registers the binding with the
/// closest hotkey group on startup. Has to be attached to any UISelectable
/// </summary>
[RequireComponent(typeof(UISelectable))]
public class UIHotkeyBinding : UIBehaviour, IHotkeyBinding
{
    [SerializeField]
    private UserInterfaceActionType actionType = UserInterfaceActionType.Cancel;

    private UIInputSystem inputGroup;

    protected override void OnEnable()
    {
        base.OnEnable();

        inputGroup = UIUtils.GetComponentInParent<UIInputSystem>(transform);

        inputGroup = GetComponentInParent<UIInputSystem>();

        if (inputGroup)
        {
            inputGroup.RegisterHotkey(this);
        }
    }

    protected override void OnTransformParentChanged()
    {
        base.OnTransformParentChanged();

        if (inputGroup)
        {
            inputGroup.UnregisterHotkey(this);
        }

        inputGroup = UIUtils.GetComponentInParent<UIInputSystem>(transform);

        if (inputGroup)
        {
            inputGroup.RegisterHotkey(this);
        }
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        if (inputGroup)
        {
            inputGroup.UnregisterHotkey(this);
        }
    }

    protected override void Awake()
    {
        base.Awake();
        Selectable = GetComponent<UISelectable>();
    }

    public UserInterfaceActionType ActionType
    {
        get
        {
            return actionType;
        }
        set
        {
            actionType = value;
        }
    }

    public UISelectable Selectable { get; private set; }
}
