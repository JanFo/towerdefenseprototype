﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// The core component of the UI solution. A gameobject with this component (or any child component)
/// can be selected by the UI Controller and can receive events such as PointerUp and PointerDown
/// </summary>
public class UISelectable : UIBehaviour
{
    /// <summary>
    /// The top neighbor selectable of this selectable 
    /// This field can be set in the editor and will be added
    /// to the Neighbors list on runtime.
    /// </summary>
    [SerializeField]
    private UISelectable neighborTop = null;

    /// <summary>
    /// The bottom neighbor selectable of this selectable 
    /// This field can be set in the editor and will be added
    /// to the Neighbors list on runtime.
    /// </summary>
    [SerializeField]
    private UISelectable neighborBottom = null;

    /// <summary>
    /// The left neighbor selectable of this selectable    
    /// This field can be set in the editor and will be added
    /// to the Neighbors list on runtime.
    /// </summary>
    [SerializeField]
    private UISelectable neighborLeft = null;

    /// <summary>
    /// The right neighbor selectable of this selectable.
    /// This field can be set in the editor and will be added
    /// to the Neighbors list on runtime.
    /// </summary>
    [SerializeField]
    private UISelectable neighborRight = null;

    /// <summary>
    /// The pointer down event
    /// </summary>
    [HideInInspector]
    public UISelectableEvent onPointerDown;

    /// <summary>
    /// The pointer up event
    /// </summary>
    [HideInInspector]
    public UISelectableEvent onPointerUp;

    /// <summary>
    /// The pointer drag event
    /// </summary>
    [HideInInspector]
    public UISelectableEvent onPointerDrag;

    /// <summary>
    /// The pointer drag event
    /// </summary>
    [HideInInspector]
    public UISelectableEvent OnSelect;

    /// <summary>
    /// The pointer drag event
    /// </summary>
    [HideInInspector]
    public UISelectableEvent onDeselect;

    /// <summary>
    /// Denotes whether the selectable is currently selected
    /// </summary>
    public bool IsSelected => selectionCounter > 0;

    /// <summary>
    /// Holds a list of all the neighbor selectables of this selectable
    /// </summary>
    public IEnumerable<UINeighbor> Neighbors => neighbors;

    private List<UINeighbor> neighbors;

    private RectTransform _rectTransform;

    private Vector2 scale;

    private Canvas canvas;

    private byte selectionCounter;

    Vector3[] corners;

    [SerializeField]
    private bool disabled;

    public RectTransform RectTransform
    {
        get
        {
            if (!_rectTransform)
            {
                _rectTransform = GetComponent<RectTransform>();
            }

            return _rectTransform;
        }
    }

    protected override void Awake()
    {
        base.Awake();

        // RegisterToNearestGroup();

        neighbors = new List<UINeighbor>();
        corners = new Vector3[4];

        if (neighborBottom)
            AddNeighbor(Vector2.down, neighborBottom);

        if (neighborTop)
            AddNeighbor(Vector2.up, neighborTop);

        if (neighborRight)
            AddNeighbor(Vector2.right, neighborRight);

        if (neighborLeft)
            AddNeighbor(Vector2.left, neighborLeft);

    }

    public bool Disabled
    {
        set
        {
            disabled = value;
            OnActiveStateChanged();
        }
        get
        {
            return disabled;
        }
    }

    protected virtual void OnActiveStateChanged()
    {

    }


    public Rect Rectangle
    {
        get
        {
            if (canvas == null)
            {
                canvas = GetComponentInParent<Canvas>();
            }

            return GetWorldRect(canvas);
        }
    }

    public Rect GetWorldRect(Canvas canvas)
    {
        if (canvas == null)
        {
            return new Rect();
        }

        RectTransform.GetWorldCorners(corners);

        return new Rect(
            corners[0].x,
            corners[0].y, 
            canvas.scaleFactor * RectTransform.rect.size.x + 1, 
            canvas.scaleFactor * RectTransform.rect.size.y + 1);
    }


    protected override void Start()
    {
        base.Start();

        canvas = UIUtils.GetComponentInParent<Canvas>(transform);
    }

    protected override void OnTransformParentChanged()
    {
        if (InputSystem)
        {
            InputSystem.UnregisterSelectable(this);
        }

        InputSystem = UIUtils.GetComponentInParent<UIInputSystem>(transform);

        if (InputSystem)
        {
            InputSystem.RegisterSelectable(this);
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        InputSystem = UIUtils.GetComponentInParent<UIInputSystem>(transform);

        if (InputSystem)
        {
            InputSystem.RegisterSelectable(this);
        }
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        if(gameObject.name == "LoadGameButton")
        {
            Debug.Log("HERE");
        }

        if (InputSystem)
        {
            if (IsSelected)
            {
                InputSystem.Deselect(this);
            }

            InputSystem.UnregisterSelectable(this);
        }
    }

    /// <summary>
    /// Can be overriden by child components to handle state transitions
    /// </summary>
    /// <param name="state"></param>
    /// <param name="instant"></param>
    protected virtual void DoStateTransition(UISelectionState state, bool instant)
    {
        
    }

    /// <summary>
    /// Adds a neighbor selectable to the list
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="selectable"></param>
    public void AddNeighbor(Vector2 direction, UISelectable selectable)
    {
        neighbors.Add(new UINeighbor() { SqrDistance = direction.sqrMagnitude, Direction = direction.normalized, Selectable = selectable });
    }
    
    /// <summary>
    /// Clears all neighbor selectables from the list
    /// </summary>
    public void ClearNeighbors()
    {
        neighbors = new List<UINeighbor>();
    }

    /// <summary>
    /// This is called by the Unity editor. A new transform might result into a new 
    /// InputGroup, thus the selectable has to be re-registered
    /// </summary>

    /// <summary>
    /// Selects this selectable
    /// </summary>
    /// <param name="forceSwitchPage"></param>
    public void Select(bool forceSwitchPage = false)
    {
        if(!gameObject.activeInHierarchy)
        {
            return;
        }

        InputSystem.Select(this, forceSwitchPage);
    }

    /// <summary>
    /// This function is called by the UIInputController on pointer drag
    /// </summary>
    /// <param name="eventArgs"></param>
    public virtual void OnPointerDrag(UIInputEventArgs eventArgs)
    {
        if (onPointerDrag != null)
        {
            onPointerDrag.Invoke(this, eventArgs);
        }
    }

    /// <summary>
    /// This function is called by the UIInputController on pointer down
    /// </summary>
    /// <param name="eventArgs"></param>
    public virtual void OnPointerDown(UIInputEventArgs eventArgs)
    {
        DoStateTransition(UISelectionState.Pressed, false);

        if(onPointerDown != null)
        {
            onPointerDown.Invoke(this, eventArgs);
        }
    }

    public virtual void OnNavigate(Vector2 value)
    {

    }

    /// <summary>
    /// This function is called by the UIInputController on pointer cancel
    /// </summary>
    /// <param name="eventArgs"></param>
    public void OnPointerCancel(UIInputEventArgs eventArgs)
    {
        if (IsSelected)
            DoStateTransition(UISelectionState.Highlighted, false);
        else
            DoStateTransition(UISelectionState.Normal, false);
    }

    /// <summary>
    /// This function is called by the UIInputController on pointer up
    /// </summary>
    /// <param name="eventArgs"></param>
    public virtual void OnPointerUp(UIInputEventArgs eventArgs)
    {
        if (IsSelected)
            DoStateTransition(UISelectionState.Highlighted, false);
        else
            DoStateTransition(UISelectionState.Normal, false);

        if (onPointerUp != null)
        {
            onPointerUp.Invoke(this, eventArgs);
        }
    }

    /// <summary>
    /// Called when the selectable is being selected
    /// </summary>
    /// <param name="eventArgs"></param>
    public virtual void OnSelectableSelect(UIInputEventArgs eventArgs)
    {
        selectionCounter++;
        OnSelect.Invoke(this, eventArgs);
        DoStateTransition(UISelectionState.Highlighted, false);
    }

    /// <summary>
    /// Called when the selectable is being deselected
    /// </summary>
    /// <param name="eventArgs"></param>
    public virtual void OnSelectableDeselect(UIInputEventArgs eventArgs)
    {
        selectionCounter--;
        onDeselect.Invoke(this, eventArgs);

        if (selectionCounter == 0)
        {
            DoStateTransition(UISelectionState.Normal, false);
        }
    }

    /// <summary>
    /// The input group responsible for this selectable's input
    /// </summary>
    public UIInputSystem InputSystem { get; private set; }

    public bool HasNeighbors => neighbors.Count > 0;

    /// <summary>
    /// Registers this selectable to the nearest UIInputGroup in a parent transform
    /// </summary>
    private void RegisterToNearestGroup()
    {
        UnregisterFromNearestGroup();

        InputSystem = UIUtils.GetComponentInParent<UIInputSystem>(transform);

        if (InputSystem)
        {
            InputSystem.RegisterSelectable(this);
        }
    }

    /// <summary>
    /// Unregisters the selectable from the current UIInputGroup (if set)
    /// </summary>
    private void UnregisterFromNearestGroup()
    {
        if (InputSystem)
        {
            InputSystem.UnregisterSelectable(this);
        }
    }
}

/// <summary>
/// The neighbor of a UISelectable in a certain direction. This approach should
/// improve the navigation with thumbsticks
/// </summary>
public class UINeighbor
{
    public Vector2 Direction { get; set; }
    public UISelectable Selectable { get; set; }
    public float SqrDistance { get; set; }
}

[Serializable]
public class UISelectableEvent : UnityEvent<UISelectable, UIInputEventArgs> { }

public enum UISelectionState
{
    // Summary:
    //     The UI object can be selected.
    Normal = 0,
    //
    // Summary:
    //     The UI object is highlighted.
    Highlighted = 1,
    //
    // Summary:
    //     The UI object is pressed.
    Pressed = 2,
}