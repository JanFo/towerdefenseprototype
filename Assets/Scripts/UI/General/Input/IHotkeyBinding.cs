﻿/// <summary>
/// A hotkey binding, grouping a selectable and an input button
/// </summary>
public interface IHotkeyBinding
{
    UserInterfaceActionType ActionType { get; }
    UISelectable Selectable { get; }
}
