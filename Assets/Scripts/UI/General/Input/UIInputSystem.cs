﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class UIInputEventArgs
{
    public UserInterfaceActionType ActionType { get; set; }
    public Vector3 CursorPosition { get; set; }
    public bool HasCursor { get; set; }
    public bool Disabled { get; set; }
    public int PlayerIndex { get; set; }
    public object Payload { get; set; }

    public bool CursorWithinRectangle(Rect rectangle)
    {
        return !HasCursor || rectangle.Contains(CursorPosition);
    }
}


/// <summary>
/// A UIInputSytem is a group of selectables that are accessible by UIInputModules.
/// Only one UI element can be selected/pressed at a time
/// </summary>
public class UIInputSystem : UIBehaviour, IIconMappingProvider
{
    [SerializeField]
    private UISelectable defaultSelectable = null;

    public delegate void UISelectableEvent(UISelectable selectable);

    public delegate void UIInputSystemEvent();

    public event UISelectableEvent OnPreferredSelectableChanged;

    public event UIInputSystemEvent OnHierarchyChanged;

    private IIconMappingProvider iconMappingProvider;

    protected override void Awake()
    {
        base.Awake();

        if (HotkeyBindings == null)
        {
            HotkeyBindings = new List<IHotkeyBinding>();
        }

        if (Selectables == null)
        {
            Selectables = new List<UISelectable>();
        }
    }

    /// <summary>
    /// Registers a hotkey binding with the system
    /// </summary>
    /// <param name="binding"></param>
    public void RegisterHotkey(IHotkeyBinding binding)
    {
        if (HotkeyBindings == null)
            HotkeyBindings = new List<IHotkeyBinding>();

        HotkeyBindings.Add(binding);
    }

    /// <summary>
    /// Unregisters a hotkey binding with the system
    /// </summary>
    /// <param name="binding"></param>
    internal void UnregisterHotkey(UIHotkeyBinding binding)
    {
        if (HotkeyBindings == null)
            HotkeyBindings = new List<IHotkeyBinding>();

        HotkeyBindings.Remove(binding);
    }

    /// <summary>
    /// Registers a selectable with the input group
    /// </summary>
    /// <param name="uISelectable"></param>
    public void RegisterSelectable(UISelectable uISelectable)
    {
        if (Selectables == null)
            Selectables = new List<UISelectable>();

        Selectables.Add(uISelectable);

        OnHierarchyChanged?.Invoke();
    }

    /// <summary>
    /// Unregisters a selectable with the input group
    /// </summary>
    /// <param name="uISelectable"></param>
    public void UnregisterSelectable(UISelectable uISelectable)
    {
        if (Selectables == null)
            Selectables = new List<UISelectable>();

        Selectables.Remove(uISelectable);

        OnHierarchyChanged?.Invoke();
    }

    /// <summary>
    /// All registered selectables of this input group
    /// </summary>
    public List<UISelectable> Selectables { get; private set; }

    /// <summary>
    /// All registered hotkeybindings of this input group
    /// </summary>
    public List<IHotkeyBinding> HotkeyBindings { get; private set; }

    public virtual void Select(UISelectable selectable, bool forceSwitchPage = false)
    {
        if(OnPreferredSelectableChanged != null)
        {
            OnPreferredSelectableChanged.Invoke(selectable);
        }
    }
    public void Deselect(UISelectable uISelectable)
    {
        if (OnPreferredSelectableChanged != null)
        {
            OnPreferredSelectableChanged.Invoke(null);
        }
    }

    public IIconMappingProvider IconMappingProvider
    {
        set
        {
            iconMappingProvider = value;
            BroadcastMessage("OnIconMappingProviderChanged", SendMessageOptions.DontRequireReceiver);
        }
    }

    public UISelectable DefaultSelectable
    {
        get { return defaultSelectable; }
        set
        {
            defaultSelectable = value;
           
        }
    }

    public Sprite GetIconForButton(UserInterfaceActionType button)
    {
        return iconMappingProvider?.GetIconForButton(button);
    }
}
