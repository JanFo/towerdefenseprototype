﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.PlayerInput;

/// <summary>
/// All actions relevant for the user interface. Feel free to add more!
/// </summary>
public enum UserInterfaceActionType
{
    Cancel,
    Navigate,
    Point,
    Click,
    MiddleClick,
    RightClick,
    InteractPrimary,
    InteractSecondary,
    InteractTertiary,
    None,
    Inventory,
    Previous,
    Next,
    AltNext,
    AltPrevious,
    Menu
}

public interface IIconProvider
{
    Sprite GetIcon(string bindingString);
}

/// <summary>
/// Translates the input from player input actions to UI stuff
/// </summary>
public class UIInputModule : IIconMappingProvider
{
    public IIconProvider iconProvider { get; set; }

    private int playerIndex;

    private UISelectable selectedSelectable;

    private UISelectable pressedSelectable;

    private PlayerInput playerInput;

    private Mouse cursor;

    private List<IHotkeyBinding> hotkeyCache;

    private UIInputSystem targetInputSystem;

    public bool Enabled { get; set; }

    public bool HasCursor => cursor != null;

    public UIInputModule(int playerIndex, PlayerInput playerInput)
    {
        this.playerIndex = playerIndex;
        this.playerInput = playerInput;
        hotkeyCache = new List<IHotkeyBinding>();
        Initialize();
    }

    /// <summary>
    /// The input system to route player input to
    /// </summary>
    public UIInputSystem TargetInputSystem
    {
        get => targetInputSystem;
        set
        {
            if(targetInputSystem != null)
            {
                targetInputSystem.OnPreferredSelectableChanged -= OnPreferredSelectableChanged;
                targetInputSystem.OnHierarchyChanged -= OnHierarchyChanged;
            }

            if (selectedSelectable != null)
            {
                selectedSelectable.OnSelectableDeselect(new UIInputEventArgs());
            }

            selectedSelectable = null;

            pressedSelectable = null;

            targetInputSystem = value;

            if (targetInputSystem != null)
            {
                targetInputSystem.OnPreferredSelectableChanged += OnPreferredSelectableChanged;
                targetInputSystem.OnHierarchyChanged += OnHierarchyChanged;

                value.IconMappingProvider = this;

                if (selectedSelectable != null)
                {
                    return;
                }

                if (targetInputSystem.DefaultSelectable && !HasCursor)
                {
                    Select(targetInputSystem.DefaultSelectable);
                }
            }
        }
    }

    private void OnHierarchyChanged()
    {
        if (HasCursor)
        {
            return;
        }

        if (selectedSelectable != null && selectedSelectable.gameObject.activeInHierarchy)
        {
            return;
        }

        if(targetInputSystem.DefaultSelectable != null)
        {
            Select(targetInputSystem.DefaultSelectable);
        }
    }


    private void OnPreferredSelectableChanged(UISelectable selectable)
    {
        if(HasCursor)
        {
            return;
        }

        Select(selectable);
    }


    private void Initialize()
    {
        cursor = null;

        foreach (var device in playerInput.devices)
        {
            if (device.displayName.Equals("Mouse"))
            {
                cursor = device as Mouse;
                break;
            }
        }

        UserInterfaceActionType[] interfaceActionTypes =
            (UserInterfaceActionType[])Enum.GetValues(typeof(UserInterfaceActionType));

        InputActionAsset actions = playerInput.actions;

        foreach (ActionEvent actionEvent in playerInput.actionEvents)
        {
            string actionId = actionEvent.actionId;
            InputAction action = actions.FindAction(actionId);

            if (action == null)
            {
                continue;
            }

            UserInterfaceActionType interfaceAction = UserInterfaceActionType.None;

            if (!Enum.TryParse(action.name, out interfaceAction))
            {
                continue;
            }

            if (action != null)
            {
                action.Enable();

                if (action.type == InputActionType.Button)
                {
                    actionEvent.AddListener((c) => OnButtonEvent(c, interfaceAction));
                }

                if (action.type == InputActionType.Value || action.type == InputActionType.PassThrough)
                {
                    actionEvent.AddListener((c) => OnValueChanged(c, interfaceAction, action.ReadValue<Vector2>()));
                }
            }
        }
    }

    /// <summary>
    /// Called for all subscribed value events
    /// </summary>
    /// <param name="actionType"></param>
    /// <param name="value"></param>
    private void OnValueChanged(InputAction.CallbackContext context, UserInterfaceActionType actionType, Vector2 value)
    {
        if(context.phase != InputActionPhase.Performed || !Enabled)
        {
            return;
        }

        if (TargetInputSystem != null && TargetInputSystem.enabled && 
            TargetInputSystem.gameObject.activeInHierarchy)
        {
            OnValueAction(actionType, value);
        }
    }

    public void OnValueAction(UserInterfaceActionType actionType, Vector2 value)
    {
        if (actionType == UserInterfaceActionType.Point)
        {
            OnMousePointerAction(actionType, value);
        }

        if (actionType == UserInterfaceActionType.Navigate && selectedSelectable != null)
        {
            selectedSelectable.OnNavigate(value);
            SelectNeighbor(selectedSelectable, value);
        }
    }

    private void OnButtonEvent(InputAction.CallbackContext context, UserInterfaceActionType actionType)
    {
        if(!Enabled)
        {
            return;
        }

        if(context.phase == InputActionPhase.Performed)
        {
            OnButtonDown( actionType);
        }
        else if(context.phase == InputActionPhase.Canceled)
        {
            OnButtonUp( actionType);
        }
    }

    /// <summary>
    /// Called for all subscribed button events
    /// </summary>
    /// <param name="actionType"></param>

    public void OnButtonDown(UserInterfaceActionType actionType)
    {
        if (!Enabled)
        {
            return;
        }
        
        if(targetInputSystem == null || !targetInputSystem.enabled || !targetInputSystem.gameObject.activeInHierarchy)
        {
            return;
        }

        if (actionType == UserInterfaceActionType.Click
            || actionType == UserInterfaceActionType.RightClick || actionType == UserInterfaceActionType.MiddleClick)
        {
            if (selectedSelectable == null)
            {
                return;
            }

            PressSelectable(selectedSelectable, actionType);
        }

        hotkeyCache.Clear();
        hotkeyCache.AddRange(targetInputSystem.HotkeyBindings);
      
        foreach (IHotkeyBinding binding in hotkeyCache)
        {
            if (binding.Selectable == null || !binding.Selectable.gameObject.activeInHierarchy)
                continue;

            if (binding.ActionType == actionType)
            {
                PressSelectable(binding.Selectable, binding.ActionType);
            }
        }
    }

    private void PressSelectable(UISelectable targetSelectable, UserInterfaceActionType actionType)
    {
        if(targetSelectable == null)
        {
            return;
        }

        if (HasCursor)
        {
            OnMousePointerAction(UserInterfaceActionType.Point, cursor.position.ReadValue());
        }

        if (pressedSelectable != null)
            pressedSelectable.OnPointerCancel(new UIInputEventArgs());

        pressedSelectable = targetSelectable;

        if (pressedSelectable != null && pressedSelectable.gameObject.activeInHierarchy)
        {
            pressedSelectable.OnPointerDown(new UIInputEventArgs()
            {
                PlayerIndex = playerIndex,
                ActionType = actionType,
                HasCursor = HasCursor,
                CursorPosition = cursor?.position?.ReadValue() ?? Vector2.zero
            });
        }
    }

    public void OnButtonUp(UserInterfaceActionType actionType)
    {
      

        if (!Enabled)
        {
            return;
        }

        if (targetInputSystem == null || !targetInputSystem.enabled || !targetInputSystem.gameObject.activeInHierarchy)
        {
            return;
        }

        if (actionType == UserInterfaceActionType.Click
            || actionType == UserInterfaceActionType.RightClick || actionType == UserInterfaceActionType.MiddleClick)
        {
            ReleaseSelectable(selectedSelectable, actionType);
        }

        hotkeyCache.Clear();
        hotkeyCache.AddRange(targetInputSystem.HotkeyBindings);

        foreach (IHotkeyBinding binding in hotkeyCache)
        {
            if (binding.Selectable == null)
                continue;

            if (binding.ActionType == actionType)
            {
                ReleaseSelectable(binding.Selectable, binding.ActionType);
            }
        }
    }

    private void ReleaseSelectable(UISelectable targetSelectable, UserInterfaceActionType actionType)
    {
        if (targetSelectable != null && targetSelectable.gameObject.activeInHierarchy)
        {
            if (targetSelectable == pressedSelectable)
            {
                targetSelectable.OnPointerUp(new UIInputEventArgs()
                {
                    PlayerIndex = playerIndex,
                    ActionType = actionType,
                    HasCursor = HasCursor,
                    CursorPosition = cursor?.position?.ReadValue() ?? Vector2.zero
                });

                pressedSelectable = null;
            }
        }
    }

    private void OnMousePointerAction(UserInterfaceActionType actionType, Vector2 cursorPosition)
    {
        if (pressedSelectable == null)
        {
          
            foreach (UISelectable selectable in targetInputSystem.Selectables)
            {
                if (selectable == null || !selectable.gameObject.activeInHierarchy)
                {
                    continue;
                }

                if (selectable.Rectangle.Contains(cursorPosition))
                {
                    if (selectable == selectedSelectable)
                    {
                        return;
                    }

                    Select(selectable, new UIInputEventArgs()
                    {
                        PlayerIndex = playerIndex,
                        CursorPosition = cursorPosition,
                        HasCursor = true
                    });

                    return;
                }
            }

            Select(null);
        }
        else
        {
            if (selectedSelectable != null)
            {
                selectedSelectable.OnPointerDrag(new UIInputEventArgs()
                {
                    PlayerIndex = playerIndex,
                    ActionType = actionType,
                    CursorPosition = cursorPosition
                });
            }
        }
    }

    public void Select(UISelectable targetSelectable, UIInputEventArgs eventArgs = null)
    {
        if (selectedSelectable == targetSelectable)
            return;

        if (targetSelectable != null && targetInputSystem != targetSelectable.InputSystem)
        {
            // Debug.Log(string.Format("Group mismatch ({0} and {1})", activeInputPage, targetSelectable.InputGroup.name));
            return;
        }

        if (selectedSelectable != null)
        {
            selectedSelectable.OnSelectableDeselect(new UIInputEventArgs());
        }

        selectedSelectable = targetSelectable;

        if (selectedSelectable != null)
        {
            // activeInputPage = selectedSelectable.InputSystem.InputPage;
            selectedSelectable.OnSelectableSelect(eventArgs);
        }
    }
    
    private void SelectNeighbor(UISelectable targetSelectable, Vector2 direction)
    {
        UISelectable nextSelectable = null;
        float maxDot = .7f;

        foreach (UINeighbor neighbor in targetSelectable.Neighbors)
        {
            float dot = Vector2.Dot(neighbor.Direction, direction.normalized);
            if (dot > maxDot)
            {
                nextSelectable = neighbor.Selectable;
                maxDot = dot;
            }
        }

        if (nextSelectable != null)
        {
            Select(nextSelectable);
        }
    }

    public string GetButtonStringForAction(UserInterfaceActionType button)
    {
        InputAction action = playerInput.actions.FindAction(button.ToString());
        return action?.GetBindingDisplayString();
    }

    //public string GetButtonStringForAction(SCGameActionType gameAction)
    //{
    //    InputAction action = playerInput.actions.FindAction(gameAction.ToString());
    //    return action?.GetBindingDisplayString();
    //}

    //public Sprite GetIconForButton(SCGameActionType gameAction)
    //{
    //    if (gameAction == SCGameActionType.None)
    //    {
    //        return null;
    //    }

    //    string bindingString = GetButtonStringForAction(gameAction);
    //    return IconConfiguration.GetIcon(bindingString);
    //}

    public Sprite GetIconForButton(UserInterfaceActionType button)
    {
        if (button == UserInterfaceActionType.None)
        {
            return null;
        }

        string bindingString = GetButtonStringForAction(button);
        return iconProvider.GetIcon(bindingString);
    }
}
