﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(UITextReveal))]
public class UIRevealOnStart : MonoBehaviour
{
    public string text { get; set; }

    private void Start()
    {
        GetComponent<UITextReveal>().Reveal(text);
    }
}
