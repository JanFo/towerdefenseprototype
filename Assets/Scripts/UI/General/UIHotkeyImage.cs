﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IIconMappingProvider
{
    Sprite GetIconForButton(UserInterfaceActionType button);
}

[RequireComponent(typeof(Image))]
public class UIHotkeyImage : MonoBehaviour
{
    [SerializeField]
    private UserInterfaceActionType button = UserInterfaceActionType.Click;

    public UserInterfaceActionType ActionType
    {
        set
        {
            button = value;
            ApplyConfig(GetComponentInParent<IIconMappingProvider>());
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ApplyConfig(GetComponentInParent<IIconMappingProvider>());
    }

    private void OnEnable()
    {
        ApplyConfig(GetComponentInParent<IIconMappingProvider>());
    }

    private void OnIconMappingProviderChanged()
    {
        ApplyConfig(GetComponentInParent<IIconMappingProvider>());
    }

    public void ApplyConfig(IIconMappingProvider provider)
    {
        if (provider != null)
        {
            if(button == UserInterfaceActionType.None)
            {
                GetComponent<Image>().enabled = false;
                return;
            }

            Sprite icon = provider.GetIconForButton(button);

            if(icon == null)
            {
                GetComponent<Image>().enabled = false;
            }
            else
            {
                GetComponent<Image>().enabled = true;
                GetComponent<Image>().sprite = icon;
            }

        }
    }
}
