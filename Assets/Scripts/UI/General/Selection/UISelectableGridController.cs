﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UISelectableGridController : MonoBehaviour {

    private GridLayoutGroup _layout;

    private UISelectable[] _selectables;

    void Awake()
    {
        _layout = GetComponent<GridLayoutGroup>();
        _selectables = GetComponentsInChildren<UISelectable>();

        setNeighbors();
    }

    private void setNeighbors()
    {
        int rowWidth = _layout.constraintCount;

        for (int i = 0; i < _selectables.Length; i++)
        {
            _selectables[i].ClearNeighbors();

            int right = UIUtils.RepeatIndex(i + 1, _selectables.Length);
            int left = UIUtils.RepeatIndex(i - 1, _selectables.Length);
            int bottom = UIUtils.RepeatIndex(i + rowWidth, _selectables.Length);
            int top = UIUtils.RepeatIndex(i - rowWidth, _selectables.Length);

            _selectables[i].AddNeighbor(Vector2.right, _selectables[right]);
            _selectables[i].AddNeighbor(Vector2.left, _selectables[left]);
            _selectables[i].AddNeighbor(Vector2.down, _selectables[bottom]);
            _selectables[i].AddNeighbor(Vector2.up, _selectables[top]);
        }
    }

    void OnTransformChildrenChanged()
    {
        _selectables = GetComponentsInChildren<UISelectable>();

        setNeighbors();
    }

}
