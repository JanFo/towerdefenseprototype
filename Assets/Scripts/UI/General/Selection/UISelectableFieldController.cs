﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UISelectableFieldController : MonoBehaviour {

    private UISelectable[] selectables;

    [SerializeField]
    private float maxDistance = 64.0f;

    [SerializeField]
    private float minAngle = 10.0f;

    [SerializeField]
    private bool ignoreInactive = false;


    void OnTransformChildrenChanged()
    {
        selectables = GetComponentsInChildren<UISelectable>();
        setNeighbors();
    }

    private void Start()
    {
        selectables = GetComponentsInChildren<UISelectable>();
    }

    private void Update()
    {
        // LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
        if(selectables.Length > 0 && !selectables[0].HasNeighbors)
        {
            // LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform);
            setNeighbors();
        }
    }

    private void setNeighbors()
    {
        Canvas canvas = GetComponentInParent<Canvas>();

        if(canvas == null)
        {
            return;
        }

        float distance = Mathf.Pow(maxDistance * canvas.scaleFactor, 2);
        // TODO: Find neighbors based on distance and center-to-center vectors
        foreach (UISelectable selectable in selectables)
        {
            selectable.ClearNeighbors();

            if(ignoreInactive && selectable.Disabled)
            {
                continue;
            }

            foreach(UISelectable other in selectables)
            {
                if(other == selectable)
                {
                    continue;
                }

                if (ignoreInactive && other.Disabled)
                {
                    continue;
                }

                Vector2 toOther = other.Rectangle.center - selectable.Rectangle.center;

                float sqrMagnitude = toOther.sqrMagnitude;

                if(sqrMagnitude < 0.001f)
                {
                    continue;
                }

                if(sqrMagnitude > distance)
                {
                    continue;
                }

                foreach(UINeighbor neighbor in selectable.Neighbors)
                {
                    if(Vector2.Angle(neighbor.Direction, toOther) > minAngle)
                    {
                        continue;
                    }

                    if(neighbor.SqrDistance > sqrMagnitude)
                    {
                        neighbor.Direction = toOther.normalized;
                        neighbor.Selectable = other;
                        neighbor.SqrDistance = sqrMagnitude;
                        goto Continue;
                    }
                }

                selectable.AddNeighbor(toOther, other);

                Continue:
                continue;
            }
        }


        // Use _selectables[i].AddNeighbor(...);
    }
}
