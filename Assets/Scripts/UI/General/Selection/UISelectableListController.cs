﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;

public enum SelectableListType
{
    Horizontal,
    Vertical
}

public class UISelectableListController : UIBehaviour {

    [SerializeField]
    private SelectableListType _type = SelectableListType.Horizontal;

    private UISelectable[] _selectables;

    protected override void Start()
    {
        base.Start();

        Refresh();
    }

    void OnTransformChildrenChanged()
    {
        Refresh();
    }

    public void Refresh()
    {
        _selectables = GetComponentsInChildren<UISelectable>();
        _selectables = _selectables.Where(s => !UIUtils.IsDestroyed(s)).ToArray();

        setNeighbors();
    }

    private void setNeighbors()
    {
        for(int i = 0; i < _selectables.Length; i++)
        {
            _selectables[i].ClearNeighbors();
            int bottom = UIUtils.RepeatIndex(i + 1, _selectables.Length);
            int top = UIUtils.RepeatIndex(i - 1, _selectables.Length);
         
            if(_type == SelectableListType.Horizontal)
            {
                _selectables[i].AddNeighbor(Vector2.right, _selectables[bottom]);
                _selectables[i].AddNeighbor(Vector2.left, _selectables[top]);
            }

            if(_type == SelectableListType.Vertical)
            {
                _selectables[i].AddNeighbor(Vector2.down, _selectables[bottom]);
                _selectables[i].AddNeighbor(Vector2.up, _selectables[top]);
            }

        }
    }

}
