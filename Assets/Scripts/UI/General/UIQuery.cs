﻿using System.Linq;
using UnityEngine;

namespace UserInterface
{
    public static class UIQuery
    {
        public static T Get<T>(RectTransform parent, string identifier) where T : Component
        {
            UIIdentifier identifiableObject = parent.GetComponentsInChildren<UIIdentifier>(true).FirstOrDefault(i => i.Identifier == identifier);
            T result = identifiableObject?.GetComponent<T>(); 

            if (result == default)
            {
                Debug.LogWarning("No UI component with identifier " + identifier + " has been found as a child of " + parent.name + ".");
            }

            return result;
        }
    }
    //    private Entity characterEntity;

    //    public UIInputModule UIInputModule { get; }

    //    public string Guid { get; }

    //    public PlayerInput PlayerInput { get; }

    //    private MessageQueueManager eventManager;
    //    private EntityManager entityManager;
    //    private InputActionMap gameActionMap;

    //    public UnityEvent OnOpenMainMenuRequest;

    //    public InteractionInterfaceController InteractionInterfaceController { get; private set; }

    //    public PlayerCharacterUIController(Player player, EntityManager entityManager, MessageQueueManager eventManager)
    //    {
    //        this.eventManager = eventManager;
    //        this.entityManager = entityManager;

    //        Guid = player.Guid;
    //        PlayerInput = player.Input;
    //        UIInputModule = player.UIInputModule;

    //        gameActionMap = PlayerInput.actions.FindActionMap("Player");
    //        OnOpenMainMenuRequest = new UnityEvent();

    //        eventManager.RegisterMessageHandler<StartTradeEvent>(OnTradeStarted);
    //        eventManager.RegisterMessageHandler<ConversationStartedEvent>(OnConversationStarted);
    //        eventManager.RegisterMessageHandler<ConversationEndedEvent>(OnConversationEnded);
    //        eventManager.RegisterMessageHandler<LootContainerEvent>(OnLootContainer);
    //        eventManager.RegisterMessageHandler<GameModeChangedMessage>(OnGameStateChanged);
    //    }

    //    private void OnGameStateChanged(GameModeChangedMessage obj)
    //    {
    //        if (obj.GameMode == GameMode.Cinematic || obj.GameMode == GameMode.Paused)
    //        {
    //            UserInterface.PlayerMenu.Close();
    //            UserInterface.HeadsUpDisplay.Hide();
    //        }
    //        else
    //        {
    //            UserInterface.HeadsUpDisplay.Show();
    //            UIInputModule.TargetInputSystem = UserInterface.HeadsUpDisplay.InputSystem;
    //        }
    //    }

    //    private void OnLootContainer(LootContainerEvent obj)
    //    {
    //        UILootContainerPage lootMenu = UnityEngine.Object.Instantiate(UIConfiguration.LootContainerPage);
    //        lootMenu.Initialize();
    //        lootMenu.Character = characterEntity;
    //        lootMenu.CollectionIndex = obj.ContainerPointer.CollectionIndex;
    //        lootMenu.ContainerEntity = entityManager.GetEntity(obj.ContainerPointer.ContainerId);

    //        UIInputModule.TargetInputSystem = UserInterface.PlayerMenu.InputSystem;
    //        UserInterface.PlayerMenu.OpenPage(lootMenu.GetComponent<UIPlayerMenuPage>());
    //        gameActionMap.Disable();
    //    }

    //    private void OnConversationEnded(ConversationEndedEvent obj)
    //    {
    //        gameActionMap.Enable();
    //        UserInterface.HeadsUpDisplay.Show();
    //        UIInputModule.TargetInputSystem = UserInterface.HeadsUpDisplay.InputSystem;
    //    }

    //    private void OnConversationStarted(ConversationStartedEvent obj)
    //    {
    //        gameActionMap.Disable();
    //        UserInterface.PlayerMenu.Close();
    //        UserInterface.HeadsUpDisplay.Hide();
    //        UIInputModule.TargetInputSystem = obj.TargetInputSystem;
    //    }

    //    public void CreateInterface()
    //    {
    //        UserInterface = UnityEngine.Object.Instantiate(UIConfiguration.PlayerCharacterInterface);
    //        UserInterface.Initialize();
    //        UserInterface.OnInteract += InteractionInterfaceController_OnInteract;
    //        UserInterface.HeadsUpDisplay.InventoryButton.OnClick.AddListener(OnOpenCharacterMenu);
    //        UserInterface.HeadsUpDisplay.MenuButton.OnClick.AddListener(OnMenuButtonClicked);
    //        UserInterface.PlayerMenu.OnClose.AddListener(OnCharacterMenuClosed);

    //        TextAnchor anchor = GetAnchor();
    //        UserInterface.HeadsUpDisplay.GetComponent<VerticalLayoutGroup>().childAlignment = anchor;
    //        UserInterface.PlayerMenu.GetComponent<HorizontalLayoutGroup>().childAlignment = anchor;
    //    }

    //    private void OnMenuButtonClicked(UISelectable arg0, UIInputEventArgs arg1)
    //    {
    //        OnOpenMainMenuRequest.Invoke();
    //    }

    //    private TextAnchor GetAnchor()
    //    {
    //        switch (PlayerInput.playerIndex)
    //        {
    //            case 0:
    //                return TextAnchor.LowerLeft;
    //            case 1:
    //                return TextAnchor.LowerRight;
    //            default:
    //                return TextAnchor.MiddleCenter;
    //        }
    //    }

    //    private void InteractionInterfaceController_OnInteract(Interaction interaction, object[] args)
    //    {
    //        if (args == null)
    //        {
    //            IInteractionCursor cursor = interaction.GetCursor(CharacterEntity);

    //            if (cursor != null)
    //            {
    //                cursor.Start(this, InteractionInterfaceController_OnInteract);
    //                return;
    //            }
    //        }

    //        if(!interaction.IsAvailable(CharacterEntity))
    //        {
    //            return;
    //        }

    //        interaction.Interact(eventManager, CharacterEntity, args);
    //    }

    //    private void OnCharacterChanged()
    //    {
    //        UIInputModule.Enabled = characterEntity != null;

    //        if (characterEntity != null)
    //        {
    //            CreateInterface();

    //            UserInterface.CharacterEntity = characterEntity;
    //            UIInputModule.TargetInputSystem = UserInterface.HeadsUpDisplay.InputSystem;

    //            InteractionInterfaceController = new InteractionInterfaceController(characterEntity, UserInterface.HeadsUpDisplay);
    //            InteractionInterfaceController.OnInteract += InteractionInterfaceController_OnInteract;

    //            // InteractionInterfaceController.HeadsUpDisplay.InteractablePanel.gameObject.SetActive(false);
    //        }
    //    }

    //    //private void OnEquipAbility(int arg0, WeaponSpecialAbility arg1)
    //    //{
    //    //    abilitySlotsController.SetAbility(characterEntity.Get<AbilitySlots>(), arg1, arg0);
    //    //}

    //    private void OnCharacterMenuClosed()
    //    {
    //        UIInputModule.TargetInputSystem = UserInterface.HeadsUpDisplay.InputSystem;
    //        gameActionMap.Enable();
    //    }

    //    private void OnOpenCharacterMenu(UISelectable sender, UIInputEventArgs arg0)
    //    {
    //        UIInputModule.TargetInputSystem = UserInterface.PlayerMenu.InputSystem;
    //        UserInterface.PlayerMenu.OpenPage(UserInterface.CharacterMenuPage);
    //        gameActionMap.Disable();
    //    }

    //    public void OnTradeStarted(StartTradeEvent args)
    //    {
    //        if(args.CharacterId != characterEntity.Id)
    //        {
    //            return;
    //        }

    //        Entity traderEntity = entityManager.GetEntity(args.TraderId);

    //        if(traderEntity == null)
    //        {
    //            return;
    //        }

    //        OpenTradeMenu(traderEntity, 0);
    //    }


    //    public void OnTrainingStarted(Entity trainer)
    //    {
    //        OpenTradeMenu(trainer, 2);
    //    }

    //    public void OpenTradeMenu(Entity entity, int tab)
    //    {
    //        UITradeMenuPage tradeMenuPage = UnityEngine.Object.Instantiate(UIConfiguration.TradeMenuPrefab);
    //        tradeMenuPage.PlayerCharacterEntity = characterEntity;
    //        tradeMenuPage.TargetEntity = entity;
    //        tradeMenuPage.ActiveTab = tab;

    //        UIInputModule.TargetInputSystem = UserInterface.PlayerMenu.InputSystem;
    //        UserInterface.PlayerMenu.OpenPage(tradeMenuPage);
    //        gameActionMap.Disable();
    //    }

    //    public Entity CharacterEntity
    //    {
    //        get
    //        {
    //            return characterEntity;
    //        }
    //        set
    //        {
    //            characterEntity = value;

    //            OnCharacterChanged();
    //        }
    //    }

    //    public UIPlayerCharacterInterface UserInterface { get; private set; }
    //}
}