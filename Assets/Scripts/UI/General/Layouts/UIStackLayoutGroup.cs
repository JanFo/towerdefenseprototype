﻿//using UnityEngine;
//using UnityEngine.UI;

//public class UIStackLayoutGroup : LayoutGroup
//{
//    [SerializeField]
//    private UIPlayerMenuPageStack stack = null;

//    protected override void OnEnable()
//    {
//        base.OnEnable();
//    }

//    public override void CalculateLayoutInputHorizontal()
//    {
//        base.CalculateLayoutInputHorizontal();
//        CalculateLayoutInputForAxis(0);
//    }

//    public override void CalculateLayoutInputVertical()
//    {
//        CalculateLayoutInputForAxis(1);
//    }

//    public override void SetLayoutHorizontal()
//    {
//        SetLayoutAlongAxis(0);
//    }

//    public override void SetLayoutVertical()
//    {
//        SetLayoutAlongAxis(1);
//    }

//    void SetLayoutAlongAxis(int axis)
//    {
//        RectTransform rect = GetComponent<RectTransform>();
       
//        for (int i = 0; i < rectChildren.Count; i++)
//        {
//            float size = axis == 0 ? rect.rect.width - (padding.left + padding.right)
//                : rect.rect.height - (padding.top + padding.bottom);
//            float pos = axis == 0 ? padding.left : padding.top; 
//            RectTransform child = rectChildren[i];
//            SetChildAlongAxis(child, axis, pos, size);
//        }
//    }

//    void CalculateLayoutInputForAxis(int axis)
//    {
//        float min = 0;
//        float preferred = 0;

//        if (stack.Pages != null)
//        {
//            foreach (UIPlayerMenuPage child in stack.Pages)
//            {
//                LayoutElement element = child.GetComponent<LayoutElement>();

//                if (element)
//                {
//                    preferred = Mathf.Max(preferred, axis == 0 ? element.preferredWidth : element.preferredHeight);
//                    min = Mathf.Max(axis == 0 ? element.minWidth : element.minHeight, min);
//                }
//            }
//        }

//        float paddingAmount = axis == 0 ? padding.left + padding.right : padding.top + padding.bottom;
//        SetLayoutInputForAxis(min + paddingAmount, preferred + paddingAmount, 1, axis);
//    }


//    internal void Clear()
//    {
//        foreach (Transform child in transform)
//        {
//            child.gameObject.SetActive(false);
//        }
//    }
//}
