﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDynamicGrid : MonoBehaviour
{
    [SerializeField]
    public int rows;

    [SerializeField]
    public int columns;

    [SerializeField]
    public int padding;

    public void Apply()
    {
        float stepX = 1.0f / columns;
        float stepY = 1.0f / rows;

        int i = 0;
        int x = 0;
        int y = 0;

        foreach (Transform child in transform)
        {
            RectTransform rectTransform = child.GetComponent<RectTransform>();

            y = rows - i / columns - 1;
            x = i % columns;

            rectTransform.anchorMin = new Vector2(x * stepX, y * stepY);
            rectTransform.anchorMax = new Vector2((x + 1) * stepX, (y + 1) * stepY);
            rectTransform.offsetMin = new Vector3(padding, padding);
            rectTransform.offsetMax = new Vector3(-padding, -padding);

            i++;
        }
    }

    void OnTransformChildrenChanged()
    {
        Apply();
    }
}
