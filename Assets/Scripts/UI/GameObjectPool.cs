using System;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool<T> where T : MonoBehaviour
{
    private Stack<T> _pool;

    private Func<T> _creationCallback;

    public GameObjectPool(Func<T> creationCallback)
    {
        _pool = new Stack<T>();
        _creationCallback = creationCallback;
    }

    public void Release(T obj)
    {
        obj.gameObject.SetActive(false);
        _pool.Push(obj);
    }

    public T GetInstance()
    {
        T instance = null;

        if (_pool.Count == 0)
        {
            instance = _creationCallback.Invoke();
        }
        else
        {
            instance = _pool.Pop();
        }

        instance.gameObject.SetActive(true);

        return instance;
    }
}
