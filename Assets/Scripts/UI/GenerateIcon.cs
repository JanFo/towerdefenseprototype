using System;
using UnityEngine;

public class GenerateIcon : MonoBehaviour
{
    public Vector3 position;

    public Vector3 rotation;

    public void CaptureCameraSettings(Camera camera)
    {
        position = camera.transform.position;
        rotation = camera.transform.eulerAngles;
    }

    
}
