using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconGenerator : MonoBehaviour
{
    [SerializeField]
    private string _iconPath;

    [SerializeField]
    private TextureFormat _format;

    [SerializeField]
    private GameObject _currentObject;

    public void SaveCurrentTargetToTexture()
    {
        RenderTexture texture = GetComponent<Camera>().targetTexture;
        byte[] bytes = ToTexture2D(texture).EncodeToPNG();
        System.IO.File.WriteAllBytes(Application.dataPath + _iconPath + _currentObject.name + ".png", bytes);
    }

    private Texture2D ToTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(rTex.width, rTex.height, _format, false);
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }
}
