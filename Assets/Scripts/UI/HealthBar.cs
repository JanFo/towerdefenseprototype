using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private float _maxHealth;

    private float _targetPercent;

    public Image foregroundFill;

    public Image backgroundFill;

    public RectTransform rectTransform;

    private void Update()
    {
        backgroundFill.fillAmount = Mathf.MoveTowards(backgroundFill.fillAmount, _targetPercent, Time.deltaTime);
    }

    public void SetHealth(int health)
    {
        _targetPercent = health / _maxHealth;
        foregroundFill.fillAmount = _targetPercent;
        
    }

    public void SetMaxHealth(int maxHealth)
    {
        _maxHealth = maxHealth;
        _targetPercent = 1.0f;
    }
}
