using System;
using UnityEngine;
/// <summary>
/// Manages entity view creation and gives access to entity views
/// </summary>
public enum MapTile : long
{
    Castle =            181230029,
    Normal =            255255255,
    Swamp =             185122087,
    Mountain =          000000000,
    Ruins =             163073164,
    Forest =            034177076,
    MonsterSpawn =      237028036,
    PlayerSpawn =       255242000
} 

public class MapParser
{
    public static MapTile[,] ParseTiles(Texture2D texture)
    {
        Color[] pixels = texture.GetPixels();

        MapTile[,] result = new MapTile[texture.width, texture.height];

        for(int i = 0; i < pixels.Length; i++)
        {
            // write to result...
            int x = i % texture.width;
            int y = i / texture.width;

            Color pixelColor = pixels[i];

            MapTile mt = GetTileType(pixelColor);

            result[x, y] = mt;
        }

        return result;
    }

    private static MapTile GetTileType(Color pixelColor)
    {
        long red = (long)(pixelColor.r * 255);
        long green = (long)(pixelColor.g * 255);
        long blue = (long)(pixelColor.b * 255);

        return (MapTile)(red * 1000000 + green * 1000 + blue);
    }
}
