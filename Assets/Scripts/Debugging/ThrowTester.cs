using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowTester : MonoBehaviour
{
    [SerializeField]
    private Transform _target;

    [SerializeField]
    private float _velocity;

    [SerializeField]
    private bool _trigger;

    private void OnValidate()
    {
        if(_target == null)
        {
            return;
        }

        float angle = MathUtils.GetThrowAngle(transform.position, _target.position, _velocity, Physics.gravity.y);
        Debug.Log(angle);
    }
}
