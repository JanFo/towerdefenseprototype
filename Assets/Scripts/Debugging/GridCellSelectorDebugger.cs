using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GridCellSelectorDebugger : MonoBehaviour
{
    [SerializeField]
    private Transform _start;

    [SerializeField]
    private Transform _end;

    [SerializeField]
    private float _gridSize = 1.0f;

    private HanUrasKoenigDebugger _optimizer;

    private List<Vector2Int> _gridCells;

    private void Start()
    {
        _gridCells = new List<Vector2Int>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_start == null || _end == null)
        {
            return;
        }

        if(_optimizer == null)
        {
            _optimizer = new HanUrasKoenigDebugger();
        }

        // _optimizer.InitializePatternLookup();

        _gridCells.Clear();
        _optimizer.FindCellsWithLookup(_gridCells, _start.position, _end.position, _gridSize, 32);
    }

    private void OnDrawGizmos()
    {
        if (_start == null || _end == null)
        {
            return;
        }

        if (_gridCells == null)
        {
            return;
        }

        Gizmos.DrawLine(_start.position + Vector3.up, _end.position + Vector3.up);

        for(int i = 0; i < _gridCells.Count; i++)
        {
            float x = (0.5f + _gridCells[i].x) * _gridSize;
            float z = (0.5f + _gridCells[i].y) * _gridSize;

            Gizmos.DrawCube(new Vector3(x, 0.0f, z), 0.2f * new Vector3(_gridSize, _gridSize, _gridSize));

        }
    }
}
