﻿public enum CharacterActionType {
    None,
    Build,
    Push,
    Enchant,
    Deconstruct,
    BuildZip,
    ConnectZip,
    UseZip,
    RotateTower,
    CallAirStrike
}
