﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class BitMaskUtils
{
    public static int WalkableBitSpan = 16;
    public static int TowerBitShift = 30;

    public static LevelTag MaxLevelTagInUse = LevelTag.Level30;
    public static NodeFlag MaxNodeFlagInUse = NodeFlag.TowerNode;


    public static bool IsWalkableFlag(uint flag)
    {
        return flag >> WalkableBitSpan == 0u;  // aka. check whether all non walkable bits are 0
    }
}
