﻿public enum MonsterType
{
    Wimp,
    Normal,
    Tank,
    Runner,
    Boss,
}
