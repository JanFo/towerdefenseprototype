﻿/// <summary>
/// Determines the distribution of monsters in the wave
/// </summary>
public enum WaveType
{
    Normal, // Normal monsters with some tanks and wimp
    Zerg, // Mostly wimps with some normal monsters
    Elite, // Mostly tanks with some normals
    Runner, // Fast monsters only
    Boss, // One or very few boss creatures
    Flying, // Flying units only
}


