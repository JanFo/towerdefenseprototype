﻿/// <summary>
/// Indicating the type of an Entity
/// </summary>
public enum EntityType
{
    Castle = 0,
    Monster = 2,
    Tower = 3,
    Spawn = 4,
    ZipTower = 5,
    Spy,
    Demolisher,
    Anchorman,
    Soldier,
    Barracks,
    AirStrikeBeacon
}
