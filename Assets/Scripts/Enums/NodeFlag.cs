﻿using System;

/// <summary>
/// Bit flag enumeration used as Tags indicating what a given Node contains (towers, monsters, etc.)
/// There are 33 possible flags. THe first 16 are reserved for generally walkable nodes, while the rest is reserved for non-walkable nodes.
/// </summary>
[Flags] public enum NodeFlag : uint
{
    // Walkable nodes
    FreeNode = 0u,
    PlayerNode = 1 << 0,
    MonsterNode = 1 << 1,
    CastleNode = 1 << 2,    // castle node is now walkable
    // free     = 1 << 3
    // free     = 1 << 4
    // free     = 1 << 5
    // free     = 1 << 6
    // free     = 1 << 7
    // free     = 1 << 8
    // free     = 1 << 9
    // free     = 1 << 10
    // free     = 1 << 11
    // free     = 1 << 12
    // free     = 1 << 13
    // free     = 1 << 14
    // free     = 1 << 15

    // generally non-walkable nodes
    BlockedNode = 1 << 16,
    // free     = 1 << 17
    // free     = 1 << 18
    // free     = 1 << 19
    // free     = 1 << 20
    // free     = 1 << 21
    // free     = 1 << 22
    // free     = 1 << 23
    // free     = 1 << 24
    // free     = 1 << 25
    // free     = 1 << 26
    // free     = 1 << 27
    // free     = 1 << 28
    // free     = 1 << 29
    TowerNode = 1 << 30
}
