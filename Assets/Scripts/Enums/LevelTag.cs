﻿
/// <summary>
/// A generic list of levels any Entity might be at.
/// Higher levels indicate tougher Monsters, Towers, Castles, etc.
/// </summary>
public enum LevelTag : short
{

    Level0 = 0,
    Level1 = 1,
    Level2 = 2,
    Level3 = 3,
    Level4 = 4,
    Level5 = 5,
    Level6 = 6,
    Level7 = 7,
    Level8 = 8,
    Level9 = 9,
    Level10 = 10,
    Level11 = 11,
    Level12 = 12,
    Level13 = 13,
    Level14 = 14,
    Level15 = 15,
    Level16 = 16,
    Level17 = 17,
    Level18 = 18,
    Level19 = 19,
    Level20 = 20,
    Level21 = 21,
    Level22 = 22,
    Level23 = 23,
    Level24 = 24,
    Level25 = 25,
    Level26 = 26,
    Level27 = 27,
    Level28 = 28,
    Level29 = 29,
    Level30 = 30

}
