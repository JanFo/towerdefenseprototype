﻿using System;

public struct Option<T>
{
    public static Option<T> None => default;
    public static Option<T> Some(T value) => new Option<T>(value);

    public readonly bool nonEmpty { get; }
    public readonly bool isEmpty { get { return ! nonEmpty; } }

    public readonly T value;

    Option(T value)
    {
        this.value = value;
        nonEmpty = this.value is { };
    }

    public T Get()
    {
        return value;
    }

    public Option<N> Map<N>(Func<T, N> mapFunc)
    {
        if (nonEmpty)
        {
            return new Option<N>(mapFunc.Invoke(value));
        }
        else
        {
            return Option<N>.None;
        }
    }
}
