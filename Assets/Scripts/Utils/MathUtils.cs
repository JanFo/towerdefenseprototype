using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MathUtils
{
    public static float GetThrowAngle(Vector3 source, Vector3 target, float velocity, float gravity)
    {
        float v = velocity;
        float h = source.y - target.y;
        Vector3 diff = target - source;
        float w = Mathf.Sqrt(diff.x * diff.x + diff.z * diff.z);
        float g = gravity;

        float v2 = v * v;
        float v4 = v2 * v2;
        float w2 = w * w;
        float w4 = w2 * w2;
        float g2 = g * g;
        float h2 = h * h;

        float n = v4 * (h2 + w2);

        float block1 = -Mathf.Sqrt(-v4 * w4 * (g2 * w2 - 2 * g * h * v2 - v4) / n);
        float block2 = g * h * v2 * w2 / n;
        float block3 = v4 * w2 / n;

        return Mathf.Rad2Deg * Mathf.Acos(-Mathf.Sqrt(block1 + block2 + block3) / Mathf.Sqrt(2));
    }

    public static float GetThrowDistance(float h, float v, float a, float g)
    {
        float sin = Mathf.Sin(a);
        return v * Mathf.Cos(a) * (v * sin + Mathf.Sqrt(v * v * sin * sin + 2 * g * h)) / g;
    }
}