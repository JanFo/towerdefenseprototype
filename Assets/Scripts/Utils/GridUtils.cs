using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public static class GridUtils
{
    public static Vector3 GetWorldPosition(int x, int y)
    {
        return new Vector3(0.5f + x, 0.0f, 0.5f + y) * GameConfiguration.tileSize;
    }

    public static Vector3 GetWorldPosition(Vector2Int gridPosition)
    {
        return new Vector3(0.5f + gridPosition.x, 0.0f, 0.5f + gridPosition.y) * GameConfiguration.tileSize;
    }


    public static Vector2Int GetGridPosition(Vector3 worldPosition)
    {
        worldPosition /= GameConfiguration.tileSize;
        return new Vector2Int((int)worldPosition.x, (int)worldPosition.z);
    }

    public static IEnumerable<GridNode> GetNeighbors(GridNode node, WorldGrid grid)
    {
        var minX = Math.Max(0, node.position.x -1);
        var minY = Math.Max(0, node.position.y -1);
        var maxX = Math.Min(node.position.x + 2, grid.cols);
        var maxY = Math.Min(node.position.y + 2, grid.rows);

        for (int i = minX; i < maxX; i++)
        {
            for (int j = minY; j < maxY; j++)
            {
                if(i != node.position.x || j != node.position.y)
                {
                    yield return grid.GetNode(i, j);
                }
            }
        }
    }

    public static List<Vector2Int> FindCells(Vector3 start, Vector3 end)
    {
        List<Vector2Int> cells = new List<Vector2Int>();

        var diffX = end.x - start.x;
        var diffY = end.z - start.z;
        var stepX = Math.Sign(diffX);
        var stepY = Math.Sign(diffY);

        start.x += GameConfiguration.tileSize * 0.5f + stepX * 0.01f;
        start.z += GameConfiguration.tileSize * 0.5f + stepY * 0.01f;
        end.x += GameConfiguration.tileSize * 0.5f - stepX * 0.01f;
        end.z += GameConfiguration.tileSize * 0.5f - stepY * 0.01f;

        //Grid cells are 1.0 X 1.0.
        int x = (int)Math.Floor(start.x);
        int z = (int)Math.Floor(start.z);

        //Ray/Slope related maths.
        //Straight distance to the first vertical grid boundary.
        var xOffset = end.x > start.x ?
            (Math.Ceiling(start.x) - start.x) :
          (start.x - Math.Floor(start.x));
        //Straight distance to the first horizontal grid boundary.
        var yOffset = end.z > start.z ?
            (Math.Ceiling(start.z) - start.z) :
          (start.z - Math.Floor(start.z));
        //Angle of ray/slope.
        var angle = Math.Atan2(-diffY, diffX);
        //NOTE: These can be divide by 0's, but JS just yields Infinity! :)
        //How far to move along the ray to cross the first vertical grid cell boundary.
        var tMaxX = xOffset / Math.Cos(angle);
        //How far to move along the ray to cross the first horizontal grid cell boundary.
        var tMaxY = yOffset / Math.Sin(angle);
        //How far to move along the ray to move horizontally 1 grid cell.
        var tDeltaX = 1.0 / Math.Cos(angle);
        //How far to move along the ray to move vertically 1 grid cell.
        var tDeltaY = 1.0 / Math.Sin(angle);

        //Travel one grid cell at a time.
        var manhattanDistance = Math.Abs(Math.Floor(end.x) - Math.Floor(start.x)) +
            Math.Abs(Math.Floor(end.z) - Math.Floor(start.z));

        for (int t = 0; t <= manhattanDistance; ++t)
        {
            cells.Add(new Vector2Int(x, z));

            //Only move in either X or Y coordinates, not both.
            if (Math.Abs(tMaxX) < Math.Abs(tMaxY))
            {
                tMaxX += tDeltaX;
                x += stepX;
            }
            else
            {
                tMaxY += tDeltaY;
                z += stepY;
            }
        }

        return cells;
    }

}