﻿using UnityEngine;
using System.Linq;

public static class TransformExtensions
{
    public static Transform ClearChildrenImmediate(this Transform transform)
    {
        GameObject[] objects = new GameObject[transform.childCount];
        int i = 0;

        foreach (Transform child in transform)
        {
            objects[i++] = child.gameObject;
        }

        foreach (GameObject obj in objects)
        {
            GameObject.DestroyImmediate(obj, true);
        }

        return transform;
    }

    public static RectTransform Fill(this RectTransform transform)
    {
        transform.anchorMin = Vector2.zero;
        transform.anchorMax = Vector2.one;
        transform.offsetMin = Vector2.zero;
        transform.offsetMax = Vector2.zero;

        return transform;
    }

    public static Transform Append(this Transform transform, GameObject child, bool applyLayer = false)
    {
        child.transform.SetParent(transform);
        child.transform.ClearPosRotScale();

        if(applyLayer) 
            child.gameObject.layer = transform.gameObject.layer;

        return transform;
    }

    public static Transform Append(this Transform transform, Component child, bool worldPositionStays = true)
    {
        child.transform.SetParent(transform, worldPositionStays);
        child.transform.ClearPosRotScale();

        return transform;
    }

    public static Transform Append(this Transform transform, Transform child, bool worldPositionStays = true)
    {
        child.SetParent(transform, worldPositionStays);
        child.ClearPosRotScale();

        return transform;
    }


    public static Transform ClearChildren(this Transform transform)
    {
        GameObject[] objects = new GameObject[transform.childCount];
        int i = 0;

        foreach (Transform child in transform)
        {
            objects[i++] = child.gameObject;
        }

        foreach (GameObject obj in objects)
        {
            obj.SetActive(false);
            Object.Destroy(obj);
        }

        transform.DetachChildren();
        return transform;
    }

    public static Transform DetachChildren(this Transform transform)
    {
        GameObject[] objects = new GameObject[transform.childCount];
        int i = 0;

        foreach (Transform child in transform)
        {
            objects[i++] = child.gameObject;
        }

        foreach (GameObject obj in objects)
        {
            obj.transform.SetParent(null);
        }

        return transform;
    }

    public static Transform ClearPosRot(this Transform transform)
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        return transform;
    }

    public static Transform ClearPosRotScale(this Transform transform)
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
        return transform;
    }

    public static Transform DebugLogHierarchy(this Transform transform)
    {
        foreach (Transform child in transform)
        {
            Debug.Log(child.name);

            child.DebugLogHierarchy();
        }

        return transform;
    }

    public static Transform FindInChildren(this Transform transform, string name)
    {
        return (from x in transform.GetComponentsInChildren<Transform>()
                where x.gameObject.name == name
                select x).First();
    }

}