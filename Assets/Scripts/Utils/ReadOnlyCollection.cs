using System;
using System.Collections.Generic;
using System.Linq;

public class ReadOnlyCollection<T>
{
    private List<T> _list;

    public List<T> ToList()
    {
        return new List<T>(_list);
    }

    public ReadOnlyCollection(List<T> list)
    {
        _list = list;
    }


    public StructEnumerator<T> GetEnumerator()
    {
        return new StructEnumerator<T>(_list);
    }

    public int Count => _list.Count;

    public T First => _list.FirstOrDefault();

    public T this[int index] => _list[index];

    public T Find(Func<T, bool> p)
    {
        foreach(T element in _list)
        {
            if(p(element))
            {
                return element;
            }
        }

        return default;
    }

    public List<T> GetAll(Func<T, bool> p)
    {
        List<T> result = new List<T>();

        foreach (T element in _list)
        {
            if (p(element))
            {
                result.Add(element);
            }
        }

        return result;
    }
}


public struct StructEnumerator<T>
{
    private readonly List<T> _pool;
    private int _index;

    public StructEnumerator(List<T> pool)
    {
        _pool = pool;
        _index = 0;
    }

    public T Current
    {
        get
        {
            if (_pool == null || _index == 0)
                throw new InvalidOperationException();

            return _pool[_index - 1];
        }
    }

    public bool MoveNext()
    {
        _index++;
        return _pool != null && _pool.Count >= _index;
    }

    public void Reset()
    {
        _index = 0;
    }
}