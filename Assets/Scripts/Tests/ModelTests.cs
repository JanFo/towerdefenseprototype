using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using static Entity;

public class ModelTests
{
    private NetworkManager _network;

    [SetUp]
    public void Setup()
    {
        _network = new NetworkManager();
    }

    [UnityTest]
    public IEnumerator SerializeBarracks()
    {
        Barracks barracks = new Barracks();
        Barracks barracksClone = ReserializeEntity(barracks) as Barracks;

        Assert.AreEqual(barracks.type, barracksClone.type);

        yield return null;
    }

    [UnityTest]
    public IEnumerator SerializeCastle()
    {
        Castle barracks = new Castle();
        Castle barracksClone = ReserializeEntity(barracks) as Castle;

        Assert.AreEqual(barracks.type, barracksClone.type);

        yield return null;
    }

    private Entity ReserializeEntity(Entity entity)
    {
        StringWriter writer = new StringWriter();
        entity.Serialize(writer);

        EntityData entityData = new EntityData();
        entityData.type = entity.type;

        StringReader reader = new StringReader(writer.Text);
        return WorldSynchronizer.CreateFromMessage(entityData, reader);
    }
}
