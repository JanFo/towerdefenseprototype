using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.TestTools;
using static Entity;

public class NetworkTests
{
    private int port = 1237;

    private NetworkManager _hostNetwork;
    private NetworkManager _clientNetwork;

    [SetUp]
    public void Setup()
    {

        _hostNetwork = new NetworkManager();
        _hostNetwork.Listen(port);
    }

    [TearDown]
    public void TearDown()
    {
        _hostNetwork.Disconnect();
    }

    [UnityTest]
    public IEnumerator Connect()
    {
        _clientNetwork = new NetworkManager();
        _clientNetwork.Connect(IPAddress.Loopback, port);
        Assert.True(_clientNetwork.IsConnecting);

        int waitCycles = 10;

        while (_clientNetwork.IsConnecting)
        {
            yield return new WaitForSecondsRealtime(.5f);
            _clientNetwork.Update();
            _hostNetwork.Update();

            if (waitCycles <= 0)
            {
                Assert.Fail("Connection timeout");
            }

            waitCycles--;
        }

        Assert.False(_clientNetwork.IsConnecting);
        Assert.True(_clientNetwork.IsActive);
        _clientNetwork.Disconnect();
        yield return null;
    }

}
