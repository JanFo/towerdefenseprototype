using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using static Entity;

public class SerializerTests
{
    [UnityTest]
    public IEnumerator SerializeFloat()
    {
        StringWriter writer = new StringWriter();
        writer.WriteFloat(5.0f);

        StringReader reader = new StringReader(writer.Text);
        reader.ReadFloat();


        yield return null;
    }

  
    private Entity ReserializeEntity(Entity entity)
    {
        StringWriter writer = new StringWriter();
        entity.Serialize(writer);

        EntityData entityData = new EntityData();
        entityData.type = entity.type;

        StringReader reader = new StringReader(writer.ToString());
        return WorldSynchronizer.CreateFromMessage(entityData, reader);
    }
}
