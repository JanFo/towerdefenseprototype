using NUnit.Framework;
using System.Collections;
using System.Net;
using UnityEngine;
using UnityEngine.TestTools;

public class WorldSynchronizerTests
{
    private int port = 1237;

    private NetworkManager _masterNetwork;
    private NetworkManager _clientNetwork;

    private World _masterWorld;
    private World _clientWorld;

    private WorldSynchronizer _masterSynchronizer;
    private WorldSynchronizer _clientSynchronizer;
    private Level _level;

    [SetUp]
    public void Setup()
    {
        GameConfiguration gameConfiguration = Resources.Load<GameConfiguration>("TD_Test_Game_Config");
        LevelConfiguration levelConfiguration = Resources.Load<LevelConfiguration>("TD_Test_Level");

        gameConfiguration.Initialize();

        _masterNetwork = new NetworkManager();
        _masterNetwork.Listen(port);

        _level = new Level(levelConfiguration);
        _level.Load();

        _masterWorld = new World(_level, true);
        _clientWorld = new World(_level, false);

        _masterSynchronizer = new WorldSynchronizer(_masterWorld, _masterNetwork);
    }

    [TearDown]
    public void TearDown()
    {
        _masterNetwork.Disconnect();
    }

    [UnityTest]
    public IEnumerator Synchronize()
    {
        _clientNetwork = new NetworkManager();
        _clientNetwork.Connect(IPAddress.Loopback, port);

        int waitCycles = 10;

        while (_clientNetwork.IsConnecting)
        {
            yield return new WaitForSecondsRealtime(.5f);
            _clientNetwork.Update();
            _masterNetwork.Update();

            if (waitCycles <= 0)
            {
                Assert.Fail("Connection timeout");
            }

            waitCycles--;
        }

        _clientSynchronizer = new WorldSynchronizer(_clientWorld, _clientNetwork);

        InitializeWorld();
        Assert.NotNull(_masterWorld.castle);
        Assert.Null(_clientWorld.castle);

        waitCycles = 10;


        Debug.Log("==== Initial Sync ====");

        while (_clientWorld.castle == null)
        {
            yield return new WaitForSecondsRealtime(.5f);
            _clientNetwork.Update();
            _clientSynchronizer.Update();
            _masterNetwork.Update();
            _masterSynchronizer.Update();
            waitCycles--;
        }


        Assert.NotNull(_clientWorld.castle);

        waitCycles = 10;

        Debug.Log("==== Snyc Barracks Master to Client ====");
        _masterWorld.barracks.x = 5;
        _masterWorld.barracks.SetRealDirty();

        while (_clientWorld.barracks.x != 5)
        {
            yield return new WaitForSecondsRealtime(.5f);
            _clientNetwork.Update();
            _clientSynchronizer.Update();
            _masterNetwork.Update();
            _masterSynchronizer.Update();
            waitCycles--;
        }

        Assert.AreEqual(_clientWorld.barracks.x, 5);

        Debug.Log("==== Snyc Barracks Client to Master ====");
        _clientWorld.barracks.x = 6;
        _clientWorld.barracks.SetRealDirty();

        while (_masterWorld.barracks.x != 6)
        {
            yield return new WaitForSecondsRealtime(.5f);
            _clientNetwork.Update();
            _clientSynchronizer.Update();
            _masterNetwork.Update();
            _masterSynchronizer.Update();
            waitCycles--;
        }

        Assert.AreEqual(_masterWorld.barracks.x, 6);
    }

    private void InitializeWorld()
    {
        _masterWorld.AddEntity(new Castle(_level));

        Barracks barracks = new Barracks();
        barracks.SetSpawnLocation(_level.playerSpawn);

        _masterWorld.AddEntity(barracks);

        _masterWorld.AddEntity(new Spy()
        {
            worldPosition = barracks.worldPosition
        });

        _masterWorld.AddEntity(new Demolisher()
        {
            worldPosition = barracks.worldPosition
        });

        _masterWorld.AddEntity(new Soldier()
        {
            worldPosition = barracks.worldPosition
        });

        _masterWorld.AddEntity(new Anchorman()
        {
            worldPosition = barracks.worldPosition
        });

        foreach (SpawnLocation spawnLocation in _level.spawnLocations)
        {
            _masterWorld.AddEntity(new Spawn()
            {
                x = spawnLocation.position.x,
                y = spawnLocation.position.y,
                name = spawnLocation.name
            });
        }
    }
}
