﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Config/Game")]
public class GameConfiguration : ScriptableObject
{
    // singleton
    public static GameConfiguration instance;


    // --------------------------------------

    public GameObject castlePrefab;

    [SerializeField]
    public GameObject monsterPrefab = null;

    [SerializeField]
    public GameObject towerPrefab = null;

    [SerializeField]
    public Mesh characterPlaceholderMesh = null;

    [SerializeField]
    public Material placeholderMaterial = null;

    [SerializeField]
    private HealthBar _healthBarPrefab;

    public static short maxPlayers = 4;

    public static short defaultCharacterHealth = 100;       // FIXME set this where the character attributes are defined

    public static float tileSize = 1.0f;

    public GameObject moveParticlePrefab;

    public GameObject airStrikePrefab;

    public float towerMoveSpeed = 4.0f;

    public Mesh cursorMesh;

    public Material cursorMaterial;

    public GameObject simpleBulletPrefab;

    public RectTransform gameUserInterface;

    public Mesh mountainMesh;

    public Material mountainMaterial;

    public Mesh socketMesh;

    public Material spawnMaterial;

    public Material castleMaterial;

    

    // private static Dictionary<int, WaveConfig> _waveConfigs;

    public float spawnInterval = 20.0f;

    public TowerConfiguration zipLineTowerConfiguration;

    public UIProgressBar buildProgressBarPrefab;

    public float deconstructDuration;

    public UIProgressBar deconstructProgressBarPrefab;

    public RectTransform towerIconPrefab;

    public CharacterClassConfiguration[] classConfigurations;

    public TowerConfiguration[] towerConfigurations;

    public float zipLineSpeed = 3;

    public static CharacterClassConfiguration GetCharacterClassConfiguration(EntityType type)
    {
        return ArrayUtils.Find(instance.classConfigurations, c => c.entityType == type);
    }
   

    //public static TowerConfiguration GetTowerConfiguration(EntityType type, int towerIndex, int tier)
    //{
    //    var config = ArrayUtils.Find(instance.classConfigurations, c => c.entityType == type);
    //    ConstructionLine line = ArrayUtils.Get(config.constructionConfig.constructionLines, towerIndex, new ConstructionLine());

    //    if (line.configs == null)
    //    {
    //        return null;
    //    }

    //    return ArrayUtils.Get(line.configs, tier, null);
    //}

 
    public static HealthBar healthBarPrefab => instance._healthBarPrefab;

    public void Initialize()
    {
        instance = this;
        // _waveConfigs = new List<WaveConfig>(waveConfigs).ToDictionary(c => c.waveId);

        int ids = 0;

        foreach(TowerConfiguration towerConfiguration in instance.towerConfigurations)
        {
            towerConfiguration.id = ids++; 
        }
    }

    // Stat functions
    public static float GetFireInterval(int speed)
    {
        return 2.5f - (speed - 1) * 0.25f;
    }

    public static float GetRange(int range)
    {
        return 0.4f + 1.0f * range;
    }

    public static int GetDamage(int damage)
    {
        return damage;
    }

    public static short GetMonsterHealth(MonsterTheme theme, MonsterType healthClass, int level)
    {
        int baseHealth = 0;
        int healthPerLevel = 0;

        switch(healthClass)
        {
            case MonsterType.Normal:
                baseHealth = 25;
                healthPerLevel = 10;
                break;
            case MonsterType.Tank:
                baseHealth = 60;
                healthPerLevel = 50;
                break;
            case MonsterType.Wimp:
                baseHealth = 10;
                healthPerLevel = 10;
                break;
        }

        switch(theme)
        {
            case MonsterTheme.Horde:
                baseHealth += 5;
                break;
        }

        return (short)(level * healthPerLevel + baseHealth);
    }

    public static int GetTowerStatPoints(byte level)
    {
        return 10 + level * 5;
    }

    internal static float GetMonsterSpeed(MonsterTheme theme, MonsterType speedClass, int monsterLevel)
    {
        switch(theme)
        {
            case MonsterTheme.Horde:
                return 1.3f;
            case MonsterTheme.Wildlife:
                return 2.0f;
        }

        return 1.5f;
    }

    internal static short GetMonsterBounty(MonsterType healthClass, byte monsterLevel)
    {
        return 2;
    }
}