﻿using Assets.Scripts.Model;
using UnityEngine;

[CreateAssetMenu(menuName = "Config/AirStrikeBeacon")]
public class AirStrikeBeaconConfiguration : TowerConfiguration
{
    public override Tower CreateTower()
    {
        AirStrikeBeacon tower = new AirStrikeBeacon();
        tower.hp = hp;
        tower.towerId = id;
        return tower;
    }
}
