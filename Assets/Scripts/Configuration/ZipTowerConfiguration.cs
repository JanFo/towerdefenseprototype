﻿using Assets.Scripts.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(menuName = "Config/ZipTower")]
public class ZipTowerConfiguration : TowerConfiguration
{
    [SerializeField]
    public float zipSpeed;

    [SerializeField]
    public int zipRange;

    [SerializeField]
    public float zipLineHeight;

    [SerializeField]
    public float costPerTile;

    public override Tower CreateTower()
    {
        ZipTower tower = new ZipTower();
        tower.speed = zipSpeed;
        tower.range = zipRange;
        tower.zipLineHeight = zipLineHeight;
        tower.hp = hp;
        tower.towerId = id;
        return tower;
    }
}
