﻿using System.Collections.Generic;
using UnityEngine;

public class WaveTypeDefinition : ScriptableObject
{
    private static Dictionary<WaveType, (MonsterTag, float)[]> _waveTypesDefinitions;

    public static void Initialize()
    {
        _waveTypesDefinitions = new Dictionary<WaveType, (MonsterTag, float)[]>();

        _waveTypesDefinitions[WaveType.Normal] = new (MonsterTag, float)[]
        {
            (MonsterTag.Small, 0.2f),
            (MonsterTag.Big, 0.2f),
            //(MonsterTag.Rich, 0.1f),
        };

        _waveTypesDefinitions[WaveType.Elite] = new (MonsterTag, float)[]
        {
            (MonsterTag.Big, 0.8f),
            (MonsterTag.Rich, 0.1f),
        };

        _waveTypesDefinitions[WaveType.Zerg] = new (MonsterTag, float)[]
        {
            (MonsterTag.Small, 0.8f),
            (MonsterTag.Rich, 0.1f),
        };

    }

    public static (MonsterTag, float)[] GetDistribution(WaveType waveType)
    {
        return ArrayUtils.Get(_waveTypesDefinitions, waveType, null);
    }

}

