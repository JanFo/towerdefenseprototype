﻿using UnityEngine;


[CreateAssetMenu(menuName = "Config/Tower")]
public class TowerConfiguration : ScriptableObject
{
    [HideInInspector]
    public int id;

    [SerializeField]
    private string label;

    [SerializeField]
    public Sprite icon;

    public TowerView prefab;

    public float constructionTime;

    public int cost;

    public int hp;

    public int maxInstances = 0;

    public bool isBlocking;

    public float attackInterval;

    public int attackDamage;

    public int attackRange;

    public bool canBeRotated;

    public TowerConfiguration upgrade;


    public string GetLocalizedName()
    {
        return label;
    }


    public virtual Tower CreateTower()
    {
        Tower tower = new Tower();
        tower.speed = attackInterval;
        tower.damage = attackDamage;
        tower.range = attackRange;
        tower.hp = hp;
        tower.towerId = id;
        tower.isBlocking = isBlocking;
        tower.canBeRotated = canBeRotated;
        tower.direction = Assets.Scripts.Model.WorldDirection.North;

        return tower;
    }
}