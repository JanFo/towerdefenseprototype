﻿using System;
using UnityEngine;

[Serializable]
public struct ConstructionLine
{
    public string name;
    public Sprite icon;
    public TowerConfiguration[] configs;
}

[CreateAssetMenu(menuName = "Config/Construction")]
public class ConstructionConfiguration : ScriptableObject
{
    public TowerConfiguration[] constructionLines;


   
}