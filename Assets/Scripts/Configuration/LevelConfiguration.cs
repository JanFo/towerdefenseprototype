using System.Collections.Generic;
using UnityEngine;

// Game data that needs to be kept in sync across clients
// Each player holds an instance of this class

[CreateAssetMenu(menuName = "Config/Level")]
public class LevelConfiguration : ScriptableObject
{
    public Texture2D mapTexture;
    public AnimationCurve budgetCurve;
    public AnimationCurve healthCurve;
    public short startingHealth;
    public int startingGold;
    public int waveIntervalSeconds;
    public int respawnCooldown;
    public MonsterView[] monsters;
    public int waveCount;
    public GameObject[] mountainPrefabs;
    public PatchSetConfiguration mountainPatchSet;
}

public struct SpawnLocation
{
    public string name;
    public Vector2Int position;
}

public class Level
{
    private LevelConfiguration _configuration;

    private MapTile[,] _map;

    public int waveCount => _configuration.waveCount;

    public float waveIntervalSeconds => _configuration.waveIntervalSeconds;

    public int size => _map.GetLength(0);

    public short startingHealth => _configuration.startingHealth;

    public int startingGold => _configuration.startingGold;

    public int respawnCooldown => _configuration.respawnCooldown;

    public AnimationCurve healthCurve => _configuration.healthCurve;

    public AnimationCurve budgetCurve => _configuration.budgetCurve;



    public MonsterView[] monsters => _configuration.monsters;

    public Level(LevelConfiguration configuration)
    {
        _configuration = configuration;
    }

    public MapTile[,] map => _map;

    public void Load()
    {
        _map = MapParser.ParseTiles(_configuration.mapTexture);

      
        spawnLocations = new List<SpawnLocation>();

        for (int x = 0; x < _map.GetLength(0); x++)
        {
            for (int y = 0; y < _map.GetLength(1); y++)
            {
                if (_map[x, y] == MapTile.Castle)
                {
                    castlePosition = new Vector2Int(x, y);
                }

                if (_map[x, y] == MapTile.PlayerSpawn)
                {
                    playerSpawn = new SpawnLocation()
                    {
                        position = new Vector2Int(x, y),
                        name = "Player"
                    };

                    GameObject mountainObject = new GameObject("player-spawn");
                    MeshFilter meshFilter = mountainObject.AddComponent<MeshFilter>();
                    meshFilter.mesh = GameConfiguration.instance.socketMesh;
                    MeshRenderer meshRenderer = mountainObject.AddComponent<MeshRenderer>();
                    meshRenderer.material = GameConfiguration.instance.spawnMaterial;
                    mountainObject.transform.localScale = new Vector3(GameConfiguration.tileSize / 2.0f, GameConfiguration.tileSize / 2.0f, GameConfiguration.tileSize / 2.0f);
                    mountainObject.transform.localEulerAngles = new Vector3(-90, 0, 0);
                    mountainObject.transform.position = new Vector3(x + 0.5f, 0, y + 0.5f);
                }

                if (_map[x, y] == MapTile.MonsterSpawn)
                {
                    spawnLocations.Add(new SpawnLocation()
                    {
                        position = new Vector2Int(x, y),
                        name = "ASDF"
                    });

                    GameObject mountainObject = new GameObject("spawn");
                    MeshFilter meshFilter = mountainObject.AddComponent<MeshFilter>();
                    meshFilter.mesh = GameConfiguration.instance.socketMesh;
                    MeshRenderer meshRenderer = mountainObject.AddComponent<MeshRenderer>();
                    meshRenderer.material = GameConfiguration.instance.spawnMaterial;
                    mountainObject.transform.localScale = new Vector3(GameConfiguration.tileSize / 2.0f, GameConfiguration.tileSize / 2.0f, GameConfiguration.tileSize / 2.0f);
                    mountainObject.transform.localEulerAngles = new Vector3(-90, 0, 0);
                    mountainObject.transform.position = new Vector3(x + 0.5f, 0, y + 0.5f);
                }
            }
        }

        bool[,] mountainMap = new bool[_map.GetLength(0) * 2 , _map.GetLength(1) * 2];

        ArrayUtils.Initialize(mountainMap, (x, y) => _map[x / 2, y / 2] == MapTile.Mountain);
        float tileSize = 1.0f;

        GameObject batchingRoot = new GameObject("Static_Level_Geometry");

        for (int x = 0; x < mountainMap.GetLength(0); x++)
        {
            for (int y = 0; y < mountainMap.GetLength(1); y++)
            {
                if (mountainMap[x, y])
                {
                    GameObject mountainObject = _configuration.mountainPatchSet.CreatePiece(mountainMap, x, y);
                   
                    if (mountainObject != null)
                    {
                        mountainObject.transform.parent = batchingRoot.transform;
                        mountainObject.transform.position = new Vector3(0.5f * x + 0.25f * tileSize, 0, 0.5f * y + 0.25f * tileSize);
                        mountainObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    }
                }
            }


        }

        bool[,] mountain = new bool[3, 3];
        ArrayUtils.Initialize(mountain, (x, y) => true);

        int width = mountainMap.GetLength(0);
        int depth = mountainMap.GetLength(1);
        int padding = 20;

        for (int x = -padding; x <= width + padding; x++)
        {
            for (int y = -padding; y <= depth + padding; y++)
            {
                if (x >= 0 && y >= 0 && x < width && y < depth) {
                    continue;
                }

                GameObject mountainObject = _configuration.mountainPatchSet.CreatePiece(mountain, 1, 1);
                mountainObject.transform.parent = batchingRoot.transform;
                mountainObject.transform.position = new Vector3(0.5f * x + 0.25f * tileSize, 0, 0.5f * y + 0.25f * tileSize);
                mountainObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }
        }

        StaticBatchingUtility.Combine(batchingRoot);
    }

    public Vector2Int castlePosition { get; private set; }

    public SpawnLocation playerSpawn { get; private set; }

    public List<SpawnLocation> spawnLocations { get; private set; }

    public Mesh CreateObject()
    {
        return null;
    }
}
