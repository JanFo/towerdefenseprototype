﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Config/Character Class")]
public class CharacterClassConfiguration : ScriptableObject
{
    public EntityType entityType;
    public Sprite icon;
    public string descriptionKey;
    public GameObject prefab;
    public TowerConfiguration[] towers;
    public TowerConfiguration GetTowerConfiguration(int lineIndex)
    {
        return ArrayUtils.Get(towers, lineIndex, null);
    }
}