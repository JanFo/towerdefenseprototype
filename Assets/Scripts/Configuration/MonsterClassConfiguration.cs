﻿using System;

[Serializable]
public struct MonsterClassConfiguration
{
    public byte minLevel;

    public byte maxLevel;

    public int baseHealth;

    public int healthPerLevel;

    public MonsterView[] views;

    public int GetCost(int level)
    {
        return 1;
    }
}

