using System;
using System.Collections.Generic;
using UnityEngine;

// Game data that needs to be kept in sync across clients
// Each player holds an instance of this class

[CreateAssetMenu(menuName = "Config/Patch Set")]
public class PatchSetConfiguration : ScriptableObject
{
    public bool outOfRangeSet = true;

    public Mesh center;
    public Mesh innerCorner;
    public Mesh bridge;
    public Mesh tee;
    public Mesh split;
    public Mesh cross;
    public Mesh outerCorner;
    public Mesh edge;
    public Mesh ridge;
    public Mesh ridgeBend;
    public Mesh ridgeTee;
    public Mesh ridgeEnd;
    public Mesh corneredEdge;
    public Mesh corneredEdgeMirrored;
    public Mesh island;
    public Material[] materials;

    private Dictionary<string, Tuple<Mesh, float>> _lookupTable;

    public void InitializeLookupTable()
    {
        _lookupTable = new Dictionary<string, Tuple<Mesh, float>>();

        _lookupTable["xxxxcxxxx"] = new Tuple<Mesh, float>(center, 0);
        _lookupTable["axxocxaoa"] = new Tuple<Mesh, float>(outerCorner, 0);
        _lookupTable["xxxxcxoxx"] = new Tuple<Mesh, float>(innerCorner, 0);
        _lookupTable["oxoxcxoxx"] = new Tuple<Mesh, float>(split, 0);
        _lookupTable["xxxxcxaoa"] = new Tuple<Mesh, float>(edge, 0);
        _lookupTable["aoaocxaoa"] = new Tuple<Mesh, float>(ridgeEnd, 0);
        _lookupTable["aoaocoaoa"] = new Tuple<Mesh, float>(island, 0);
        _lookupTable["xxoxcxoxx"] = new Tuple<Mesh, float>(bridge, 0);
        _lookupTable["xxxxcxoxo"] = new Tuple<Mesh, float>(tee, 0);
        _lookupTable["aoaxcxoxo"] = new Tuple<Mesh, float>(ridgeTee, 0);
        _lookupTable["aoaxcxaoa"] = new Tuple<Mesh, float>(ridge, 0);
        _lookupTable["aoaocxaxo"] = new Tuple<Mesh, float>(ridgeBend, 0);
        _lookupTable["aoaxcxxxo"] = new Tuple<Mesh, float>(corneredEdge, 0);
        _lookupTable["aoaxcxoxx"] = new Tuple<Mesh, float>(corneredEdgeMirrored, 0);

        CompleteTable();
    }

    private void CompleteTable()
    {
        SplitPatterns();
        RotatePatterns();
    }

    private void RotatePatterns()
    {
        List<string> keys = new List<string>(_lookupTable.Keys);

        for (int i = 0; i < keys.Count; i++)
        {
            string pattern = keys[i];
            Mesh mesh = _lookupTable[pattern].Item1;
            float rotation = _lookupTable[pattern].Item2;

            for(int k = 0; k < 3; k++)
            {
                rotation += 90.0f;
                pattern = RotatePatternClockwise(pattern);

                _lookupTable[pattern] = new Tuple<Mesh, float>(mesh, rotation);
            }
        }
    }

    private string RotatePatternClockwise(string pattern)
    {
        string result = "";

        result += pattern[6];
        result += pattern[3];
        result += pattern[0];
        result += pattern[7];
        result += pattern[4];
        result += pattern[1];
        result += pattern[8];
        result += pattern[5];
        result += pattern[2];

        return result;
    }


    private void SplitPatterns()
    {
        List<string> keys = new List<string>(_lookupTable.Keys);

        for (int i = 0; i < keys.Count; i++)
        {
            string key = keys[i];

            if (key.Contains("a"))
            {
                int aIndex = key.IndexOf("a");

                _lookupTable[key.Substring(0, aIndex) + "o" + key.Substring(aIndex + 1)] = _lookupTable[key];
                _lookupTable[key.Substring(0, aIndex) + "x" + key.Substring(aIndex + 1)] = _lookupTable[key];
                _lookupTable.Remove(key);

                SplitPatterns();
                return;
            }
        }
    }

    public GameObject CreatePiece(bool[,] field, int x, int y)
    {
        if(_lookupTable == null)
        {
            InitializeLookupTable();
        }

        if(!IsSet(field, x, y))
        {
            return null;
        }

       
        string key = GetLookupKey(field, x, y);
        Tuple<Mesh, float> meshAndRotation = ArrayUtils.Get(_lookupTable, key, null);

        if(meshAndRotation == null)
        {
            return null;
        }

        GameObject piece = new GameObject("Patch_" + key);
        GameObject modelObject = new GameObject("Model");
        MeshFilter modelMeshFilter = modelObject.AddComponent<MeshFilter>();
        modelMeshFilter.sharedMesh = meshAndRotation.Item1;

        MeshRenderer modelRenderer = modelObject.AddComponent<MeshRenderer>();

        Material[] materialArray = new Material[meshAndRotation.Item1.subMeshCount];

        for(int i = 0; i < materialArray.Length; i++)
        {
            materialArray[i] = materials[i];
        }

        modelRenderer.sharedMaterials = materialArray;

        modelObject.transform.parent = piece.transform;
        modelObject.transform.localPosition = Vector3.zero;
        modelObject.transform.localEulerAngles = new Vector3(-90, meshAndRotation.Item2, 0);

        return piece;
    }

    private bool IsSet(bool[,] field, int x, int y)
    {
        if(x < 0 || y < 0 || x >= field.GetLength(0) || y >= field.GetLength(1))
        {
            return outOfRangeSet;
        }

        return field[x, y];
    }

    private string GetLookupKey(bool[,] field, int x, int y)
    {
        string result = "";

        for (int i = 1; i >= -1; i--)
        {
            for (int j = -1; j <= 1; j++)
            {
                if (i == 0 && j == 0)
                {
                    result += "c";
                }
                else
                {
                    result += IsSet(field, x + j, y + i) ? "x" : "o";
                }
            }
        }

        return result;
    }
}
