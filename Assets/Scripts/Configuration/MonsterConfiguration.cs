﻿using UnityEngine;

[CreateAssetMenu(menuName = "Config/Beastiary Entry")]
public class MonsterConfiguration : ScriptableObject
{
    public MonsterView view;

    public float moveSpeed;

    public int loot;
}