using System.Collections.Generic;

public class WaveGenerator
{
    private const float DEFAULT_SPAWN_INTERVAL = 0.6f;

    private const int DEFAULT_INITIAL_DELAY = 10;

    //public static List<Wave> GenerateWaves(Level level)
    //{
    //    WaveTypeDefinition.Initialize();

    //    List<Wave> waves = new List<Wave>();

    //    for(int i = 0; i < level.waveCount; i++)
    //    {
    //        float curveIndex = i / (float)level.waveCount;

    //        Wave wave = new Wave();
    //        wave.budget = level.budgetCurve.Evaluate(curveIndex);
    //        wave.healthScale = level.healthCurve.Evaluate(curveIndex);


    //        wave.prevalence = new float[level.spawnLocations.Count];
    //        wave.prevalence[0] = 1.0f;

    //        MonsterTag themeTag = MonsterTag.Wildlife; // RollTheme(); 

    //        wave.amounts = new Dictionary<MonsterTag, float>();
    //        wave.amounts[themeTag] = 1.0f;

    //        wave.waveType = WaveType.Normal;

    //        foreach(var entry in WaveTypeDefinition.GetDistribution(WaveType.Normal))
    //        {
    //            wave.amounts[entry.Item1] = entry.Item2;
    //        }

    //        wave.initialDelay = DEFAULT_INITIAL_DELAY;
    //        wave.spawnInterval = DEFAULT_SPAWN_INTERVAL;

    //        waves.Add(wave);
    //    }

    //    return waves;
    //}

    public static List<Monster> GenerateMonsters(Wave wave)
    {
        List<Monster> monsters = new List<Monster>();

        int powerBudget = (int)wave.budget; // wave.healthScale;

        for(int i = 0; i < wave.monsterCount; i++)
        {
            Monster monster = new Monster();
            // monster.health = 1.0f;
            monster.speed = 1.0f;
            monster.initialSpeed = 1.0f;

            //foreach (var amount in wave.amounts)
            //{
            //    float randomValue = UnityEngine.Random.value;

            //    if (randomValue < amount.Value)
            //    {
            //        MonsterTagModifier.Apply(monster, amount.Key);
            //        // Problem: Sequentielles w�rfeln erzeugt bedingte Wahrscheinlichkeitsscheise und so
            //    }
            //}

            // powerBudget -= monster.GetCost();

            monster.initialHp = (short)wave.monsterHealth;
            monster.hp = monster.initialHp;
            monster.bounty = (short)wave.monsterBounty;
            monster.viewIndex = wave.monsterViewIndex;
            monsters.Add(monster);

        }

        return monsters;
    }

    private static MonsterTag RollTheme()
    {
        return ArrayUtils.GetRandom(new MonsterTag[]
        {
            MonsterTag.Horde,
            MonsterTag.Wildlife
        });
    }
}
