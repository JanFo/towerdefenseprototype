﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class WaveEngine : MonoBehaviour
{
    private static WaveEngine _instance;

    private void Start()
    {
        _instance = this;
    }

    public static Coroutine StartRoutine(IEnumerator routine)
    {
        return _instance.StartCoroutine(routine);
    }

    public static void StopRoutine(Coroutine routine)
    {
        if (routine == null) return;
        _instance.StopCoroutine(routine);
    }

}
