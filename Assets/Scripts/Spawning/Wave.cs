using System;
using System.Collections.Generic;
using System.Linq;

// How do we make sure that a wave is not super strong (randomness). Point system?
public class Wave
{
    public static int MinDiffBetweenMonstersMs = 500;

    // public int waveId;               // the current wave id
    public float initialDelay;        // the delay before the wave starts beginning from the time of creation
    public float healthScale;        // number of monsters still to come in the current wave
    public float spawnInterval = 0.5f; // How fast the monster spawn after another in seconds
    public int monsterMinLevel;   // the average monster level in the wave
    public int monsterMaxLevel;
    public float[] prevalence;

    public Dictionary<MonsterTag, float> amounts;

    public MonsterTheme monsterTheme;
    internal float budget;
    internal WaveType waveType;
    public int index;
    public int monsterHealth;
    public int monsterBounty;
    public int monsterCount;
    public int monsterViewIndex;

    public Wave(Level level, int i)
    {
        monsterCount = 10;
        initialDelay = level.waveIntervalSeconds;

        int baseMonsterStep = level.waveCount / level.monsters.Length;
        monsterViewIndex = Math.Min(i / baseMonsterStep, level.monsters.Length - 1);


        index = i;
        monsterHealth = (int)level.healthCurve.Evaluate(i / (float)level.waveCount);
        monsterBounty = (int)level.budgetCurve.Evaluate(i / (float)level.waveCount);

    }
}
