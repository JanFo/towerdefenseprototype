using UnityEngine;

public interface IGameInput
{
    void JoinGame();
    void BeginPrimaryAction();
    void CancelPrimaryAction();
    void MoveCharacter(Vector3 position);
}
