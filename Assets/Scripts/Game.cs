using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Game 
{
    public int port = 1235;

    [SerializeField]
    private GameConfiguration _gameConfiguration;

    [SerializeField]
    private LevelConfiguration _levelConfiguration;

    private World _world;

    private PlayerInput _input;

    private NetworkManager _network;

    private Player _player;

    private PlayerInputController _playerInputController;

    private Level _level;

    private GameController _gameController;

    private EntityViewController _entityViewManager;

    private CharacterActionController _characterActionController;

    private WorldSynchronizer _worldSynchronizer;

    private WorldController _worldController;


    private UserInterfaceController _userInterfaceController;

    private string _ipAddressString;

    private bool _isPlaying;

    public Game(NetworkManager networkManager, PlayerInput playerInput, GameConfiguration gameConfiguration,
        LevelConfiguration levelConfiguration)
    {
        _input = playerInput;
        _network = networkManager;
        _gameConfiguration = gameConfiguration;
        _levelConfiguration = levelConfiguration;
    }

    // Start is called before the first frame update
    void Start()
    {
        _player = new Player();
        _network.RegisterProcedures(this);
    }

    public void HostGame()
    {
        _network.Listen(port);

        _level = new Level(_levelConfiguration);
        _level.Load();

        _world = new World(_level, true);

        InitializeWorld();
        InitializeCommonControllers();
        
        _gameController = new GameController(_world, _level);

        _isPlaying = true;
    }

    private void JoinGame()
    {
        _network.Connect(IPAddress.Parse(_ipAddressString), port);
        _network.OnConnectedToHost.AddListener(OnConnectedToHost);
    }

    private void OnConnectedToHost()
    {
        _level = new Level(_levelConfiguration);
        _level.Load();

        _world = new World(_level, false);
        InitializeCommonControllers();
        _isPlaying = true;

        Debug.Log($"[{_network.Name}] Connected to Host! Sending Ready-Message!");
        IMessageWriter writer = _network.AppendMessage("OnClientReadyToPlay", true);
        writer.Send();
    }

    private void InitializeWorld()
    {
        _world.AddEntity(new Castle(_level));

        Barracks barracks = new Barracks();
        barracks.SetSpawnLocation(_level.playerSpawn);

        _world.AddEntity(barracks);

        _world.AddEntity(new Spy()
        {
            worldPosition = barracks.worldPosition
        });

        _world.AddEntity(new Demolisher()
        {
            worldPosition = barracks.worldPosition
        });

        _world.AddEntity(new Soldier()
        {
            worldPosition = barracks.worldPosition
        });

        _world.AddEntity(new Anchorman()
        {
            worldPosition = barracks.worldPosition
        });

        foreach (SpawnLocation spawnLocation in _level.spawnLocations)
        {
            _world.AddEntity(new Spawn()
            {
                x = spawnLocation.position.x,
                y = spawnLocation.position.y,
                name = spawnLocation.name
            });
        }
    }

 

    [HostProcedure]
    private void OnClientReadyToPlay(IMessageReader reader)
    {
        Debug.Log($"[{_network.Name}] Ready message received!");
        _worldSynchronizer.Resend();
    }

    /// <summary>
    /// Controllers that are being run on ALL clients (including host)
    /// </summary>
    private void InitializeCommonControllers()
    {
        Canvas canvas = UnityEngine.Object.FindObjectOfType<Canvas>();

        _player.index = _network.ClientId;

        // Manages all scene representations of entities. Can be used to retrieve GameObjects for entities
        _entityViewManager = new EntityViewController(_world);

        // Handles player input
        _playerInputController = new PlayerInputController(_player, _input, _world);

        // Runs the character actions
        _characterActionController = new CharacterActionController(_world, _entityViewManager, canvas);

        // Synchronizes the world between clients
        _worldSynchronizer = new WorldSynchronizer(_world, _network);

        // Keeps the world grid in sync with the world entities
        _worldController = new WorldController(_world);

        // User interface controller
        _userInterfaceController = new UserInterfaceController(_player, _world, _input, canvas);
    }

    // Update is called once per frame
    void Update()
    {
        _network.Update();

        if(!_isPlaying)
        {
            return;
        }

        if (_network.IsHost)
        {
            _gameController.Update();
        }

        _playerInputController.Update(Time.deltaTime);
        _characterActionController.Update();
        _entityViewManager.Update();
        _worldSynchronizer.Update();
        _userInterfaceController.Update();
        _worldController.Update();
    }


}
