﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;

namespace GameActions.Editor
{
    [CustomEditor(typeof(ModularGameActionElement), true)]
    public class GameActionElementEditor : UnityEditor.Editor
    {
        private GUIStyle labelStyle;


        public override void OnInspectorGUI()
        {
            Event e = Event.current;
            ModularGameActionElement abilityElement = target as ModularGameActionElement;

            DrawDefaultInspector();

            GUILayout.Space(16);

            int k = 0;

            GUILayout.Label("Inputs");

            GUILayout.Space(8);

            FieldInfo[] fieldInfos = abilityElement.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic);
            
            foreach (FieldInfo info in fieldInfos)
            {
                if (info.GetValue(abilityElement) is GameActionInputConnector)
                {
                    GameActionConnector connector = info.GetValue(abilityElement) as GameActionConnector;
                    object[] objects = info.GetCustomAttributes(typeof(GameActionPropertyDescription), true);

                    if (objects != null && connector != null)
                    {
                        EditorGUILayout.LabelField(connector.Type.ToString(), objects[0].ToString());
                    }
                }
            }

            GUILayout.Space(16);

            GUILayout.Label("Outputs");

            GUILayout.Space(8);

            foreach (FieldInfo info in fieldInfos)
            {
                if (info.GetValue(abilityElement) is GameActionOutputConnector)
                {
                    GameActionConnector connector = info.GetValue(abilityElement) as GameActionConnector;
                    object[] objects = info.GetCustomAttributes(typeof(GameActionPropertyDescription), true);

                    if (objects != null && objects.Length > 0 && connector != null)
                    {
                        EditorGUILayout.LabelField(connector.Type.ToString(), objects[0].ToString());
                    }
                }
            }


            GUILayout.Space(16);

            GUILayout.Label("Descriptors");

            foreach (GameActionDescriptor descriptor in abilityElement.Descriptors)
            {
                int descriptorIndex = k++;

                GUILayout.Box(descriptor.Name, GUILayout.ExpandWidth(true));

                Rect boxRect = GUILayoutUtility.GetLastRect();

                if (boxRect.Contains(e.mousePosition))
                {

                    if (e.type == EventType.MouseDown)
                    {
                        DragAndDrop.PrepareStartDrag();
                    }

                    if (e.type == EventType.MouseDrag)
                    {
                        GameActionElementMapping currentMapping = new GameActionElementMapping()
                        {
                            elementId = abilityElement.id,
                            descriptorIndex = descriptorIndex,
                        };

                        DragAndDrop.SetGenericData("mapping", currentMapping);
                        DragAndDrop.StartDrag("Dragging title");
                        Event.current.Use();
                    }
                }
            }

            GUILayout.Space(16);

            GUILayout.Label("Conditions");

            k = 0;

            foreach (GameActionCondition condition in abilityElement.Conditions)
            {
                int descriptorIndex = k++;

                GUILayout.Box(condition.Name, GUILayout.ExpandWidth(true));

                Rect boxRect = GUILayoutUtility.GetLastRect();

                if (boxRect.Contains(e.mousePosition))
                {

                    if (e.type == EventType.MouseDown)
                    {
                        DragAndDrop.PrepareStartDrag();
                    }

                    if (e.type == EventType.MouseDrag)
                    {
                        GameActionElementMapping currentCondition = new GameActionElementMapping()
                        {
                            elementId = abilityElement.id,
                            descriptorIndex = descriptorIndex,
                        };

                        DragAndDrop.SetGenericData("condition", currentCondition);
                        DragAndDrop.StartDrag("Dragging title");
                        Event.current.Use();
                    }
                }
            }
        }

        public static string GetDefaultString(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type)?.ToString() ?? "null";
            }
            return "null";
        }
    }
}