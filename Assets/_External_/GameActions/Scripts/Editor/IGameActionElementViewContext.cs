﻿using UnityEngine;

namespace GameActions.Editor
{
    public interface IGameActionElementViewContext
    {
        Vector3 Zoom { get; set; }
        float TimelineTrackHeight { get; }
        int DragConnectFromIndex { get; set; }
        int DragConnectFromItem { get; set; }
        float GridPrecision { get; }
        ModularGameActionObject CurrentActionObject { get; }
        int TimelineTrackCount { get; }
        int ConnectorSize { get; }
        int ConnectorMargin { get; }
        float DragMargin { get; }
        GameActionEditorDragMode CurrentDragMode { get; set; }
        float NewElementSize { get; }

        void Repaint();
        Color GetConnectorColor(string v);
        void SelectConnection(int id, int index);
        bool IsConnectionSelected(int id, int index);
        bool IsConnected(int id, int index);
    }
}