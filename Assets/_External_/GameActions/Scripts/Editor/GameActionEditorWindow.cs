﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace GameActions.Editor
{
    public enum GameActionEditorDragMode
    {
        None,
        ResizeLeft,
        Move,
        ResizeRight
    }

    public enum GameActionMappingDragMode
    {
        None,
        Drag,
    }

    class GameActionEditorWindow : EditorWindow, IGameActionTimelineContext
    {
        public Vector2 scrollPosition = Vector2.zero;

        [MenuItem("Window/Sequencing/Game Action Editor _%#E")]
        public static void ShowWindow()
        {
            GetWindow(typeof(GameActionEditorWindow), false, "Game Action Editor");
        }

        private ModularGameActionObject currentActionObject;

        private Vector2 mappingScrollPosition;

        private ReorderableList conditionsList;

        private ReorderableList descriptorList;

        private GameActionEditorTimeline actionEditorTimeline;

        private GameActionCoroutineRunner runner;

        private void OnEnable()
        {
            GameActionEditorStyles.Create();

            if (Selection.activeObject is ModularGameActionObject)
            {
                currentActionObject = Selection.activeObject as ModularGameActionObject;

                if (actionEditorTimeline != null)
                {
                    actionEditorTimeline.SetActionObject(currentActionObject);
                }
            }

            Undo.undoRedoPerformed += UndoPerformed;
        }


        private void Runner_OnStartAction(GameAction action)
        {
            ModularGameAction modularGameAction = action as ModularGameAction;

            if(modularGameAction == null)
            {
                return;
            }

            if(actionEditorTimeline.CurrentAction != null && !actionEditorTimeline.CurrentAction.IsDone)
            {
                return;
            }

            if(currentActionObject != null && modularGameAction.Name == currentActionObject.name)
            {
                actionEditorTimeline.SetAction(modularGameAction);
            }
        }

        private void OnDisable()
        {
            Undo.undoRedoPerformed -= UndoPerformed;
        }

        void Update()
        {
            if(actionEditorTimeline == null)
            {
                return;
            }

            //if(Application.isPlaying)
            //{
            //    if (runner == null)
            //    {
            //        runner = FindObjectOfType<GameActionCoroutineRunner>();
            //        runner.OnStartAction += Runner_OnStartAction;
            //    }

            //    if(actionEditorTimeline.CurrentAction != null)
            //    {
            //        Repaint();
            //    }
            //}
            //else
            //{
            //    if(runner != null)
            //    {
            //        actionEditorTimeline.SetAction(null);
            //        runner.OnStartAction -= Runner_OnStartAction;
            //        runner = null;
            //    }
            //}
        }

        private void UndoPerformed()
        {
            Repaint();
        }

        private void OnSelectionChange()
        {
            UpdateSelection();
            Repaint();
        }

        private void UpdateSelection()
        {
            ModularGameActionObject targetAction = null;

            if (Selection.activeObject is ModularGameActionObject)
            {
                targetAction = Selection.activeObject as ModularGameActionObject;
            }

            if (Selection.activeObject is ModularGameActionElement)
            {
                string assetPath = AssetDatabase.GetAssetPath(Selection.activeObject);
                if (AssetDatabase.GetMainAssetTypeAtPath(assetPath) == typeof(ModularGameActionObject))
                {
                    targetAction = AssetDatabase.LoadAssetAtPath<ModularGameActionObject>(assetPath);
                }
            }

            actionEditorTimeline.SetActionObject(targetAction);

            if (currentActionObject != targetAction)
            {
                currentActionObject = targetAction;
                descriptorList = null;
                conditionsList = null;
            }
        }

        private void UpdateConditionsList()
        {
            conditionsList = new ReorderableList(currentActionObject.ConditionMappings,
                                typeof(GameActionElementMapping), true, true, true, true);

            conditionsList.drawHeaderCallback = (Rect rect) =>
            {
                EditorGUI.LabelField(rect, "Condition Mappings");
            };

            conditionsList.drawNoneElementCallback = (Rect rect) =>
            {
                Event e = Event.current;

                EditorGUI.LabelField(rect, "No mappings");
                if (rect.Contains(e.mousePosition))
                {
                    if (e.type == EventType.DragUpdated)
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    }

                    if (e.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();

                        GameActionElementMapping currentMapping = DragAndDrop.GetGenericData("condition") as GameActionElementMapping;

                        if (currentMapping != null)
                        {
                            currentActionObject.ConditionMappings.Add(currentMapping);
                        }
                    }
                }
            };

            conditionsList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                Event e = Event.current;
                GameActionElementMapping mapping = currentActionObject.ConditionMappings[index];
                ModularGameActionElement element = currentActionObject.GetElementById(mapping.elementId);
                GameActionCondition condition = null;

                if (element != null)
                {
                    if (mapping.descriptorIndex < element.Conditions.Count)
                    {
                        condition = element.Conditions[mapping.descriptorIndex];
                    }
                }

                rect.y += 2;
                EditorGUI.LabelField(new Rect(rect.x, rect.y, 120, EditorGUIUtility.singleLineHeight),
                    condition != null ? condition.Name : "[none]");

                if (rect.Contains(e.mousePosition))
                {
                    if (e.type == EventType.DragUpdated)
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    }

                    if (e.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();

                        GameActionElementMapping currentMapping = DragAndDrop.GetGenericData("condition") as GameActionElementMapping;

                        if (currentMapping != null)
                        {
                            currentActionObject.ConditionMappings.Insert(index + 1, currentMapping);
                        }
                    }
                }
            };
        }

        private void UpdateDescriptorList()
        {
            descriptorList = new ReorderableList(currentActionObject.DescriptorMappings,
                            typeof(GameActionElementMapping), true, true, true, true);

            descriptorList.drawHeaderCallback = (Rect rect) =>
            {
                EditorGUI.LabelField(rect, "Descriptor Mappings");
            };

            descriptorList.drawNoneElementCallback = (Rect rect) =>
            {
                Event e = Event.current;

                EditorGUI.LabelField(rect, "No mappings");
                if (rect.Contains(e.mousePosition))
                {
                    if (e.type == EventType.DragUpdated)
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    }

                    if (e.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();

                        GameActionElementMapping currentMapping = DragAndDrop.GetGenericData("mapping") as GameActionElementMapping;

                        if (currentMapping != null)
                        {
                            currentActionObject.DescriptorMappings.Add(currentMapping);
                        }
                    }
                }
            };

            descriptorList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                Event e = Event.current;
                GameActionElementMapping mapping = currentActionObject.DescriptorMappings[index];
                ModularGameActionElement element = currentActionObject.GetElementById(mapping.elementId);
                GameActionDescriptor descriptor = null;

                if (element != null)
                {
                    descriptor = element.Descriptors[mapping.descriptorIndex];
                }


                rect.y += 2;
                EditorGUI.LabelField(new Rect(rect.x, rect.y, 60, EditorGUIUtility.singleLineHeight),
                    descriptor != null ? descriptor.Description : "[none]");

                if (rect.Contains(e.mousePosition))
                {
                    if (e.type == EventType.DragUpdated)
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    }

                    if (e.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();

                        GameActionElementMapping currentMapping = DragAndDrop.GetGenericData("mapping") as GameActionElementMapping;

                        if (currentMapping != null)
                        {
                            currentActionObject.DescriptorMappings.Insert(index + 1, currentMapping);
                        }
                    }
                }
            };
        }

        private void OnGUI()
        {
            GameActionEditorStyles.Create();

            if(actionEditorTimeline == null)
            {
                actionEditorTimeline = new GameActionEditorTimeline(this);
            }

            // InitializeStyles();
            UpdateSelection();

            if (currentActionObject == null)
            {
                // Add an empty box with GUILayout to reserve space for the timline
                GUILayout.Box("Select a ModularGameActionObject to edit.",
                    GameActionEditorStyles.labelStyle2, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
                return;
            }

            if (descriptorList == null)
            {
                UpdateDescriptorList();
            }

            if (conditionsList == null)
            {
                UpdateConditionsList();
            }

            Event e = Event.current;

            GUI.backgroundColor = Color.white;

            GUI.color = Color.white;


            GUILayout.BeginHorizontal(GameActionEditorStyles.styleEvenSpacing);

            mappingScrollPosition = GUILayout.BeginScrollView(mappingScrollPosition, GUILayout.MinWidth(220));

            GUILayout.BeginVertical();

            DrawMappingsMenu(e);

            GUILayout.Space(10);

            GUIStyle bgColor = new GUIStyle();

            bgColor.normal.background = EditorGUIUtility.whiteTexture;

            GUILayout.EndVertical();

            GUILayout.EndScrollView();

            GUILayout.Space(8);

            GUILayout.BeginHorizontal(GameActionEditorStyles.styleFramePadding, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            actionEditorTimeline.Draw();

            GUILayout.EndHorizontal();

            if (Event.current.type == EventType.Repaint)
            {
                Rect timelineRect = GUILayoutUtility.GetLastRect();

                GUI.color = new Color(0, 0, 0, 0.3f);
                GUI.DrawTexture(new Rect(timelineRect.x, timelineRect.y, 1, timelineRect.height), GameActionEditorStyles.baseColorTexture);
                GUI.DrawTexture(new Rect(timelineRect.x, timelineRect.y, timelineRect.width, 1), GameActionEditorStyles.baseColorTexture);
                GUI.DrawTexture(new Rect(timelineRect.x + timelineRect.width, timelineRect.y, 1, timelineRect.height), GameActionEditorStyles.baseColorTexture);
                GUI.DrawTexture(new Rect(timelineRect.x, timelineRect.y + timelineRect.height, timelineRect.width, 1), GameActionEditorStyles.baseColorTexture);

            }

            GUILayout.EndHorizontal();


            if (e.type == EventType.MouseDown && e.button == 0)
            {
                Selection.activeObject = currentActionObject;
            }
        }

        private void DrawMappingsMenu(Event e)
        {
            if (currentActionObject != null)
            {
                if (descriptorList != null)
                {
                    descriptorList.DoLayoutList();
                }

                GUILayout.Space(10);

                if (conditionsList != null)
                {
                    conditionsList.DoLayoutList();
                }
            }
        }
    }
}