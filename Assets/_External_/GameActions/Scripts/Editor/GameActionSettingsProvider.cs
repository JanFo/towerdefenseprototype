﻿using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEngine.UIElements;
using UnityEditorInternal;
using System.Collections.Generic;
using System.Linq;

namespace GameActions
{
    // Create MyCustomSettingsProvider by deriving from SettingsProvider:
    class GameActionSettingsProvider : SettingsProvider
    {
        private GameActionEditorSettings settings;
        private ReorderableList typeColorMappingsList;
        private GUIStyle paddingStyle;
        private bool isStyleVisible;
        private GUIStyle sectionStyle;
        private bool isParamsVisible;

        public GameActionSettingsProvider(string path, SettingsScope scope = SettingsScope.User)
            : base(path, scope) { }

        public override void OnActivate(string searchContext, VisualElement rootElement)
        {
            // This function is called when the user clicks on the MyCustom element in the Settings window.
            settings = GameActionEditorSettings.GetOrCreateSettings();
           
            typeColorMappingsList = new ReorderableList(settings.typeColorMappings, typeof(TypeColorMapping), true, true, true, true);
            typeColorMappingsList.drawHeaderCallback = (Rect rect) =>
            {
                EditorGUI.LabelField(rect, "Connector Colors");
            };
            typeColorMappingsList.drawNoneElementCallback = (Rect rect) =>
            {
                EditorGUI.LabelField(rect, "None");
            };
            typeColorMappingsList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                Rect rect1 = new Rect(rect.x, rect.y, rect.width / 2 - 4, EditorGUIUtility.singleLineHeight);
                Rect rect2 = new Rect(rect.x + rect.width / 2, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight);
                settings.typeColorMappings[index].typeName = EditorGUI.TextField(rect1, settings.typeColorMappings[index].typeName);
                settings.typeColorMappings[index].color = EditorGUI.ColorField(rect2, settings.typeColorMappings[index].color);
            };

            paddingStyle = new GUIStyle();
            paddingStyle.padding = new RectOffset(3, 3, 3, 3);


            sectionStyle = new GUIStyle();
            sectionStyle.padding = new RectOffset(6, 6, 6, 6);

            isStyleVisible = true;
            isParamsVisible = true;
        }

        public override void OnGUI(string searchContext)
        {

            isStyleVisible = EditorGUILayout.BeginFoldoutHeaderGroup(isStyleVisible, "Editor");

            if(isStyleVisible)
            {
                EditorGUILayout.BeginVertical(sectionStyle);
                // Use IMGUI to display UI:
                settings.timelineTimebarHeight = EditorGUILayout.IntField("Timeline Timebar Height", settings.timelineTimebarHeight);
                settings.timelineTrackHeight = EditorGUILayout.IntField("Timeline Track Height", settings.timelineTrackHeight);
                settings.timelineTrackCount = EditorGUILayout.IntField("Timeline Track Count", settings.timelineTrackCount);
                settings.timelineBackgroundColor = EditorGUILayout.ColorField("Timeline Background Color", settings.timelineBackgroundColor);
                settings.timelineLineColor = EditorGUILayout.ColorField("Timeline Line Color", settings.timelineLineColor);
                settings.timelineTimebarColor = EditorGUILayout.ColorField("Timeline Timebar Color", settings.timelineTimebarColor);
                settings.defaultTimedElementColor = EditorGUILayout.ColorField("Timed Element Background Color", settings.defaultTimedElementColor);
                settings.defaultFunctionalElementColor = EditorGUILayout.ColorField("Functional Element Background Color", settings.defaultFunctionalElementColor);
                settings.elementTextColor = EditorGUILayout.ColorField("Element Text Color", settings.elementTextColor);
                settings.defaultConnectionColor = EditorGUILayout.ColorField("Default Connection Color", settings.defaultConnectionColor); 
                EditorGUILayout.BeginVertical();
                typeColorMappingsList.DoLayoutList();
                EditorGUILayout.EndVertical();

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndFoldoutHeaderGroup();

           
            settings.typeColorMap = ArrayUtils.CreateMap(settings.typeColorMappings, t => t.typeName, t => t.color);

            File.WriteAllText(GameActionEditorSettings.actionEditorSettingsPath, JsonUtility.ToJson(settings, true));
        }

        // Register the SettingsProvider
        [SettingsProvider]
        public static SettingsProvider CreateActionEditorSettingsProvider()
        {
            var provider = new GameActionSettingsProvider("Project/Game Actions", SettingsScope.Project);
            return provider;
        }
    }
}