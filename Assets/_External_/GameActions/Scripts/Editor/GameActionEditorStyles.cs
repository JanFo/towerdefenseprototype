﻿using UnityEngine;

namespace GameActions.Editor
{
    public class GameActionEditorStyles
    {
        public static GUIStyle styleFramePadding { get; private set; }
        public static GUIStyle styleEvenSpacing { get; private set; }
        public static GUIStyle timelineStyle { get; private set; }
        public static Texture timebarTexture { get; private set; }
        public static Texture2D elementTexture { get; private set; }
        public static GUIStyle rectangularElementStyle { get; private set; }
        public static Texture2D eventTexture { get; private set; }
        public static GUIStyle roundElementStyle { get; private set; }
        public static GUIStyle mappingSlotStyle { get; private set; }
        public static GUIStyle mappingElementStyle { get; private set; }
        public static GUIStyle labelStyle { get; private set; }
        public static GUIStyle labelStyle2 { get; private set; }
        public static Texture circleConnectorTexture { get; private set; }
        public static Texture arrowLeftTexture { get; private set; }
        public static Texture arrowRightTexture { get; private set; }
        public static Texture circleTexture { get; private set; }
        public static Texture rectConnectorTexture { get; private set; }
        public static Texture2D baseColorTexture { get; private set; }

        public static void Create()
        {
            if (styleEvenSpacing == null)
            {
                int padding = 8;
                styleEvenSpacing = new GUIStyle();
                styleEvenSpacing.padding = new RectOffset(padding, padding, padding, padding);
            }

            if (styleFramePadding == null)
            {
                styleFramePadding = new GUIStyle();
                styleFramePadding.padding = new RectOffset(1, 1, 1, 1);
            }

            if (timelineStyle == null)
            {
                timelineStyle = new GUIStyle();
                timelineStyle.padding = new RectOffset(0, 0, 0, 0);
                timelineStyle.margin = new RectOffset(0, 0, 0, 0);
            }

            if (timebarTexture == null)
                timebarTexture = (Texture)Resources.Load("abilityTimebar");


            if (elementTexture == null) 
            {
                elementTexture = (Texture2D)Resources.Load("abilityElement");
                rectangularElementStyle = new GUIStyle();
                rectangularElementStyle.alignment = TextAnchor.MiddleCenter;
                rectangularElementStyle.normal = new GUIStyleState();
                rectangularElementStyle.normal.background = Texture2D.whiteTexture;
                rectangularElementStyle.border = new RectOffset(4, 4, 4, 4);
            }

            if (eventTexture == null)
            {
                eventTexture = (Texture2D)Resources.Load("roundedElement");
                roundElementStyle = new GUIStyle();
                roundElementStyle.alignment = TextAnchor.MiddleCenter;
                roundElementStyle.normal = new GUIStyleState();
                roundElementStyle.normal.background = eventTexture;
                roundElementStyle.normal.scaledBackgrounds = new Texture2D[]
                {
                    (Texture2D)Resources.Load("roundedElementHiRes")
                };
                roundElementStyle.border = new RectOffset(16, 16, 16, 16);
            }


            if (mappingSlotStyle == null)
            {
                mappingSlotStyle = new GUIStyle();
                mappingSlotStyle.alignment = TextAnchor.MiddleCenter;
                mappingSlotStyle.normal = new GUIStyleState();
                mappingSlotStyle.normal.background = (Texture2D)Resources.Load("mappingSlot");
                mappingSlotStyle.border = new RectOffset(12, 12, 12, 12);
                mappingSlotStyle.margin = new RectOffset(4, 4, 4, 4);
            }

            if (mappingElementStyle == null)
            {
                mappingElementStyle = new GUIStyle();
                mappingElementStyle.alignment = TextAnchor.MiddleCenter;
                mappingElementStyle.normal = new GUIStyleState();
                mappingElementStyle.normal.background = (Texture2D)Resources.Load("mappingElement");
                mappingElementStyle.border = new RectOffset(12, 12, 12, 12);
                mappingElementStyle.margin = new RectOffset(4, 4, 4, 4);
            }

            if (labelStyle == null)
            {
                labelStyle = new GUIStyle();
                labelStyle.alignment = TextAnchor.MiddleCenter;
                labelStyle.fontSize = 10;
                labelStyle.normal.textColor = Color.black;
            }

            if (labelStyle2 == null)
            {
                labelStyle2 = new GUIStyle();
                labelStyle2.alignment = TextAnchor.MiddleCenter;
                labelStyle2.fontSize = 15;
                labelStyle2.normal.textColor = Color.white;
            }

            if (circleConnectorTexture == null)
                circleConnectorTexture = (Texture)Resources.Load("circleConnector");


            if (arrowLeftTexture == null)
                arrowLeftTexture = (Texture)Resources.Load("arrowLeft");
            if (arrowRightTexture == null)
                arrowRightTexture = (Texture)Resources.Load("arrowRight");
            if (circleTexture == null)
                circleTexture = (Texture)Resources.Load("circle");

            if (rectConnectorTexture == null)
                rectConnectorTexture = (Texture)Resources.Load("rectConnector");

            if (baseColorTexture == null)
                baseColorTexture = Texture2D.whiteTexture;
        }
    }
}