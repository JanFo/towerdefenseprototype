﻿using UnityEngine;
using UnityEditor;

namespace GameActions.Editor
{
    [CustomEditor(typeof(GameActionSettings), true)]
    public class GameActionLibraryEditor : UnityEditor.Editor
    {

        public override void OnInspectorGUI()
        {

            DrawDefaultInspector();

            GUILayout.Space(16);

            if (GUILayout.Button("Export All"))
            {
                // (target as GameActionSettings).ExportAll();
            }
        }
    }
}