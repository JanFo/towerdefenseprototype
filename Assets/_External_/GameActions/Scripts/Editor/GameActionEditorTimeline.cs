﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;

namespace GameActions.Editor
{
    class GameActionEditorTimeline : IGameActionElementViewContext
    {
        private IGameActionTimelineContext context;

        private List<GameActionElementView> actionElementViews;

        private ModularGameActionObject currentActionObject;

        public Vector2 scrollPosition = Vector2.zero;

        private bool isPanning;

        private bool isZooming;

        private float timelinePanelHeight = 500;

        private float timelinePanelWidth = 10000;

        private float contextPosition;

        private int contextTrack;

        private List<Type> elementTypes;

        private float gridWidthLabels = 1f;

        private float gridWidthLines = 1f;

        private int maxZoomLevels = 5;

        private Vector2 zoom;

        private int dragConnectFromIndex;

        private int dragConnectFromItem;

        private GameActionEditorDragMode currentDragMode;

        private GameActionEditorSettings gameActionEditorSettings;

        private int selectedConnectionItem;

        private int selectedConnectionConnector;

        private float windowWidth;

        public ModularGameAction CurrentAction { get; private set; }

        public float TimelineTrackHeight => gameActionEditorSettings.timelineTrackHeight;

        public float TimelineTimebarHeight => gameActionEditorSettings.timelineTimebarHeight;

        public ModularGameActionObject CurrentActionObject => currentActionObject;

        public int TimelineTrackCount => gameActionEditorSettings.timelineTrackCount;

        public int ConnectorSize => 10;

        public int ConnectorMargin => 4;

        public float DragMargin => 4;

        public GameActionEditorDragMode CurrentDragMode { get => currentDragMode; set => currentDragMode = value; }

        internal void SetAction(ModularGameAction modularGameAction)
        {
            CurrentAction = modularGameAction;
        }

        public float NewElementSize => 200;

        public GameActionEditorTimeline(IGameActionTimelineContext context)
        {
            zoom = new Vector2(200, 1);
            this.context = context;
            actionElementViews = new List<GameActionElementView>();
            gameActionEditorSettings = GameActionEditorSettings.GetOrCreateSettings();
            elementTypes = new List<Type>();

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                elementTypes.AddRange(GetAbilityElementTypes(assembly));
            }

            DragConnectFromItem = -1;
            DragConnectFromIndex = -1;
        }

        public void Draw()
        {
            if (gameActionEditorSettings == null) 
            {
                gameActionEditorSettings = GameActionEditorSettings.GetOrCreateSettings();
            }

            // Enable window scrolling (put everything into a scroll view)
            scrollPosition = GUILayout.BeginScrollView(scrollPosition);

            timelinePanelHeight = TimelineTrackCount * TimelineTrackHeight;

            // Add an empty box with GUILayout to reserve space for the timline
            GUILayout.Box(GUIContent.none, GameActionEditorStyles.timelineStyle, GUILayout.Height(timelinePanelHeight), GUILayout.Width(timelinePanelWidth));

            // Fetch the timline rectangle from the reserved space
            Rect timelineRect = GUILayoutUtility.GetLastRect();

            // Draw the timeline into the rectangle
            DrawTimeLine(timelineRect);

            GUILayout.EndScrollView();

            
        }

        public void SetActionObject(ModularGameActionObject actionObject)
        {
            if (currentActionObject == actionObject)
            {
                return;
            }

            currentActionObject = actionObject;

            actionElementViews.Clear();

            if (currentActionObject == null)
            {
                return;
            }

            foreach(ModularGameActionElement element in actionObject.Elements)
            {
                if(float.IsNaN(element.duration) || float.IsNaN(element.offset) || element.duration == 0)
                {
                    element.duration = 1;
                    element.offset = 0;
                }

                actionElementViews.Add(new GameActionElementView(this, element));
            }



            DragConnectFromItem = -1;
            DragConnectFromIndex = -1;
            currentActionObject = actionObject;
            selectedConnectionItem = -1;
            zoom = new Vector2(800 / actionObject.Length, 1);
        }

        private void DrawTimeLine(Rect timelineRect)
        {
            Event e = Event.current;

            if (e.type == EventType.MouseUp)
            {
                isZooming = false;
                isPanning = false;
            }

            if (e.type == EventType.MouseDown && timelineRect.Contains(e.mousePosition))
            {
                ResetSelection();
            }

            GUI.BeginGroup(timelineRect);

            Rect timebarRect = new Rect(0, 0, timelineRect.width, TimelineTimebarHeight);
            Rect tracksRect = new Rect(0, TimelineTimebarHeight, timelineRect.width, timelineRect.height - TimelineTrackHeight);

            HandlePanning(timelineRect);
            DrawTimebar(timebarRect);

            GUI.BeginGroup(tracksRect);

            for (int i = 0; i < TimelineTrackCount; i++)
            {
                DrawTrack(i);
            }

            DrawLines();
            DrawMinTimeMarker();


            if (currentActionObject != null)
            {
                DrawConnections();
            }

            DrawDragConnection(e);

            for (int i = 0; i < TimelineTrackCount; i++)
            {
                DrawSequenceItemRow(i);
            }

            if (currentActionObject != null)
            {

                if (currentActionObject != null && CurrentAction != null)
                {
                    DrawProgressMarker();
                }
            }


            GUI.EndGroup();
            GUI.EndGroup();

            if (e.type == EventType.MouseUp)
            {
                DragConnectFromItem = -1;
                DragConnectFromIndex = -1;
            }

            if (e != null && e.keyCode == KeyCode.Delete && e.type == EventType.KeyDown)
            {
                if (selectedConnectionItem >= 0)
                {
                    currentActionObject.Connections.RemoveAll(c => c.toItem == selectedConnectionItem && c.toConnector == selectedConnectionConnector);
                    ResetSelection();
                    e.Use();
                }
                else if (Selection.activeObject is ModularGameActionElement)
                {
                    e.Use();

                    ModularGameActionElement actionElement = Selection.activeObject as ModularGameActionElement;
                    Undo.DestroyObjectImmediate(actionElement);
                    EditorUtility.SetDirty(currentActionObject);

                    currentActionObject.RemoveItem(actionElement);
                    UnityEngine.Object.DestroyImmediate(actionElement, true);
                    ResetSelection();

                    actionElementViews.Clear();

                    foreach (ModularGameActionElement element in currentActionObject.Elements)
                    {
                        actionElementViews.Add(new GameActionElementView(this, element));
                    }

                    Selection.activeObject = currentActionObject;
                }
            }
        }

        private void DrawDragConnection(Event e)
        {
            if (DragConnectFromItem >= 0 && DragConnectFromIndex >= 0)
            {
                ModularGameActionElement element = currentActionObject.GetElementById(DragConnectFromItem);
                GameActionOutputConnector connector = element.GetOutput(DragConnectFromIndex);

                Color color = GetConnectorColor(connector.Type.ToString());
                DrawNodeCurve(GetOutputConnectorRect(DragConnectFromItem, DragConnectFromIndex),
                       new Rect(e.mousePosition.x, e.mousePosition.y, 0, 0), color);

                context.Repaint();
            }
        }

        private void DrawTrack(int trackIndex)
        {
            Rect trackArea = new Rect(0, trackIndex * TimelineTrackHeight, timelinePanelWidth, TimelineTrackHeight);

            GUI.color = gameActionEditorSettings.timelineBackgroundColor;
            GUI.DrawTexture(trackArea, GameActionEditorStyles.baseColorTexture);
        }


        private void ResetSelection()
        {
            // currentAbilityElement = null;
            DragConnectFromItem = -1;
            DragConnectFromIndex = -1;
            CurrentDragMode = GameActionEditorDragMode.None;
            selectedConnectionItem = -1;
            selectedConnectionConnector = -1;
            Repaint();
        }

        private void DrawConnections()
        {
            if (currentActionObject != null)
            {
                foreach (GameActionElementConnection connection in currentActionObject.Connections)
                {
                    ModularGameActionElement element = currentActionObject.GetElementById(connection.toItem);
                    GameActionInputConnector connector = element.GetInput(connection.toConnector);

                    Color color = IsConnectionSelected(connection.toItem, connection.toConnector) ? Color.white : 
                        GetConnectorColor(connector.Type.ToString());

                    DrawNodeCurve(GetOutputConnectorRect(connection.fromItem, connection.fromConnector),
                        GetInputConnectorRect(connection.toItem, connection.toConnector), color);
                }
            }
        }

        private Rect GetOutputConnectorRect(int elementIndex, int connectorIndex)
        {
            ModularGameActionElement item = currentActionObject.GetElementById(elementIndex);

            float height = GetConnectorsHeight(item.OutputConnectors.Count);
            float offsetY = item.trackIndex * TimelineTrackHeight + TimelineTrackHeight / 2.0f - height / 2.0f
                + connectorIndex * (ConnectorSize + ConnectorMargin);
            float offsetX = Zoom.x * (item.offset + item.duration) - DragMargin - ConnectorSize;

            return new Rect(offsetX, offsetY, ConnectorSize, ConnectorSize);
        }

        private Rect GetInputConnectorRect(int elementIndex, int connectorIndex)
        {
            ModularGameActionElement item = currentActionObject.GetElementById(elementIndex);

            float height = GetConnectorsHeight(item.InputConnectors.Count);
            float offsetY = item.trackIndex * TimelineTrackHeight + TimelineTrackHeight / 2.0f - height / 2.0f
                + connectorIndex * (ConnectorSize + ConnectorMargin);
            float offsetX = Zoom.x * item.offset + DragMargin;

            return new Rect(offsetX, offsetY, ConnectorSize, ConnectorSize);
        }

        private float GetConnectorsHeight(int count)
        {
            return count * ConnectorSize + (count - 1) * ConnectorMargin;
        }

        public void DrawSequenceItemRow(int trackIndex)
        {
            var e = Event.current;
            Rect trackArea = new Rect(0, trackIndex * TimelineTrackHeight, timelinePanelWidth, TimelineTrackHeight);

            if (currentActionObject != null)
            {
                GUI.color = Color.white;
                GUI.backgroundColor = Color.white;

                GUI.BeginGroup(trackArea);

                for (int i = 0; i < actionElementViews.Count; i++)
                {
                    GameActionElementView elementView = actionElementViews[i];

                    if (elementView.TrackIndex == trackIndex)
                    {
                        elementView.Draw();
                    }
                }

                GUI.EndGroup();

                HandleContextClick(trackIndex, trackArea);

                HandleDragAndDrop(trackIndex, trackArea);
            }
        }

        /// <summary>
        /// Handles a drag and drop on the track
        /// </summary>
        /// <param name="trackIndex"></param>
        /// <param name="trackArea"></param>
        private void HandleDragAndDrop(int trackIndex, Rect trackArea)
        {
            Event e = Event.current;

            if (e.type == EventType.DragUpdated || e.type == EventType.DragPerform)
            {
                if (trackArea.Contains(e.mousePosition))
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                    if (e.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();

                        foreach (UnityEngine.Object dragged_object in DragAndDrop.objectReferences)
                        {
                            if (dragged_object is ModularGameActionElement)
                            {
                                ModularGameActionElement element = dragged_object as ModularGameActionElement;

                                if (currentActionObject != null)
                                {
                                    float position = e.mousePosition.x / Zoom.x - (NewElementSize / Zoom.x) / 2.0f;

                                    ModularGameActionElement elementInstance = UnityEngine.Object.Instantiate(element);
                                    elementInstance.name = element.name;

                                    CreateSequenceItem(elementInstance, position, NewElementSize / Zoom.x, trackIndex);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void CreateSequenceItem(ModularGameActionElement element, float v, float contextPosition, int contextTrack)
        {
            Undo.RegisterCreatedObjectUndo(element, "create element");
            EditorUtility.SetDirty(element);
            Undo.RegisterCompleteObjectUndo(currentActionObject, "change");
            EditorUtility.SetDirty(currentActionObject);

            element.offset = contextPosition;
            element.duration = NewElementSize / Zoom.x;
            element.trackIndex = contextTrack;
            currentActionObject.AddElement(element);

            actionElementViews.Add(new GameActionElementView(this, element));
        }

        /// <summary>
        /// Handles a context click (right click) on the track
        /// </summary>
        /// <param name="trackIndex"></param>
        /// <param name="trackArea"></param>
        private void HandleContextClick(int trackIndex, Rect trackArea)
        {
            Event e = Event.current;

            if (e.type == EventType.ContextClick)
            {
                contextPosition = e.mousePosition.x / Zoom.x - (NewElementSize / Zoom.x) / 2.0f;
                contextTrack = trackIndex;

                if (trackArea.Contains(e.mousePosition))
                {
                    GenericMenu menu = new GenericMenu();

                    foreach (Type type in elementTypes)
                    {
                        GameActionElementDescription desc = type.GetCustomAttribute<GameActionElementDescription>();

                        if (desc != null)
                        {
                            menu.AddItem(new GUIContent(desc.Path), false, OnTypeContextMenuSelect, type);
                        }
                    }

                    menu.ShowAsContext();

                    e.Use();
                }
            }
        }

        private void OnTypeContextMenuSelect(object type)
        {
            ScriptableObject instance = ScriptableObject.CreateInstance(type as Type);
            instance.name = type.ToString();

            if (instance is ModularGameActionElement)
            {
                CreateSequenceItem(instance as ModularGameActionElement, NewElementSize / Zoom.x, contextPosition, contextTrack);
            }
            else
            {
                Debug.Log("Cannot add, this type is not an Ability Element!");
            }
        }


        private void DrawMinTimeMarker()
        {
            GUI.color = gameActionEditorSettings.timelineLineColor;
            GUI.DrawTexture(new Rect(Zoom.x * currentActionObject?.MinTime ?? 0, 0.0f, 3f, timelinePanelHeight), GameActionEditorStyles.baseColorTexture);
        }

        private void DrawProgressMarker()
        {
            GUI.color = Color.blue;
            GUI.DrawTexture(new Rect(Zoom.x * CurrentAction.Progress, 0.0f, 2f, timelinePanelHeight), GameActionEditorStyles.baseColorTexture);

            if(CurrentAction.IsDone)
            {
                CurrentAction = null;
            }

        }

        /// <summary>
        /// Handles panning on the ability track
        /// </summary>
        /// <param name="trackArea"></param>
        private void HandlePanning(Rect trackArea)
        {
            Event e = Event.current;
            
            if (e.type == EventType.MouseDown && e.button == 2)
            {
                isPanning = trackArea.Contains(e.mousePosition);
            }
            

            if (e.type == EventType.MouseDrag && isPanning)
            {
                scrollPosition.x = Mathf.Max(0, scrollPosition.x - e.delta.x);
                Repaint();
            }
        }

        /// <summary>
        /// Draws the lines of the abilities view
        /// </summary>
        private void DrawLines()
        {
            Color timlineLineColorLight = gameActionEditorSettings.timelineLineColor;
            timlineLineColorLight.a /= 4;

            GUI.color = gameActionEditorSettings.timelineLineColor;

            for (int i = 1; i < TimelineTrackCount + 1; i++)
            {
                GUI.DrawTexture(new Rect(0, i * TimelineTrackHeight, timelinePanelWidth, 1), GameActionEditorStyles.baseColorTexture);
            }

            float gridStep = gridWidthLines * Zoom.x;

            int k = 0;
            while (gridStep > 50.0f)
            {
                gridStep /= 10.0f;

                k++;
                if (k > maxZoomLevels)
                    break;
            }

            k = 0;
            while (gridStep < 80)
            {
                gridStep *= 10.0f;

                k++;
                if (k > maxZoomLevels)
                    break;
            }

            float subStep = 0.1f * gridStep;

            for (int i = 0; i < 100; i++)
            {
                if (i > 0)
                {
                    GUI.color = gameActionEditorSettings.timelineLineColor;
                    GUI.DrawTexture(new Rect(i * gridStep, 0, 1, TimelineTrackCount * TimelineTrackHeight),
                        GameActionEditorStyles.baseColorTexture);
                }

                for(int j = 1; j < 10; j++)
                {
                    GUI.color = timlineLineColorLight;
                    GUI.DrawTexture(new Rect(i * gridStep + j * subStep, 0, 1, TimelineTrackCount * TimelineTrackHeight), 
                        GameActionEditorStyles.baseColorTexture);
                }
            }
        }

        /// <summary>
        /// Draws the timeline of the abilities view
        /// </summary>
        private void DrawTimebar(Rect timebarRect)
        {
            Event e = Event.current;

            GUI.BeginGroup(timebarRect);
            
            GUI.color = gameActionEditorSettings.timelineTimebarColor;
            GUI.DrawTexture(timebarRect, GameActionEditorStyles.baseColorTexture);

            float gridStep = gridWidthLabels * Zoom.x;
            float scaleFactor = 1.0f;

            int k = 0;
            while (gridStep > 50.0f)
            {
                gridStep /= 10.0f;
                scaleFactor /= 10.0f;

                k++;
                if (k > maxZoomLevels)
                    break;
            }

            k = 0;
            while (gridStep < 40.0f)
            {
                gridStep *= 10.0f;
                scaleFactor *= 10.0f;

                k++;
                if (k > maxZoomLevels)
                    break;
            }

            for (int i = 1; i < 50; i++)
            {
                float ms = (Mathf.Round(100 * i * gridWidthLines * scaleFactor)) / 100.0f;
                string label = "s";


                GUI.Label(new Rect(i * gridStep - gridStep / 2.0f, 0, gridStep, timebarRect.height), ms + label, GameActionEditorStyles.labelStyle);
            }

            if (e.type == EventType.MouseDown && e.button == 0 && timebarRect.Contains(e.mousePosition))
            {
                isZooming = true;
            }

            if (e.type == EventType.MouseDrag && isZooming)
            {
                Vector2 zoom = Zoom;
                zoom.x += 0.01f * e.delta.y * zoom.x;

                Zoom = zoom;
                Repaint();
            }

            

            GUI.EndGroup();
        }



     

        private void DrawNodeCurve(Rect start, Rect end, Color color)
        {

            Vector2 diff = end.center - start.center;
            float tangentStrength = Mathf.Min(60, Mathf.Max(Mathf.Abs(diff.x), Mathf.Abs(diff.y)) * 0.25f);

            Vector2 startPos = start.center;
            Vector2 endPos = end.center;
            Vector2 startTan = startPos;
            Vector2 endTan = endPos;

            if (diff.x > 0)
            {
                startPos += new Vector2(start.width / 2.0f, 0.0f);
                endPos += new Vector2(-start.width / 2.0f, 0.0f);
                startTan += Vector2.right * tangentStrength;
                endTan += Vector2.left * tangentStrength;
            }
            else if (diff.y > 0)
            {
                startPos += new Vector2(0.0f, start.width / 2.0f);
                endPos += new Vector2(0.0f, -start.width / 2.0f);
                startTan += Vector2.up * tangentStrength;
                endTan += Vector2.down * tangentStrength;
            }
            else
            {
                startPos += new Vector2(0.0f, -start.width / 2.0f);
                endPos += new Vector2(0.0f, start.width / 2.0f);
                startTan += Vector2.down * tangentStrength;
                endTan += Vector2.up * tangentStrength;
            }

            Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 3);

           
         
        }

        public IEnumerable<Type> GetAbilityElementTypes(Assembly assembly)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsSubclassOf(typeof(ModularGameActionElement)))
                {
                    yield return type;
                }
            }
        }

        public void Repaint()
        {
            context.Repaint();
        }

        public Color GetConnectorColor(string v)
        {
            if(gameActionEditorSettings.typeColorMap.ContainsKey(v))
            {
                return gameActionEditorSettings.typeColorMap[v];
            }

            return gameActionEditorSettings.defaultConnectionColor;
        }

        public void SelectConnection(int id, int index)
        {
            Selection.activeObject = currentActionObject; 
            selectedConnectionConnector = index;
            selectedConnectionItem = id;
        }

        public bool IsConnectionSelected(int id, int index)
        {
            return selectedConnectionConnector == index && selectedConnectionItem == id;
        }

        public bool IsConnected(int id, int index)
        {
            foreach(GameActionElementConnection connection in currentActionObject.Connections)
            {
                if(connection.toItem == id && connection.toConnector == index)
                {
                    return true;
                }
            }

            return false;
        }

        public Vector3 Zoom { get => zoom; set => zoom = value; }
        public int DragConnectFromIndex { get => dragConnectFromIndex; set => dragConnectFromIndex = value; }
        public int DragConnectFromItem { get => dragConnectFromItem; set => dragConnectFromItem = value; }
        public float GridPrecision => 10f;
    }
}