﻿namespace GameActions.Editor
{
    public interface IGameActionTimelineContext
    {
        void Repaint();
    }
}