﻿using System;
using UnityEditor;
using UnityEngine;

namespace GameActions
{
    [CustomPropertyDrawer(typeof(GameActionParamDropdownAttribute))]
    public class GameActionParamDropdownDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }


        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {
            string[] values = ArrayUtils.Concat(new string[] { "none" }, GameActionSettings.Instance.GameActionParameters);
            property.intValue = EditorGUI.Popup(position, label.text, property.intValue, values);
        }
    }
}