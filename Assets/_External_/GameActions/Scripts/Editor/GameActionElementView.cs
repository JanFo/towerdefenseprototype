﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEditor;

namespace GameActions.Editor
{
    public class GameActionElementView
    {
        private IGameActionElementViewContext context;

        private ModularGameActionElement element;

        private float itemDragOffset;

        private float itemDragOffsetEnd;

        public int TrackIndex => element.trackIndex;

        public GameActionElementView(IGameActionElementViewContext context, ModularGameActionElement element)
        {
            this.context = context;
            this.element = element;
        }

        public void Draw()
        {
            Rect itemRect = new Rect(element.offset * context.Zoom.x, 1, element.duration * context.Zoom.x,
                context.TimelineTrackHeight - 1.0f);

            var e = Event.current;

            Rect itemRectLeft = new Rect(element.offset * context.Zoom.x, 1.0f, context.DragMargin, 
                context.TimelineTrackHeight - 1.0f);
            Rect itemRectRight = new Rect((element.offset + element.duration) * 
                context.Zoom.x - context.DragMargin, 1.0f, context.DragMargin,
                context.TimelineTrackHeight - 1.0f);

            EditorGUIUtility.AddCursorRect(itemRectLeft, MouseCursor.SplitResizeLeftRight);
            EditorGUIUtility.AddCursorRect(itemRectRight, MouseCursor.SplitResizeLeftRight);


            GUI.color = Color.white;
        
            string label = string.IsNullOrEmpty(element.Label) ? element.ToString() : element.Label;

            Type type = element.GetType();

            bool hasUpdate = type.GetMethod("OnElementUpdate", BindingFlags.Instance | BindingFlags.NonPublic).DeclaringType 
                != typeof(ModularGameActionElement);
            bool hasBegin = type.GetMethod("OnElementBegin", BindingFlags.Instance | BindingFlags.NonPublic).DeclaringType 
                != typeof(ModularGameActionElement);
            bool hasEnd = type.GetMethod("OnElementEnd", BindingFlags.Instance | BindingFlags.NonPublic).DeclaringType 
                != typeof(ModularGameActionElement);
            bool hasTimeMarkerOverrides = type.GetMethod("GetTimeMarkers").DeclaringType != typeof(ModularGameActionElement);

            if(hasTimeMarkerOverrides)
            {
                element.GetTimeMarkers(out hasBegin, out hasUpdate, out hasEnd);
            }

            bool isTimeless = !hasUpdate && !hasBegin && !hasEnd;
            GUI.backgroundColor = GetElementColor(element, isTimeless);


            GameActionEditorStyles.roundElementStyle.normal.textColor = GameActionEditorSettings.GetOrCreateSettings().elementTextColor;
            GameActionEditorStyles.rectangularElementStyle.normal.textColor = GameActionEditorSettings.GetOrCreateSettings().elementTextColor;

            if (!hasUpdate && !hasBegin && !hasEnd)
            {
                Color color = GUI.backgroundColor;
                // color.a *= 0.5f;
                GUI.backgroundColor = color;
                GUI.Box(itemRect, new GUIContent(label), GameActionEditorStyles.roundElementStyle);

                if (element == Selection.activeObject)
                {
                    GUI.backgroundColor = new Color(1, 1, 1, 0.15f);
                    GUI.Box(itemRect, new GUIContent(label), GameActionEditorStyles.roundElementStyle);
                }
            }
            else
            {
                GUI.Box(itemRect, new GUIContent(label), GameActionEditorStyles.rectangularElementStyle);


                GUI.color = new Color(0, 0, 0, 0.15f);
                itemRectLeft.width = 1;
                itemRectRight.x += itemRectRight.width - 1;
                itemRectRight.width = 1;
                GUI.DrawTexture(itemRectLeft, Texture2D.whiteTexture);
                GUI.DrawTexture(itemRectRight, Texture2D.whiteTexture);


                if (element == Selection.activeObject)
                {
                    GUI.backgroundColor = new Color(1, 1, 1, 0.15f);
                    GUI.Box(itemRect, new GUIContent(label), GameActionEditorStyles.rectangularElementStyle);
                }
            }

            DrawInputConnectors(element, itemRectLeft.position.x + context.DragMargin);
            DrawOutputConnectors(element, itemRectRight.position.x - context.ConnectorSize);

            element.DrawEditorGUI(itemRect, context.Zoom.x);

            HandleItemClick(element, itemRect);
            HandleItemDrag(element, itemRect);

            float arrowSize = 10;
            float arrowPadding = 4;

            GUI.color = new Color(1f, 1f, 1f, 0.3f);

            if (hasBegin)
            {
                Rect connectorArea = new Rect(itemRect.x + arrowPadding, itemRect.y + arrowPadding, arrowSize * 0.8f, arrowSize);
                GUI.DrawTexture(connectorArea, GameActionEditorStyles.arrowRightTexture);
            }

            if (hasEnd)
            {
                Rect connectorArea = new Rect(itemRect.x + itemRect.width - arrowSize - arrowPadding, itemRect.y + arrowPadding, 0.8f * arrowSize, arrowSize);
                GUI.DrawTexture(connectorArea, GameActionEditorStyles.arrowLeftTexture);
            }

            if (hasUpdate)
            {
                Rect connectorArea = new Rect(itemRect.x + itemRect.width * 0.5f - arrowSize * 0.5f, itemRect.y + arrowPadding, arrowSize, arrowSize);
                GUI.DrawTexture(connectorArea, GameActionEditorStyles.circleTexture);
            }
        }

        private void HandleItemDrag(ModularGameActionElement element, Rect itemRect)
        {
            Event e = Event.current;

            if (e.type == EventType.MouseDrag && element == Selection.activeObject)
            {
                float pos = Mathf.Max(0, (e.mousePosition.x - itemDragOffset) / context.Zoom.x);
                float mousePos = e.mousePosition.x / context.Zoom.x;

                if (e.control)
                {
                    pos = Mathf.Round(pos * context.GridPrecision) / context.GridPrecision;
                }

                switch (context.CurrentDragMode)
                {
                    case GameActionEditorDragMode.ResizeLeft:
                        float prevEnd = element.offset + element.duration;
                        element.offset = pos;
                        element.duration = Mathf.Max(0, prevEnd - element.offset);
                        break;
                    case GameActionEditorDragMode.ResizeRight:
                        float endPos = (mousePos + itemDragOffsetEnd / context.Zoom.x) - element.offset;

                        if (e.control)
                        {
                            endPos = Mathf.Round(endPos * context.GridPrecision) / context.GridPrecision;
                        }

                        element.duration = endPos;
                        break;
                    case GameActionEditorDragMode.Move:

                        if (e.mousePosition.y < itemRect.y)
                            element.trackIndex = Mathf.Max(0, element.trackIndex - 1);
                        if (e.mousePosition.y > itemRect.y + itemRect.height)
                            element.trackIndex = Mathf.Min(context.TimelineTrackCount - 1, element.trackIndex + 1);

                        element.offset = pos;
                        break;
                }

                e.Use();
                context.Repaint();
            }
        }

        private void HandleItemClick(ModularGameActionElement element, Rect itemRect)
        {
            Event e = Event.current;

            if (e.type == EventType.MouseDown && e.button == 0)
            {
                if (itemRect.Contains(e.mousePosition))
                {
                    Selection.activeObject = element;
                    itemDragOffset = e.mousePosition.x - itemRect.x;
                    itemDragOffsetEnd = itemRect.x + itemRect.width - e.mousePosition.x;

                    EditorUtility.SetDirty(context.CurrentActionObject);

                    if (context.DragConnectFromItem < 0)
                    {
                        if (Mathf.Abs(itemRect.x + itemRect.width - e.mousePosition.x) < context.DragMargin)
                        {
                            context.CurrentDragMode = GameActionEditorDragMode.ResizeRight;
                        }
                        else if (Mathf.Abs(itemRect.x - e.mousePosition.x) < context.DragMargin)
                        {
                            context.CurrentDragMode = GameActionEditorDragMode.ResizeLeft;
                        }
                        else
                        {
                            context.CurrentDragMode = GameActionEditorDragMode.Move;
                        }
                    }

                    e.Use();
                }
                else
                {
                    context.CurrentDragMode = GameActionEditorDragMode.None;
                }

            }
        }

        private Color GetElementColor(ModularGameActionElement item, bool isTimeless)
        {
            Color color = item.GetEditorColor();

            if(color == Color.clear)
            {
                color = !isTimeless ? GameActionEditorSettings.GetOrCreateSettings().defaultTimedElementColor
                    : GameActionEditorSettings.GetOrCreateSettings().defaultFunctionalElementColor;
            }
            

            return color;
        }

        private void DrawOutputConnectors(ModularGameActionElement element, float offsetX)
        {
            Event e = Event.current;

            int connectorIndex = 0;
            int count = element.OutputConnectors.Count;
            float height = count * context.ConnectorSize + (count - 1) * context.ConnectorMargin;

            float offsetY = context.TimelineTrackHeight / 2.0f - height / 2.0f;

            foreach (GameActionOutputConnector connector in element.OutputConnectors)
            {
                GUI.color = context.GetConnectorColor(connector.Type.ToString());

                Rect connectorArea = new Rect(offsetX, offsetY, context.ConnectorSize, context.ConnectorSize);

                EditorGUIUtility.AddCursorRect(connectorArea, MouseCursor.Link);

                if (connector.IsDelegate)
                {
                    GUI.DrawTexture(connectorArea, GameActionEditorStyles.circleConnectorTexture);
                }
                else
                {
                    GUI.DrawTexture(connectorArea, GameActionEditorStyles.rectConnectorTexture);
                }


                if (e.type == EventType.MouseDown && connectorArea.Contains(e.mousePosition))
                {
                    context.DragConnectFromIndex = connectorIndex;
                    context.DragConnectFromItem = element.id;
                    e.Use();
                }

                offsetY += context.ConnectorSize + context.ConnectorMargin;
                connectorIndex++;
            }
        }

        private void DrawInputConnectors(ModularGameActionElement element, float offsetX)
        {
            Event e = Event.current;
            int connectorIndex = 0;
            int count = element.InputConnectors.Count;
            float height = count * context.ConnectorSize + (count - 1) * context.ConnectorMargin;

            float offsetY = context.TimelineTrackHeight / 2.0f - height / 2.0f;

            foreach (GameActionInputConnector connector in element.InputConnectors)
            {
                GUI.color = context.GetConnectorColor(connector.Type.ToString());

                Rect connectorArea = new Rect(offsetX, offsetY, context.ConnectorSize, context.ConnectorSize);

                EditorGUIUtility.AddCursorRect(connectorArea, MouseCursor.Link);
                GUI.DrawTexture(connectorArea, GameActionEditorStyles.circleConnectorTexture);

                if (e.type == EventType.MouseDown && connectorArea.Contains(e.mousePosition) && context.IsConnected(element.id, connectorIndex))
                {
                    context.SelectConnection(element.id, connectorIndex);
                    e.Use();
                }

                if(context.IsConnectionSelected(element.id, connectorIndex))
                {
                    GUI.color = Color.white;
                    GUI.DrawTexture(connectorArea, GameActionEditorStyles.circleConnectorTexture);
                }


                if (e.type == EventType.MouseUp && connectorArea.Contains(e.mousePosition) && context.DragConnectFromItem >= 0)
                {
                    Connect(context.CurrentActionObject, context.DragConnectFromItem, element.id,
                        context.DragConnectFromIndex, connectorIndex);
                    context.Repaint();
                }

                offsetY += context.ConnectorSize + context.ConnectorMargin;
                connectorIndex++;
            }

        }

        private bool Connect(ModularGameActionObject ability, int fromElement, int toElement, int fromIndex, int toIndex)
        {
            if (ability.GetElementById(fromElement).GetOutput(fromIndex).Type ==
                ability.GetElementById(toElement).GetInput(toIndex).Type
                ||
                ability.GetElementById(fromElement).GetOutput(fromIndex).Type
                .IsSubclassOf(ability.GetElementById(toElement).GetInput(toIndex).Type))
            {
                GameActionElementConnection connection = new GameActionElementConnection();
                connection.fromItem = fromElement;
                connection.fromConnector = fromIndex;
                connection.toItem = toElement;
                connection.toConnector = toIndex;

                ability.Connections.Add(connection);

                return true;
            }

            return false;
        }
    }
}


