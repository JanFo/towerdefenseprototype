﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Animation/Set Shape Key", "Modifies the shapekey of a skinned mesh renderer.")]
    public class SetShapeKeyElement : ModularGameActionElement
    {
        [SerializeField]
        private AnimationCurve valueCurve = null;

        [SerializeField]
        private int shapeKeyIndex = 0;

        [GameActionPropertyDescription("The skinned mesh renderer")]
        private GameActionInputConnector<SkinnedMeshRenderer> rendererConnector;

        [GameActionPropertyDescription("The shape key value")]
        private GameActionInputConnector<float> valueConnector;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            rendererConnector = new GameActionInputConnector<SkinnedMeshRenderer>();
            valueConnector = new GameActionInputConnector<float>();
            inputs.Add(rendererConnector);
            inputs.Add(valueConnector);
        }

        protected override void OnElementUpdate(float deltaTime, float time, float normalizedTime)
        {
            SkinnedMeshRenderer renderer = rendererConnector.Get();

            if (renderer)
            {
                float value = valueConnector.IsConnected ? valueConnector.Get() : valueCurve.Evaluate(normalizedTime);
                renderer.SetBlendShapeWeight(shapeKeyIndex, 100.0f * value);
            }
        }

        public override string ToString()
        {
            return "Set Shape Key";
        }
    }
}
