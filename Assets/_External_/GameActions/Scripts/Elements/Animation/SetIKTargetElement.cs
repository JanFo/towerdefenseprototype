﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Animation/Set IK Target", "Modifies the shapekey of a skinned mesh renderer.")]
    public class SetIKTargetElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The skinned mesh renderer")]
        private GameActionInputConnector<AnimatorIK> animatorConnector;

        [GameActionPropertyDescription("The shape key value")]
        private GameActionInputConnector<Vector3> targetConnector;

        [GameActionPropertyDescription("The shape key value")]
        private GameActionInputConnector<float> weightConnector;

        [SerializeField]
        private AvatarIKGoal target;

        [SerializeField]
        private float weight = 1;

        private AnimatorIK animator;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            animatorConnector = new GameActionInputConnector<AnimatorIK>();
            targetConnector = new GameActionInputConnector<Vector3>();
            weightConnector = new GameActionInputConnector<float>();
            inputs.Add(animatorConnector);
            inputs.Add(targetConnector);
            inputs.Add(weightConnector);
        }


        protected override void OnElementUpdate(float deltaTime, float time, float normalizedTime)
        {
            animator = animatorConnector.Get();

            if(animator == null)
            {
                return;
            }

            animator.SetIKPositionAndWeight(target, targetConnector.Get(), weightConnector.Get(weight));
        }

        protected override void OnElementEnd()
        {
            if (animator != null)
            {
                animator.SetIKPositionAndWeight(target, Vector3.zero, 0);
            }
        }

        public override string ToString()
        {
            return "Set IK Target";
        }
    }
}