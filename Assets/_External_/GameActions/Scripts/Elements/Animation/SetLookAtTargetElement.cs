﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Animation/Set LookAt Target", "Sets the LookAt position of an Animator component.")]
    public class SetLookAtTargetElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The animator")]
        private GameActionInputConnector<AnimatorIK> animatorConnector;

        [GameActionPropertyDescription("The shape key value")]
        private GameActionInputConnector<Vector3> targetConnector;

        [GameActionPropertyDescription("The shape key value")]
        private GameActionInputConnector<float> weightConnector;

        [SerializeField]
        private float bodyWeight = 1;

        [SerializeField]
        private float headWeight = 1;

        [SerializeField]
        private float clampWeight = 1;

        private AnimatorIK animator;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            animatorConnector = new GameActionInputConnector<AnimatorIK>();
            targetConnector = new GameActionInputConnector<Vector3>();
            weightConnector = new GameActionInputConnector<float>();
            inputs.Add(animatorConnector);
            inputs.Add(targetConnector);
            inputs.Add(weightConnector);
        }


        protected override void OnElementBegin()
        {
            animator = animatorConnector.Get();
        }

        protected override void OnElementUpdate(float deltaTime, float time, float normalizedTime)
        {
            if(animator == null)
            {
                return;
            }

            Vector3 target = targetConnector.Get();

            animator.SetLookAtPosition(target);
            animator.SetLookAtWeight(weightConnector.Get(), bodyWeight, headWeight, clampWeight);


        }

        protected override void OnElementEnd()
        {
            if (animator == null)
            {
                return;
            }

            animator.SetLookAtWeight(0, 0, 0, 0);
        }

        public override string ToString()
        {
            return "Set LookAt Target";
        }
    }
}
