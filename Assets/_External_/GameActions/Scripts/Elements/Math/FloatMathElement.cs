﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Float", "Peform a math operation on float values.")]
    public class FloatMathElement : ModularGameActionElement
    {
        [SerializeField]
        private float modifier = 0;

        [SerializeField]
        private MathOperation operation = MathOperation.Add;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<float> inputConnector;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<float> modifierConnector;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<float> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<float>(GetFloat);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<float>();
            modifierConnector = new GameActionInputConnector<float>();
            inputs.Add(inputConnector);
            inputs.Add(modifierConnector);
        }

        private float GetFloat()
        {
            float modifier = modifierConnector.Get(this.modifier);

            switch (operation)
            {
                case MathOperation.Add:
                    return inputConnector.Get(0) + modifier;
                case MathOperation.Sub:
                    return inputConnector.Get(0) - modifier;
                case MathOperation.Div:
                    return inputConnector.Get(0) / modifier;
                case MathOperation.Mult:
                    return inputConnector.Get(0) * modifier;
                case MathOperation.Pow:
                    return (int)Math.Pow(inputConnector.Get(0), modifier);
                case MathOperation.Mod:
                    return inputConnector.Get(0) % modifier;
                case MathOperation.Max:
                    return Mathf.Max(inputConnector.Get(0), modifier);
                case MathOperation.Min:
                    return Mathf.Min(inputConnector.Get(0), modifier);
                case MathOperation.Avg:
                    return (inputConnector.Get(0) + modifier) / 2.0f;
            }

            return 0;
        }

        public override string ToString()
        {
            if (modifier != 0.0f)
            {
                return operation.ToString() + "(IN, " + modifier + ")";
            }

            return operation.ToString();
        }

    }
}