﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Vector3/Distance", "Returns the value of a curve.")]
    public class Vector3DistanceElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> inputConnector1;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> inputConnector2;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<float> outputConnector;

        [SerializeField]
        private bool squared;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<float>(GetMatch);

            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector1 = new GameActionInputConnector<Vector3>();
            inputConnector2 = new GameActionInputConnector<Vector3>();
            inputs.Add(inputConnector1);
            inputs.Add(inputConnector2);
        }

        private float GetMatch()
        {
            Vector3 in1 = inputConnector1.Get();
            Vector3 in2 = inputConnector2.Get();

            return squared ? (in1 - in2).sqrMagnitude : Vector3.Distance(in1, in2);
        }

        public override string ToString()
        {
            return "Distance";
        }
    }
}
