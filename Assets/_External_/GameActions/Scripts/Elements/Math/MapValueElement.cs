﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Map Value", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.", true)]
    public class MapValueElement : ModularGameActionElement
    {
        [SerializeField]
        private AnimationCurve curve;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<float> inputConnector;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<float> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<float>(GetFloat);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<float>();
            inputs.Add(inputConnector);
        }

        private float GetFloat()
        {
            return curve.Evaluate(inputConnector.Get(0));
        }

        public override string ToString()
        {
            return "Map Value";
        }
    }
}