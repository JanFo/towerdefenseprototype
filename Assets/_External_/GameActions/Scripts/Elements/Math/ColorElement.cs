﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Color", "Transform a boolean input into a float output.")]
    public class ColorElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The boolean input.")]
        private GameActionInputConnector<float> redInput;

        [GameActionPropertyDescription("The boolean input.")]
        private GameActionInputConnector<float> blueInput;

        [GameActionPropertyDescription("The boolean input.")]
        private GameActionInputConnector<float> greenInput;

        [GameActionPropertyDescription("The boolean input.")]
        private GameActionInputConnector<float> alphaInput;

        [GameActionPropertyDescription("The integer output.")]
        private GameActionOutputDelegate<Color> integerOutput;

        [SerializeField]
        private Color color;

        private Color result;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            redInput = new GameActionInputConnector<float>();
            greenInput = new GameActionInputConnector<float>();
            blueInput = new GameActionInputConnector<float>();
            alphaInput = new GameActionInputConnector<float>();

            inputs.Add(redInput);
            inputs.Add(greenInput);
            inputs.Add(blueInput);
            inputs.Add(alphaInput);
        }

        protected override void OnElementInitialize(IGameActionContext context, IGameActionActor actor)
        {
            result = color;
        }

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            integerOutput = new GameActionOutputDelegate<Color>(CreateColor);

            outputs.Add(integerOutput);
        }

        private Color CreateColor()
        {
            result.r = redInput.Get(color.r);
            result.g = greenInput.Get(color.g);
            result.b = blueInput.Get(color.b);
            result.a = alphaInput.Get(color.a);

            return result;
        }

        public override string ToString()
        {
            return "Color";
        }
    }
}
