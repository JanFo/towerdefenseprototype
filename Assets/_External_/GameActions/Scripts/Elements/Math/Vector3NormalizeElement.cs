﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Vector3/Normalize", "Normalizes a Vector3.")]
    public class Vector3NormalizeElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The input vector")]
        private GameActionInputConnector<Vector3> inputConnector;

        [GameActionPropertyDescription("The normalized result vector.")]
        private GameActionOutputDelegate<Vector3> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<Vector3>(GetResult);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<Vector3>();
            inputs.Add(inputConnector);
        }

        private Vector3 GetResult()
        {
            return inputConnector.Get(Vector3.zero).normalized;
        }

        public override string ToString()
        {
            return "Normalize";
        }
    }
}
