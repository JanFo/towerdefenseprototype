﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Clamp", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.", true)]
    public class ClampElement : ModularGameActionElement
    {
        [SerializeField]
        private float min = 0.0f;

        [SerializeField]
        private float max = 1.0f;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<float> inputConnector;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<float> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<float>(GetFloat);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<float>();
            inputs.Add(inputConnector);
        }

        private float GetFloat()
        {
            return Mathf.Clamp(inputConnector.Get(0), min, max);
        }

        public override string ToString()
        {
            return "Clamp(" + min + ", " + max + ")";
        }
    }
}