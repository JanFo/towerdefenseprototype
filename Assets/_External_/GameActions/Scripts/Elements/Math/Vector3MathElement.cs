﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Asdf", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.")]
    public class Vector3MathElement : ModularGameActionElement
    {
        [SerializeField]
        private Vector3 vectorModifier = Vector3.zero;

        [SerializeField]
        private float scalarModifier = 0.0f;

        [SerializeField]
        private MathOperation operation = MathOperation.Add;

        [SerializeField]
        private bool normalizeResult = false;

        [GameActionPropertyDescription("Vector A")]
        private GameActionInputConnector<Vector3> inputConnector;

        [GameActionPropertyDescription("Vector B")]
        private GameActionInputConnector<Vector3> vectorModifierConnetor;

        [GameActionPropertyDescription("Scalar C")]
        private GameActionInputConnector<float> scalarModifierConnector;

        [GameActionPropertyDescription("The operation result vector.")]
        private GameActionOutputDelegate<Vector3> outputConnector;

        [SerializeField]
        private bool truncateResult;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<Vector3>(GetResult);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<Vector3>();
            vectorModifierConnetor = new GameActionInputConnector<Vector3>();
            scalarModifierConnector = new GameActionInputConnector<float>();
            inputs.Add(inputConnector);
            inputs.Add(vectorModifierConnetor);
            inputs.Add(scalarModifierConnector);
        }

        private Vector3 GetResult()
        {
            Vector3 result = GetVector3();

            if (normalizeResult)
            {
                result.Normalize();
            }

            if (truncateResult)
            {
                result.Truncate();
            }

            return result;
        }

        private Vector3 GetVector3()
        {
            Vector3 vector = inputConnector.Get(Vector3.zero);
            Vector3 modifier = vectorModifierConnetor.Get();
            float scalarModifier = scalarModifierConnector.Get(this.scalarModifier);

            switch (operation)
            {
                case MathOperation.Add:
                    return vector + scalarModifier * (modifier + vectorModifier);
                case MathOperation.Sub:
                    return vector - scalarModifier * (modifier + vectorModifier);
                case MathOperation.Div:
                    return vector / scalarModifier;
                case MathOperation.Mult:
                    return inputConnector.Get(vectorModifier) * scalarModifier;
                case MathOperation.Max:
                    return vector.sqrMagnitude > modifier.sqrMagnitude ? vector : modifier;
                case MathOperation.Min:
                    return vector.sqrMagnitude < modifier.sqrMagnitude ? vector : modifier;
                case MathOperation.Avg:
                    return (inputConnector.Get(Vector3.zero) + modifier) / 2.0f;
                case MathOperation.Lerp:
                    return Vector3.Lerp(inputConnector.Get(), modifier, scalarModifier);
            }

            return Vector3.zero;
        }

        public override string ToString()
        {
            return operation.ToString() + (normalizeResult ? " (normalized)" : "");
        }
    }
}
