﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;


namespace GameActions
{
    [GameActionElementDescription("Math/Curve", "Returns the value of a curve.")]
    public class CurveElement : ModularGameActionElement
    {
        [SerializeField]
        private AnimationCurve valueCurve = null;

        [GameActionPropertyDescription("The output value")]
        private GameActionOutputRegister<float> outConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outConnector = new GameActionOutputRegister<float>();
            outputs.Add(outConnector);
        }

        protected override void OnElementUpdate(float delta, float time, float normalizedTime)
        {
            outConnector.Set(valueCurve.Evaluate(time));
        }

#if UNITY_EDITOR
        public override void DrawEditorGUI(Rect itemRect, float scale)
        {
            if (valueCurve != null)
            {
                Keyframe[] frames = valueCurve.keys;

                if (frames.Length == 0)
                {
                    return;
                }

                float heightScale = frames.Max(f => f.value);

                Vector2 origin = itemRect.position + Vector2.up * itemRect.height;
                Vector2 right = Vector2.right * itemRect.width / duration;
                Vector2 up = Vector2.down * itemRect.height / heightScale;
                Color color = new Color(0.9f, 0.4f, 0.4f);

                for (int i = 0; i < frames.Length - 1; i++)
                {
                    Keyframe current = frames[i];
                    Keyframe next = frames[i + 1];

                    Vector2 currentPos = origin + up * current.value + right * current.time;
                    Vector2 nextPos = origin + up * next.value + right * next.time;
                    Vector2 currentTan = new Vector2(right.x, current.outTangent * up.y).normalized;
                    Vector2 nextTan = new Vector2(-right.x, -next.inTangent * up.y).normalized;

                    float tanLength = 0.5f * (nextPos.x - currentPos.x);

                    Handles.DrawBezier(currentPos, nextPos, currentPos + currentTan * tanLength, nextPos + nextTan * tanLength,
                        color, null, 3);
                }
            }

            base.DrawEditorGUI(itemRect, scale);
        }
#endif

        public override Color GetEditorColor()
        {
            return new Color(.8f, .8f, .8f, 0.7f); 
        }
        public override string ToString()
        {
            return "Curve";
        }
    }
}