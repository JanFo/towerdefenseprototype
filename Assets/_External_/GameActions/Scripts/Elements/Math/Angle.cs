﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Angle", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.", true)]
    public class Angle : ModularGameActionElement
    {
        [SerializeField]
        private Vector3 reference = Vector3.right;

        [SerializeField]
        private float angleOffset = 90.0f;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> inputConnector;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<float> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<float>(GetFloat);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<Vector3>();
            inputs.Add(inputConnector);
        }

        private float GetFloat()
        {
            return Vector3.SignedAngle(reference, inputConnector.Get(reference), Vector3.up) + angleOffset;
        }

        public override string ToString()
        {
            return "Angle";
        }
    }
}