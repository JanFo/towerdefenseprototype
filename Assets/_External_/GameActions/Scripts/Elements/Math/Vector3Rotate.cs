﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions {
    [GameActionElementDescription("Math/Rotate", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.")]
    public class Vector3Rotate : ModularGameActionElement
    {
        [SerializeField]
        private float angleModifier = 1.0f;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> inputConnector;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<float> angleConnector;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<Vector3> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<Vector3>(GetResult);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<Vector3>();
            angleConnector = new GameActionInputConnector<float>();
            inputs.Add(inputConnector);
            inputs.Add(angleConnector);
        }

        private Vector3 GetResult()
        {
            Vector3 vector = inputConnector.Get(Vector3.zero);
            float angle = angleConnector.Get(1.0f);

            return Quaternion.Euler(0, angle * angleModifier, 0) * vector;
        }

        public override string ToString()
        {
            return "Rotate";
        }
    }
}
