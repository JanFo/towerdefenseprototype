﻿using System.Collections.Generic;

namespace GameActions
{

    [GameActionElementDescription("Math/Boolean/Not", "Inverts a boolean value.")]
    public class BooleanNotElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The first input value.")]
        private GameActionInputConnector<bool> inConnector;

        [GameActionPropertyDescription("The output bool")]
        private GameActionOutputDelegate<bool> outConnector;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inConnector = new GameActionInputConnector<bool>();
            inputs.Add(inConnector);
        }

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outConnector = new GameActionOutputDelegate<bool>(GetOutBoolean);
            outputs.Add(outConnector);
        }

        private bool GetOutBoolean()
        {
            return !inConnector.Get();
        }

        public override string ToString()
        {
            return "Not";
        }
    }
}