﻿using System.Collections.Generic;

namespace GameActions
{
    [GameActionElementDescription("Math/Conversion/Boolean To Integer", "Transform a boolean input into an integer output.")]
    public class BooleanToInteger : ModularGameActionElement
    {
        [GameActionPropertyDescription("The boolean input.")]
        private GameActionInputConnector<bool> boolInput;

        [GameActionPropertyDescription("The integer output.")]
        private GameActionOutputDelegate<int> integerOutput;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            boolInput = new GameActionInputConnector<bool>();

            inputs.Add(boolInput);
        }

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            integerOutput = new GameActionOutputDelegate<int>(DoTheCast);

            outputs.Add(integerOutput);
        }

        private int DoTheCast()
        {
            return boolInput.Get() ? 1 : 0;
        }
    }
}
