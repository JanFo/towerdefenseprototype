﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions { 
public enum MathOperation
{
    Add,
    Sub,
    Mult, 
    Div,
    Pow,
    Mod,
    Max,
    Min,
    Avg,
    Lerp
}


    [GameActionElementDescription("Math/Integer", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.")]
    public class IntegerMath : ModularGameActionElement
    {
        [SerializeField]
        private int modifier = 0;

        [SerializeField]
        private MathOperation operation = MathOperation.Add;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<int> inputConnector;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<int> modifierConnector;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<int> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<int>(GetInteger);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<int>();
            modifierConnector = new GameActionInputConnector<int>();
            inputs.Add(inputConnector);
            inputs.Add(modifierConnector);
        }

        private int GetInteger()
        {
            int modifier = modifierConnector.Get(this.modifier);

            switch (operation)
            {
                case MathOperation.Add:
                    return inputConnector.Get(0) + modifier;
                case MathOperation.Sub:
                    return inputConnector.Get(0) - modifier;
                case MathOperation.Div:
                    return inputConnector.Get(0) / modifier;
                case MathOperation.Mult:
                    return inputConnector.Get(0) * modifier;
                case MathOperation.Pow:
                    return (int)Math.Pow(inputConnector.Get(0), modifier);
                case MathOperation.Mod:
                    return inputConnector.Get(0) % modifier;
                case MathOperation.Max:
                    return Mathf.Max(inputConnector.Get(0), modifier);
                case MathOperation.Min:
                    return Mathf.Min(inputConnector.Get(0), modifier);
                case MathOperation.Avg:
                    return (inputConnector.Get(0) + modifier) / 2;
            }

            return 0;
        }
    }
}
