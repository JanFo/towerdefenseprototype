﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Rotate Towards", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.", true)]
    public class Vector3RotateTowards : ModularGameActionElement
    {
        [SerializeField]
        private float angle = 1.0f;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> inputConnector;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> targetConnector;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<Vector3> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<Vector3>(GetResult);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<Vector3>();
            targetConnector = new GameActionInputConnector<Vector3>();
            inputs.Add(inputConnector);
            inputs.Add(targetConnector);
        }

        private Vector3 GetResult()
        {
            Vector3 current = inputConnector.Get(Vector3.zero);
            Vector3 target = targetConnector.Get(Vector3.zero);

            return Vector3.RotateTowards(current, target, angle * Mathf.Deg2Rad, 0.0f);
        }

        public override string ToString()
        {
            return "Rotate Towards";
        }
    }
}
