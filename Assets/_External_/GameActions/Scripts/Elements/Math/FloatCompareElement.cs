﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    public enum CompareOperation
    {
        Equals,
        Greater,
        Less,
        GreaterEquals,
        LessEquals
    }

    [GameActionElementDescription("Math/Float/Compare", "Peform a math operation on float values.")]
    public class FloatCompareElement : ModularGameActionElement
    {
        [SerializeField]
        private float threshold = 0;

        [SerializeField]
        private CompareOperation operation = CompareOperation.Greater;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<float> inputConnector;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<float> thresholdConnector;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<bool> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<bool>(GetResult);
            outputs.Add(outputConnector);
        }

        private bool GetResult()
        {
            float threshold = thresholdConnector.Get(this.threshold);

            switch (operation)
            {
                case CompareOperation.Equals:
                    return inputConnector.Get() == threshold;
                case CompareOperation.Greater:
                    return inputConnector.Get() > threshold;
                case CompareOperation.GreaterEquals:
                    return inputConnector.Get() >= threshold;
                case CompareOperation.Less:
                    return inputConnector.Get() < threshold;
                case CompareOperation.LessEquals:
                    return inputConnector.Get() <= threshold;
                default:
                    return false;
            }
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<float>();
            thresholdConnector = new GameActionInputConnector<float>();
            inputs.Add(inputConnector);
            inputs.Add(thresholdConnector);
        }


        public override string ToString()
        {
            if (threshold != 0.0f)
            {
                return operation.ToString() + " than " + threshold;
            }

            return operation.ToString();
        }

    }
}