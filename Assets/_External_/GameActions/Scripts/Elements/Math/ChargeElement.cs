﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Charge", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.")]
    public class ChargeElement : ModularGameActionElement
    {
        [SerializeField]
        private float baseValue = 1.0f;

        [SerializeField]
        private float chargeSpeed = 1.0f;

        [SerializeField]
        private bool resetOnExit = false;

        [GameActionPropertyDescription("The charge speed")]
        private GameActionInputConnector<float> speedConnector;

        [GameActionPropertyDescription("The output bool")]
        private GameActionOutputRegister<float> outConnector;

        private float value;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outConnector = new GameActionOutputRegister<float>();
            outputs.Add(outConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            speedConnector = new GameActionInputConnector<float>();
            inputs.Add(speedConnector);
        }

        protected override void OnElementBegin()
        {
            value = baseValue;
            outConnector.Set(value);
        }

        protected override void OnElementEnd()
        {
            if (resetOnExit)
            {
                value = baseValue;
            }
        }

        protected override void OnElementUpdate(float deltaTime, float time, float normalizedTime)
        {
            value += speedConnector.Get(chargeSpeed) * deltaTime;
            outConnector.Set(value);
        }

        public override string ToString()
        {
            return "Charge";
        }
    }
}
