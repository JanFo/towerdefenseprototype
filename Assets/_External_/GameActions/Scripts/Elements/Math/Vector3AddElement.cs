﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Vector3/Add", "Adds two Vector3.")]
    public class Vector3AddElement : ModularGameActionElement
    {
        [SerializeField]
        private Vector3 vectorModifier = Vector3.zero;

        [GameActionPropertyDescription("Vector A")]
        private GameActionInputConnector<Vector3> inputConnector;

        [GameActionPropertyDescription("Vector B")]
        private GameActionInputConnector<Vector3> vectorModifierConnetor;

        [GameActionPropertyDescription("The operation result vector.")]
        private GameActionOutputDelegate<Vector3> outputConnector;

        [SerializeField]
        private bool truncateResult;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<Vector3>(GetResult);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<Vector3>();
            vectorModifierConnetor = new GameActionInputConnector<Vector3>();
            inputs.Add(inputConnector);
            inputs.Add(vectorModifierConnetor);
        }

        private Vector3 GetResult()
        {
            Vector3 result = GetVector3();
            return result;
        }

        private Vector3 GetVector3()
        {
            return inputConnector.Get() + vectorModifierConnetor.Get(vectorModifier);
        }

        public override string ToString()
        {
            return "Add";
        }
    }
}
