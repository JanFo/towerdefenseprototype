﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{

    [GameActionElementDescription("Math/Boolean", "A boolean operation on the input connectors. Can also emit a static TRUE or FALSE.", true)]
    public class BooleanElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The first input value.")]
        private GameActionInputConnector<bool> inConnector1;

        [GameActionPropertyDescription("The second input value.")]
        private GameActionInputConnector<bool> inConnector2;

        [GameActionPropertyDescription("The output bool")]
        private GameActionOutputDelegate<bool> outConnector;

        [SerializeField]
        private BooleanOperation operation = BooleanOperation.And;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inConnector1 = new GameActionInputConnector<bool>();
            inConnector2 = new GameActionInputConnector<bool>();
            inputs.Add(inConnector1);
            inputs.Add(inConnector2);
        }

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outConnector = new GameActionOutputDelegate<bool>(GetOutBoolean);
            outputs.Add(outConnector);
        }

        private bool GetOutBoolean()
        {
            switch (operation)
            {
                case BooleanOperation.True:
                    return true;
                case BooleanOperation.False:
                    return false;
                case BooleanOperation.Not:
                    return !inConnector1.Get();
                case BooleanOperation.Or:
                    return inConnector1.Get() || inConnector2.Get();
                case BooleanOperation.And:
                    return inConnector1.Get() && inConnector2.Get();
                case BooleanOperation.Xor:
                    return inConnector1.Get() != inConnector2.Get();
                default:
                    return false;
            }
        }

        public override string ToString()
        {
            return "Bool [" + operation + "]";
        }
    }
}