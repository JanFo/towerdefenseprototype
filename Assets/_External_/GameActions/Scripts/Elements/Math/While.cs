﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Flow/While", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.")]
    public class While : ModularGameActionElement
    {
        [GameActionPropertyDescription("The input vector.")]
        private GameActionInputConnector<bool> conditionConnector;

        [SerializeField]
        private bool repeat = true;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            conditionConnector = new GameActionInputConnector<bool>();

            inputs.Add(conditionConnector);
        }

        protected override void OnElementBegin()
        {

        }

        protected override void OnElementUpdate(float deltaTime, float time, float normalizedTime)
        {
            if (!conditionConnector.Get())
            {
                Flag = ModularGameActionElementFlag.Skip;
            }
        }

        protected override void OnElementEnd()
        {
            if (conditionConnector.Get() && repeat)
            {
                Flag = ModularGameActionElementFlag.Repeat;
            }
        }

        public override string ToString()
        {
            return "While";
        }

        public override Color GetEditorColor()
        {
            return new Color(0.9f, 0.8f, 0.5f);
        }
    }
}
