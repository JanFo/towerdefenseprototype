﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Vector3/Direction Towards", "Retrieves the normalized direction from one Vector3 to another", true)]
    public class Vector3DirectionTowardsElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The from position")]
        private GameActionInputConnector<Vector3> fromConnector;

        [GameActionPropertyDescription("The to position")]
        private GameActionInputConnector<Vector3> toConnector;

        [GameActionPropertyDescription("The character")]
        private GameActionOutputDelegate<Vector3> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<Vector3>(GetDirection);
            outputs.Add(outputConnector);
        }

        private Vector3 GetDirection()
        {
            return (toConnector.Get() - fromConnector.Get()).normalized;
        }


        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            fromConnector = new GameActionInputConnector<Vector3>();
            toConnector = new GameActionInputConnector<Vector3>();
            inputs.Add(fromConnector);
            inputs.Add(toConnector);
        }

        public override string ToString()
        {
            return "Direction Towards";
        }

    }
}
