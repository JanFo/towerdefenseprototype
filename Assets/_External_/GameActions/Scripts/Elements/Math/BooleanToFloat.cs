﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Conversion/Boolean To Float", "Transform a boolean input into a float output.")]
    public class BooleanToFloat : ModularGameActionElement
    {
        [GameActionPropertyDescription("The boolean input.")]
        private GameActionInputConnector<bool> boolInput;

        [GameActionPropertyDescription("The integer output.")]
        private GameActionOutputDelegate<float> integerOutput;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            boolInput = new GameActionInputConnector<bool>();

            inputs.Add(boolInput);
        }

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            integerOutput = new GameActionOutputDelegate<float>(DoTheCast);

            outputs.Add(integerOutput);
        }

        private float DoTheCast()
        {
            return boolInput.Get() ? 1.0f : 0.0f;
        }

        public override string ToString()
        {
            return "Bool2Float";
        }
    }
}