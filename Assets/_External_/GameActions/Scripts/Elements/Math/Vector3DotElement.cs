﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Vector3/Dot", "Returns the value of a curve.", true)]
    public class Vector3Dot : ModularGameActionElement
    {
        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> inputConnector1;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> inputConnector2;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<float> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<float>(GetDotProduct);

            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector1 = new GameActionInputConnector<Vector3>();
            inputConnector2 = new GameActionInputConnector<Vector3>();
            inputs.Add(inputConnector1);
            inputs.Add(inputConnector2);
        }

        private float GetDotProduct()
        {
            Vector3 in1 = inputConnector1.Get();
            Vector3 in2 = inputConnector2.Get();
            float result = Vector3.Dot(in1, in2);
            return result;
        }

        public override string ToString()
        {
            return "Dot";
        }
    }
}
