﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Vector3/Scale", "Returns the scaled input vector.", true)]
    public class Vector3ScaleElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The input vector")]
        private GameActionInputConnector<Vector3> inputConnector1;

        [GameActionPropertyDescription("The scale value")]
        private GameActionInputConnector<float> scaleConnector;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<Vector3> outputConnector;

        [SerializeField]
        private Vector3 defaultVector;

        [SerializeField]
        private float defaultScale = 1;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<Vector3>(GetScaledVector);

            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector1 = new GameActionInputConnector<Vector3>();
            scaleConnector = new GameActionInputConnector<float>();
            inputs.Add(inputConnector1);
            inputs.Add(scaleConnector);
        }

        private Vector3 GetScaledVector()
        {
            Vector3 in1 = inputConnector1.Get(defaultVector);
            float in2 = scaleConnector.Get(defaultScale);
            Vector3 result = in1 * in2;
            return result;
        }

        public override string ToString()
        {
            return "Scale";
        }
    }
}
