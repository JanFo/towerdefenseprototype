﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Math/Vector3/Match", "Returns the value of a curve.", true)]
    public class Vector3MatchElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> inputConnector1;

        [GameActionPropertyDescription("The input value")]
        private GameActionInputConnector<Vector3> inputConnector2;

        [GameActionPropertyDescription("The input vector.")]
        private GameActionOutputDelegate<float> outputConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<float>(GetMatch);

            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector1 = new GameActionInputConnector<Vector3>();
            inputConnector2 = new GameActionInputConnector<Vector3>();
            inputs.Add(inputConnector1);
            inputs.Add(inputConnector2);
        }

        private float GetMatch()
        {
            Vector3 in1 = inputConnector1.Get();
            Vector3 in2 = inputConnector2.Get();

            return Mathf.Clamp01(Vector3.Dot(in1, in2));
        }

        public override string ToString()
        {
            return "Match";
        }
    }
}
