﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Params/Set Bool", "Sets the HasControl value of a character component to false for the duration.")]
    public class SetBoolParamElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The character")]
        private GameActionInputConnector<bool> boolConnector;

        [SerializeField, GameActionParamDropdown]
        private byte actionParam;

        [SerializeField]
        private bool value;

        private IGameActionActor actor;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            boolConnector = new GameActionInputConnector<bool>();
            inputs.Add(boolConnector);
        }

        protected override void OnElementInitialize(IGameActionContext context, IGameActionActor actor)
        {
            this.actor = actor;
        }

        protected override void OnElementBegin()
        {
            actor.Parameters.SetActionParam(actionParam, GameActionParamValue.FromBool(boolConnector.Get(value)));
        }

        public override string ToString()
        {
            var values = GameActionSettings.Instance.GameActionParameters;
            return "Set " + ArrayUtils.Get(values, actionParam, "ERROR");
        }

        public override Color GetEditorColor()
        {
            return new Color(82 / 255.0f, 84 / 255.0f, 109 / 255.0f, 0.75f);
        }
    }
}