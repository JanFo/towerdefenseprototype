﻿using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Params/Vector3", "Vector3 Variable.", true)]
    public class Vector3ParamElement : GameActionParamElement<Vector3>
    {
        protected override Vector3 GetVariable()
        {
            return actor.Parameters.GetVector3(key);
        }
    }
}
