﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Params/Bool", "Bool Variable.", true)]
    public class BoolParamElement : GameActionParamElement<bool>
    {
        protected override bool GetVariable()
        {
            return actor.Parameters.GetBool(key);
        }
    }
}
