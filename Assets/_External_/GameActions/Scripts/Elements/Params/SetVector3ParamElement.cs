﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Params/Set Vector3", "Sets the HasControl value of a character component to false for the duration.")]
    public class SetVector3ParamElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The character")]
        private GameActionInputConnector<Vector3> vectorConnector;

        [SerializeField, GameActionParamDropdown]
        private byte actionParam;

        [SerializeField]
        private Vector3 value;

        [SerializeField]
        private bool writeOnUpdate = true;

        private IGameActionActor actor;

        public override void GetTimeMarkers(out bool hasBegin, out bool hasUpdate, out bool hasEnd)
        {
            hasBegin = true;
            hasUpdate = writeOnUpdate;
            hasEnd = false;
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            vectorConnector = new GameActionInputConnector<Vector3>();
            inputs.Add(vectorConnector);
        }

        protected override void OnElementInitialize(IGameActionContext context, IGameActionActor actor)
        {
            this.actor = actor;
        }

        protected override void OnElementBegin()
        {
            actor.Parameters.SetActionParam(actionParam, GameActionParamValue.FromVector3(vectorConnector.Get(value)));
        }

        protected override void OnElementUpdate(float deltaTime, float localProgress, float normalizedLocalProgress)
        {
            if (writeOnUpdate)
            {
                actor.Parameters.SetActionParam(actionParam, GameActionParamValue.FromVector3(vectorConnector.Get(value)));
            }
        }

        public override string ToString()
        {
            var values = GameActionSettings.Instance.GameActionParameters;
            return "Set " + ArrayUtils.Get(values, actionParam - 1, "ERROR");
        }

        public override Color GetEditorColor()
        {
            return new Color(82 / 255.0f, 84 / 255.0f, 109 / 255.0f, 0.75f);
        }
    }
}