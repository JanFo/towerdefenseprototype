﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    public class GameActionParamElement : ModularGameActionElement { }

    public abstract class GameActionParamElement<T> : GameActionParamElement
    {
        [SerializeField, GameActionParamDropdown]
        protected byte key;

        [GameActionPropertyDescription("The register content.")]
        private GameActionOutputDelegate<T> outputConnector;

        protected IGameActionActor actor;

        protected override void OnElementInitialize(IGameActionContext context, IGameActionActor actor)
        {
            this.actor = actor;
        }

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<T>(GetVariable);
            outputs.Add(outputConnector);
        }

        protected abstract T GetVariable();

        public override string ToString()
        {
            var values = GameActionSettings.Instance.GameActionParameters;
            return ArrayUtils.Get(values, key - 1, "none");
        }

        public override Color GetEditorColor()
        {
            return new Color(82 / 255.0f, 84 / 255.0f, 109 / 255.0f, 0.75f);
        }
    }
}