﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Params/Unsinged Integer", "Int Variable.", true)]
    public class UIntParamElement : GameActionParamElement<uint>
    {
        protected override uint GetVariable()
        {
            return actor.Parameters.GetUInt(key);
        }
    }
}
