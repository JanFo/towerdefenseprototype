﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameActions
{
    [GameActionElementDescription("Params/Integer", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.")]
    public class IntegerParamElement : GameActionParamElement<int>
    {
        protected override int GetVariable()
        {
            return actor.Parameters.GetInt(key);
        }
    }
}