﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Spatial/Clamp to Sphere", "Adds two Vector3.")]
    public class Vector3ClampToSphere : ModularGameActionElement
    {
        [GameActionPropertyDescription("Vector A")]
        private GameActionInputConnector<Vector3> inputConnector;

        [GameActionPropertyDescription("Vector A")]
        private GameActionInputConnector<Vector3> centerConnector;

        [GameActionPropertyDescription("Vector A")]
        private GameActionInputConnector<float> radiusConnector;

        [GameActionPropertyDescription("The operation result vector.")]
        private GameActionOutputDelegate<Vector3> outputConnector;

        [SerializeField]
        private float radius;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputDelegate<Vector3>(GetResult);
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<Vector3>();
            centerConnector = new GameActionInputConnector<Vector3>();
            radiusConnector = new GameActionInputConnector<float>();
            inputs.Add(inputConnector);
            inputs.Add(centerConnector);
            inputs.Add(radiusConnector);
        }

        private Vector3 GetResult()
        {
            float radius = radiusConnector.Get(this.radius);
            Vector3 position = inputConnector.Get();
            Vector3 center = centerConnector.Get();
            Vector3 diff = position - center;

            if (diff.sqrMagnitude > radius * radius)
            {
                return center + diff.normalized * radius;
            }

            return position;
        }

        public override string ToString()
        {
            return "Clamp to Sphere";
        }
    }
}
