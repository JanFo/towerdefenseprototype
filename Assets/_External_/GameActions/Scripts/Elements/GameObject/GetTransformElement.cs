﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("GameObject/Get Transform", "Retrieves the transform component of a character", true)]
    public class GetTransformElement : GetComponentElement<Transform>
    {
      
        public override string ToString()
        {
            return "Get Transform";
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            base.OnCreateInputs(inputs);

        }
    }
}