﻿using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("GameObject/Get Animator", "Retrieves the animator component of a gameobject.")]
    public class GetAnimatorElement : GetComponentElement<Animator>
    {

    }
}