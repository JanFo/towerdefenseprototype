﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("GameObject/Get Transform Values", "Retrieves the values of a transform", true)]
    public class GetTransformValuesElement : GetComponentElement<Transform>
    {
        [GameActionPropertyDescription("The transform")]
        private GameActionInputConnector<Transform> transformConnector;

        [GameActionPropertyDescription("The transform position (default is Vector3.zero)")]
        private GameActionOutputDelegate<Vector3> positionConnector;

        [GameActionPropertyDescription("The transform eulerAngles (default is Vector3.zero)")]
        private GameActionOutputDelegate<Vector3> eulerConnector;

        [GameActionPropertyDescription("The transform scale (default is Vector3.one)")]
        private GameActionOutputDelegate<Vector3> scaleConnector;

        [SerializeField]
        private bool localValues = false;

        public override string ToString()
        {
            return "Get Transform";
        }

        
        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            transformConnector = new GameActionInputConnector<Transform>();
            inputs.Add(transformConnector);
        }

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            positionConnector = new GameActionOutputDelegate<Vector3>(GetPosition);
            eulerConnector = new GameActionOutputDelegate<Vector3>(GetEulerAngles);
            scaleConnector = new GameActionOutputDelegate<Vector3>(GetScale);
            outputs.Add(positionConnector);
            outputs.Add(eulerConnector);
            outputs.Add(scaleConnector);
        }

        private Vector3 GetScale()
        {
            Transform transform = transformConnector.Get();

            if(transform == null)
            {
                return Vector3.one;
            }

            return transform.localScale;
        }

        private Vector3 GetEulerAngles()
        {
            Transform transform = transformConnector.Get();

            if (transform == null)
            {
                return Vector3.zero;
            }

            return localValues ? transform.localEulerAngles : transform.eulerAngles;
        }

        private Vector3 GetPosition()
        {
            Transform transform = transformConnector.Get();

            if (transform == null)
            {
                return Vector3.zero;
            }

            return localValues ? transform.localPosition : transform.position;
        }
    }
}