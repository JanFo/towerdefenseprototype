﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("GameObject/Set Transform Values", "Sets the position and rotation values of a transform component.")]
    public class SetTransformValuesElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The input transform")]
        private GameActionInputConnector<Transform> inputConnector;

        [GameActionPropertyDescription("The target position")]
        private GameActionInputConnector<Vector3> positionConnector;

        [GameActionPropertyDescription("The target rotation")]
        private GameActionInputConnector<Vector3> rotationConnector;

        [GameActionPropertyDescription("The target scale")]
        private GameActionInputConnector<Vector3> scaleConnector;

        [SerializeField]
        private bool local = true;

        private Transform targetTransform;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<Transform>();
            positionConnector = new GameActionInputConnector<Vector3>();
            rotationConnector = new GameActionInputConnector<Vector3>();
            scaleConnector = new GameActionInputConnector<Vector3>();

            inputs.Add(inputConnector);
            inputs.Add(positionConnector);
            inputs.Add(rotationConnector);
            inputs.Add(scaleConnector);
        }

        protected override void OnElementBegin()
        {
            targetTransform = inputConnector.Get();
        }


        protected override void OnElementUpdate(float deltaTime, float localProgress, float normalizedLocalProgress)
        {
            if (targetTransform == null)
            {
                return;
            }

            if(scaleConnector.IsConnected)
            {
                targetTransform.localScale = scaleConnector.Get();
            }

            if (local)
            {
                if (positionConnector.IsConnected)
                {
                    targetTransform.localPosition = positionConnector.Get();
                }

                if (rotationConnector.IsConnected)
                {
                    targetTransform.localEulerAngles = rotationConnector.Get();
                }

            }
            else
            {
                if (positionConnector.IsConnected)
                {
                    targetTransform.position = positionConnector.Get();
                }

                if (rotationConnector.IsConnected)
                {
                    targetTransform.eulerAngles = rotationConnector.Get();
                }
            }
        }

        public override string ToString()
        {
            return "Set Transform Values";
        }
    }
}
