﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("GameObject/Set Transform Parent", "Sets the position and rotation values of a transform component.")]
    public class SetTransformParentElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The input transform")]
        private GameActionInputConnector<Transform> inputConnector;

        [GameActionPropertyDescription("The parent transform")]
        private GameActionInputConnector<Transform> parentConnector;

        [SerializeField]
        private bool worldPositionStays;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<Transform>();
            parentConnector = new GameActionInputConnector<Transform>();

            inputs.Add(inputConnector);
            inputs.Add(parentConnector);
        }

        protected override void OnElementBegin()
        {
            Transform targetTransform = inputConnector.Get();
            Transform parentTransform = parentConnector.Get();

            targetTransform.SetParent(parentTransform, worldPositionStays);
        }


        public override string ToString()
        {
            return "Set Transform Parent";
        }
    }
}
