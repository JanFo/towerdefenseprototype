﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{

    [GameActionElementDescription("GameObject/Effects/Line Renderer", "Spawns a prefab at a specific position.")]
    public class LineRendererElement : ModularGameActionElement
    {
        [SerializeField, GameActionPropertyDescription("The prefab to spawn.")]
        private LineRenderer prefab = null;

        [SerializeField, GameActionPropertyDescription("Indicates whether to destroy the instance in OnElementEnd.")]
        private bool destroyOnEnd = true;

        [SerializeField, GameActionPropertyDescription("Indicates whether to destroy the instance in OnElementEnd.")]
        private float destroyDelay = 0;

        [SerializeField]
        private bool transformOnUpdate = false;

        private LineRenderer instance;

        [GameActionPropertyDescription("The spawn position")]
        private GameActionInputConnector<Vector3> startConnector;

        [GameActionPropertyDescription("The spawn rotation")]
        private GameActionInputConnector<Vector3> endConnector;

        [GameActionPropertyDescription("The spawned game object")]
        private GameActionOutputRegister<GameObject> spawnedInstance;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            startConnector = new GameActionInputConnector<Vector3>();
            endConnector = new GameActionInputConnector<Vector3>();

            inputs.Add(startConnector);
            inputs.Add(endConnector);
        }

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            spawnedInstance = new GameActionOutputRegister<GameObject>();
            outputs.Add(spawnedInstance);
        }


        protected override void OnElementBegin()
        {
            instance = Instantiate(prefab);
            instance.useWorldSpace = true;


            instance.SetPosition(0, startConnector.Get());
            instance.SetPosition(instance.positionCount - 1, endConnector.Get());
            spawnedInstance.Set(instance.gameObject);
        }

        protected override void OnElementUpdate(float deltaTime, float localProgress, float normalizedLocalProgress)
        {
            if (!transformOnUpdate)
            {
                return;
            }

            instance.SetPosition(0, startConnector.Get());
            instance.SetPosition(instance.positionCount - 1, endConnector.Get());
        }

        protected override void OnElementEnd()
        {
            if (destroyOnEnd)
            {
                if (instance != null)
                {
                    foreach (ParticleSystem system in instance.GetComponentsInChildren<ParticleSystem>())
                    {
                        if (system != null)
                        {
                            system.Stop();
                        }
                    }

                    instance.SendMessage("OnKill", SendMessageOptions.DontRequireReceiver);
                    Destroy(instance, destroyDelay);
                }
            }
        }

        public override string ToString()
        {
            if (prefab == null)
            {
                return "Line Renderer";
            }

            return prefab?.name ?? "Line Renderer";
        }
    }
}
