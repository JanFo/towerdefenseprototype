﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameActions
{
    public abstract class GetComponentElement<T> : ModularGameActionElement
    {
        protected GameActionOutputDelegate<T> getConnector;

        private GameActionInputConnector<GameObject> inputConnector;

        [SerializeField]
        private bool searchInChildren = false;

        [SerializeField]
        private string childName;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<GameObject>();
            inputs.Add(inputConnector);
        }

        public override string ToString()
        {
            return typeof(T).ToString();
        }
        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            getConnector = new GameActionOutputDelegate<T>(Get);
            outputs.Add(getConnector);
        }

        protected T Get()
        {
            GameObject gameObject = inputConnector.Get();

            if (gameObject == null)
            {
                return default;
            }

            if (searchInChildren)
            {
                if (!string.IsNullOrWhiteSpace(childName))
                {
                    Transform child = FindInChildren(gameObject.transform, childName);

                    if (child == null)
                    {
                        return default;
                    }

                    return child.GetComponent<T>();
                }
                else
                {
                    return gameObject.GetComponentInChildren<T>();
                }
            }
            else
            {
                return gameObject.GetComponent<T>();
            }
        }

        private Transform FindInChildren(Transform transform, string name)
        {
            return (from x in transform.GetComponentsInChildren<Transform>()
                    where x.gameObject.name == name
                    select x).FirstOrDefault();
        }
    }
}