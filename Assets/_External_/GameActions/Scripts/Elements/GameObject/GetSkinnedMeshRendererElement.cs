﻿using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("GameObject/Get Skinned Mesh Renderer", "Retrieves the mesh renderer component of a gameobject.")]
    public class GetSkinnedMeshRendererElement : GetComponentElement<SkinnedMeshRenderer>
    {

    }
}