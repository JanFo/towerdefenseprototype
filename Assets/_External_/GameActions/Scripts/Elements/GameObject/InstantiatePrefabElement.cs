﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    public interface IPrefabDescriptor
    {
        string Description { get; }
    }

    [GameActionElementDescription("GameObject/Spawn Prefab", "Spawns a prefab at a specific position.")]
    public class InstantiatePrefabElement : ModularGameActionElement
    {
        [SerializeField, GameActionPropertyDescription("The prefab to spawn.")]
        private GameObject prefab = null;

        [SerializeField, GameActionPropertyDescription("Indicates whether to destroy the instance in OnElementEnd.")]
        private bool destroyOnEnd = true;

        [SerializeField, GameActionPropertyDescription("Indicates whether to destroy the instance in OnElementEnd.")]
        private float destroyDelay = 0;

        [SerializeField]
        private bool transformOnUpdate = false;

        private GameObject instance;

        [GameActionPropertyDescription("The spawn position")]
        private GameActionInputConnector<Vector3> positionConnector;

        [GameActionPropertyDescription("The spawn rotation")]
        private GameActionInputConnector<Vector3> rotationConnector;

        [GameActionPropertyDescription("The spawn scale")]
        private GameActionInputConnector<float> scaleConnector;

        [GameActionPropertyDescription("The spawn parent")]
        private GameActionInputConnector<Transform> parentConnector;

        [GameActionPropertyDescription("The spawned game object")]
        private GameActionOutputRegister<GameObject> spawnedInstance;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            positionConnector = new GameActionInputConnector<Vector3>();
            rotationConnector = new GameActionInputConnector<Vector3>();
            scaleConnector = new GameActionInputConnector<float>();
            parentConnector = new GameActionInputConnector<Transform>();

            inputs.Add(positionConnector);
            inputs.Add(rotationConnector);
            inputs.Add(scaleConnector);
            inputs.Add(parentConnector);
        }

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            spawnedInstance = new GameActionOutputRegister<GameObject>();
            outputs.Add(spawnedInstance);
        }

        protected override void OnCreateDescriptors(List<GameActionDescriptor> descriptors)
        {
            GameActionDescriptor prefabDescriptor = new GameActionDescriptor("Prefab Descriptor", DescribePrefab);
            descriptors.Add(prefabDescriptor);
        }

        private string DescribePrefab()
        {
            string result = "";

            if (prefab != null)
            {
                IPrefabDescriptor[] descriptions = prefab.GetComponents<IPrefabDescriptor>();

                if (descriptions != null)
                {
                    foreach (IPrefabDescriptor desc in descriptions)
                    {
                        result += desc.Description;
                    }
                }
            }

            return result;
        }

        protected override void OnElementBegin()
        {
            instance = Instantiate(prefab);
            Transform parent = parentConnector.Get();

            if (parent != null)
            {
                instance.transform.SetParent(parent);
            }

            instance.transform.localPosition = positionConnector.Get(Vector3.zero);
            instance.transform.localEulerAngles = rotationConnector.Get(Vector3.zero);
            float scale = scaleConnector.Get(1.0f);
            instance.transform.localScale = new Vector3(scale, scale, scale);
            spawnedInstance.Set(instance);
        }

        protected override void OnElementUpdate(float deltaTime, float localProgress, float normalizedLocalProgress)
        {
            if (!transformOnUpdate)
            {
                return;
            }

            float scale = scaleConnector.Get(1.0f);
            instance.transform.localScale = new Vector3(scale, scale, scale);
            instance.transform.position = positionConnector.Get(Vector3.zero);
            instance.transform.eulerAngles = rotationConnector.Get(Vector3.zero);
        }

        protected override void OnElementEnd()
        {
            if (destroyOnEnd)
            {
                if (instance != null)
                {
                    foreach (ParticleSystem system in instance.GetComponentsInChildren<ParticleSystem>())
                    {
                        if (system != null)
                        {
                            system.Stop();
                        }
                    }

                    instance.SendMessage("OnKill", SendMessageOptions.DontRequireReceiver);

                    Destroy(instance, destroyDelay);
                }
            }
        }

        public override string ToString()
        {
            if (prefab == null)
            {
                return "Prefab";
            }

            return prefab?.name ?? "Prefab";
        }
    }
}
