﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("GameObject/Get Actor GameObject", "Get the GameObject holding the ActorBehaviour.")]
    public class GetActorGameObjectElement : ModularGameActionElement<IGameActionContext, IGameObjectProvider>
    {
        [GameActionPropertyDescription("The GameObject holding the ActorBehaviour")]
        private GameActionOutputDelegate<GameObject> outConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outConnector = new GameActionOutputDelegate<GameObject>(GetGameObject);
            outputs.Add(outConnector);
        }

        private GameObject GetGameObject()
        {
            if(Self == null)
            {
                return null;
            }

            return Self.GetGameObject(Context);
        }

        public override string ToString()
        {
            return "Get GameObject";
        }
    }
}