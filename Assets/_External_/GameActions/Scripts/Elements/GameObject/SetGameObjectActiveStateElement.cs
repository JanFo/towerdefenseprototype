﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("GameObject/Set GameObject Active State", "Spawns a prefab at a specific position.")]
    public abstract class SetGameObjectActiveStateElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The GameObject")]
        private GameActionInputConnector<GameObject> gameObjectConnector;

        [GameActionPropertyDescription("The target state")]
        private GameActionInputConnector<bool> stateConnector;

        [SerializeField]
        private bool onUpdate = false;

        private GameObject gameObject;

        public override void GetTimeMarkers(out bool hasBegin, out bool hasUpdate, out bool hasEnd)
        {
            hasBegin = true;
            hasUpdate = onUpdate;
            hasEnd = false;
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            gameObjectConnector = new GameActionInputConnector<GameObject>();
            stateConnector = new GameActionInputConnector<bool>();
            inputs.Add(gameObjectConnector); 
            inputs.Add(stateConnector);

        }

        public override string ToString()
        {
            return "Set Active State";
        }

        protected override void OnElementBegin()
        {
            gameObject = gameObjectConnector.Get();
        }
    }
}