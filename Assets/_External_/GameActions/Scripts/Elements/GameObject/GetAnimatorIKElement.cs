﻿using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("GameObject/Get Animator IK", "Retrieves the animator component of a gameobject.")]
    public class GetAnimatorIKElement : GetComponentElement<AnimatorIK>
    {

    }
}