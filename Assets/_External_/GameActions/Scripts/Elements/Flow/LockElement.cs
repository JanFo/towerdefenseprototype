﻿using UnityEngine;

namespace GameActions
{

    [GameActionElementDescription("Flow/Lock", "Prevents the action from being cancelled by a call to 'Cancel()'.")]
    public class LockElement : ModularGameActionElement
    {
        public override string ToString()
        {
            return "Lock";
        }

        public override Color GetEditorColor()
        {
            return new Color(0.7f, 0.7f, 0.7f, 0.2f);
        }

        public override void GetTimeMarkers(out bool hasBegin, out bool hasUpdate, out bool hasEnd)
        {
            hasBegin = true;
            hasUpdate = false;
            hasEnd = true;
        }
    }
}
