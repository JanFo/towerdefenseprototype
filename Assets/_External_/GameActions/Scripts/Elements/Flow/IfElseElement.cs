﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Flow/If-Else", "Can be used to control the flow of an ability based on a bool input.")]
    public class IfElseElement : ModularGameActionElement
    {
        [SerializeField]
        private ModularGameActionElementFlag ifFlag = ModularGameActionElementFlag.None;

        [SerializeField]
        private ModularGameActionElementFlag elseFlag = ModularGameActionElementFlag.None;

        [GameActionPropertyDescription("The bool input.")]
        private GameActionInputConnector<bool> conditionConnector;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            conditionConnector = new GameActionInputConnector<bool>();

            inputs.Add(conditionConnector);
        }

        protected override void OnElementBegin()
        {
            if (conditionConnector.Get())
            {
                Flag = ifFlag;
            }
            else
            {
                Flag = elseFlag;
            }
        }

        public override string ToString()
        {
            return "If-Else";
        }
        public override Color GetEditorColor()
        {
            return new Color(0.4f, 0.35f, 0.3f, 0.8f);
        }
    }
}