﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Flow/Is Cancelled", "Checks whether the GameAction has been cancelled.")]
    public class IsCancelledElement : ModularGameActionElement
    {
        [GameActionPropertyDescription("The bool output.")]
        private GameActionOutputDelegate<bool> isCancelledConnector;

        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            isCancelledConnector = new GameActionOutputDelegate<bool>(() => IsActionCancelled);
            outputs.Add(isCancelledConnector);
        }

        public override string ToString()
        {
            return "Is Cancelled";
        }
        public override Color GetEditorColor()
        {
            return new Color(0.4f, 0.35f, 0.3f, 0.8f);
        }
    }
}