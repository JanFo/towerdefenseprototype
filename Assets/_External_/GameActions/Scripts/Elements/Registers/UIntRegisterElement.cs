﻿namespace GameActions
{
    [GameActionElementDescription("Register/UInt", "UInt Register.")]
    public class UIntRegisterElement : RegisterAbilityElement<uint>
    {

    }
}