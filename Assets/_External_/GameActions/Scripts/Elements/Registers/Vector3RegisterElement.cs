﻿using UnityEngine;

namespace GameActions
{
    [GameActionElementDescription("Register/Vector3", "Vector3 Register.")]
    public class Vector3RegisterElement : RegisterAbilityElement<Vector3>
    {

    }
}
