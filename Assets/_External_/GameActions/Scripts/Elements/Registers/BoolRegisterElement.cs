﻿namespace GameActions
{
    [GameActionElementDescription("Register/Bool", "Bool Register.")]
    public class BoolRegisterElement : RegisterAbilityElement<bool>
    {

    }
}