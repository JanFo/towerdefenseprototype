﻿namespace GameActions
{
    [GameActionElementDescription("Register/Float", "Float Register.")]
    public class FloatRegisterElement : RegisterAbilityElement<float>
    {

    }
}