﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    public class RegisterAbilityElement : ModularGameActionElement
    {

        [SerializeField]
        public bool writeOnUpdate = false;

        public override void GetTimeMarkers(out bool hasBegin, out bool hasUpdate, out bool hasEnd)
        {
            hasBegin = true;
            hasUpdate = writeOnUpdate;
            hasEnd = false;
        }

        public override Color GetEditorColor()
        { 
            return new Color(0.3f, .4f, 0.3f, 0.8f);
        }
    }

    public abstract class RegisterAbilityElement<T> : RegisterAbilityElement
    {
        [GameActionPropertyDescription("The register input")]
        private GameActionInputConnector<T> inputConnector;

        [GameActionPropertyDescription("The register content.")]
        private GameActionOutputRegister<T> outputConnector;

        [SerializeField]
        protected T value = default;


        protected override void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {
            outputConnector = new GameActionOutputRegister<T>();
            outputs.Add(outputConnector);
        }

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            inputConnector = new GameActionInputConnector<T>();
            inputs.Add(inputConnector);
        }


        protected override void OnCreateDescriptors(List<GameActionDescriptor> descriptors)
        {
            base.OnCreateDescriptors(descriptors);
            descriptors.Add(new GameActionDescriptor("Value", () => value.ToString()));
        }

        protected override void OnElementBegin()
        {
            outputConnector.Set(inputConnector.Get(value));
        }

        protected override void OnElementUpdate(float deltaTime, float localProgress, float normalizedLocalProgress)
        {
            if (writeOnUpdate && inputConnector.IsConnected)
            {
                outputConnector.Set(inputConnector.Get());
            }
        }

        public override string ToString()
        {
            return typeof(T).ToString();
        }
    }
}
