﻿using UnityEngine;

namespace GameActions.Example
{
    public class ActionManager : MonoBehaviour, IGameActionContext
    {
        private GameActionCoroutineRunner runner;

        public ActorBehaviour[] actors;

        public GameActionObject actionObject;

        private GameAction action;

        public IGameActionCoroutineRunner Runner => runner;

        private void Start()
        {
            runner = GetComponent<GameActionCoroutineRunner>();
        }

        private void Update()
        {
            if (action != null && !action.IsDone)
            {
                return;
            }

            foreach (ActorBehaviour actor in actors)
            {
                action = actionObject.CreateAction();
                action.Initialize(this, actor);
                action.Start();
            }


        }
    }
}