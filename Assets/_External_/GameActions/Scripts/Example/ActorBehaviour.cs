﻿using UnityEngine;

namespace GameActions.Example
{
    public class ActorBehaviour : MonoBehaviour, IGameActionActor, IGameObjectProvider, IBananaThrower
    {
        private GameActionParameters parameters;

        public GameActionParameters Parameters => parameters;

        public bool HasNext => false;

        public GameObject GetGameObject(IGameActionContext context)
        {
            return gameObject;
        }

        public void ThrowBanana(Vector3 direction)
        {
            transform.position += 5 * direction;
        }

        private void Start()
        {
            parameters = new GameActionParameters(12);
        }
    }
}