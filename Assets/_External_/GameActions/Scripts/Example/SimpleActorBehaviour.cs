﻿using UnityEngine;

namespace GameActions.Example
{
    public class SimpleActorBehaviour : MonoBehaviour, IGameActionActor, IGameObjectProvider
    {
        private GameActionParameters parameters;

        public GameActionParameters Parameters => parameters;

        public bool HasNext => false;

        private void Start()
        {
            parameters = new GameActionParameters(12);
        }
        
        public GameObject GetGameObject(IGameActionContext context)
        {
            return gameObject;
        }
    }
}