﻿using System.Collections.Generic;
using UnityEngine;

namespace GameActions.Example
{
    [GameActionElementDescription("Example/Throw Banana", "Compares the squared magnitude of the input vector to a threshold value and sets a respective flag.")]

    public class ThrowBananaElement : ModularGameActionElement<IGameActionContext, IBananaThrower>
    {

        [GameActionPropertyDescription("The direction for the banana throw")]
        private GameActionInputConnector<Vector3> directionConnector;

        protected override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            directionConnector = new GameActionInputConnector<Vector3>();
            inputs.Add(directionConnector);
        }

        protected override void OnElementBegin()
        {
            Vector3 direction = directionConnector.Get(new Vector3(1, 0, 0));
            Self.ThrowBanana(direction);
        }

        public override string ToString()
        {
            return "Throw Banana!";
        }
    }
}
