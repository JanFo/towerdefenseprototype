﻿
using UnityEngine;

namespace GameActions.Example
{
    public interface IBananaThrower
    {
        void ThrowBanana(Vector3 direcion);
    }
}
