﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    public class AnimatorIK : MonoBehaviour
    {
        private struct AvatarIKTask
        {
            public AvatarIKGoal goal;
            public Vector3 position;
            public float weight;
        }

        [SerializeField]
        private Animator animator;

        private Vector4 lookAtWeights;

        private Vector3 lookAtPosition;

        private List<AvatarIKTask> tasks;

        private void Awake()
        {
            tasks = new List<AvatarIKTask>();
        }

        public void SetIKPositionAndWeight(AvatarIKGoal goal, Vector3 position, float v)
        {
            tasks.Add(new AvatarIKTask()
            {
                goal = goal,
                position = position,
                weight = v
            });
        }

        public void SetLookAtPosition(Vector3 target)
        {
            lookAtPosition = target;
        }

        public void SetLookAtWeight(float totalWeight, float bodyWeight, float headWeight, float eyeWeight)
        {
            lookAtWeights = new Vector4(totalWeight, bodyWeight, headWeight, eyeWeight);
        }

        private void OnAnimatorIK(int layerIndex)
        {
            animator.SetLookAtPosition(lookAtPosition);
            animator.SetLookAtWeight(lookAtWeights.x, lookAtWeights.y, lookAtWeights.z, lookAtWeights.w);

            foreach (AvatarIKTask task in tasks)
            {
                animator.SetIKPosition(task.goal, task.position);
                animator.SetIKPositionWeight(task.goal, Mathf.Clamp01(task.weight));
            }

            tasks.Clear();
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(lookAtPosition, .12f);
        }
    }
}