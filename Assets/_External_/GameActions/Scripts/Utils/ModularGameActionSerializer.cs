﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace GameActions 
{
    public class ModularGameActionSerializer
    {
        [Serializable]
        private class ModularGameActionData
        {
            public string name;
            public string iconPath;
            public string descriptionKey;
            public float cooldown;  
            public float minTime;
            public float timeScale;
            public GameActionElementConnection[] connections;
            public ModularGameActionElementData[] elements;
            public GameActionElementMapping[] descriptorMappings;
            public GameActionElementMapping[] conditionMappings;
        }

        [Serializable]
        private class ModularGameActionElementData
        {
            public string type;
            public string data;
        }

        public static ModularGameActionObject FromJson(string json, string assetPath, Dictionary<string, string[]> typeMappings)
        {
            if(typeMappings == null)
            {
                typeMappings = new Dictionary<string, string[]>();
            }

            ModularGameActionData data = new ModularGameActionData();
            JsonUtility.FromJsonOverwrite(json, data);

            ModularGameActionObject modularGameActionObject = ScriptableObject.CreateInstance<ModularGameActionObject>();
            modularGameActionObject.name = data.name;
            modularGameActionObject.TimeScale = data.timeScale;
            modularGameActionObject.Connections = data.connections.ToList();
            modularGameActionObject.ConditionMappings = data.conditionMappings.ToList();
            modularGameActionObject.DescriptorMappings = data.descriptorMappings.ToList();
            modularGameActionObject.DescriptionKey = data.descriptionKey;
            modularGameActionObject.Cooldown = data.cooldown;
            modularGameActionObject.MinTime = data.minTime;

            AssetDatabase.CreateAsset(modularGameActionObject, assetPath);

            foreach (ModularGameActionElementData elementData in data.elements)
            {
                Type type = FindType(elementData.type, typeMappings);

                if(type == null)
                {
                    Debug.LogWarning("Unable to import. Missing type: " + elementData.type);
                    continue;
                }

                ModularGameActionElement element = ScriptableObject.CreateInstance(type) as ModularGameActionElement;
               
                if(element == null)
                {
                    Debug.LogWarning("Unable to import. Missing type!");
                    continue;
                }

                modularGameActionObject.AddElement(element);

                JsonUtility.FromJsonOverwrite(elementData.data, element);
                element.name = type.ToString();
            }

            return null;
        }

        private static Type FindType(string typeName, Dictionary<string, string[]> typeMappings)
        {
            string[] possibleTypeNames;

            if (typeMappings.ContainsKey(typeName))
            {
                possibleTypeNames = typeMappings[typeName];
            }
            else
            {
                possibleTypeNames = new string[]
                {
                    typeName
                };
            }

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach(string name in possibleTypeNames)
                {
                    Type type = assembly.GetType(name, false, true);

                    if(type != null && type.IsSubclassOf(typeof(ModularGameActionElement)))
                    {
                        return type;
                    }
                }
            }

            return null;
        }

        public static string ToJson(ModularGameActionObject gameActionObject)
        {
            ModularGameActionData actionData = new ModularGameActionData()
            {
                conditionMappings = gameActionObject.ConditionMappings.ToArray(),
                connections = gameActionObject.Connections.ToArray(),
                descriptionKey = gameActionObject.DescriptionKey,
                elements = gameActionObject.Elements.Select(e => new ModularGameActionElementData()
                {
                    type = e.GetType().ToString(),
                    data = JsonUtility.ToJson(e)
                }).ToArray(),
                cooldown = gameActionObject.Cooldown,
                iconPath = gameActionObject.Icon.ToString(),
                minTime = gameActionObject.MinTime,
                descriptorMappings = gameActionObject.DescriptorMappings.ToArray(),
                name = gameActionObject.name,
                timeScale = gameActionObject.TimeScale
            };

            return JsonUtility.ToJson(actionData, true);

        }
    }
}
#endif