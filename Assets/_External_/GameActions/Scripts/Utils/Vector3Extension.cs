﻿using UnityEngine;

namespace GameActions
{
    public static class Vector3Extension
    {
        public static Vector3 Truncate(this Vector3 vector, float length = 1.0f)
        {
            float sqrLen = length * length;
            if (vector.sqrMagnitude < sqrLen)
            {
                return vector;
            }

            return vector.normalized * length;
        }

        public static Vector3 Flatten(this Vector3 vector)
        {
            return new Vector3(vector.x, 0.0f, vector.z);
        }

        public static Vector2 Flat(this Vector3 vector)
        {
            return new Vector2(vector.x, vector.z);
        }
    }
}