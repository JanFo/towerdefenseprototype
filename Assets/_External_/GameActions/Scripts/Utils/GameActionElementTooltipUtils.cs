﻿using System;
using System.Reflection;

namespace GameActions
{
    [AttributeUsage(AttributeTargets.Class)]
    public class GameActionElementDescription : Attribute
    {
        private string description;

        private string path;

        public GameActionElementDescription(string path, string description, bool isDelegate = false)
        {
            this.description = description;
            this.path = path;
            IsDelegate = isDelegate;
        }

        public string Path
        {
            get
            {
                return path;
            }
        }

        public bool IsDelegate { get; }

        public override string ToString()
        {
            return description;
        }

    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Method)]
    public class GameActionPropertyDescription : Attribute
    {
        private string description;

        public GameActionPropertyDescription(string description)
        {
            this.description = description;
        }

        public override string ToString()
        {
            return description;
        }
    }

    /// <summary>
    /// Manages the creation of tooltips for abilities
    /// </summary>
    public class GameActionElementTooltipUtils
    {

        /// <summary>
        /// Creates a tooltip for an ability element
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static string CreateTooltip(ModularGameActionElement instance)
        {
            string description = GetClassAttributeString<GameActionPropertyDescription>(instance);


            string paramsDescription = GetFieldAttributeString<GameActionPropertyDescription, GameActionConnector>(instance);

            if (paramsDescription.Length > 0)
            {
                description += "\n\nParameters:";
                description += paramsDescription;
            }

            string inputDescription = GetConnectorFieldAttributeString<GameActionPropertyDescription, GameActionInputConnector>(instance);

            if (inputDescription.Length > 0)
            {
                description += "\n\nInputs:";
                description += inputDescription;
            }

            string outputDescription = GetConnectorFieldAttributeString<GameActionPropertyDescription, GameActionOutputConnector>(instance);

            if (outputDescription.Length > 0)
            {
                description += "\n\nOutputs:";
                description += outputDescription;
            }

            return description;
        }


        private static string GetClassAttributeString<T>(object instance)
        {
            object[] objects = instance.GetType().GetCustomAttributes(typeof(T), true);

            string result = string.Empty;

            if (objects != null)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    result += objects[i].ToString();
                }
            }

            return result;
        }

        public static string GetFieldAttributeString<T1, T2>(object instance) where T2 : GameActionConnector
        {
            FieldInfo[] fieldInfos = instance.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic);
            string result = string.Empty;

            foreach (FieldInfo info in fieldInfos)
            {
                if (!(info.GetValue(instance) is T2))
                {
                    object field = info.GetValue(instance);
                    object[] objects = info.GetCustomAttributes(typeof(T1), true);

                    if (objects != null && field != null)
                    {
                        for (int i = 0; i < objects.Length; i++)
                        {
                            result += "\n[" + field.GetType() + "] " + objects[i].ToString() + " (" + field.ToString() + ")";
                        }
                    }
                }
            }

            return result;
        }

        private static string GetConnectorFieldAttributeString<T1, T2>(object instance) where T2 : GameActionConnector
        {
            FieldInfo[] fieldInfos = instance.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic);
            string result = string.Empty;

            foreach (FieldInfo info in fieldInfos)
            {
                if (info.GetValue(instance) is T2)
                {
                    T2 connector = info.GetValue(instance) as T2;
                    object[] objects = info.GetCustomAttributes(typeof(T1), true);

                    if (objects != null)
                    {
                        for (int i = 0; i < objects.Length; i++)
                        {
                            result += "\n[" + connector.Type + "] " + objects[i].ToString();
                        }
                    }
                }
            }

            return result;
        }
    }
}