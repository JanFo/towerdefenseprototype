﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace GameActions
{
    public static class ArrayUtils
    {
        public static T Get<T>(T[] array, int index, T defaultValue)
        {
            if (array == null)
            {
                return defaultValue;
            }

            if (index < 0 || index >= array.Length)
            {
                return defaultValue;
            }

            return array[index];
        }

        public static T Get<T>(IEnumerable<T> array, int index, T defaultValue)
        {
            if (array == null)
            {
                return defaultValue;
            }

            if (index < 0 || index >= array.Count())
            {
                return defaultValue;
            }

            return array.ElementAt(index);
        }

        public static T Get<T>(List<T> array, int index, T defaultValue)
        {
            if (array == null)
            {
                return defaultValue;
            }

            if (index < 0 || index >= array.Count)
            {
                return defaultValue;
            }

            return array[index];
        }


        public static T GetRandom<T>(T[] array)
        {
            if (array == null)
            {
                return default;
            }

            int index = UnityEngine.Random.Range(0, array.Length);
            return Get(array, index, default);
        }

        public static T GetRandom<T>(IEnumerable<T> array)
        {
            if (array == null)
            {
                return default;
            }

            int index = UnityEngine.Random.Range(0, array.Count());
            return Get(array, index, default);
        }

        public static int GetRandomIndex<T>(T[] array)
        {
            if (array == null)
            {
                return default;
            }

            return UnityEngine.Random.Range(0, array.Length);
        }

        public static int GetRandomIndex<T>(IEnumerable<T> array)
        {
            if (array == null)
            {
                return default;
            }

            return UnityEngine.Random.Range(0, array.Count());
        }

        public static T GetRandom<T>(T[] array, Func<T, float> weightFunc)
        {
            if (array == null)
            {
                return default;
            }

            float total = array.Sum(weightFunc);
            float target = UnityEngine.Random.value * total;
            float k = 0;

            for (int i = 0; i < array.Length; i++)
            {
                k += weightFunc(array[i]);

                if (target < k)
                {
                    return array[i];
                }
            }

            return array.Last();
        }

        public static T GetRandom<T>(List<T> array)
        {
            int index = UnityEngine.Random.Range(0, array.Count);
            return Get(array, index, default);
        }

        public static void Set<T>(T[] array, int index, T value)
        {
            if (index < 0 || index >= array.Length)
            {
                return;
            }

            array[index] = value;
        }

        public static T Get<T>(IReadOnlyList<T> array, int index, T defaultValue)
        {
            if (index < 0 || index >= array.Count)
            {
                return defaultValue;
            }

            return array[index];
        }

        public static T Find<T>(IEnumerable<T> array, Func<T, bool> p)
        {
            foreach (T val in array)
            {
                if (p(val))
                {
                    return val;
                }
            }

            return default;
        }

        public static T[] Resize<T>(T[] array, int size)
        {
            T[] newArr = new T[size];

            for (int i = 0; i < Mathf.Min(size, array.Length); i++)
            {
                newArr[i] = array[i];
            }

            return newArr;
        }

        public static int IndexOf<T>(IEnumerable<T> list, Func<T, bool> p)
        {
            int index = 0;

            foreach (T val in list)
            {
                if (p(val))
                {
                    return index;
                }

                index++;
            }

            return -1;
        }

        public static int IndexOf<T>(IEnumerable<T> list, T p)
        {
            int index = 0;

            foreach (T val in list)
            {
                if (p.Equals(val))
                {
                    return index;
                }

                index++;
            }

            return -1;
        }

        public static void Clear<T>(T[] trackStates, T v)
        {
            for (int i = 0; i < trackStates.Length; i++)
            {
                trackStates[i] = v;
            }
        }

        public static TValue Get<TKey, TValue>(Dictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            if (dictionary == null)
            {
                return defaultValue;
            }

            if (key == null)
            {
                return defaultValue;
            }

            if (dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }

            return defaultValue;

            //if(key == null)
            //{
            //    return defaultValue;
            //}

            //TValue outValue;

            //return dictionary.TryGetValue(key, out outValue) ? outValue : defaultValue;
        }

        public static void Add<T>(Dictionary<T, int> dictionary, T key, int amount)
        {
            int value = Get(dictionary, key, 0);
            SetOrCreate(dictionary, key, value + amount);
        }

        public static Dictionary<K, T> CreateMap<K, T>(IEnumerable<T> values, Func<T, K> keySelector)
        {
            Dictionary<K, T> map = new Dictionary<K, T>();

            foreach (T value in values)
            {
                K key = keySelector(value);

                if (map.ContainsKey(key))
                {
                    Debug.Log("Duplicate key " + key + " detected while trying to build map. Ignoring entry...");
                    continue;
                }

                map.Add(key, value);
            }

            return map;
        }

        public static void Reset<T>(T[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = default;
            }
        }


        public static Dictionary<K, List<V>> CreateListMap<K, V, T>(
            IEnumerable<T> values, Func<T, K> keySelector, Func<T, V> valueSelector)
        {
            Dictionary<K, List<V>> map = new Dictionary<K, List<V>>();

            foreach (T value in values)
            {
                K k = keySelector(value);
                V v = valueSelector(value);

                if (!map.ContainsKey(k))
                {
                    map.Add(k, new List<V>());
                }

                map[k].Add(v);
            }

            return map;
        }

        public static Dictionary<K, V> CreateMap<K, V, T>(IEnumerable<T> values, Func<T, K> keySelector, Func<T, V> valueSelector)
        {
            if (values == null)
            {
                return null;
            }

            Dictionary<K, V> map = new Dictionary<K, V>();

            foreach (T value in values)
            {
                K k = keySelector(value);

                if(k == null)
                {
                    continue;
                }

                V v = valueSelector(value);

                if (map.ContainsKey(k))
                {
                    Debug.Log("Duplicate key " + k + " detected while trying to build map. Ignoring entry " + v.ToString() + ".");
                    continue;
                }

                map.Add(k, v);
            }

            return map;
        }

        public static T GetRepeated<T>(T[] array, int i)
        {
            if (array == null)
            {
                return default;
            }

            return array[Repeat(i, 0, array.Length)];
        }

        public static int Repeat(int index, int min, int max)
        {
            int length = max - min;

            if (length == 0)
                return min;

            while (index < min)
                index += length;

            while (index >= max)
            {
                index -= length;
            }

            return index;
        }


        public static void SetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary[key] = value;
            }
            else
            {
                dictionary.Add(key, value);
            }
        }

        public static T[] Concat<T>(params T[][] leftColliders)
        {
            int size = 0;

            for (int i = 0; i < leftColliders.Length; i++)
            {
                size += leftColliders[i].Length;
            }

            T[] result = new T[size];
            int k = 0;

            for (int i = 0; i < leftColliders.Length; i++)
            {
                for (int j = 0; j < leftColliders[i].Length; j++)
                {
                    result[k++] = leftColliders[i][j];
                }
            }

            return result;
        }

        internal static T[] Create<T>(int length, T v)
        {
            T[] result = new T[length];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = v;
            }

            return result;
        }

        public static int Next<T>(List<T> list, int index, Func<T, bool> condition)
        {
            for (int i = 1; i < list.Count; i++)
            {
                int result = Repeat(index + i, 0, list.Count - 1);

                if (condition(list[result]))
                {
                    return result;
                }
            }

            return index;
        }

        public static int Previous<T>(List<T> list, int index, Func<T, bool> condition)
        {
            for (int i = 1; i < list.Count; i++)
            {
                int result = Repeat(index - i, 0, list.Count - 1);

                if (condition(list[result]))
                {
                    return result;
                }
            }

            return index;
        }

        public static void InsertSorted<T>(List<T> list, T value, Func<T, int> p)
        {
            for (int i = 1; i < list.Count; i++)
            {
                if (p(value) > p(list[i]))
                {
                    continue;
                }

                list.Insert(i, value);
                return;
            }

            list.Add(value);
        }

        public static void InsertSortedDesc<T>(List<T> list, T value, Func<T, int> p)
        {
            for (int i = 1; i < list.Count; i++)
            {
                if (p(value) < p(list[i]))
                {
                    continue;
                }

                list.Insert(i, value);
                return;
            }

            list.Add(value);
        }
    }
}