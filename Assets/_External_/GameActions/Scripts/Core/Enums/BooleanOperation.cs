﻿namespace GameActions
{
    public enum BooleanOperation
    {
        True,
        False,
        And,
        Or,
        Xor,
        Not
    }
}