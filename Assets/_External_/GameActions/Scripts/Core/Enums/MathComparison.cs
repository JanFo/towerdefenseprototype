﻿namespace GameActions
{
    public enum MathComparison
    {
        Equals,
        Greater,
        GreaterEquals,
        Less,
        LessEquals
    }
}