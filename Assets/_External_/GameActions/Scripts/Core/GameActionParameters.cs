﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    public class GameActionParameters
    {
        public readonly int paramArrayLength = 24;

        public GameActionParamValue[] actionParams;

        private Dictionary<string, byte> indexLookup;

        public GameActionParameters(int paramArrayLength)
        {
            this.paramArrayLength = paramArrayLength;
            actionParams = new GameActionParamValue[paramArrayLength];

            GameActionSettings settings = GameActionSettings.Instance;
            indexLookup = ArrayUtils.CreateMap(settings.GameActionParameters, a => a, a => (byte)Array.IndexOf(settings.GameActionParameters, a));
        }
        public float GetFloat(byte index)
        {
            return actionParams[index].Value1;
        }

        public float GetFloat(string key)
        {
            return GetFloat(indexLookup[key]);
        }

        public GameActionParamValue GetValue(string key)
        {
            return GetValue(indexLookup[key]);
        }

        public GameActionParamValue GetValue(int index)
        {
            return actionParams[index];
        }

        public int GetInt(string key)
        {
            return GetInt(indexLookup[key]);
        }

        public int GetInt(byte index)
        {
            return (int)actionParams[index].Value1;
        }

        public bool GetBool(string key)
        {
            return GetBool(indexLookup[key]);
        }

        public bool GetBool(byte index)
        {
            return actionParams[index].Value1 == 1;
        }

        public uint GetUInt(string key)
        {
            return GetUInt(indexLookup[key]);
        }

        public uint GetUInt(byte index)
        {
            return (uint)actionParams[index].Value1;
        }

        public Vector3 GetVector3(string key)
        {
            return GetVector3(indexLookup[key]);
        }

        public Vector3 GetVector3(byte index)
        {
            GameActionParamValue value = actionParams[index];
            return new Vector3(value.Value1, value.Value2, value.Value3);
        }

        public void SetActionParam(string key, GameActionParamValue value)
        {
            SetActionParam(indexLookup[key], value);
        }

        public void SetActionParam(byte index, GameActionParamValue value)
        {
            if(index == 0)
            {
                return;
            }

            actionParams[index] = value;
        }
    }
}
