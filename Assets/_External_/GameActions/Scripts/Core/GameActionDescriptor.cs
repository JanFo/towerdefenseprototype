﻿using System;

namespace GameActions
{
    public class GameActionDescriptor
    {
        private Func<string> func;

        public string Name { get; }

        public GameActionDescriptor(string name, Func<string> func)
        {
            this.func = func;
            Name = name;
        }

        public string Description
        {
            get
            {
                return func();
            }
        }

        public override string ToString()
        {
            return func();
        }
    }
}