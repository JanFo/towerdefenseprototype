﻿using System;
using UnityEngine;

namespace GameActions
{
    [Serializable]
    public class GameActionElementMapping
    {
        [SerializeField]
        public int elementId;

        [SerializeField]
        public int descriptorIndex;
    }
}