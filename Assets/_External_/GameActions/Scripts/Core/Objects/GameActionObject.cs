﻿using UnityEngine;

namespace GameActions
{
    public abstract class GameActionObject : ScriptableObject
    {
        [SerializeField]
        private Sprite icon = null;

        [SerializeField]
        private string descriptionKey = null;

        [SerializeField]
        private float cooldown = 0.0f;

        [SerializeField]
        private float cost = 0.0f;  

        public Sprite Icon { get => icon; set => icon = value; }
        public string DescriptionKey { get => descriptionKey; set => descriptionKey = value; }
        public float Cooldown { get => cooldown; set => cooldown = value; }
        public float Cost { get => cost; set => cost = value; }

        public GameAction CreateAction()
        {
            return OnCreateAction(); 
        }

        protected abstract GameAction OnCreateAction();
    }
}