﻿using UnityEngine;

namespace GameActions
{
    /// <summary>
    /// An ability that a caster can use
    /// </summary>
    [CreateAssetMenu(fileName = "NewAbility", menuName = "Actions/Combo Action")]
    public class ComboActionObject : GameActionObject
    {
        [SerializeField]
        private float resetTime = 0.5f;

        [SerializeField]
        private GameActionObject[] gameActions = null;

        protected override GameAction OnCreateAction()
        {
            GameAction[] actions = new GameAction[gameActions.Length];

            for (int i = 0; i < actions.Length; i++)
            {
                actions[i] = gameActions[i].CreateAction();
            }

            return new ComboAction(actions, resetTime);
        }
    }
}