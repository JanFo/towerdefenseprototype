﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameActions
{
    public enum ModularGameActionElementFlag
    {
        None,
        Repeat,
        Skip,
        Exit,
    }

    public abstract class ModularGameActionElement : ScriptableObject
    {
        public const string DESCRIPTOR_OFFSET = "Offset";

        public const string DESCRIPTOR_DURATION = "Duration";

        [SerializeField, ReadOnly]
        public int trackIndex;

        [SerializeField, ReadOnly]
        public float offset;

        [SerializeField, ReadOnly]
        public float duration;

        [SerializeField, ReadOnly]
        public int id;


        [SerializeField]
        public string label = null;

        public float TimeScale { get; set; }

        public bool IsActive { get; private set; }

        private List<GameActionInputConnector> cachedInputConnectors;

        private List<GameActionOutputConnector> cachedOutputConnectors;

        private List<GameActionDescriptor> cachedDescriptors;

        private List<GameActionCondition> cachedConditions;

        private IGameActionActor actor;

        public ModularGameActionElementFlag Flag { get; protected set; }

        public virtual string GetDescriptionString(float offset, float length)
        {
            return string.Empty;
        }

        public void Initialize(IGameActionContext context, IGameActionActor actor)
        {
            this.actor = actor;
            OnElementInitialize(context, actor);
        }

        public GameActionParameters Parameters => actor.Parameters;

        public List<GameActionDescriptor> Descriptors
        {
            get
            {
                if (cachedDescriptors == null)
                {
                    cachedDescriptors = new List<GameActionDescriptor>();
                    CreateDescriptors(cachedDescriptors);
                }

                return cachedDescriptors;
            }
        }

        public List<GameActionCondition> Conditions
        {
            get
            {
                if (cachedConditions == null)
                {
                    cachedConditions = new List<GameActionCondition>();
                    CreateConditions(cachedConditions);
                }

                return cachedConditions;
            }
        }

        public List<GameActionOutputConnector> OutputConnectors
        {
            get
            {
                if (cachedOutputConnectors == null)
                {
                    cachedOutputConnectors = new List<GameActionOutputConnector>();
                    OnCreateOutputs(cachedOutputConnectors);
                }

                return cachedOutputConnectors;
            }
        }

        public List<GameActionInputConnector> InputConnectors
        {
            get
            {
                if (cachedInputConnectors == null)
                {
                    cachedInputConnectors = new List<GameActionInputConnector>();
                    OnCreateInputs(cachedInputConnectors);
                }



                return cachedInputConnectors;
            }
        }

        protected virtual void OnCreateInputs(List<GameActionInputConnector> inputs)
        {

        }

        protected virtual void OnCreateOutputs(List<GameActionOutputConnector> outputs)
        {

        }


        protected virtual void OnCreateDescriptors(List<GameActionDescriptor> descriptors)
        {

        }

        protected virtual void OnCreateConditions(List<GameActionCondition> conditions)
        {

        }

        private void CreateConditions(List<GameActionCondition> cachedConditions)
        {
            OnCreateConditions(cachedConditions);
        }

        public void CreateDescriptors(List<GameActionDescriptor> descriptorList)
        {
            descriptorList.Add(new GameActionDescriptor(DESCRIPTOR_OFFSET, () => offset.ToString()));
            descriptorList.Add(new GameActionDescriptor(DESCRIPTOR_DURATION, () => duration.ToString()));
            OnCreateDescriptors(descriptorList);
        }

        public void CreateConnectors()
        {
            cachedInputConnectors = new List<GameActionInputConnector>();
            OnCreateInputs(cachedInputConnectors);

            cachedOutputConnectors = new List<GameActionOutputConnector>();
            OnCreateOutputs(cachedOutputConnectors);
        }

        public GameActionOutputConnector GetOutput(int index)
        {
            return OutputConnectors[index];
        }

        public GameActionInputConnector GetInput(int index)
        {
            return ArrayUtils.Get(InputConnectors, index, null);
        }

        public void EndElement()
        {
            IsActive = false;
            OnElementEnd();
        }

        public void BeginElement()
        {
            IsActive = true;
            OnElementBegin();
        }

        public void UpdateElement(float deltaTime, float progress)
        {
            float time = progress - offset;
            float normalizedTime = (progress - offset) / duration;

            OnElementUpdate(deltaTime, time, normalizedTime);
        }

        public virtual void GetTimeMarkers(out bool hasBegin, out bool hasUpdate, out bool hasEnd)
        {
            hasBegin = false;
            hasUpdate = false;
            hasEnd = false;
        }

        
        protected virtual void OnElementInitialize(IGameActionContext context, IGameActionActor actor)
        {

        }
        protected virtual void OnElementBegin()
        {

        }

        protected virtual void OnElementUpdate(float deltaTime, float localProgress, float normalizedLocalProgress)
        {

        }

        protected virtual void OnElementEnd()
        {

        }

        public void ClearFlag()
        {
            Flag = ModularGameActionElementFlag.None;
        }

        public GameActionDescriptor GetDescriptorFunction(int descriptorIndex)
        {
            return ArrayUtils.Get(Descriptors, descriptorIndex, null);
        }

        public string Label => label;

        public bool IsActionCancelled { get; set; }

        public virtual void DrawEditorGUI(Rect itemRect, float scale)
        {

        }

        public virtual Color GetEditorColor()
        {
            return Color.clear;
        }
    }

    public abstract class ModularGameActionElement<C, A> : ModularGameActionElement
    {
        protected C Context { get; private set; }

        public A Self { get; private set; }

        protected sealed override void OnElementInitialize(IGameActionContext context, IGameActionActor actor)
        {
            try
            {
                Self = (A)actor;
                Context = (C)context;
            }
            catch (InvalidCastException)
            {
                Debug.LogWarning($"The actor of type {actor.GetType()} does not have the type {typeof(A)} that is required by {GetType()}. " +
                    $"This will most likely cause errors.");
            }

            OnElementInitialize();
        }

        protected virtual void OnElementInitialize()
        {

        }
    }
}