﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GameActions
{
    [CreateAssetMenu(fileName = "NewAbility", menuName = "Actions/Modular Game Action")]
    public class ModularGameActionObject : GameActionObject
    {
        /// <summary>
        /// The amount of tracks usable for an ability
        /// </summary>
        public const int TRACK_COUNT = 20;

        [SerializeField]
        protected float minTime = 0.0f;

        [SerializeField]
        protected float timeScale = 1.0f;

        /// <summary>
        /// A list of all connections between ability elements
        /// </summary>
        [SerializeField]
        protected List<GameActionElementConnection> connections;

        /// <summary>
        /// A list of all the ability elements
        /// </summary>
        [SerializeField]
        protected List<ModularGameActionElement> actionElements;

        /// <summary>
        /// The ability descriptor mappings pointing to descriptors in elements
        /// </summary>
        [SerializeField]
        private List<GameActionElementMapping> descriptorMappings;

        /// <summary>
        /// The ability condition mappings pointing to conditions in elements
        /// </summary>
        [SerializeField]
        private List<GameActionElementMapping> conditionMappings;


        private string[] inserts;


        protected override GameAction OnCreateAction()
        {
            ModularGameActionElement[] elements = new ModularGameActionElement[actionElements.Count];

            for (int i = 0; i < actionElements.Count; i++)
            {
                elements[i] = Instantiate(actionElements[i]);
                elements[i].CreateConnectors();
            }

            foreach (GameActionElementConnection connection in connections)
            {
                var output = elements.FirstOrDefault(s => s.id == connection.fromItem)?.GetOutput(connection.fromConnector);
                var input = elements.FirstOrDefault(s => s.id == connection.toItem)?.GetInput(connection.toConnector);
                input.Connect(output);
            }

            elements = elements.OrderBy(s => s.trackIndex).ToArray();

            return new ModularGameAction(elements, conditionMappings, minTime, timeScale)
            {
                Name = name
            };
        }

#if UNITY_EDITOR
        /// <summary>
        /// Adds an ability element to the ability and assigns a element integer id
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public int AddElement(ModularGameActionElement element)
        {
            AssetDatabase.AddObjectToAsset(element, this);

            element.id = Elements.Count > 0 ? Elements.Max(e => e.id) + 1 : 1;
            Elements.Add(element);
            return element.id;
        }

        /// <summary>
        /// Remove an ability element from the ability
        /// </summary>
        /// <param name="elementToRemove"></param>
        public void RemoveItem(ModularGameActionElement elementToRemove)
        {
            actionElements.Remove(elementToRemove);

            for (int i = connections.Count - 1; i >= 0; i--)
            {
                if (connections[i].fromItem == elementToRemove.id || connections[i].toItem == elementToRemove.id)
                {
                    connections.RemoveAt(i);
                }
            }
        }
#endif

        /// <summary>
        /// The progress of the ability
        /// </summary>
        public float Progress { get; private set; }

        /// <summary>
        /// The length of the ability
        /// </summary>
        public float Length => ModularGameAction.CalculateAbilityLength(Elements.ToArray());

        /// <summary>
        /// Retrieves an ability element based on its id
        /// </summary>
        /// <param name="elementIndex"></param>
        /// <returns></returns>
        public ModularGameActionElement GetElementById(int elementIndex)
        {
            return actionElements.FirstOrDefault(s => s.id == elementIndex);
        }

        /// <summary>
        /// The ability elements
        /// </summary>
        public List<ModularGameActionElement> Elements
        {
            get
            {
                if (actionElements == null)
                    actionElements = new List<ModularGameActionElement>();

                return actionElements;
            }
        }

        /// <summary>
        /// Connections between elements
        /// </summary>
        public List<GameActionElementConnection> Connections
        {
            get
            {
                if (connections == null)
                    connections = new List<GameActionElementConnection>();

                return connections;
            }
            set
            {
                connections = value;
            }
        }

        public List<GameActionElementMapping> DescriptorMappings
        {
            get
            {
                if (descriptorMappings == null)
                    descriptorMappings = new List<GameActionElementMapping>();

                return descriptorMappings;
            }
            set
            {
                descriptorMappings = value;
            }
        }

        public List<GameActionElementMapping> ConditionMappings
        {
            get
            {
                if (conditionMappings == null)
                    conditionMappings = new List<GameActionElementMapping>();

                return conditionMappings;
            }
            set
            {
                conditionMappings = value;
            }
        }

        public float TimeScale { get; set; }

        public string Description
        {
            get
            {
                string description = string.Empty; // base.Abstract;

                if (inserts == null || inserts.Length != descriptorMappings.Count)
                {
                    inserts = new string[descriptorMappings.Count];
                }

                for (int i = 0; i < descriptorMappings.Count; i++)
                {
                    GameActionElementMapping mapping = descriptorMappings[i];
                    ModularGameActionElement element = GetElementById(mapping.elementId);

                    if (element != null)
                    {
                        inserts[i] = element.GetDescriptorFunction(mapping.descriptorIndex).Description;
                    }
                }

                return string.Format(description, inserts);
            }
        }

        public float MinTime { get => minTime; set => minTime = value; }
    }
}
