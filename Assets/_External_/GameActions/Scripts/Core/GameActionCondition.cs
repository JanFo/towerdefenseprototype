﻿using System;
using UnityEngine;

namespace GameActions
{
    public class GameActionCondition
    {
        private Func<bool> func;

        public string DescriptionKey { get; private set; }

        public string Name { get; private set; }

        public GameActionCondition(string name, string descriptionKey, Func<bool> func)
        {
            this.func = func;
            DescriptionKey = descriptionKey;
            Name = name;
        }

        public bool IsMet
        {
            get
            {
                return func();
            }
        }

    }
}