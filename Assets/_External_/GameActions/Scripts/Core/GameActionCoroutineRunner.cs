﻿using System.Collections;
using UnityEngine;

namespace GameActions
{
    public class GameActionCoroutineRunner : MonoBehaviour, IGameActionCoroutineRunner
    {
        public delegate void GameActionEvent(GameAction action);

        public event GameActionEvent OnStartAction;

        public object Run(IEnumerator gameAction)
        {
            return null;
        }

        public object Run(GameAction gameAction)
        {
            object handle = StartCoroutine(gameAction.GetProcess());

            if(OnStartAction != null)
            {
                OnStartAction(gameAction);
            }

            return handle;
        }
    }
}
