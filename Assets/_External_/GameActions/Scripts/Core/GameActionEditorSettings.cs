﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System;

namespace GameActions 
{
    [System.Serializable]
    public class TypeColorMapping
    {
        public string typeName;
        public Color color;
    }

    [Serializable]
    public class GameActionEditorSettings : ScriptableObject
    {
        public static string actionEditorSettingsPath = "ProjectSettings/GameActionEditorSettings.asset";

        private static GameActionEditorSettings instance;

        public Color timelineBackgroundColor;

        public Color timelineLineColor;

        public Color timelineTimebarColor;

        public Color defaultTimedElementColor;

        public Color elementTextColor;

        public int timelineTrackHeight;

        public int timelineTrackCount;

        public int timelineTimebarHeight;

        public List<TypeColorMapping> typeColorMappings;

        public Dictionary<string, Color> typeColorMap;

        public Color defaultConnectionColor;

        public Color defaultFunctionalElementColor;

        public static GameActionEditorSettings GetOrCreateSettings()
        {
            if (instance != null)
            {
                return instance;
            }

            if (!File.Exists(actionEditorSettingsPath))
            {
                GameActionEditorSettings settings = CreateInstance<GameActionEditorSettings>();
                settings.timelineTrackHeight = 40;
                settings.timelineBackgroundColor = Color.grey;
                settings.timelineTimebarHeight = 10;
                settings.timelineTimebarColor = Color.grey;
                settings.typeColorMappings = new List<TypeColorMapping>();
                File.WriteAllText(actionEditorSettingsPath, JsonUtility.ToJson(settings, true));
            }

            string settingsString = File.ReadAllText(actionEditorSettingsPath);
            instance = CreateInstance<GameActionEditorSettings>();
            JsonUtility.FromJsonOverwrite(settingsString, instance);

            instance.typeColorMap = ArrayUtils.CreateMap(instance.typeColorMappings, t => t.typeName, t => t.color);
            return instance;
        }

    }
}