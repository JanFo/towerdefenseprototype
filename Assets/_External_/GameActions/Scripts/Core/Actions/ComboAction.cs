﻿
using System;
using System.Collections;
using UnityEngine;

namespace GameActions
{
    [Serializable]
    public class ComboAction : GameAction
    {
        private GameAction[] actions;

        private float resetInterval;

        private float lastCastTime;

        private GameAction currentAction;

        private int actionIndex;

        public ComboAction(GameAction[] actions, float resetInterval) 
        {
            this.actions = actions;
            this.resetInterval = resetInterval;
        }

        protected override void OnInitialize(IGameActionContext context, IGameActionActor actor)
        {
            for (int i = 0; i < actions.Length; i++)
            {
                actions[i].Initialize(context, actor);
            }
        }

        protected override void OnStart()
        {
            if(Time.time - lastCastTime > resetInterval)
            {
                actionIndex = 0;
            }
            else
            {
                actionIndex = ArrayUtils.Repeat(actionIndex + 1, 0, actions.Length);
            }

            currentAction = actions[actionIndex];
            currentAction.Start();
        }

        protected override IEnumerator OnCreateProcess()
        {
            while (!currentAction.IsDone)
            {
                if(IsCancelled)
                {
                    currentAction.Cancel();
                }

                yield return null;
            }
        }

        protected override void OnEnd()
        {
            lastCastTime = Time.time;
        }
    }
}
