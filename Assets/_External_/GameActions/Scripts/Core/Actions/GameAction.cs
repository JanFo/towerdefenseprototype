﻿using System.Collections;
using UnityEngine;

namespace GameActions
{
    public abstract class GameAction
    {
        private object runningRoutine;
        public float Cooldown { get; set; }
        public Sprite Icon { get; set; }
        public string DescriptionKey { get; set; }
        public bool IsInitialized { get; protected set; }
        public float TimeScale { get; set; }
        public IGameActionCoroutineRunner Runner { get; private set; }

        public void Start()
        {
            IsCancelled = false;
            runningRoutine = Runner.Run(this);
        }

        public void Initialize(IGameActionContext context, IGameActionActor actor)
        {
            Runner = context.Runner;
            OnInitialize(context, actor);
        }

        protected abstract void OnInitialize(IGameActionContext context, IGameActionActor actor);

        public virtual bool IsAvailable(out string message)
        {
            message = null;
            return true;
        }

        public IEnumerator GetProcess()
        {
            OnStart();
            yield return OnCreateProcess();
            runningRoutine = null;
            OnEnd();
        }

        public void Cancel()
        {
            IsCancelled = true;
            OnCancel();
        }

        protected virtual void OnCancel()
        {

        }

        public bool IsCancelled { get; private set; }

        protected abstract void OnStart();

        protected abstract void OnEnd();

        protected abstract IEnumerator OnCreateProcess();

        public bool IsDone => runningRoutine == null;

    }

    /// <summary>
    /// A playable sequence with a target actor entity. This powerful sequence can modify any 
    /// entity, entity component or entity view over time. Base class for all time-based 
    /// entity focused game processes
    /// </summary>
    public abstract class GameAction<C, A> : GameAction
    {
        protected sealed override void OnInitialize(IGameActionContext context, IGameActionActor actor)
        {
            Context = (C)context;
            Self = (A)actor;

            OnInitialize();
            IsInitialized = true;
        }

        protected abstract void OnInitialize();

        public C Context { get; private set; }

        public A Self { get; private set; }
    }
}
