﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System;

namespace GameActions
{
    public class ModularGameAction : GameAction
    {
        private float previousProgress;

        private bool hasJumpingFlag;

        private float progress;

        private float minLength;

        private float length;

        private ModularGameActionElement[] actionElements;

        private IGameActionActor actor;
        
        public string Name { get; set; }

        private List<GameActionElementMapping> conditionMappings;

        private int blockingElementCount;

        public ModularGameAction(ModularGameActionElement[] actionElements,
            List<GameActionElementMapping> conditionMappings,
            float minLength,
            float timeScale = 1.0f)
        {
            this.conditionMappings = conditionMappings;
            this.actionElements = actionElements;
            this.minLength = minLength;
            TimeScale = timeScale;

            length = CalculateAbilityLength(actionElements);
        }

        public static float CalculateAbilityLength(ModularGameActionElement[] actionElements)
        {
            float length = 0.0f;

            if (actionElements != null)
            {
                foreach (ModularGameActionElement element in actionElements)
                {
                    if (element.offset + element.duration > length)
                    {
                        length = element.offset + element.duration;
                    }
                }
            }

            return length;
        }

        protected override void OnCancel()
        {
            foreach (ModularGameActionElement element in actionElements)
            {
                element.IsActionCancelled = true;
            }
        }

        protected override void OnInitialize(IGameActionContext context, IGameActionActor actor)
        {
            this.actor = actor;

            foreach (ModularGameActionElement element in actionElements)
            {
                element.Initialize(context, actor);
            }
        }

        public override bool IsAvailable(out string messageDescriptionKey)
        {
            if (!base.IsAvailable(out messageDescriptionKey))
            {
                return false;
            }

            foreach (GameActionElementMapping mapping in conditionMappings)
            {
                GameActionCondition condition = GetElementById(mapping.elementId).Conditions[mapping.descriptorIndex];

                if (!condition.IsMet)
                {
                    messageDescriptionKey = condition.DescriptionKey;
                    return false;
                }
            }

            messageDescriptionKey = null;
            return true;
        }

        private ModularGameActionElement GetElementById(int elementId)
        {
            foreach (ModularGameActionElement element in actionElements)
            {
                if (element.id == elementId)
                {
                    return element;
                }
            }

            return null;
        }

        protected override void OnStart()
        {
            progress = 0.0f;
            previousProgress = 0.0f;
            hasJumpingFlag = false;

            foreach (ModularGameActionElement element in actionElements)
            {
                element.IsActionCancelled = true;
            }
        }

        protected override IEnumerator OnCreateProcess()
        {

            Start:
            // Loop to update the progress
            while (progress < length)
            {
                // Terminate on loss of actor
                if (actor == null)
                {
                    yield break;
                }

                float timeScale = 1;

                progress += Time.deltaTime * TimeScale * timeScale;

                // We might have been jumping last frame.
                bool isJumping = hasJumpingFlag;

                // This frame has not seen a jumping flag yet.
                hasJumpingFlag = false;

                // Update all elements
                foreach (ModularGameActionElement element in actionElements)
                {
                    element.TimeScale = TimeScale;

                    // Cache begin and end
                    float itemBegin = element.offset;
                    float itemEnd = element.offset + element.duration;

                    // Clear all flags
                    element.ClearFlag();

                    // Update element if the element is active
                    if (element.IsActive)
                    {
                        try
                        {
                            // Update the action element
                            element.UpdateElement(Time.deltaTime * TimeScale, progress);

                            // Cursor is outside of the item bounds, quit the sequence item!
                            if (progress < itemBegin || progress > itemEnd)
                            {
                                element.EndElement();

                                if (element.GetType() == typeof(LockElement))
                                {
                                    blockingElementCount--;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e);
                            yield break;
                        }

                        // Do a jump if we have a repeat flag, remember the progress as the previous progress and break. Set the jumping flag.
                        if (!IsCancelled && element.Flag == ModularGameActionElementFlag.Repeat)
                        {
                            hasJumpingFlag = true;
                            previousProgress = progress;
                            progress = itemBegin;
                            goto Start;
                        }
                    }
                    else
                    {
                        // Has not started yet
                        if (progress < itemBegin)
                        {
                            continue;
                        }

                        // Has already ended properly or has been skipped on purpose (isJumping)
                        if (progress > itemEnd)
                        {
                            if (isJumping || previousProgress > itemEnd)
                            {
                                continue;
                            }
                        }

                        // Track the number of blockers
                        if (element.GetType() == typeof(LockElement))
                        {
                            blockingElementCount++;
                        }

                        try
                        {
                            // Cursor is on the element or has passed it without jump
                            element.BeginElement();
                            element.UpdateElement(Time.deltaTime * TimeScale, progress);
                        }
                        catch(Exception e)
                        {
                            Debug.LogError(e);
                            yield break;
                        }
                    }

                    // Look for a skip flag, remember the previous progress and break. Set the jumping flag
                    if (!IsCancelled)
                    {
                        if (element.Flag == ModularGameActionElementFlag.Skip)
                        {
                            previousProgress = progress;
                            hasJumpingFlag = true;
                            progress = element.offset + element.duration;
                            goto Start;
                        }
                    }
                }

                // We did not encounter a jumping flag this frame, remember the previous progress normally
                if (!hasJumpingFlag)
                {
                    previousProgress = progress;
                }

                if ((actor.HasNext && progress > minLength) || (IsCancelled && blockingElementCount == 0))
                {
                    yield break;
                }

                yield return null;
            }
        }
            

        protected override void OnEnd()
        {
            foreach (ModularGameActionElement element in actionElements)
            {
                if (element.IsActive)
                {
                    element.EndElement();
                }
            }
        }
        
        public float Progress => progress;
    }
}