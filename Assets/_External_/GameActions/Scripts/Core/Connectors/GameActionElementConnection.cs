﻿using System;

namespace GameActions
{
    [Serializable]
    public class GameActionElementConnection
    {
        public int fromItem;
        public int toItem;
        public int fromConnector;
        public int toConnector;
    }
}