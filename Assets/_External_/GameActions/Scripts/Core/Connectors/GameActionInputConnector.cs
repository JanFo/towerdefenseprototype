﻿using System;

namespace GameActions
{
    [Serializable]
    public abstract class GameActionInputConnector : GameActionConnector
    {
        public abstract void Connect(GameActionOutputConnector connector);

        public abstract bool IsConnected { get; }
    }

    public class GameActionEventReceiver : GameActionInputConnector
    {
        private GameActionEventTrigger connector;

        private Action<object> callback;

        public GameActionEventReceiver(Action<object> callback)
        {
            this.callback = callback;
        }

        public object GetPayload()
        {
            return connector.GetPayload();
        }

        public override void Connect(GameActionOutputConnector connector)
        {
            this.connector = connector as GameActionEventTrigger;

            if (this.connector != null)
            {
                this.connector.AddListener(callback);
            }
        }


        public override Type Type
        {
            get
            {
                return typeof(Action);
            }
        }

      
        public override bool IsConnected => connector != null;
    }

    [Serializable]
    public class GameActionInputConnector<T> : GameActionInputConnector
    {
        private GameActionOutputConnector connector;

        private GameActionOutputConnector<T> valuedConnector;

        public T Get()
        {
            return Get(default);
        }

        public T Get(T defaultValue)
        {
            return IsConnected ? valuedConnector.Value : defaultValue;
        }

        public override void Connect(GameActionOutputConnector connector)
        {
            if (connector is GameActionOutputConnector<T>)
            {
                valuedConnector = connector as GameActionOutputConnector<T>;
            }



            this.connector = connector;
        }

        public override Type Type
        {
            get
            {
                return typeof(T);
            }
        }

        public override bool IsConnected
        {
            get
            {
                return connector != null;
            }
        }
    }
}