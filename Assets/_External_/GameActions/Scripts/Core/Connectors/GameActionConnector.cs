﻿using System;

namespace GameActions
{
    public abstract class GameActionConnector
    {
        public abstract Type Type { get; }
    }
}