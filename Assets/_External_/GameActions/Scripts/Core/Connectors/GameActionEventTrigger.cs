﻿using System;
using System.Collections.Generic;

namespace GameActions
{
    public class GameActionEventTrigger : GameActionOutputConnector
    {
        private List<Action<object>> callbacks;

        private object payload;

        public override object ObjectValue
        {
            get
            {
                return null;
            }
        }

        public void AddListener(Action<object> callback)
        {
            if (callbacks == null)
            {
                callbacks = new List<Action<object>>();
            }

            callbacks.Add(callback);
        }

        public override bool IsSet => true;

        public override bool IsDelegate => false;

        public override Type Type => typeof(Action);

        public void Invoke()
        {
            if (callbacks != null)
            {
                foreach (Action<object> callback in callbacks)
                {
                    callback(payload);
                }
            }
        }

        public void SetPayload(object arg0)
        {
            payload = arg0;
        }

        public object GetPayload()
        {
            return payload;
        }
    }
}