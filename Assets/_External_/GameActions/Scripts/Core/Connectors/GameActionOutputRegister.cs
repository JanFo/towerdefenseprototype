﻿using System;

namespace GameActions
{
    public class GameActionOutputRegister<T> : GameActionOutputConnector<T>
    {

        private bool isSet;

        private T value;

        public void Set(T value)
        {
            this.value = value;
            isSet = true;
        }

        public override bool IsDelegate => false;

        public override bool IsSet
        {
            get
            {
                return isSet;
            }
        }

        public override Type Type
        {
            get
            {
                return typeof(T);
            }
        }

        public override T Value
        {
            get
            {
                return value;
            }
        }

        public override object ObjectValue
        {
            get
            {
                return value;
            }
        }
    }
}