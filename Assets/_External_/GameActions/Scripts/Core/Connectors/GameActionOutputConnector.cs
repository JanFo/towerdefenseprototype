﻿using System;

namespace GameActions
{
    public abstract class GameActionOutputConnector : GameActionConnector
    {
        public abstract bool IsSet { get; }
        public abstract object ObjectValue { get; }
        public abstract bool IsDelegate { get; }
    }

    public abstract class GameActionOutputConnector<T> : GameActionOutputConnector
    {
        public override Type Type
        {
            get
            {
                return typeof(T);
            }
        }

        public abstract T Value { get; }
    }

    public class GameActionOutputDelegate<T> : GameActionOutputConnector<T>
    {
        private Func<T> func;

        public GameActionOutputDelegate(Func<T> func)
        {
            this.func = func;
        }

        public override T Value
        {
            get
            {
                return func();
            }
        }

        public override bool IsDelegate => true;

        public override bool IsSet
        {
            get
            {
                return true;
            }
        }

        public override object ObjectValue
        {
            get
            {
                return func();
            }
        }

        public override Type Type
        {
            get
            {
                return typeof(T);
            }
        }
    }
}