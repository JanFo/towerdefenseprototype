﻿using System.Collections.Generic;

namespace GameActions
{
    public abstract class GameActionEvent : ModularGameActionElement
    {
        private GameActionEventReceiver receiverInput;

        protected abstract void OnEventTriggered(object payload);

        protected abstract void OnCreateEventInputs(List<GameActionInputConnector> inputs);

        protected sealed override void OnCreateInputs(List<GameActionInputConnector> inputs)
        {
            base.OnCreateInputs(inputs);

            receiverInput = new GameActionEventReceiver(OnEventTriggered);
            inputs.Add(receiverInput);

            OnCreateEventInputs(inputs);
        }
    }
}
