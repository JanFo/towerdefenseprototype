﻿using System;
using UnityEngine;

namespace GameActions
{
    [Serializable]
    public struct GameActionParamValue
    {
        public float Value1;
        public float Value2;
        public float Value3;

        public static GameActionParamValue Zero => new GameActionParamValue();

        public static GameActionParamValue FromInt(int index)
        {
            GameActionParamValue value = new GameActionParamValue();
            value.Value1 = index;
            return value;
        }

        public GameActionParamValue(float value1, float value2, float value3)
        {
            Value1 = value1;
            Value2 = value2;
            Value3 = value3;
        }

        public static implicit operator Vector3(GameActionParamValue a) => new Vector3(a.Value1, a.Value2, a.Value3);

        public static explicit operator GameActionParamValue(Vector3 v) => FromVector3(v);


        public static GameActionParamValue FromVector3(Vector3 direction)
        {
            GameActionParamValue value = new GameActionParamValue();
            value.Value1 = direction.x;
            value.Value2 = direction.y;
            value.Value3 = direction.z;
            return value;
        }

        public static GameActionParamValue FromUInt(uint index)
        {
            GameActionParamValue value = new GameActionParamValue();
            value.Value1 = index;
            return value;
        }



        public static GameActionParamValue FromFloat(float minDistance)
        {
            GameActionParamValue value = new GameActionParamValue();
            value.Value1 = minDistance;
            return value;
        }

        public static GameActionParamValue FromBool(bool v)
        {
            GameActionParamValue value = new GameActionParamValue();
            value.Value1 = v ? 1 : 0;
            return value;
        }

        public void SetValues(Vector3 direction)
        {
            Value1 = direction.x;
            Value2 = direction.y;
            Value3 = direction.z;
        }
    }
}
