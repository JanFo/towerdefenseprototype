﻿using System;
using System.IO;
using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif



namespace GameActions
{
    [Serializable]
    public struct ImportTypeMapping
    {
        public string type;
        public string[] targetTypes;
    }

#if UNITY_EDITOR
    [InitializeOnLoad]
#endif
    public class GameActionSettings : ScriptableObject
    {
        const string SettingsAssetName = "GameActionSettings";

        private static bool isInitializing;

        private static GameActionSettings instance;

        public string[] gameActionParamters;

        private Dictionary<string, byte> gameActionParamterMap;

        private bool hasLoaded;

        public static GameActionSettings Instance
        {
            get
            {
                if (isInitializing)
                {
                    return null;
                }

                if (instance == null || instance.GameActionParameters == null)
                {
                    Initialize();
                }

                return instance;
            }
        }

        public static void Initialize()
        {
            isInitializing = true;
            instance = Resources.Load(SettingsAssetName) as GameActionSettings;


            if (instance == null)
            {
                Debug.Log("Cannot find GameAction settings, creating default settings");
                instance = CreateInstance<GameActionSettings>();
                instance.name = "GameAction Settings";

#if UNITY_EDITOR
                if (!Directory.Exists("Assets/GameActions"))
                {
                    AssetDatabase.CreateFolder("Assets", "GameActions");
                }

                if (!Directory.Exists("Assets/GameActions/Resources"))
                {
                    AssetDatabase.CreateFolder("Assets/GameActions", "Resources");
                }

                AssetDatabase.CreateAsset(instance, "Assets/GameActions/Resources/" + SettingsAssetName + ".asset");
#endif
            }

            isInitializing = false;
        }

        public string[] GameActionParameters => gameActionParamters;

        public byte GetParamIndex(string key)
        {
            if(gameActionParamterMap == null)
            {
                CreateMap();
            }

            return ArrayUtils.Get(gameActionParamterMap, key, (byte)0 );
        }

        private void OnEnable()
        {
            isInitializing = false;

            if (hasLoaded)
            {
                // Already loaded
                return;
            }

            hasLoaded = true;
           
        }


        private void CreateMap()
        {
            gameActionParamterMap = ArrayUtils.CreateMap(gameActionParamters, a => a, a => (byte)(Array.IndexOf(gameActionParamters, a) + 1));
        }

    }
}
