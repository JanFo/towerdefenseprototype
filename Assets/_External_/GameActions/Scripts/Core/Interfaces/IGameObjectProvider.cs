﻿using UnityEngine;

namespace GameActions
{
    public interface IGameObjectProvider
    {
        GameObject GetGameObject(IGameActionContext context);
    }
}