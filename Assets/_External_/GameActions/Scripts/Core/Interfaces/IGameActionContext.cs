﻿namespace GameActions
{
    public interface IGameActionContext
    {
        IGameActionCoroutineRunner Runner { get; }
    }
}