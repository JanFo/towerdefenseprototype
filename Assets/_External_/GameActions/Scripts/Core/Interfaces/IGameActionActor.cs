﻿namespace GameActions
{
    public interface IGameActionActor
    {
        GameActionParameters Parameters { get; }
        bool HasNext { get; }
    }
}