﻿using System.Collections;

namespace GameActions
{
    public interface IGameActionCoroutineRunner
    {
        object Run(IEnumerator enumerator);
        object Run(GameAction enumerator);
    }
}
