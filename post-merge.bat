setlocal EnableExtensions DisableDelayedExpansion

set "search=C:\Program Files\Unity Editors\2020.2.0b14"
set "replace=C:\Program Files\Unity\Hub\Editor\2020.2.0b14"

set "textFile=*.csproj"
set "rootDir=."

for %%j in ("%rootDir%\%textFile%") do (
    for /f "delims=" %%i in ('type "%%~j" ^& break ^> "%%~j"') do (
        set "line=%%i"
        setlocal EnableDelayedExpansion
        set "line=!line:%search%=%replace%!"
        >>"%%~j" echo(!line!
        endlocal
    )
)

endlocal